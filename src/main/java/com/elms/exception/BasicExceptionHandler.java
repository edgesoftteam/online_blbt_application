package com.elms.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.elms.model.ExceptionResponse;

//@ControllerAdvice
public class BasicExceptionHandler extends Exception{


private static final Logger logger = Logger.getLogger(BasicExceptionHandler.class);

	 public BasicExceptionHandler(Exception errorMessage) {
		 
		 super(errorMessage);
}

	@ExceptionHandler(MultipartException.class)
		@ResponseStatus(value = HttpStatus.NOT_FOUND)
		public @ResponseBody ExceptionResponse handleMultipartException(final MultipartException exception,
				final HttpServletRequest request) {

			ExceptionResponse error = new ExceptionResponse();
			error.setErrorMessage(exception.getMessage());
			error.callerURL(request.getRequestURI());

			return error;
		}
	 
	    //CommonsMultipartResolver
	    @ExceptionHandler(MaxUploadSizeExceededException.class)
	    public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes) {
	    	logger.debug("status...456");
	        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
	        return "uploadStatus";

	    }

}
