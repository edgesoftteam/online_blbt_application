package com.elms.controller;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.EmailDetails;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.ProcessTeamForm;
import com.elms.model.ProcessTeamRecord;
import com.elms.model.QuantityType;
import com.elms.model.User;
import com.elms.repository.CommonRepository;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.BusinessTaxRenewalService;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBLService;
import com.elms.service.OnlineApplicationBTService;
import com.elms.util.EmailSender;
import com.elms.util.StringUtils;
/**
 * @author Siva Kumari
 *
 */

@Controller
public class OnlineApplicationBTController {

	@Autowired
	CommonService commonService;
	@Autowired
	BusinessLicenseRenewalService businessLicenseRenewalService;
	@Autowired
	OnlineApplicationBLService onlineApplicationBLService;
	@Autowired
	OnlineApplicationBTService onlineApplicationBTService;
	@Autowired
	EmailDetails emailDetails;
	@Autowired
	User user;
	@Autowired
	CommonRepository commonRepository;
	@Autowired
	BusinessTaxRenewalService businessTaxRenewalService;
	
	String viewName = "";
	private static final Logger logger = Logger.getLogger(OnlineApplicationBTController.class);

	ModelAndView modelAndView = new ModelAndView();


	@RequestMapping(value="/addBusinessTaxActivity", method = RequestMethod.POST)
	public ModelAndView addBusinessTaxActivity(@ModelAttribute("activityForm") BusinessTaxActivityForm businessTaxActivityForm, HttpServletRequest request, HttpServletResponse response) throws BasicExceptionHandler {
		
		List activitySubTypes = new ArrayList();
		Map codes = null;
		String email=businessTaxActivityForm.getEmailAddress();
		logger.debug("email.."+businessTaxActivityForm.getEmailAddress());
		response.setContentType("text");
		List activityTypes;
		String activityType = request.getParameter("activityType") != null ? request.getParameter("activityType") : "";
		activityType = activityType.trim();
		boolean flag = StringUtils.s2b(request.getParameter("flag"));
		
		//this code written in jsp as it is not recommended moving it to controller
		List streetList = new ArrayList();
		List ownershipTypes= new ArrayList();
		
		String resetValue = (StringUtils.nullReplaceWithEmpty(request.getParameter("resetValue")));

		try {
			// Getting all Activity Types by passing Business Tax module id
			activityTypes = onlineApplicationBLService.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_TAX);			
			String businessAddressStreetName = businessTaxActivityForm.getBusinessAddressStreetName();
			businessTaxActivityForm.setApplicationDate(StringUtils.cal2str(Calendar.getInstance()));
		
			if (!activityType.equals("") || activityType != null) {
				if (flag == true) {
					businessTaxActivityForm.reset();
					activitySubTypes = commonService.getActivitySubTypes(businessTaxActivityForm.getActivityType());
					businessTaxActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BT_PENDING_APPROVAL));
					businessTaxActivityForm.setBusinessLocation(true);
				} else {
					activitySubTypes = commonService.getActivitySubTypes(businessTaxActivityForm.getActivityType());
					businessTaxActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
					flag = false;
				}
			}
	
			if (activityType.trim().equals("")) {
				businessTaxActivityForm.setApplicationType(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSTAX));
				businessTaxActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BT_PENDING_APPROVAL));
			}
			// Start of Activity Type Refresh using Ajax
			if (!activityType.equals("")) {
				businessTaxActivityForm.setActivityType(activityType);
				activitySubTypes = commonService.getActivitySubTypes(activityType);
				businessTaxActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
				codes = commonService.getCodes(activityType);					
				QuantityType quantityType = commonService.getBLQuantityTypesBasedOnActType(activityType);
				if (resetValue == "") {
					PrintWriter pw = response.getWriter();
					if(quantityType.getId() != 0 && quantityType.getDescription() != null) {
						pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ','+ codes.get("CLASS_CODE").toString()+','+quantityType.getDescription()+','+quantityType.getId());
					}else {
						pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ','+ codes.get("CLASS_CODE").toString());
					}
					return null;
				}
			}// End of Activity Type Refresh using Ajax
			
			businessTaxActivityForm.setEmailAddress(email);
			logger.debug("email.."+businessTaxActivityForm.getEmailAddress());
			flag = false;
			streetList = commonService.getBTBLStreetArrayList();
			activityTypes = commonService.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_TAX);
			activitySubTypes = (List)request.getAttribute("activitySubTypes");
			ownershipTypes = commonService.getOwnershipTypes();
			businessTaxActivityForm.setDisplaySuccessMsg("");

			 modelAndView.addObject("activityTypes", activityTypes);
			 modelAndView.addObject("businessAddressStreetName", businessAddressStreetName);
			 modelAndView.addObject("businessTaxActivityForm", businessTaxActivityForm);
			 modelAndView.addObject("activitySubTypes", activitySubTypes);
			 modelAndView.addObject("streetList", streetList);
			 modelAndView.addObject("activityTypes", activityTypes);
			 modelAndView.addObject("activitySubTypes", activitySubTypes);
	 		 modelAndView.addObject("applicationType", commonRepository.getApplicationType(StringUtils.s2i(businessTaxActivityForm.getApplicationType())).getDescription());
	 		 modelAndView.addObject("applicationTypeId", businessTaxActivityForm.getApplicationType());
			modelAndView.addObject("ownershipTypes", ownershipTypes);
			modelAndView.addObject("emailAddr", businessTaxActivityForm.getEmailAddress());
			modelAndView.addObject("activityDetails", businessTaxActivityForm);
			modelAndView.setViewName("online/newBTApplication");	   
		    
		}catch(Exception e){
			activityTypes = new ArrayList();
			e.printStackTrace();
		logger.error(e+e.getMessage());
		}
		return modelAndView;
	}


	@SuppressWarnings("unused")
	@RequestMapping(value="/saveBusinessTaxActivity", method = RequestMethod.POST)
	public ModelAndView saveBusinessTaxActivity(@ModelAttribute("activityForm") BusinessTaxActivityForm businessTaxActivityForm,HttpServletRequest request,HttpSession session) throws BasicExceptionHandler, ServletException {
		
		boolean editable = true;
		String typeId = "";
		int projectId = 0;
		Map classcode = null;
		int activityId = -1;
		int lsoId = -1;
		Map codes = null;
		String lsoAddress = "";
		List activitySubTypes;
		Activity activity = new Activity();
		String apnNoStr = "";
		int inspetorId = 0;
		String inspetor = "";
		int projectNameId = -1;
	
		businessTaxActivityForm=(BusinessTaxActivityForm)session.getAttribute("businessTaxActivityForm");
		logger.debug("Entered into SaveBusinessTaxActivityAction "+businessTaxActivityForm.toString());
		try {	
			logger.debug("businessLocation :"+businessTaxActivityForm.isBusinessLocation());

	        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
			//setting default values 
	        businessTaxActivityForm.setActivityStatus(commonRepository.getDefaultLkupActStatus(StringUtils.i2s(Constants.MODULE_NAME_BUSINESS_TAX)));
			businessTaxActivityForm.setApplicationDate(sdf.format(timestamp));
			businessTaxActivityForm.setIssueDate(sdf.format(timestamp));
			if(businessTaxActivityForm.getActivityType() != null) {
				codes = commonService.getCodes(businessTaxActivityForm.getActivityType());
				businessTaxActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
				businessTaxActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
				businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));
			}else {
				businessTaxActivityForm.setMuncipalCode("");
				businessTaxActivityForm.setSicCode("");
				businessTaxActivityForm.setClassCode("");
			}
			
			BusinessTaxActivity businessTaxActivity = onlineApplicationBTService.getBusinessTaxActivity(businessTaxActivityForm);
			MultiAddress[] multiAddessList = businessTaxActivity.getMultiAddress();
            logger.debug("multiAddessList :"+multiAddessList);
			user.setUserId(Constants.BT_USER_ID);
			businessTaxActivity.setCreatedBy(user.getUserId());
			logger.debug("user id is " + user.getUserId());

			boolean businessLocation = businessTaxActivityForm.isBusinessLocation();
			logger.debug("Business Location Is " + businessLocation);
			if (!businessLocation) {
				businessTaxActivity.setBusinessAddressStreetNumber("1");
				businessTaxActivity.setBusinessAddressStreetName("CITYWIDE");
				businessTaxActivity.setStreet(commonService.getStreet("53"));
				logger.debug("Address Street Id is " + businessTaxActivity.getStreet().getStreetId());
			}

			lsoId = commonService.checkAddress(businessTaxActivity.getBusinessAddressStreetNumber(), businessTaxActivity.getBusinessAddressStreetFraction(), businessTaxActivity.getStreet().getStreetId(), businessTaxActivity.getBusinessAddressUnitNumber(), businessTaxActivity.getBusinessAddressZip(), businessTaxActivity.getBusinessAddressZip4(), businessTaxActivity);
			logger.debug("Lso Id Returned Is " + lsoId);

			activityId = onlineApplicationBTService.saveBusinessTaxActivity(businessTaxActivity, lsoId);
			onlineApplicationBTService.updateMultiAddress(multiAddessList, activityId);
			logger.debug("Activity created, the new ID is " + activityId);

			// Add Process Team from project
			ProcessTeamForm processTeamForm = new ProcessTeamForm();
			processTeamForm.setPsaId(StringUtils.i2s(projectId));
			processTeamForm.setPsaType("P");
			processTeamForm = commonService.populateTeam(processTeamForm);
			logger.debug("after populateTeam : " + processTeamForm.getPsaId() + " and " + processTeamForm.getPsaType());
			processTeamForm.setPsaId(StringUtils.i2s(activityId));
			processTeamForm.setPsaType("A");

			ProcessTeamRecord tmRec = new ProcessTeamRecord();
			
			tmRec.setIsNew(true);
			tmRec.setLead("of");
			
			projectNameId = commonService.getProjectNameId("A", activityId);
			logger.debug("Obtained project name id is" + projectNameId);

			if (projectNameId == Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID) {
				User unassignedUser;
				codes = commonService.getCodes(businessTaxActivityForm.getActivityType());
				logger.debug("obtained codes of size " + codes.size());
				businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));

				if ((!businessTaxActivity.getBusinessAddressStreetName().trim().endsWith("CITYWIDE")) && (!businessTaxActivity.getClassCode().trim().startsWith("K01") && !businessTaxActivity.getClassCode().trim().startsWith("K03"))) {

					unassignedUser = commonService.getUser(Constants.USER_UNASSIGNED);
					tmRec.setNameId(StringUtils.i2s(unassignedUser.getUserId()));
					tmRec.setName(unassignedUser.getFirstName() + "  " + unassignedUser.getLastName());

					commonService.saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);

					// added for BT done Gayathri on 10thDec
					// For Dept - Planning Division & Approved by - Unassigned Planner in Approval List Screen
					int deptId = commonService.getDepartmentId(unassignedUser.getUserId());
					// added for BT done by Gayathri on 10thDec
					// For Dept - Planning Division & Approved by - Unassigned Planner in Approval List Screen
					commonService.insertIntoBTapproval(activityId, deptId, unassignedUser.getUserId());
				} else if (businessTaxActivity.getBusinessAddressStreetName().trim().endsWith("CITYWIDE")) {

					User edUser = commonService.getUser(Constants.USER_ED_KHOURDADJIAN);
					tmRec.setNameId(StringUtils.i2s(edUser.getUserId()));
					tmRec.setName(edUser.getFirstName() + "  " + edUser.getLastName());

					commonService.saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);

					//new CommonAgent();
					// added for BT done by Gayathri
					int deptId = commonService.getDepartmentId(edUser.getUserId());
					// added for BT done by Gayathri
					commonService.insertIntoBTapproval(activityId, deptId, edUser.getUserId());
				}
				unassignedUser = commonService.getUser(Constants.USER_EULA_EWARREN);

				tmRec.setNameId(StringUtils.i2s(unassignedUser.getUserId()));
				tmRec.setName(unassignedUser.getFirstName() + "  " + unassignedUser.getLastName());

				commonService.saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);
				int deptId = commonService.getDepartmentId(unassignedUser.getUserId());
				commonService.insertIntoBTapproval(activityId, deptId, unassignedUser.getUserId());

				// added for BT done by Gayathri on 15thJan For Dept - License and Code Services, Approved by - Inspector depending on the APN from LSO_ID in Approval List Screen
				apnNoStr = StringUtils.apnWithoutHyphens(commonService.getApnForLsoId(StringUtils.i2s(lsoId)));
				logger.debug("Got APN # :: " + apnNoStr);
					if (apnNoStr != "0" || apnNoStr.equals("")) {
						inspetorId = commonService.getInspectorForApn(apnNoStr);
						if (inspetorId != 0) {
							inspetor = commonService.getInspectorUser(inspetorId);
						}
						if (!inspetor.equalsIgnoreCase("")) {
							commonService.insertIntoBTapproval(activityId, Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES, inspetorId);
						}
					}
				}
		
				businessTaxActivity = onlineApplicationBTService.getBusinessTaxActivity(activityId, lsoId);		
				lsoAddress = commonService.getActivityAddressForId(activityId);
				logger.debug("Set the lso address to session as " + lsoAddress);
				typeId = businessTaxActivity.getActivityType().getTypeId();
				boolean added = commonService.copyRequiredCondition(typeId, activityId, user);
				logger.debug("added new conditions" + added);
				
				EmailSender emailSender = new EmailSender();
				emailDetails = new EmailDetails();
				Map<String, String> lkupSystemDataMap = new HashMap<String, String>();
				lkupSystemDataMap = commonService.getLkupSystemDataMap();
				emailDetails.setActId(StringUtils.i2s(activityId));
				emailDetails.setBusinessAccNo(businessTaxActivity.getBusinessAccountNumber());
				emailDetails.setPermitNumberForAddBLBT(businessTaxActivity.getActivityNumber());
				emailDetails.setLkupSystemDataMap(lkupSystemDataMap);						
				if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
    				emailDetails.setEmailId(lkupSystemDataMap.get(Constants.EDGESOFT_SUPPORT_EMAIL));
    			}else if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
					emailDetails.setEmailId(businessTaxActivity.getEmailAddress());
    			}
				emailDetails.setBusinessName(businessTaxActivity.getBusinessName());
				emailDetails.setEmailType(Constants.BUSINESS_TAX_APPLICATION_SUBMIT);
				emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
				emailSender.sendEmail(emailDetails);
				
				businessTaxActivityForm.setDisplaySuccessMsg("New Business Tax activity submitted. Please verify the registered email for credentials to login.");
				modelAndView.addObject("activityDetails", businessTaxActivityForm);
				activity.setDisplaySuccessMsg("New Business Tax activity submitted. Please verify the registered email for credentials to login");
				modelAndView.addObject("activityDetails", activity);
		} catch (Exception e) {
			logger.error("Exception occured while saving business tax activity " + e.getMessage());
			throw new ServletException("Exception occured while saving business Tax activity " + e.getMessage()+ e);
		}
		modelAndView.setViewName("permitRenewalDetails");
		return modelAndView;
		
	}
	

	@RequestMapping(value="/addBtFeeDetails")
	 public ModelAndView addBtFeeDetails(@ModelAttribute("activityForm") Activity activity) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("Entered into addbtFeeDetails getActType  "+activity.toString());
	
			activityDetails=onlineApplicationBTService.saveBTFeeDetails(activity);
			if(activityDetails.getTempId() == null) {
				modelAndView.setViewName("permitRenewalDetails");
			}else {
				modelAndView.setViewName("feeDetails");
			}
			activityDetails.setViewName(viewName);
			modelAndView.addObject("activityDetails", activityDetails);	
			modelAndView.addObject("feeList",activityDetails.getFeeList());
			modelAndView.addObject("totalFee", activityDetails.getTotalFee());

			logger.debug("Exiting addBtFeeDetails..."+activityDetails.toString());
		} catch (BasicExceptionHandler e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
           return modelAndView;
	 }


	@RequestMapping(value="/addBTPreviewPage", method=RequestMethod.POST)
	 public Activity addBTPreviewPage(@ModelAttribute("activityForm") Activity activity) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("inside addBTPreviewPage .. "+activity.toString());
		
			activityDetails=onlineApplicationBTService.getPreviewPageDetails(activity);		
			
			if(activityDetails.getTempId() == null) {
				viewName="permitRenewalDetails";
			}else {
				viewName="preview";
			}
			activityDetails.setViewName(viewName);
			} catch (BasicExceptionHandler e) {
				e.printStackTrace();
				logger.error(e.getMessage());			
			}
	       return activityDetails;
	 }	


	@RequestMapping(value="/addBTPaymentPage", method=RequestMethod.POST)

	 public Activity addBTPaymentPage(@ModelAttribute("activityForm") Activity activity,HttpSession session) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("Inside addBTPaymentPage  "+activity.toString());
			activityDetails=businessTaxRenewalService.getTempActivityDetails(activity.getTempId());
			activityDetails=businessTaxRenewalService.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
			List<Fee> feeList=onlineApplicationBTService.getBTFees(activity.getActType(),activity.getTempId(), activity.getScreenName());
			activityDetails.setFeeList(feeList);
			
			activityDetails.setTotalFee(activity.getTotalFee());		
			logger.debug("Total Fee previewPage  "+activity.getTotalFee());		
			
	    	if(activityDetails.getTempId() == null) {
				 viewName="permitRenewalDetails";
			}else {
				  viewName="redirect:/payment";
			}
			activityDetails.setViewName(viewName);

		} catch (BasicExceptionHandler e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
        return activityDetails;
	 }

	/**
	 * This method is to redirect to multi address page. If no mapping is given then it 
	 * will redirect to preview page
	 */

	@RequestMapping(value = "/btAddressPage", method = RequestMethod.POST)
	public ModelAndView btAddressPage(@ModelAttribute("activityForm") BusinessTaxActivityForm businessTaxActivityForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		int lsoId=-1;
		String email=businessTaxActivityForm.getEmailAddress();
		logger.debug("tostring.."+businessTaxActivityForm.toString());
		try {
			BusinessTaxActivity businessTaxActivity = onlineApplicationBTService.getBusinessTaxActivity(businessTaxActivityForm);

			boolean businessLocation = businessTaxActivityForm.isBusinessLocation();
			logger.debug("Business Location Is " + businessLocation);
			if (!businessLocation) {
				businessTaxActivity.setBusinessAddressStreetNumber("1");
				businessTaxActivity.setBusinessAddressStreetName("CITYWIDE");
				businessTaxActivity.setStreet(commonService.getStreet("53"));
				logger.debug("Address Street Id is " + businessTaxActivity.getStreet().getStreetId());
			}

			lsoId = commonService.checkAddress(businessTaxActivity.getBusinessAddressStreetNumber(), businessTaxActivity.getBusinessAddressStreetFraction(), businessTaxActivity.getStreet().getStreetId(), businessTaxActivity.getBusinessAddressUnitNumber(), businessTaxActivity.getBusinessAddressZip(), businessTaxActivity.getBusinessAddressZip4(), businessTaxActivity);
			logger.debug("Lso Id Returned Is " + lsoId);

			if (lsoId == 0) {
				String businessAddressStreetName = businessTaxActivityForm.getBusinessAddressStreetName();
				// if errors exist, then forward to the input page.
				logger.debug("Address did not match");
				businessTaxActivityForm.setDisplayErrorMsg("Please enter a valid combination of Street Number,Street Name and Zip Code.");
			}

			businessTaxActivityForm.setMultiAddress(commonService.getMultiAddress(0, "BT"));
			MultiAddress[] multiAddresses = businessTaxActivityForm.getMultiAddress();
			List<MultiAddress> multiAddressList = Arrays.asList(multiAddresses); 
			businessTaxActivityForm.setEmailAddress(email);	
		 	businessTaxActivityForm.setDisplaySuccessMsg("");
			modelAndView.addObject("multiAddress",multiAddressList);
			modelAndView.addObject("businessTaxActivityForm", businessTaxActivityForm);
			modelAndView.addObject("activityDetails", businessTaxActivityForm);
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.debug("Exiting AddBusinessTaxAction");	
		modelAndView.setViewName("online/addressBTApplication");	   
	    return modelAndView;
	}

	/**
	 * This method is to redirect to preview page.
	 */

	@RequestMapping(value = "/previewBTPage", method = RequestMethod.POST)
	public ModelAndView previewBTPage(@ModelAttribute("activityForm") BusinessTaxActivityForm businessTaxActivityForm,
		HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		logger.debug("businessTaxActivityForm... "+businessTaxActivityForm.toString());
		

		BusinessTaxActivityForm businessTaxActivityForm1 = new BusinessTaxActivityForm();
		businessTaxActivityForm1 = (BusinessTaxActivityForm)session.getAttribute("businessTaxActivityForm");
		logger.debug("businessTaxActivityForm... "+businessTaxActivityForm1.toString());
		businessTaxActivityForm1.setMultiAddress(businessTaxActivityForm.getMultiAddress());
		
		if(businessTaxActivityForm1.getActivityStatus() != null && businessTaxActivityForm1.getActivityStatus() != "") {
			businessTaxActivityForm1.setActivityStatusDesc(commonRepository.getActivityStatusDesc(businessTaxActivityForm1.getActivityStatus()));
		}
		if(businessTaxActivityForm1.getActivitySubType() != null && businessTaxActivityForm1.getActivitySubType() != "") {
			businessTaxActivityForm1.setActivitySubTypeDesc(commonRepository.getActivitySubTypeDesc(businessTaxActivityForm1.getActivitySubType()));
		}
		if(businessTaxActivityForm1.getActivityType() != null && businessTaxActivityForm1.getActivityType() != "") {
			businessTaxActivityForm1.setActivityTypeDescription(commonRepository.getActivityTypeDesc(businessTaxActivityForm1.getActivityType()));
		}
		if(businessTaxActivityForm1.getOwnershipType() != null && businessTaxActivityForm1.getOwnershipType() != "") {
			businessTaxActivityForm1.setOwnershipTypeDesc(commonRepository.getOwnershipTypeDesc(businessTaxActivityForm1.getOwnershipType()));
		}
		if(businessTaxActivityForm1.getBusinessAddressStreetName() != null && businessTaxActivityForm1.getBusinessAddressStreetName() != "") {
			businessTaxActivityForm1.setBusinessAddressStreetNameDesc(commonRepository.getStreetName(businessTaxActivityForm1.getBusinessAddressStreetName()));
		}
		modelAndView.addObject("businessTaxActivityForm", businessTaxActivityForm1);
		modelAndView.setViewName("online/previewBTApplication");
		return modelAndView;
	}
	

}