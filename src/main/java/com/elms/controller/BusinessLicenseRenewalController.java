package com.elms.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.ActivityStatus;
import com.elms.model.ActivityType;
import com.elms.model.Attachment;
import com.elms.model.BmcDescription;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.EmailDetails;
import com.elms.model.Fee;
import com.elms.model.Group;
import com.elms.model.MultiAddress;
import com.elms.model.User;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBLService;
import com.elms.util.EmailSender;
import com.elms.util.StringUtils;

@Controller
public class BusinessLicenseRenewalController {

	@Autowired
	BusinessLicenseRenewalRepository businessLicenseRenewalRepository;
	@Autowired
	BusinessLicenseRenewalService businessLicenseRenewalService;
	@Autowired
	CommonService commonService;
	@Autowired
	EmailDetails emailDetails;	
	@Autowired
	User user;
	@Autowired
	OnlineApplicationBLService onlineApplicationBLService;
	
	String viewName = "";
	private static final Logger logger = Logger.getLogger(BusinessLicenseRenewalController.class);
	 
	ModelAndView modelAndView = new ModelAndView();

	
	@RequestMapping("/fopen")
	public String fopen(Map<String, Object> model) {
		return "fopen";
	}

	@RequestMapping(value="/blActivityDetails", method=RequestMethod.POST)
	 public Activity blActivityDetails(@ModelAttribute("loginForm") Activity login) throws BasicExceptionHandler{
		Activity activityDetails= new Activity();
		
		activityDetails=businessLicenseRenewalService.saveBLRenewalDetails(login);
		logger.error("inside blActivityDetails() ::   "+activityDetails.toString());
		logger.debug(""+activityDetails.getTempId());
		logger.debug(""+login.getOnlineBLBTApplication());
		activityDetails.setOnlineBLBTApplication(login.getOnlineBLBTApplication());
		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			viewName="userDetails";
		}
		logger.debug(""+viewName);
		activityDetails.setViewName(viewName);
		return activityDetails;
	 }
	
	@RequestMapping(value="/blActivityDetailsUpdates", method = RequestMethod.GET)
	 public Activity blActivityDetailsUpdates(@ModelAttribute("activityForm") Activity activity) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		logger.error("inside blActivityDetailsUpdates() :: "+activity.toString());

		activityDetails=businessLicenseRenewalService.saveUserDetails(activity);
		
		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			if(activityDetails.getBlQtyFlag().equalsIgnoreCase("Y")) {
				viewName="quantityDetails";	
			}else {
				if(activityDetails.getAttachmentList() != null && activityDetails.getAttachmentList().size() != 0) {
					viewName="downloadAttachmentDetails";	
				}else {
					viewName="feeDetails";						
				}
			}
		}
		activityDetails.setViewName(viewName);
       return activityDetails;
	 }

	@RequestMapping(value="/blNoOfQtyDetails", method=RequestMethod.POST)
	 public Activity blNoOfQtyDetails(@ModelAttribute("qtyForm") Activity activity) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		logger.error("inside blNoOfQtyDetails() :: "+activity.toString());		
		activityDetails=businessLicenseRenewalService.saveQtyDetails(activity);

		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			if(activityDetails.getAttachmentList().size() == 0) {
				viewName="redirect:/feeDetails";	
			}else {
				viewName="attachmentDetails";					
			}	
		}
		activityDetails.setViewName(viewName);
		return activityDetails;
	 }
	
	@RequestMapping(value="/uploadAttachments", method=RequestMethod.POST)
	 public ModelAndView uploadAttachments(@ModelAttribute("activityForm") Activity activity,HttpServletRequest request) throws Exception {
		Activity activityDetails= new Activity();
		logger.error("Inside attachUploads ::  "+request.getParameter("id"));
		String attachmentTypeId=(String) request.getParameter("id");
		
		activityDetails=businessLicenseRenewalService.saveAttachmentDetails(activity,attachmentTypeId);		        
		if(activityDetails.getTempId() == null) {
			modelAndView.setViewName("permitRenewalDetails");
		}else {
			modelAndView.setViewName("uploadAttachmentDetails");
	    }
		modelAndView.addObject("attachmentList",activityDetails.getAttachmentList());
		modelAndView.addObject("activityDetails", activityDetails);

       return modelAndView;
	 }
	

	@RequestMapping(value="/feeDetails")
	 public ModelAndView feeDetails(@ModelAttribute("activityForm") Activity activity) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		logger.error("inside feeDetails() :: "+activity.toString());
		
		activityDetails=businessLicenseRenewalService.saveFeeDetails(activity);
		logger.error("getTotalFee... "+StringUtils.roundOffDouble(StringUtils.s2d(activityDetails.getTotalFee())));
			if(activityDetails.getTempId() == null) {
				 modelAndView.setViewName("permitRenewalDetails");
			}else {
				modelAndView.setViewName("feeDetails");
			}
			modelAndView.addObject("activityDetails", activityDetails);	
			modelAndView.addObject("feeList",activityDetails.getFeeList());
			modelAndView.addObject("totalFee", StringUtils.roundOffDouble(StringUtils.s2d(activityDetails.getTotalFee())));

			logger.error("inside feeDetails, Activity Object before exit :: "+activityDetails.toString());
            return modelAndView;
	 }

	@RequestMapping(value="/blPreviewPage", method=RequestMethod.POST)
	 public Activity blPreviewPage(@ModelAttribute("activityForm") Activity activity) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		logger.error("inside blPreviewPage :: "+activity.toString());
		
		activityDetails=businessLicenseRenewalService.getPreviewPageDetails(activity);
		
		modelAndView.addObject("totalAmt", StringUtils.roundOffDouble(StringUtils.s2d(activityDetails.getTotalFee())));	
		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			viewName="preview";
		}
				
		List<Attachment> attachedList = businessLicenseRenewalRepository.getAttachmentListForUpdatedAttachments(activity.getTempId(), Constants.EMPTY_FLAG);
		logger.error("attachmentList List size:: " +attachedList.size());

		activityDetails.setAttachmentList(attachedList);
		activityDetails.setViewName(viewName);
       return activityDetails;
	 }
	
	@RequestMapping(value="/blPaymentPage", method=RequestMethod.POST)
	 public Activity blPaymentPage(@ModelAttribute("activityForm") Activity activity,HttpSession session) {
		Activity activityDetails= new Activity();
		logger.error("inside blPaymentPage :: "+activity.toString());
		
		activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(activity.getTempId());
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=businessLicenseRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		activityDetails.setFeeList(feeList);
		
		activityDetails.setTotalFee(activity.getTotalFee());		
		logger.debug("Total Fee previewPage :: "+activity.getTotalFee());		
		
    	if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			viewName="redirect:/payment";
		}
		activityDetails.setViewName(viewName);
        return activityDetails;
	 }

	@RequestMapping(value="/responsePayment", method=RequestMethod.POST)
	 public ModelAndView responsePayment(@ModelAttribute("activityForm") Activity activity,HttpServletRequest request,HttpSession session) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		String authCode=request.getParameter("x_auth_code");
		String onlineTransactionId=request.getParameter("x_trans_id");
		
		logger.error("**********************"+activity.getTotalFee());  
		logger.error("**********************"+request.getParameter("CardHoldersName"));   
		logger.error("**********************"+request.getParameter("x_ship_to_last_name"));
		logger.error("**********************"+request.getParameter("x_trans_id") + " "+request.getParameter("Bank_Resp_Code_2")+ " "+request.getParameter("x_auth_code")+ " "+request.getParameter("Bank_Resp_Code"));
		logger.error("**********************"+request.getParameter("x_address") + " " +request.getParameter("x_zip")+" "+request.getParameter("x_city")+" "+request.getParameter("x_state"));  
		String responseCode=StringUtils.nullReplaceWithEmpty((String)request.getParameter("x_response_code"));
		logger.error("responseCode :: "+responseCode);
		
		EmailSender emailSender = new EmailSender();
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>(); 
        
		String x_invoice_num=request.getParameter("x_invoice_num");
		logger.error("inside responsePayment  "+activity.toString());
		activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(x_invoice_num);
		activityDetails.setResponseCode(responseCode);
		activityDetails.setAuthCode(authCode);
		activityDetails.setOnlineTransactionId(onlineTransactionId);
		activityDetails.setPermitNumber(activityDetails.getPermitNumber());
		activityDetails.setTempId(activity.getTempId());
		activityDetails.setName(request.getParameter("CardHoldersName"));
		activityDetails.setRenewalCode(activity.getRenewalCode());
		activityDetails.setTotalFee(activity.getTotalFee());
		activityDetails.setActType(activity.getActType());
		logger.error("ActivityDetails... "+activityDetails.toString());
		logger.error("Activity...  "+activity.toString());
		logger.error("responseCode.."+responseCode);

		lkupSystemDataMap = commonService.getLkupSystemDataMap();

		if( responseCode.equalsIgnoreCase("1")) {

			activity=businessLicenseRenewalRepository.saveApplicationDetails(activityDetails);
    		try {
    			
    			emailDetails.setBusinessName(activityDetails.getBusinessName());
//    			emailDetails.setEmailId(activityDetails.getEmail());  
    			if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
    				emailDetails.setEmailId(lkupSystemDataMap.get(Constants.EDGESOFT_SUPPORT_EMAIL));
    			}else if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
    				emailDetails.setEmailId(activityDetails.getEmail());
    			}
    			emailDetails.setNoOfEmployees(activityDetails.getNoOfEmp());
    			emailDetails.setPermitNumber(activityDetails.getPermitNumber());
    			emailDetails.setTotalFee(activityDetails.getTotalFee());
    			emailDetails.setLkupSystemDataMap(lkupSystemDataMap);
    			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
	    			emailDetails.setEmailType(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT);
	    			emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
	        		emailSender.sendEmail(emailDetails);
    			}else {
    				emailDetails.setEmailType(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL);
    				emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
    				emailSender.sendEmail(emailDetails);
    			}
    		}catch (Exception e) { 
    			logger.error(""+e);
    			e.printStackTrace();
			}
		}
		businessLicenseRenewalRepository.deleteTempActivity(activity.getTempId());
		activity=activityDetails;
		session.removeAttribute("activityDetails");
	    modelAndView.setViewName("responsePayment");			  
		session.setAttribute("activity", activity);
		session.setAttribute("lkupSystemDataMap", lkupSystemDataMap);
	    modelAndView.addObject("activity", activity);
	    modelAndView.addObject("lkupSystemDataMap", lkupSystemDataMap);
		return modelAndView;
	 }
	
	@RequestMapping(value="/blBackHistory", method=RequestMethod.POST)
	   public Activity blBackHistory(@ModelAttribute("activityForm") Activity activityForm, HttpSession session, HttpServletRequest request,String onlineBLBTApplication) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		String scrnNameForBack = activityForm.getScreenName();
	       logger.error("activityForm "+activityForm.toString());

	       activityDetails = businessLicenseRenewalRepository.getBLActivityInfo(activityForm.getBusinessAccNo(),activityForm.getRenewalCode());
	       Activity act=businessLicenseRenewalRepository.getQtyFlag(activityForm.getRenewalCode());
	       activityDetails.setBlQtyFlag(act.getBlQtyFlag());
	       //activityDetails.setTempBlQtyFlag(act.getBlQtyFlag());
	       logger.error("getTempBlQtyFlag..."+activityDetails.getTempBlQtyFlag());
	       logger.error("inside getBlQtyFlag "+act.getBlQtyFlag() + " Temp phone number :: " +activityDetails.getTempBusinessPhone());
	       
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.USER_DETAILS_SCREEN)){
	    	   
	    	   viewName="permitRenewalDetails";
	    	   activityDetails.setViewName(viewName);
	    	   return activityDetails;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.QUANTITY_DETAILS_SCREEN)){
	           logger.error("QUANTITY_DETAILS_SCREEN");
	    	   viewName="userDetails";
	    	   activityDetails.setViewName(viewName);
	    	   return activityDetails;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.ATTACHMENT_DETAILS_SCREEN)){
	           logger.error("ATTACHMENT_DETAILS_SCREEN");	
	           if(activityDetails.getTempId() == null) {
		   			viewName="permitRenewalDetails";
		   		}else {
		   			if(act.getBlQtyFlag().equalsIgnoreCase("Y")) {
		 	           viewName="quantityDetails";	
		   			}else {
		 	           viewName="userDetails";
		   			}
		   		}
	    	   activityDetails.setViewName(viewName);
	    	   return activityDetails;
	       }	       
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.FEE_DETAILS_SCREEN)){
	            logger.error("FEE_DETAILS_SCREEN");
	            List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activityForm.getRenewalCode()),activityForm.getTempId());
	   		
	            activityForm.setAttachmentList(attachmentList);
		   		logger.error("attachmentList List size:: " +attachmentList.size());
		   		
		   		if(activityDetails.getTempId() == null) {
		   			viewName="permitRenewalDetails";
		   		}else {
					if(attachmentList.size() == 0) {
						if(act.getBlQtyFlag().equalsIgnoreCase("Y")) {
							viewName="quantityDetails";
			   			}else {
			   				viewName="userDetails";
			   			}	
					}else {
			   			viewName="uploadAttachmentDetails";
					}
				}
		   		activityDetails.setAttachmentList(attachmentList);
		   		activityDetails.setViewName(viewName);
		   		return activityDetails;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.PREVIEW_DETAILS_SCREEN)){
	    	    logger.error("PREVIEW_DETAILS_SCREEN");
		           if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
		    	   		activityDetails = onlineApplicationBLService.saveBLFeeDetails(activityDetails);		        	   
		           }else {
			           List<Fee> feeList=businessLicenseRenewalRepository.getFees(activityForm.getActType(),activityForm.getTempId());
			   		   activityForm.setFeeList(feeList);
			   		   
				   		Map<String, String> lkupSystemData =   commonService.getLkupSystemDataMap();
	                    String lateFeeFromDate = "";
	                    String lateFeeToDate = "";
	                    logger.error("lkupSystemDataMap.size() :: " +lkupSystemData.size());
	                   
	                        if(lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
	                            lateFeeFromDate = lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE);
	                        }                         
	                        if(lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
	                            lateFeeToDate = lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE);
	                        }
				   				   
						double totalFee = 0;
						double lateFeeFactor = 0;
						double licenseFeeAnnualTotalFactor = 0;

						SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");  
						 String strDate = formatter.format(new Date());  
					     
						for (int i=0;i<feeList.size();i++) {
							if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
								logger.error("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

								 if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
									lateFeeFactor = feeList.get(i).getFeeFactor();
									logger.error("lateFeeFactor ::  " +lateFeeFactor);				
								}
							}						
							if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
								licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
								logger.error("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
							}
						}
						
						for (int i=0;i<feeList.size();i++) {
							if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
								logger.error("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());
								
								 if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
									totalFee = totalFee + (lateFeeFactor * licenseFeeAnnualTotalFactor);// When it is Late Fee, get Factor instead of feeFactor	
									feeList.get(i).setFeeTotal(lateFeeFactor * licenseFeeAnnualTotalFactor);
									feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor));
									logger.error("totalFee... " +totalFee);					
								}else {
									feeList.get(i).setFeeTotal(StringUtils.s2d(""));
									feeList.get(i).setFeeTotalStr("");
									feeList.get(i).setFeeDesc("");
								}
							}
							logger.error("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
							
							if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
								logger.error("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
								feeList.get(i).setFeeTotal(StringUtils.s2d(""));
								feeList.get(i).setFeeTotalStr("");
								feeList.get(i).setFeeDesc("");
							}
							if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC) && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
								totalFee = totalFee + feeList.get(i).getFeeTotal();// When it is other than Late Fee, get FeeFactor instead of Factor					
								logger.error("Line 324  Fee details.... " +feeList.get(i).getFeeTotal() + " :: Total Fee :: " +totalFee);
							}
						}
						
					logger.error("totalFee :: "+totalFee);
					activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
					activityDetails.setFeeList(feeList);
					activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));		        	   
		           }		
				activityDetails.setViewName("feeDetails");
				return activityDetails;

	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.UPLOAD_ATTACHMENT_DETAILS_SCREEN)){
	           logger.error("UPLOAD_ATTACHMENT_DETAILS_SCREEN");	
	    	   activityDetails.setViewName("downloadAttachmentDetails");
	    	   return activityDetails;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.DOWNLOAD_ATTACHMENT_DETAILS_SCREEN)){
	           logger.error("DOWNLOAD_ATTACHMENT_DETAILS_SCREEN");	   	   
		   		
	           if(act.getBlQtyFlag().equalsIgnoreCase("Y")) {
	        	   activityDetails.setViewName("quantityDetails");	
	   			}else {
	   				activityDetails.setViewName("userDetails");
	   			}
	    	   return activityDetails;
	       }
			
			activityDetails.setViewName("permitRenewalDetails");
			return activityDetails;
	   }

	@RequestMapping(value="/attachReUpload")
	
	 public ModelAndView attchmentReUpload(@ModelAttribute("attachReUploadForm") Activity activity) throws BasicExceptionHandler {

		Activity activityDetails=activity;
		logger.error("inside attchmentReUpload :: "+activityDetails.toString());	
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentListForReUpload(activityDetails.getRenewalCode());
		String permitNumber=commonService.checkRenewalType(activityDetails.getRenewalCode(),activityDetails.getBusinessAccNo());
		activityDetails.setPermitNumber(permitNumber);
		
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>(); 
		lkupSystemDataMap = commonService.getLkupSystemDataMap();
		
		activity.setAttachmentList(attachmentList);
		logger.error("attachmentList List size :: " +attachmentList.size());

		String tempFileLoc = lkupSystemDataMap.get(Constants.TEMP_FILE_LOC);
		activityDetails.setTempFileLoc(tempFileLoc);
		logger.error("tempFileLoc :: "+tempFileLoc);

		modelAndView.addObject("activityDetails",activityDetails);
		modelAndView.addObject("attachmentList",attachmentList);
		modelAndView.setViewName("afterPaymentDownloads");	
        return modelAndView;
	 }
	

	@RequestMapping(value="/reUploadAttachments", method=RequestMethod.POST)
	 public ModelAndView reUploadAttachments(@ModelAttribute("activityForm") Activity activity,HttpServletRequest request) throws Exception {
		Activity activityDetails= new Activity();
		logger.debug("inside reUploadAttachments :: "+request.getParameter("id"));
		String attachmentTypeId=(String) request.getParameter("id");
		
		activityDetails=businessLicenseRenewalService.saveReUploadAttachments(activity, attachmentTypeId);
	
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>();
		lkupSystemDataMap = commonService.getLkupSystemDataMap();
		
		String tempFileLoc = lkupSystemDataMap.get(Constants.TEMP_FILE_LOC);
		activityDetails.setTempFileLoc(tempFileLoc);
		logger.debug("tempFileLoc :: "+tempFileLoc);

		modelAndView.addObject("activityDetails",activityDetails);
		modelAndView.addObject("attachmentList",activityDetails.getAttachmentList());
		
		modelAndView.setViewName("attachmentReUploadDetails");
        return modelAndView;
	 }
	
	/**
	 * This method is for checking whether the particular business type file is uploaded or not
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/checkFileUploadedOrNot", method=RequestMethod.GET)
	public void checkFileUploadedOrNot(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("Control going to checkMandatoryFiles");
		int renewalCode = Integer.parseInt(request.getParameter("renewalCode"));
		businessLicenseRenewalService.checkFileUploadedOrNot(response,renewalCode);
	}
	
	/**
	 * For showing comments and bmc description in fancy box 
	 */
	@RequestMapping(value="/bmcDescription", method=RequestMethod.GET)
	 public ModelAndView getCommentsBmcDesc(HttpServletRequest request, HttpServletResponse response) {
		String comments = request.getParameter("comments");
		logger.debug("comments = "+comments);
		
		/*String bmcDesc = request.getParameter("bmcDesc");
		logger.debug("bmcDesc = "+bmcDesc);*/
		String count = request.getParameter("count");
		logger.debug("count = "+count);
		int counter = Integer.parseInt(count);
		logger.debug("counter = "+counter);
		
		List<String> bmcNameLinkList = new ArrayList<String>();
		for(int i=0; i < counter; i++) {
			bmcNameLinkList.add(request.getParameter("bmcDesc"+i));
			logger.debug("i = "+bmcNameLinkList.get(i));
		}
		
		List<BmcDescription> bmcDescList = new ArrayList<BmcDescription>();
		
		for(int i = 0; i < bmcNameLinkList.size(); i++) {	
			logger.debug(bmcNameLinkList.get(i));
			String bmcUrlDesc[] = bmcNameLinkList.get(i).split("=");
			
			BmcDescription bmcDescription = new BmcDescription();
			bmcDescription.setBmcName(bmcUrlDesc[0].trim());
			bmcDescription.setBmcLink(bmcUrlDesc[1].replace("!!!","#"));
			logger.debug("After removing # ::"+bmcDescription.getBmcLink());
			bmcDescList.add(bmcDescription);	
			
		}	
		
	     modelAndView.addObject("comments",comments); 
	     modelAndView.addObject("bmcDescList",bmcDescList);
	     modelAndView.setViewName("bmcDescription");
		return modelAndView;
	}
	

}