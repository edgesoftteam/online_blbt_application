package com.elms.controller;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Attachment;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.EmailDetails;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.QuantityType;
import com.elms.model.User;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.repository.CommonRepository;
import com.elms.repository.OnlineApplicationBLRepository;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBLService;
import com.elms.util.EmailSender;
import com.elms.util.StringUtils;

/**
 * @author Siva Kumari
 *
 */

@Controller
public class OnlineApplicationBLController {

	@Autowired
	CommonService commonService;
	@Autowired
	OnlineApplicationBLService onlineApplicationBLService;
	@Autowired
	OnlineApplicationBTController onlineApplicationBTController;
	@Autowired
	CommonRepository commonRepository;
	@Autowired
	EmailDetails emailDetails;
	@Autowired
	User user;
	@Autowired
	BusinessLicenseRenewalRepository businessLicenseRenewalRepository;
	@Autowired
	OnlineApplicationBLRepository onlineApplicationBLRepository;
	
	String viewName = "";
	private static final Logger logger = Logger.getLogger(OnlineApplicationBLController.class);

	ModelAndView modelAndView = new ModelAndView();

	
	
	/**
	 * This method is to default controller. If no mapping is given then it 
	 * will get called during the time of initial loading of application and loads the
	 * disclaimer message from lkup_system table
	 * @param Map<String, String>
	 * @param returns String
	 * @throws BasicExceptionHandler 
	 */
	@RequestMapping(value = "/emailVerification", method = RequestMethod.POST)
	public ModelAndView emailVerification(@ModelAttribute("businessLicenseActivityForm") BusinessLicenseActivityForm businessLicenseActivityForm, HttpServletRequest request, HttpServletResponse response,HttpSession session) throws BasicExceptionHandler {		

			String email = (StringUtils.nullReplaceWithEmpty(request.getParameter("emailAddr")));
			if(email == null || email == "") {
				email = businessLicenseActivityForm.getEmailAddress();
			}
			logger.debug("email::" + email);
			String action = (StringUtils.nullReplaceWithEmpty(request.getParameter("action")));
			logger.debug("action::" + action);
			String applicationType = (StringUtils.nullReplaceWithEmpty(request.getParameter("applicationType")));
			if(applicationType == null || applicationType == "") {
				applicationType = businessLicenseActivityForm.getApplicationType();
			}
			logger.debug("applicationType::" + applicationType);
			String flag = (StringUtils.nullReplaceWithEmpty(request.getParameter("flag")));
			logger.debug("flag::" + flag);
			int code =0;
			String statusMsg="";

			BusinessTaxActivityForm businessTaxActivityForm = new BusinessTaxActivityForm();
			List applicationTypes = new ArrayList();			
			try {
				if(action != null && action.equalsIgnoreCase("emailCheck")) {
					if(email != null) {

						code=commonService.generateRandomDigits(8);
						logger.debug("code "+code);
						logger.debug("email "+email);
						
						EmailSender emailSender = new EmailSender();
						emailDetails = new EmailDetails();
						Map<String, String> lkupSystemDataMap = new HashMap<String, String>();
						lkupSystemDataMap = commonService.getLkupSystemDataMap();
						emailDetails.setLkupSystemDataMap(lkupSystemDataMap);
						if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
		    				emailDetails.setEmailId(lkupSystemDataMap.get(Constants.EDGESOFT_SUPPORT_EMAIL));
		    			}else if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
			    			emailDetails.setEmailId(email);	
		    			}
						emailDetails.setSecurityCode(code);
						if (applicationType.equalsIgnoreCase(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSLICENSE))) {
							emailDetails.setEmailType(Constants.BL_SECURITY_CODE_VERIFICATION);							
						}else {
							emailDetails.setEmailType(Constants.BT_SECURITY_CODE_VERIFICATION);
						}
	    				emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
	    				emailSender.sendEmail(emailDetails);
	    				
		    			statusMsg="Email sent successfully.";
		    			logger.debug(code);
						logger.debug(statusMsg);
						PrintWriter pw = response.getWriter();
						pw.write(statusMsg + "," + code ); 

						request.setAttribute("emailAddr", email);
						return null;
					}	
				}
				if(action!=null && action.equalsIgnoreCase("initialOne")) {
					
					user = (User) commonService.saveUserIfnotExistBasedonEmailAddr(email);
					commonService.updateEmailVerified(email);
					String message = "User added for this Email Address";
					session.setAttribute(Constants.USER_KEY, user);
					logger.debug("user object put in the session");
					request.setAttribute("message", message);	
					businessLicenseActivityForm.setEmailAddress(email);
					
					if(applicationType.equalsIgnoreCase(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSLICENSE))) {
						businessLicenseActivityForm.setEmailAddress(email);
						modelAndView = addBusinessLicenseActivity(businessLicenseActivityForm, request, response);						
					}else if(applicationType.equalsIgnoreCase(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSTAX))) {
						businessTaxActivityForm.setEmailAddress(email);
						businessTaxActivityForm.setApplicationType(applicationType);
						modelAndView = onlineApplicationBTController.addBusinessTaxActivity(businessTaxActivityForm, request, response);						
					}
					return modelAndView;
				}
				
				applicationTypes = commonService.getApplicationTypes();
				modelAndView.addObject("applicationTypes", applicationTypes);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		modelAndView.setViewName("online/emailVerification");
		return modelAndView;
	}

	@RequestMapping(value = "/addBusinessLicenseActivity", method = RequestMethod.POST)
	public ModelAndView addBusinessLicenseActivity(@ModelAttribute("businessLicenseActivityForm") BusinessLicenseActivityForm businessLicenseActivityForm,HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.debug("businessLicenseActivityForm "+businessLicenseActivityForm.toString());
		Map codes = null;
		List activitySubTypes = new ArrayList();
		List activityTypes;
		List streetList = new ArrayList();
		List activityStatuses = new ArrayList();
		List qtytList = new ArrayList();
		List exemtList = new ArrayList();
		List ownershipTypes = new ArrayList();
		String activityType = request.getParameter("activityType")!=null ? request.getParameter("activityType") : "";
		try {
			activityTypes = onlineApplicationBLService.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_LICENSE);
			String addressStreetName = businessLicenseActivityForm.getAddressStreetName();
			modelAndView.addObject("activityTypes", activityTypes);
			modelAndView.addObject("addressStreetName", addressStreetName);
	
			String resetValue = (StringUtils.nullReplaceWithEmpty(request.getParameter("resetValue")));
			logger.debug("resetVaue::" + resetValue);
	
			boolean flag = StringUtils.s2b(request.getParameter("flag"));
			
			String businessLoc = StringUtils.nullReplaceWithEmpty(request.getParameter("businessLoc"));
			logger.debug("To set the busines location defaulted to :: " + businessLoc);
	
//			if (!activityType.equals("") && (businessLicenseActivityForm.getMuncipalCode() != "")
//					&& (businessLicenseActivityForm.getSicCode() != "")
//					&& (businessLicenseActivityForm.getClassCode() != "")) {
//				activitySubTypes = commonService.getActivitySubTypes(businessLicenseActivityForm.getActivityType());
//				businessLicenseActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
//			}

			// Start of Activity Type Refresh using Ajax
			if (activityType !="") {
				businessLicenseActivityForm.setActivityType(activityType);
				QuantityType quantityType = new QuantityType();
				activitySubTypes = commonService.getActivitySubTypes(activityType);
				businessLicenseActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
				codes = commonService.getCodes(activityType);
				quantityType = commonService.getBLQuantityTypesBasedOnActType(activityType);
				logger.debug("quantity values.."+quantityType.getId()+" "+quantityType.getDescription());
				if (resetValue == "") {
					PrintWriter pw = response.getWriter();
					if(quantityType.getId() != 0 && quantityType.getDescription() != null) {
						pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ','+ codes.get("CLASS_CODE").toString()+','+quantityType.getDescription()+','+quantityType.getId());
					}else {
						pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ','+ codes.get("CLASS_CODE").toString());
					}
					return null;
				}
			} // End of Activity Type Refresh using Ajax
			streetList = commonService.getBTBLStreetArrayList();
			activityTypes = commonService.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_LICENSE);
			activitySubTypes = (List) request.getAttribute("activitySubTypes");
			activityStatuses = commonService.getBLActivityStatuses(Constants.MODULE_NAME_BUSINESS_LICENSE);
			qtytList = commonService.getQuantityTypes();
			exemtList = commonService.getExemptionTypes();
			ownershipTypes = commonService.getOwnershipTypes();

			modelAndView.addObject("businessLicenseActivityForm", businessLicenseActivityForm);
			modelAndView.addObject("streetList", streetList);
			modelAndView.addObject("activityTypes", activityTypes);
			modelAndView.addObject("activitySubTypes", activitySubTypes);		
			modelAndView.addObject("applicationType", commonRepository.getApplicationType(StringUtils.s2i(businessLicenseActivityForm.getApplicationType())).getDescription());
			modelAndView.addObject("applicationTypeId", businessLicenseActivityForm.getApplicationType());
			modelAndView.addObject("activityStatuses", activityStatuses);
			modelAndView.addObject("qtytList", qtytList);
			modelAndView.addObject("exemtList", exemtList);
			modelAndView.addObject("ownershipTypes", ownershipTypes);
			modelAndView.addObject("emailAddr", businessLicenseActivityForm.getEmailAddress());
			modelAndView.addObject("activityDetails", businessLicenseActivityForm);
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelAndView.setViewName("online/newBLApplication");
		return modelAndView;

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/saveBusinessLicenseActivity", method = RequestMethod.POST)
	public ModelAndView saveBusinessLicenseActivity(@ModelAttribute("activityForm") BusinessLicenseActivityForm businessLicenseActivityForm,HttpServletRequest request,HttpSession session) throws Exception {

		String typeId = "";
		Map codes = null;
		int lsoId = -1;
		Activity activity = new Activity();
		int activityId = -1;
		businessLicenseActivityForm=(BusinessLicenseActivityForm)session.getAttribute("businessLicenseActivityForm");
		logger.debug("Entered into SaveBusinessLicenseActivityAction "+businessLicenseActivityForm.toString());
		try {
			if (!(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetNumber()).equalsIgnoreCase(""))
					&& !(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetName()).equalsIgnoreCase(""))) {
				businessLicenseActivityForm.setBusinessLocation(false);
				businessLicenseActivityForm.setAddressStreetName(null);
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        //return number of milliseconds since January 1, 1970, 00:00:00 GMT
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
			//setting default activityStatus 
			businessLicenseActivityForm.setActivityStatus(commonRepository.getDefaultLkupActStatus(StringUtils.i2s(Constants.MODULE_NAME_BUSINESS_LICENSE)));
	        businessLicenseActivityForm.setApplicationDate(sdf.format(timestamp));
	        businessLicenseActivityForm.setIssueDate(sdf.format(timestamp));
	        if(businessLicenseActivityForm.getActivityType() != null) {
				codes = commonService.getCodes(businessLicenseActivityForm.getActivityType());
				businessLicenseActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
				businessLicenseActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
				businessLicenseActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));
			}else {
				businessLicenseActivityForm.setMuncipalCode("");
				businessLicenseActivityForm.setSicCode("");
				businessLicenseActivityForm.setClassCode("");
			}
	        	        
			BusinessLicenseActivity businessLicenseActivity = onlineApplicationBLService.getBusinessLicenseActivity(businessLicenseActivityForm);
			MultiAddress[] multiAddessList = businessLicenseActivityForm.getMultiAddress();
			logger.debug("multiAddessList :" + multiAddessList.toString());
			logger.debug("multiAddessList :" + businessLicenseActivityForm.getMultiAddress().toString());
			
			user.setUserId(Constants.BL_USER_ID);
			logger.debug("user id is " + user.getUserId());

			boolean businessLocation = businessLicenseActivityForm.isBusinessLocation();
			logger.debug("Business Location Is " + businessLocation);

			if (!businessLocation) {
				businessLicenseActivity.setAddressStreetNumber("1");
				businessLicenseActivity.setAddressStreetName("CITYWIDE");
				businessLicenseActivity.setStreet(commonService.getStreet("53"));
				logger.debug("Address Street Id is " + businessLicenseActivity.getStreet().getStreetId());
			}

			lsoId = commonService.checkAddress(businessLicenseActivity.getAddressStreetNumber(), businessLicenseActivity.getAddressStreetFraction(), businessLicenseActivity.getStreet().getStreetId(), businessLicenseActivity.getAddressUnitNumber(), businessLicenseActivity.getAddressZip(), businessLicenseActivity);
			logger.debug("Lso Id Returned Is " + lsoId);

			businessLicenseActivity.setCreatedBy(user.getUserId());
			activityId = onlineApplicationBLService.saveBusinessLicenseActivity(businessLicenseActivity);
			logger.debug("Activity created, the new ID is " + activityId);
			onlineApplicationBLService.updateMultiAddress(multiAddessList, activityId);

			logger.debug("businessLicenseActivity.. "+businessLicenseActivity.toString());
			businessLicenseActivity = onlineApplicationBLService.getBusinessLicenseActivity(activityId,lsoId);
			logger.debug("type id after.. "+businessLicenseActivity.getActivityType().getTypeId());
			typeId = businessLicenseActivity.getActivityType().getTypeId();

			boolean added = commonService.copyRequiredCondition(typeId, activityId, user);
			logger.debug("added new conditions" + added);

			EmailSender emailSender = new EmailSender();
			emailDetails = new EmailDetails();
			Map<String, String> lkupSystemDataMap = new HashMap<String, String>();
			lkupSystemDataMap = commonService.getLkupSystemDataMap();
			emailDetails.setActId(StringUtils.i2s(activityId));
			emailDetails.setBusinessAccNo(businessLicenseActivity.getBusinessAccountNumber());
			emailDetails.setPermitNumberForAddBLBT(businessLicenseActivity.getActivityNumber());
			emailDetails.setBusinessName(businessLicenseActivity.getBusinessName());
			emailDetails.setLkupSystemDataMap(lkupSystemDataMap);
			if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")) {
				emailDetails.setEmailId(lkupSystemDataMap.get(Constants.EDGESOFT_SUPPORT_EMAIL));
			}else if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("Y")) {
				emailDetails.setEmailId(businessLicenseActivity.getEmailAddress());
			}
			emailDetails.setEmailType(Constants.BUSINESS_LICENSE_APPLICATION_SUBMIT);
			emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
			emailSender.sendEmail(emailDetails);
			activity.setDisplaySuccessMsg("New Business License activity submitted. Please verify the registered email for credentials to login.");
			
			modelAndView.addObject("activityDetails", activity);
			logger.debug("Token reset successfully");

		} catch (Exception e) {
			logger.error("Exception occured while saving business license activity " + e.getMessage()+e);
			throw new Exception("Exception occured while saving business license activity " + e.getMessage()+e);
		}

		modelAndView.setViewName("permitRenewalDetails");
		return modelAndView;
	}


	@RequestMapping(value="/addBLPreviewPage", method=RequestMethod.POST)
	 public Activity addBLPreviewPage(@ModelAttribute("activityForm") Activity activity) throws BasicExceptionHandler {
		Activity activityDetails= new Activity();
		logger.error("inside blPreviewPage :: "+activity.toString());
		activityDetails=onlineApplicationBLService.getBLPreviewPageDetails(activity);
		modelAndView.addObject("totalAmt", StringUtils.roundOffDouble(StringUtils.s2d(activityDetails.getTotalFee())));	
		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			viewName="preview";
		}
				
		List<Attachment> attachedList = businessLicenseRenewalRepository.getAttachmentListForUpdatedAttachments(activity.getTempId(), Constants.EMPTY_FLAG);
		logger.error("attachmentList List size:: " +attachedList.size());

		activityDetails.setAttachmentList(attachedList);
		activityDetails.setViewName(viewName);
       return activityDetails;
	 }
	
	@RequestMapping(value="/addBLPaymentPage", method=RequestMethod.POST)
	 public Activity addBLPaymentPage(@ModelAttribute("activityForm") Activity activity,HttpSession session) {
		Activity activityDetails= new Activity();
		logger.debug("inside blPaymentPage :: "+activity.toString());
		
		activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(activity.getTempId());
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=onlineApplicationBLRepository.getFees(activity.getActType(),activity.getTempId());
		activityDetails.setFeeList(feeList);
		
		activityDetails.setTotalFee(activity.getTotalFee());		
		logger.debug("Total Fee previewPage :: "+activity.getTotalFee());		
		
    	if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			viewName="redirect:/payment";
		}
		activityDetails.setViewName(viewName);
        return activityDetails;
	 }
	
	/**
	 * This method is to redirect to multi address page. If no mapping is given then it 
	 * will redirect to preview page
	 */

	@RequestMapping(value = "/blAddressPage", method = RequestMethod.POST)
	public ModelAndView blAddressPage(@ModelAttribute("activityForm") BusinessLicenseActivityForm businessLicenseActivityForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		int lsoId=-1;
		logger.debug("businessLicenseActivityForm... "+businessLicenseActivityForm.toString());
		String activityType = request.getParameter("activityType") != null ? request.getParameter("activityType") : "";
		activityType = activityType.trim();

		BusinessLicenseActivity businessLicenseActivity = onlineApplicationBLService.getBusinessLicenseActivity(businessLicenseActivityForm);
		try {
			boolean businessLocation = businessLicenseActivityForm.isBusinessLocation();
			logger.debug("Business Location Is " + businessLocation);
			if (!businessLocation) {
				businessLicenseActivity.setAddressStreetNumber("1");
				businessLicenseActivity.setAddressStreetName("CITYWIDE");
				businessLicenseActivity.setStreet(commonService.getStreet("53"));
				logger.debug("Address Street Id is " + businessLicenseActivity.getStreet().getStreetId());
			}
			lsoId = commonService.checkAddress(businessLicenseActivity.getAddressStreetNumber(),
						businessLicenseActivity.getAddressStreetFraction(),	businessLicenseActivity.getStreet().getStreetId(),
						businessLicenseActivity.getAddressUnitNumber(), businessLicenseActivity.getAddressZip(), businessLicenseActivity);
			logger.debug("Lso Id Returned Is " + lsoId);
			businessLicenseActivityForm.setLsoId(StringUtils.i2s(lsoId));
			if (lsoId == 0) {
				String addressStreetName = (String) request.getAttribute("addressStreetName");
				logger.debug("Address did not match");
				businessLicenseActivityForm.setDisplayErrorMsg("Address entered does not exist, Please check.");
				modelAndView.addObject("addressStreetName", addressStreetName);
				modelAndView.addObject("activityDetails", businessLicenseActivityForm);
			}
			if (businessLicenseActivityForm.getDisplayErrorMsg() != null) {
				activityType = (request.getParameter("activityType") != null) ? request.getParameter("activityType"): "";
				logger.debug("activity type got from the jsp is " + activityType);
				logger.debug("Errors exist, going back to the Business License Activity page");
				request.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
				return modelAndView;
			}
			businessLicenseActivityForm.setApplicationType(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSLICENSE));
			businessLicenseActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BL_PENDING));			
			businessLicenseActivityForm.setMultiAddress(commonService.getMultiAddress(0, "BL"));
			MultiAddress[] multiAddresses = businessLicenseActivityForm.getMultiAddress();
			List<MultiAddress> multiAddressList = Arrays.asList(multiAddresses);
			businessLicenseActivityForm.setActivitySubType("-1");
			businessLicenseActivityForm.setDisplaySuccessMsg("");
			modelAndView.addObject("multiAddress", multiAddressList);
			modelAndView.addObject("emailAddr", businessLicenseActivityForm.getEmailAddress());
			modelAndView.addObject("businessLicenseActivityForm", businessLicenseActivityForm);
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelAndView.setViewName("online/addressBLApplication");
		return modelAndView;
	}

	@RequestMapping(value = "/checkAddress", method = RequestMethod.POST)
	public String checkAddress(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String streetNo = request.getParameter("streetNo") != null ? request.getParameter("streetNo") : "";
		String streetFraction = request.getParameter("streetFraction") != null ? request.getParameter("streetFraction") : "";
		String streetId = request.getParameter("streetId") != null ? request.getParameter("streetId") : "";
		String unit = request.getParameter("unit") != null ? request.getParameter("unit") : "";
		String zip = request.getParameter("zip") != null ? request.getParameter("zip") : "";
		String lsoId="";
		
		BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
		logger.debug(streetNo + streetId+zip);
		try {
			if(streetNo != null && streetId!= null && zip!= null) {
				lsoId =StringUtils.i2s(commonService.checkAddress(streetNo,streetFraction,StringUtils.s2i(streetId),unit,zip, businessLicenseActivity));
				logger.debug("Lso Id Returned Is " + lsoId);				
				PrintWriter pw = response.getWriter();
				pw.write(lsoId);
				return null;
			}
		} catch (Exception e) {
			logger.error("error in checkaddress "+e+e.getMessage());
			e.printStackTrace();
		}
		
		return lsoId;
	}

	/**
	 * This method is to redirect to preview page
	 */

	@RequestMapping(value = "/previewBLPage", method = RequestMethod.POST)
	public ModelAndView previewBLPage(@ModelAttribute("activityForm") BusinessLicenseActivityForm businessLicenseActivityForm,
		HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		BusinessLicenseActivityForm businessLicenseActivityForm1 = new BusinessLicenseActivityForm();
		businessLicenseActivityForm1 = (BusinessLicenseActivityForm)session.getAttribute("businessLicenseActivityForm");
		logger.debug("businessLicenseActivityForm... "+businessLicenseActivityForm1.toString());
		logger.debug("businessLicenseActivityForm... "+businessLicenseActivityForm.toString());
		businessLicenseActivityForm1.setMultiAddress(businessLicenseActivityForm.getMultiAddress());
		if(businessLicenseActivityForm1.getActivityStatus() != null && businessLicenseActivityForm1.getActivityStatus() != "") {
			businessLicenseActivityForm1.setActivityStatusDesc(commonRepository.getActivityStatusDesc(businessLicenseActivityForm1.getActivityStatus()));
		}
		if(businessLicenseActivityForm1.getActivitySubType() != null && businessLicenseActivityForm1.getActivitySubType() != "") {
			businessLicenseActivityForm1.setActivitySubTypeDesc(commonRepository.getActivitySubTypeDesc(businessLicenseActivityForm1.getActivitySubType()));
		}
		if(businessLicenseActivityForm1.getActivityType() != null && businessLicenseActivityForm1.getActivityType() != "") {
			businessLicenseActivityForm1.setActivityTypeDescription(commonRepository.getActivityTypeDesc(businessLicenseActivityForm1.getActivityType()));
		}
		if(businessLicenseActivityForm1.getOwnershipType() != null && businessLicenseActivityForm1.getOwnershipType() != "") {
			businessLicenseActivityForm1.setOwnershipTypeDesc(commonRepository.getOwnershipTypeDesc(businessLicenseActivityForm1.getOwnershipType()));
		}
		if(businessLicenseActivityForm1.getAddressStreetName() != null && businessLicenseActivityForm1.getAddressStreetName() != "") {
			businessLicenseActivityForm1.setAddressStreetNameDesc(commonRepository.getStreetName(businessLicenseActivityForm1.getAddressStreetName()));
		}
		if(businessLicenseActivityForm1.getTypeOfExemptions() != null && businessLicenseActivityForm1.getTypeOfExemptions() != "") {
			businessLicenseActivityForm1.setTypeOfExemptionsDesc(commonRepository.getExemptionTypeDesc(businessLicenseActivityForm1.getTypeOfExemptions()));
		}
		
		modelAndView.addObject("businessLicenseActivityForm", businessLicenseActivityForm1);
		modelAndView.setViewName("online/previewBLApplication");
		return modelAndView;
	}

}