/**
 * 
 */
package com.elms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.EmailDetails;
import com.elms.model.Fee;
import com.elms.model.User;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.BusinessTaxRenewalService;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBTService;

/**
 * @author Gayathri Turlapati
 *
 */
@Controller
public class BusinessTaxRenewalController {
	
	@Autowired
	BusinessTaxRenewalService businessTaxRenewalService;
	@Autowired
	BusinessLicenseRenewalService businessLicenseRenewalService;
	@Autowired
	CommonService commonService;
	@Autowired
	User user;
	@Autowired
	EmailDetails emailDetails;	
	@Autowired
	OnlineApplicationBTService onlineApplicationBTService;
	
	String viewName = "";
	
	private static final Logger logger = Logger.getLogger(BusinessTaxRenewalController.class);
	 
	ModelAndView modelAndView = new ModelAndView();	
	
	@RequestMapping(value="/btActivityDetails", method=RequestMethod.POST)
	 public Activity btActivityDetails(@ModelAttribute("loginForm") Activity btActivityForm) throws BasicExceptionHandler {
		
		Activity activityDetails = new Activity();
		
		logger.debug("inside save of activityDetails :: " +btActivityForm.getRenewalCode()+btActivityForm.getBusinessAccNo());
		
		activityDetails=businessTaxRenewalService.saveBTRenewalDetails(btActivityForm);
		logger.debug("inside save **  "+activityDetails.toString());
		
		activityDetails.setViewName("userDetails");
		return activityDetails;
	 }
	
	
	 @RequestMapping(value="/btActivityDetailsUpdates", method=RequestMethod.GET)
	 public Activity btActivityDetailsUpdates(@ModelAttribute("activityForm") Activity activity) throws BasicExceptionHandler {
		 Activity activityDetails = new Activity();
		 
		logger.debug("inside userDetails getPermitNumber :: "+activity.getPermitNumber());
		logger.debug("inside userDetails temp ID :: "+activity.getTempId());
		
		String viewName = "";
		logger.debug("inside userDetails getPermitNumber :: "+activityDetails.getPermitNumber());
		logger.debug("inside userDetails temp ID :: "+activityDetails.toString());
		
		activityDetails=businessTaxRenewalService.saveUserDetails(activity);
		
		if(activityDetails.getTempId() == null) {
			viewName="permitRenewalDetails";
		}else {
			if(activityDetails.getBtQtyFlag().equalsIgnoreCase("Y")) {
				logger.debug("permitRenewalDetails redirect");
				viewName="quantityDetails";	
			}else {
				logger.debug("Fee redirect");
				viewName="redirect:/btFeeDetails";
			}
		}		
		activityDetails.setViewName(viewName);
		logger.debug("Temp Street Number :: " +activityDetails.getDescription() + " :: Temp Street Name :: " +activityDetails.getAddress());
      return activityDetails;
	 }

	@RequestMapping(value="/btNoOfQtyDetails", method=RequestMethod.POST)
	 public Activity btNoOfQtyDetails(@ModelAttribute("qtyForm") Activity activity) {
			Activity activityDetails = new Activity();

		try {
			logger.debug("inside btQtyDetails"+activity.toString());
			activityDetails=businessTaxRenewalService.saveQtyDetails(activity);
			
			if(activityDetails.getTempId() == null) {
				logger.debug("permitRenewalDetails redirect");
				viewName = "permitRenewalDetails";
			}else {
				logger.debug("Fee redirect");
				viewName = "redirect:/btFeeDetails";
			}
		} catch (BasicExceptionHandler e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("Temp Street Number :: " +activityDetails.getStrNo() + " :: Temp Street Name :: " +activityDetails.getAddress() );
		activityDetails.setViewName(viewName);
		return activityDetails;
	 }
	
	@RequestMapping(value="/btFeeDetails")
	 public ModelAndView btFeeDetails(@ModelAttribute("activityForm") Activity activity) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("inside btFeeDetails getActType  "+activity.toString());
	
			activityDetails=businessTaxRenewalService.saveFeeDetails(activity);
			logger.debug("getTotalFee... "+activityDetails.getTotalFee());
			if(activityDetails.getTempId() == null) {
				modelAndView.setViewName("permitRenewalDetails");
			}else {
				modelAndView.setViewName("feeDetails");
			}
			activityDetails.setViewName(viewName);
			modelAndView.addObject("activityDetails", activityDetails);	
			modelAndView.addObject("feeList",activityDetails.getFeeList());
			modelAndView.addObject("totalFee", activityDetails.getTotalFee());

			logger.debug("inside feeDetails getActType  "+activityDetails.toString());
		} catch (BasicExceptionHandler e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
           return modelAndView;
	 }

	@RequestMapping(value="/btPreviewPage", method=RequestMethod.POST)
	 public Activity btPreviewPage(@ModelAttribute("activityForm") Activity activity) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("inside btPreviewPage .. "+activity.toString());
		
			activityDetails=businessTaxRenewalService.getPreviewPageDetails(activity);		
			
//			modelAndView.addObject("totalAmt", StringUtils.s2d(StringUtils.roundOffDouble(activityDetails.getTotalFee())));	
			if(activityDetails.getTempId() == null) {
				viewName="permitRenewalDetails";
			}else {
				viewName="preview";
			}
			activityDetails.setViewName(viewName);
			} catch (BasicExceptionHandler e) {
				e.printStackTrace();
				logger.error(e.getMessage());			
			}
	       return activityDetails;
	 }	

	@RequestMapping(value="/btPaymentPage", method=RequestMethod.POST)

	 public Activity btPaymentPage(@ModelAttribute("activityForm") Activity activity,HttpSession session) {
		Activity activityDetails = new Activity();
		try {
			logger.debug("inside btPaymentPage  "+activity.toString());
			activityDetails=businessTaxRenewalService.getTempActivityDetails(activity.getTempId());
			activityDetails=businessTaxRenewalService.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
			List<Fee> feeList=businessTaxRenewalService.getFees(activity.getActType(),activity.getTempId(), activity.getScreenName());
			activityDetails.setFeeList(feeList);
			
			activityDetails.setTotalFee(activity.getTotalFee());		
			logger.debug("Total Fee previewPage  "+activity.getTotalFee());		
			
	    	if(activityDetails.getTempId() == null) {
				 viewName="permitRenewalDetails";
			}else {
				  viewName="redirect:/payment";
			}
			activityDetails.setViewName(viewName);

		} catch (BasicExceptionHandler e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
        return activityDetails;
	 }
	
	@RequestMapping(value="/btBackHistory", method=RequestMethod.POST)
	   public ModelAndView btBackHistory(@ModelAttribute("activityForm") Activity activityForm, HttpSession session, HttpServletRequest request,String onlineBLBTApplication) {
		Activity activityDetails = new Activity();
	       String scrnNameForBack = activityForm.getScreenName();
	       logger.debug("scrnNameForBack "+scrnNameForBack);

	       try {
				activityDetails = businessTaxRenewalService.getBTActivityInfo(activityForm.getBusinessAccNo(),activityForm.getRenewalCode());
				Activity act=businessTaxRenewalService.getBtQtyFlag(activityForm.getRenewalCode());
				activityDetails.setBtQtyFlag(act.getBtQtyFlag());
			       
				logger.debug("inside getBtQtyFlag "+act.getBtQtyFlag() + "Temp phone number :: " +activityDetails.getTempBusinessPhone());
	       
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.USER_DETAILS_SCREEN)){
	           modelAndView.setViewName("permitRenewalDetails");
	           modelAndView.addObject("activityDetails", activityDetails);		   		
		       return modelAndView;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.QUANTITY_DETAILS_SCREEN)){
	           logger.debug("QUANTITY_DETAILS_SCREEN");	           
	   			modelAndView.setViewName("userDetails");
	   			modelAndView.addObject("activityDetails", activityDetails);		   		
		        return modelAndView;
	       }
	       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.FEE_DETAILS_SCREEN)){
	            logger.debug("FEE_DETAILS_SCREEN");
	            
	            if(activityDetails.getTempId() == null) {
		   			modelAndView.setViewName("permitRenewalDetails");
		   		}else {
		   			if(act.getBtQtyFlag().equalsIgnoreCase("Y")) {
		   				  modelAndView.setViewName("quantityDetails");	
		   			}else {
		   				  modelAndView.setViewName("userDetails");
		   			}		   				
				}
		   		modelAndView.addObject("activityDetails", activityDetails);		   		
	           return modelAndView;
	       }
		       if(activityDetails.getBusinessAccNo()!=null && scrnNameForBack.equalsIgnoreCase(Constants.PREVIEW_DETAILS_SCREEN)){
		    	   logger.debug("PREVIEW_DETAILS_SCREEN");
		    	   if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
		    	   		activityDetails = onlineApplicationBTService.saveBTFeeDetails(activityDetails);
		    	   }else {
		    	   		activityDetails = businessTaxRenewalService.btFeeDetailsOnBack(activityDetails);
		    	   }
		    		modelAndView.addObject("feeList",activityDetails.getFeeList());
					modelAndView.addObject("totalFee", activityDetails.getTotalFee());		
					modelAndView.addObject("activityDetails", activityDetails);					
					modelAndView.setViewName("feeDetails");
			        return modelAndView;
	       		}

	       } catch (BasicExceptionHandler e) {
				e.printStackTrace();
				logger.error(e.getStackTrace());
			}
	       modelAndView.addObject("activityDetails", activityDetails);
			modelAndView.setViewName("permitRenewalDetails");	   
	       return modelAndView;
		}	
		
}