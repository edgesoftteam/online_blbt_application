package com.elms.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Attachment;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBLService;
import com.elms.service.OnlineApplicationBTService;
import com.elms.util.StringUtils;

@Controller
public class CommonController {

	@Autowired
	BusinessLicenseRenewalRepository businessLicenseRenewalRepository;
	@Autowired
	BusinessLicenseRenewalController businessLicenseRenewalController;
	@Autowired
	BusinessTaxRenewalController businessTaxRenewalController;
	@Autowired
	OnlineApplicationBLController onlineApplicationBLController;
	@Autowired
	OnlineApplicationBTController onlineApplicationBTController;
	@Autowired
	OnlineApplicationBTService onlineApplicationBTService;
	@Autowired
	OnlineApplicationBLService onlineApplicationBLService;
	@Autowired
	CommonService commonService;
	@Autowired
	BusinessLicenseRenewalService businessLicenseRenewalService;
	
	private static final Logger logger = Logger.getLogger(CommonController.class);
	RedirectView rv = new RedirectView();
	ModelAndView modelAndView = new ModelAndView();
	HttpServletRequest request;
	@Autowired
	HttpSession session;
	
	/**
	 * This method is to default controller. If no mapping is given then it 
	 * will get called during the time of initial loading of application and loads the
	 * disclaimer message from lkup_system table
	 * @param Map<String, String>
	 * @param returns String
	 * @throws BasicExceptionHandler 
	 */
	@RequestMapping("/")
	public String home(Map<String, Object> model) throws BasicExceptionHandler {		
		Map<String, String> lkupSystemDataMap =   commonService.getLkupSystemDataMap();
		String lkupSystemValue = "";
		logger.error("lkupSystemDataMap.size() :: " +lkupSystemDataMap.size());
			if(lkupSystemDataMap.get(Constants.RENEWAL_DISCLAIMER_TEXT_MESSAGE)!=null) {
				lkupSystemValue = lkupSystemDataMap.get(Constants.RENEWAL_DISCLAIMER_TEXT_MESSAGE);
			}
		model.put("lkupSystemValue", lkupSystemValue);
		return "permitRenewalDetails";
	}

	/**
	 * This method is to verify the Permit type is valid or not. 
	 * If the permit type is valid then it will find the Permit type is BL or BT using renewal code and Business account number.
	 * disclaimer message from lkup_system table
	 * @param Activity Form
	 * @param returns ModelAndView
	 */
	@RequestMapping(value="/verifyRenewalType", method=RequestMethod.POST)
	 public ModelAndView verifyRenewalType(@ModelAttribute("activityForm") Activity activityForm) throws BasicExceptionHandler {

		 Activity activity = new Activity();
		String permitNumber=commonService.checkRenewalType(activityForm.getRenewalCode(),activityForm.getBusinessAccNo());
		logger.error("PermitNumber :: " +permitNumber);
		activityForm.setPermitNumber(permitNumber);		
		
		String renewalOnline = "";
		String onlineBLBTApplication = "";
		double partialPayFlag = 0.00;
		String attachFlag="N";
		String actTypeOnline = "";
		if(permitNumber != null) {
			renewalOnline = commonService.getBlOrBtActivityRenewalOnlineFlag(activityForm);
			partialPayFlag = commonService.getFullOrPartialPaymentFlag(activityForm);
			actTypeOnline = commonService.getActTypeOnlineFlag(activityForm);
			onlineBLBTApplication = commonService.getBlBtOnlineApplicationFlag(activityForm);
			activityForm.setOnlineBLBTApplication(onlineBLBTApplication);
			logger.debug("onlineBLBTApplication..."+onlineBLBTApplication);
		}
		activity.setOnlineBLBTApplication(onlineBLBTApplication);
		logger.error("PartialPayFlag :: "+partialPayFlag);
        //redirecting to businessLicenseRenewalController / businessTaxRenewalController for further details
		if(permitNumber !=null && permitNumber.startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
	        	logger.error(" :: Inside BL Condition :: ");
	        	
				if((renewalOnline!=null && renewalOnline.equalsIgnoreCase("Y"))) {
					activityForm.setDisplayErrorMsg("This renewal has been paid in full or fees is not assigned");
					List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentListForReUpload(activityForm.getRenewalCode());
					logger.error("size :: "+attachmentList.size());
					if(attachmentList.size() > 0) {
						for(int i=0;i<attachmentList.size();i++) {							
		
							if(StringUtils.nullReplaceWithEmpty(attachmentList.get(i).getFileName()).equals("")) {							
								attachFlag="Y";
							}
						}
					}
					activityForm.setAttachFlag(attachFlag);
					modelAndView.setViewName("permitRenewalDetails");
		    		modelAndView.addObject("activityDetails", activityForm);
				}else if((actTypeOnline != null && actTypeOnline.equalsIgnoreCase("Y") && renewalOnline!=null && renewalOnline.equalsIgnoreCase("N")) || (onlineBLBTApplication != null && onlineBLBTApplication.equalsIgnoreCase("Y"))){
					activity=businessLicenseRenewalController.blActivityDetails(activityForm);
					logger.debug("activity.. "+activity.getOnlineBLBTApplication());
					modelAndView.addObject("activityDetails", activity);
					modelAndView.setViewName(activity.getViewName());
				}else {
					activityForm.setDisplayErrorMsg("Please enter valid Account Number and Application/Renewal Code");
					modelAndView.addObject("activityDetails", activityForm);
					modelAndView.setViewName("permitRenewalDetails");					
				}
				
	        }else if(permitNumber!=null && permitNumber.startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
	        	logger.error(" :: Inside BT Condition :: ");    
	        	if(partialPayFlag > 0.00 || (onlineBLBTApplication != null && onlineBLBTApplication.equalsIgnoreCase("Y"))) {
						activity=businessTaxRenewalController.btActivityDetails(activityForm);
						modelAndView.addObject("onlineBLBTApplication", onlineBLBTApplication);
						modelAndView.addObject("activityDetails", activity);
						modelAndView.setViewName(activity.getViewName());
				}else {
					activityForm.setDisplayErrorMsg("This tax renewal has been paid in full (or) fees is not assigned to this Business.");
					modelAndView.addObject("activityDetails", activityForm);
					modelAndView.setViewName("permitRenewalDetails");
	        	}
	        }else {
				activityForm.setDisplayErrorMsg("Please enter valid Account Number and Application/Renewal Code");
				modelAndView.addObject("activityDetails", activityForm);
				modelAndView.setViewName("permitRenewalDetails");					
	        }
		logger.debug("view name... "+modelAndView.getViewName());
		modelAndView.addObject("onlineBLBTApplication",onlineBLBTApplication);
		return modelAndView;
	 }
	
	
	/**
	 * This method is to redirect the existing request 
	 * to redirect to appropriate controller either BusinessLicenseRenewalController or BusinessTaxRenewalController
	 * @param screenName
	 * @param request
	 */
	@RequestMapping(value="/redirectToBlOrBt", method=RequestMethod.POST)
	 public ModelAndView redirectToBlOrBt(@ModelAttribute("activityForm") Activity activityForm) throws BasicExceptionHandler {
		 String scrnName = activityForm.getScreenName();
	     logger.error("activityForm :: "+activityForm.toString());
	     logger.error("scrnName :: "+scrnName);
	      
		 Activity activity = new Activity();
		 String permitNumber=commonService.checkRenewalType(activityForm.getRenewalCode(),activityForm.getBusinessAccNo());
		 logger.error("PermitNumber :: " +permitNumber);

		String onlineBLBTApplication = "";
		if(permitNumber != null) {
			onlineBLBTApplication = commonService.getBlBtOnlineApplicationFlag(activityForm);
		}
		 activityForm.setPermitNumber(permitNumber);
		 if(activityForm.getPermitNumber() !=null && !activityForm.getPermitNumber().equals("")) {
		
			if(scrnName.equalsIgnoreCase(Constants.USER_DETAILS_SCREEN)){   		
	    		
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
					activity=businessLicenseRenewalController.blActivityDetailsUpdates(activityForm);
					logger.error("activity form returned from businessLicenseRenewalController :: "+activity.toString());
					
					List<Attachment> attachmentsList = activity.getAttachmentList();
					List<String> fileType = new ArrayList<String>();
					for(Attachment attachment: attachmentsList) {						
						fileType.add(attachment.getDownloadOrUploadType());
					}
					
					logger.error("activity :: "+activity.toString());
					modelAndView.addObject("activityDetails", activity);
					modelAndView.addObject("attachmentList",activity.getAttachmentList()); 
					
					if(activity.getBlQtyFlag().equalsIgnoreCase("Y")) {
						modelAndView.setViewName(activity.getViewName());
					}else {
						if(fileType.contains("B") || fileType.contains("D")) {	//	B means both upload download,D means download					
							modelAndView.setViewName(activity.getViewName());
						}else if(fileType.contains("U")){				//U means upload		
							modelAndView.setViewName("uploadAttachmentDetails");
						}else {						
							if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
								activity=onlineApplicationBLService.saveBLFeeDetails(activity);								
							}else {
								activity=businessLicenseRenewalService.saveFeeDetails(activity);
							}
							logger.error("getTotalFee :: "+StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));						
							modelAndView.addObject("feeList",activity.getFeeList());
							modelAndView.addObject("totalFee", StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));
							modelAndView.setViewName("feeDetails");
						}
					}					
					return modelAndView;
				}
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
					activity=businessTaxRenewalController.btActivityDetailsUpdates(activityForm);
					modelAndView.addObject("activityDetails", activity);
					modelAndView.setViewName(activity.getViewName());
					if(activity.getViewName().equalsIgnoreCase("redirect:/btFeeDetails")){
						activity.setScreenName(scrnName);
						if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
							modelAndView=onlineApplicationBTController.addBtFeeDetails(activity);							
						}else {
							modelAndView=businessTaxRenewalController.btFeeDetails(activity);							
						}
					}
					return modelAndView;
		 	    }
			}		
	
			if(scrnName.equalsIgnoreCase(Constants.QUANTITY_DETAILS_SCREEN)){

				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
		    		activity=businessLicenseRenewalController.blNoOfQtyDetails(activityForm);
		    		
		    		List<Attachment> attachmentsList = activity.getAttachmentList();
					//boolean flag = false;
					List<String> fileType = new ArrayList<String>();
					for(Attachment attachment: attachmentsList) {						
						fileType.add(attachment.getDownloadOrUploadType());
					}
					
					logger.error("activity :: "+activity.toString());
					modelAndView.addObject("activityDetails", activity);
					modelAndView.addObject("attachmentList",activity.getAttachmentList()); 
					
					if(fileType.contains("B") || fileType.contains("D")) {	//	if file type is B that means both upload download, D that means download		   
						modelAndView.setViewName("downloadAttachmentDetails");
					}else if(fileType.contains("U")){				//U that means upload		
						modelAndView.setViewName("uploadAttachmentDetails");
					}else {						

						if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
							activity=onlineApplicationBLService.saveBLFeeDetails(activity);								
						}else {
							activity=businessLicenseRenewalService.saveFeeDetails(activity);
						}
						logger.error("getTotalFee ::  "+StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));						
						modelAndView.addObject("feeList",activity.getFeeList());
						modelAndView.addObject("totalFee", StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));
						modelAndView.setViewName("feeDetails");
					}					
		    		return modelAndView;
				}
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
					activity=businessTaxRenewalController.btNoOfQtyDetails(activityForm);
					modelAndView.addObject("activityDetails", activity); 
					modelAndView.setViewName(activity.getViewName());
					if(activity.getViewName().equalsIgnoreCase("redirect:/btFeeDetails")){
						activity.setScreenName(scrnName);
						
						if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
							modelAndView=onlineApplicationBTController.addBtFeeDetails(activity);							
						}else {
							modelAndView=businessTaxRenewalController.btFeeDetails(activity);							
						}
					}
					return modelAndView;
				}
			}
			
			if(scrnName.equalsIgnoreCase(Constants.DOWNLOAD_ATTACHMENT_DETAILS_SCREEN)){
				logger.debug("businessLicenseRenewalController :: "+ activityForm.toString());	   		
	    		
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
					activity=businessLicenseRenewalRepository.getBLActivityInfo(activityForm.getBusinessAccNo(),activityForm.getRenewalCode());
					List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
					List<String> fileType = new ArrayList<String>();
					
					for(Attachment attachment: attachmentList) {						
						fileType.add(attachment.getDownloadOrUploadType());
					}
					
					Activity act=businessLicenseRenewalRepository.getQtyFlag(activity.getRenewalCode());
					activity.setBlQtyFlag(act.getBlQtyFlag());
					
					activity.setAttachmentList(attachmentList);
					modelAndView.addObject("activityDetails", activity);
					modelAndView.addObject("attachmentList",activity.getAttachmentList());  
					if(fileType.contains("U") || fileType.contains("B")) {
						modelAndView.setViewName("uploadAttachmentDetails");
					}else {
						if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
							activity=onlineApplicationBLService.saveBLFeeDetails(activity);								
						}else {
							activity=businessLicenseRenewalService.saveFeeDetails(activity);
						}
						logger.debug("getTotalFee ::  "+StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));						
						modelAndView.addObject("feeList",activity.getFeeList());
						modelAndView.addObject("totalFee", StringUtils.roundOffDouble(StringUtils.s2d(activity.getTotalFee())));						
						modelAndView.setViewName("feeDetails");
					}
					return modelAndView;
				}				
			}
			
			if(scrnName.equalsIgnoreCase(Constants.FEE_DETAILS_SCREEN)){
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
					if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
						activity=onlineApplicationBLController.addBLPreviewPage(activityForm);
					}else {
						activity=businessLicenseRenewalController.blPreviewPage(activityForm);	
					}
		    		
		    		modelAndView.addObject("attachedList",activity.getAttachmentList());
		    		modelAndView.addObject("feeList",activity.getFeeList());			
		    		modelAndView.addObject("activityDetails", activity); 
					modelAndView.setViewName(activity.getViewName());
		    		return modelAndView;
				}
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
					if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
						activity=onlineApplicationBTController.addBTPreviewPage(activityForm);
					}else {
						activity=businessTaxRenewalController.btPreviewPage(activityForm);	
					}
		    		
					modelAndView.addObject("feeList",activity.getFeeList());			
					modelAndView.addObject("activityDetails", activity);
					modelAndView.setViewName(activity.getViewName());
		    		return modelAndView;
				}
			}		
	
			if(scrnName.equalsIgnoreCase(Constants.PREVIEW_DETAILS_SCREEN)){
				String viewName="";
				
				Map<String, String> lkupSystemDataMap = new HashMap<String,String>();
				lkupSystemDataMap = commonService.getLkupSystemDataMap();				
				
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
		    		if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
		    			activity=onlineApplicationBLController.addBLPaymentPage(activityForm,session);
		    		}else {
		    			activity=businessLicenseRenewalController.blPaymentPage(activityForm,session);
		    		}
		    		activity.setRenewalCodeIntValue(StringUtils.s2i(activity.getRenewalCode()));
		    		session.setAttribute("activityDetails", activity);
	    			session.setAttribute("lkupSystemDataMap",lkupSystemDataMap);
	    			
		    		modelAndView.addObject("lkupSystemDataMap",session.getAttribute("lkupSystemDataMap"));
		    		modelAndView.addObject("feeList",activity.getFeeList());
		    		modelAndView.addObject("activityDetails", activity);
					if(activity.getViewName().equalsIgnoreCase("redirect:/payment")) {
		    			viewName = payment();
		    		}
		    		modelAndView.setViewName(viewName);
					return modelAndView;
				}
				if(activityForm.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
					if(onlineBLBTApplication.equalsIgnoreCase("Y")) {
		    			activity=onlineApplicationBTController.addBTPaymentPage(activityForm,session);
		    		}else {
		    			activity=businessTaxRenewalController.btPaymentPage(activityForm, session);
		    		}

					activity.setRenewalCodeIntValue(StringUtils.s2i(activity.getRenewalCode()));

	    			session.setAttribute("activityDetails", activity);
	    			session.setAttribute("lkupSystemDataMap",lkupSystemDataMap);
	    			
					modelAndView.addObject("lkupSystemDataMap",session.getAttribute("lkupSystemDataMap"));
		    		modelAndView.addObject("feeList",activity.getFeeList());
		    		modelAndView.addObject("activityDetails", activity);
		    		if(activity.getViewName().equalsIgnoreCase("redirect:/payment")) {
		    			viewName = payment();
		    		}
		    		modelAndView.setViewName(viewName);
					return modelAndView;
				}
			}
		 }else {
			 modelAndView.setViewName("permitRenewalDetails");
			 return modelAndView;
		 }
		 modelAndView.addObject("activityDetails", activityForm);
		 return modelAndView;
	}
	
	/**
	 * This method is to redirect the existing request 
	 * to appropriate screen when clicked on Back button on UI
	 * @param screenName
	 * @param Activity Form
	 */	
	@RequestMapping(value="/redirectToBlOrBtForBack", method=RequestMethod.POST)
	 public ModelAndView redirectToBlOrBtForBack(@ModelAttribute("activityForm") Activity activityForm) throws BasicExceptionHandler {
		 String scrnName = activityForm.getScreenName();
		 logger.error("activityForm :: "+activityForm.toString());
		 logger.error("scrnName :: "+scrnName);
		 Activity activity = new Activity();  

		String permitNumber=commonService.checkRenewalType(activityForm.getRenewalCode(),activityForm.getBusinessAccNo());
		logger.error("permitNumber :: " +permitNumber);
		
		activityForm.setPermitNumber(permitNumber);		
		String onlineBLBTApplication = "";
		if(permitNumber != null) {
			onlineBLBTApplication = commonService.getBlBtOnlineApplicationFlag(activityForm);
		}
		logger.debug("onlineBLBTApplication.. "+onlineBLBTApplication);
        //redirecting to businessLicenseRenewalController or businessTaxRenewalController for further details

			if(permitNumber!=null && permitNumber.startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
				activity=businessLicenseRenewalController.blBackHistory(activityForm, session, request,onlineBLBTApplication);
	    		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
	    		activity.setAttachmentList(attachmentList);
	    		
	    		Activity act=businessLicenseRenewalRepository.getQtyFlag(activity.getRenewalCode());
	    		activity.setBlQtyFlag(act.getBlQtyFlag());
	    		
	    		List<String> fileType = new ArrayList<String>();
				for(Attachment attachment: attachmentList) {						
					fileType.add(attachment.getDownloadOrUploadType());
				}
				
				if(scrnName.equalsIgnoreCase(Constants.PREVIEW_DETAILS_SCREEN)) {
					modelAndView.setViewName("feeDetails");
				}else if(fileType.contains("B") || (fileType.contains("U") && !scrnName.equalsIgnoreCase(Constants.UPLOAD_ATTACHMENT_DETAILS_SCREEN))) {		   //B means both upload download,U means upload
					modelAndView.setViewName(activity.getViewName());
				}else if(fileType.contains("D") && !scrnName.equalsIgnoreCase(Constants.DOWNLOAD_ATTACHMENT_DETAILS_SCREEN)){		//D means download		
					modelAndView.setViewName("downloadAttachmentDetails");
				}else if(activity.getBlQtyFlag().equals("Y") && !scrnName.equalsIgnoreCase(Constants.QUANTITY_DETAILS_SCREEN)){			
					modelAndView.setViewName("quantityDetails");
				}else {
					modelAndView.setViewName("userDetails");
				}
	    		//modelAndView.setViewName(activity.getViewName());
	    		modelAndView.addObject("feeList",activity.getFeeList());
				modelAndView.addObject("totalFee", activity.getTotalFee());
		   		modelAndView.addObject("attachmentList",activity.getAttachmentList());
		   		modelAndView.addObject("activityDetails", activity);
	    		return modelAndView;
			}
			if(permitNumber!=null && permitNumber.startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
				modelAndView=businessTaxRenewalController.btBackHistory(activityForm, session, request,onlineBLBTApplication);
	    		return modelAndView;
			}
	       modelAndView.addObject("activityDetails", activityForm);
	       modelAndView.setViewName("permitRenewalDetails");
	       return modelAndView;
	 }

	
	/**
	 * This method is to cancel the existing request and
	 * remove all the objects from session by invalidating it.
	 * @param activityForm
	 * @param session
	 * @return permitRenewal.jsp
	 */
	
	@RequestMapping(value="/cancelSession", method=RequestMethod.POST)
	public ModelAndView cancelToHome(@ModelAttribute("activityForm") Activity activityForm, HttpSession session) {
		int count = 0;
		logger.error("inside cancelToHome() "+session.getAttribute("tempId"));
		count = businessLicenseRenewalRepository.deleteTempActivity(activityForm.getTempId());		
		
		if(count > 0){
			session.invalidate();
			modelAndView.setViewName("permitRenewalDetails");
		}
		return modelAndView;
	}

	/**
	 * This method is to fetch the data from lookup system table 
	 * for processing the payment.
	 * @param 
	 * @param 
	 * @return payment.jsp
	 * @throws BasicExceptionHandler 
	 */
	@RequestMapping(value="/payment")
    public String payment() throws BasicExceptionHandler {
		logger.error("inside payment");
		
		Map<String, String> lkupSystemDataMap =   commonService.getLkupSystemDataMap();
		String lkupSystemValue = "";
		if(lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG)!=null) {
				lkupSystemValue = lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG);
			}
		
		if(lkupSystemValue!=null && lkupSystemValue.equalsIgnoreCase("Y")) {
			logger.error(":: payment ::");
			return "payment";			
		}else {
			logger.error(":: paymentTestEnv :: ");
			return "paymentTestEnv";
		}        
    }	
	
	/**
	 * This method is to BL download Documents 
	 * for processing the payment.
	 * @param request
	 * @param Activity Object
	 * @return ModelAndView
	 * @throws BasicExceptionHandler 
	 */
	@RequestMapping(value="/downloadAttachment", method=RequestMethod.GET)
	public ModelAndView downloadAttachemnt(HttpServletRequest request, HttpServletResponse response) throws BasicExceptionHandler {
		commonService.downloadAttachment(request, response);
		modelAndView.setViewName("downloadAttachmentDetails");
		return modelAndView;
	}
	
	@RequestMapping(value="/blReUploadAttachments", method=RequestMethod.POST)
	public ModelAndView attchmentReUpload(@ModelAttribute("attachReUploadForm") Activity activity) throws BasicExceptionHandler {

		Activity activityDetails=activity;
		logger.debug("inside attchmentReUpload.."+activityDetails.toString());	
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentListForReUpload(activityDetails.getRenewalCode());
		
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>(); 	
		lkupSystemDataMap = commonService.getLkupSystemDataMap();
		
		activity.setAttachmentList(attachmentList);
		logger.debug("attachmentList List size:: " +attachmentList.size());

		String tempFileLoc = lkupSystemDataMap.get(Constants.TEMP_FILE_LOC);
		activityDetails.setTempFileLoc(tempFileLoc);
		logger.debug("tempFileLoc.."+tempFileLoc);

		modelAndView.addObject("activityDetails",activityDetails);
		modelAndView.addObject("attachmentList",attachmentList);
		modelAndView.setViewName("attachmentReUploadDetails");			
        return modelAndView;
	 }	
	
	/**
	 * For reset bl upload attachments 
	 */
	@RequestMapping(value="/resetBlUploadAttachments", method=RequestMethod.POST)
	 public ModelAndView resetBlUploadAttachments(@ModelAttribute("activityForm") Activity activityForm) throws BasicExceptionHandler {
		 String scrnName = activityForm.getScreenName();
	     logger.error("activityForm :: "+activityForm.toString());
	     logger.error("scrnName :: "+scrnName);
	     commonService.resetBlUploadAttachments(activityForm.getTempId());
	     List<Attachment> attachmentList = businessLicenseRenewalService.getAttachmentList(StringUtils.s2i(activityForm.getRenewalCode()),activityForm.getTempId());	     
	     modelAndView.addObject("attachmentList",attachmentList); 
	     modelAndView.setViewName("uploadAttachmentDetails");
		return modelAndView;
	}
	
}