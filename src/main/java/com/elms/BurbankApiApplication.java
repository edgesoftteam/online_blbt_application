package com.elms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class BurbankApiApplication extends SpringBootServletInitializer{
	@Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(BurbankApiApplication.class);
	    }
	public static void main(String[] args) {
		SpringApplication.run(BurbankApiApplication.class, args);
	}

}
