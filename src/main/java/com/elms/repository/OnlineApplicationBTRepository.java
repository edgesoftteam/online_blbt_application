package com.elms.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;

import com.elms.common.Constants;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.NextId;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;

/**
 * @author Siva Kumari
 *
 */


@Repository
public class OnlineApplicationBTRepository {

private static final Logger logger = Logger.getLogger(OnlineApplicationBTRepository.class);
	
@Autowired
JdbcTemplate  jdbcTemplate;
@Autowired
NextIdRepository nextIdRepository;
@Autowired
PlatformTransactionManager transactionManager;
@Autowired
DataSource dataSource;
@Autowired
CommonService commonService;
@Autowired
CommonRepository commonRepository;
@Autowired
BusinessTaxActivity businessTaxActivity;

private double pfTotal;

	public void setDataSource(DataSource dataSource) {
	    this.dataSource = dataSource;
	    this.jdbcTemplate = new JdbcTemplate(dataSource);
	 }
	 public void setTransactionManager(PlatformTransactionManager transactionManager) {
	    this.transactionManager = transactionManager;
	 }

	/**
	 * get codes for Class Code
	 * 
	 * @param Class
	 *            Code
	 * @return
	 * @throws Exception
	 */
	public Map getCodesForClassCode(String classCode) throws Exception {
		logger.debug("getCodesForClassCode(" + classCode + ")");

		Map codes = new HashMap();
		String sql = "";
		sql = "select * from lkup_act_type where upper(CLASS_CODE)=" + StringUtils.checkString(classCode.toUpperCase());
		logger.debug(sql);

		try {

			//RowSet rs = new Wrapper().select(sql);
			
			codes= jdbcTemplate.query(sql, new ResultSetExtractor<Map<String, String>>() {                
		        @Override
		        public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {    
		            Map<String, String> info = new HashMap<String, String>();
		            if (rs != null && rs.next()) {
		            	info.put("TYPE", StringUtils.nullReplaceWithEmpty(rs.getString("TYPE")));
		            	info.put("MUNI_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
		            	info.put("SIC_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
		            	info.put("CLASS_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));                   
		            }            
		            return info;
		        }
	    	});

			
		} catch (Exception e) {
			logger.error("Error in getCodes " + e.getMessage());
			throw e;
		}
		return codes;
	}

	/**
	 * Check class codes is exists or not
	 * 
	 * @param Class Code
	 * @return
	 * @throws Exception
	 */
	public boolean isClassCodeExists(String classCode) throws Exception {
		logger.debug("isClassCodeExists(" + classCode + ")");

		boolean isClassCodeExists=false;
	
		String sql = "SELECT COUNT(*) FROM lkup_act_type where  upper(CLASS_CODE)=" + StringUtils.checkString(classCode.toUpperCase());
		logger.debug("isClassCodeExists query = "+sql);				

		try {
		    int count = jdbcTemplate.queryForObject(sql, new Object[] { }, Integer.class);
		    logger.debug("count = "+count);
		    if (count > 0) {
		    	isClassCodeExists = true;
		    }						
		} catch (Exception e) {
			logger.error("Error in getCodes " + e.getMessage());
			throw e;
		}
		return isClassCodeExists;
	}


    /**
     * SaveBusinessTaxActivity()
     * 
     * @param businesstaxactivity
     *            , lsoId
     * @return activityId
     * @param businessTaxActivity
     * @throws Exception
     */
	public int saveBusinessTaxActivity(BusinessTaxActivity businessTaxActivity, int LsoId) throws Exception {

		logger.info("saveBusinessTaxActivity(" + businessTaxActivity + ", " + LsoId + ")");

		int projNum = -1;
		int sprojNum = -1;
		int activityId = -1;
		int activityNum = -1;
		int subprojectId = 0;
		String activityNumber = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yy");

		try {
			//Wrapper db = new Wrapper();
			String sql = "";
			String projectSql = "";
			String subProjectSql = "";
			String activitySql = "";
			String lkupPtypeSql = "";
			int businessAccNo = 0;
			int pTypeId = 0;
			int projectId = 0;
			int addressId = 0;
			String peopleSql = "";
			int peopleId = 0;
			
			
			addressId=commonService.getAddressIdForPsaId(LsoId+"");
			String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
			projectId = commonRepository.checkProjectName(projName, LsoId);
			logger.debug("Project Id Returned is  " + projectId);

			if (businessTaxActivity != null) {
				if (projectId == 0) {
					/**
					 * Project Number Generation
					 */
					//projNum = db.getNextId("PR_NUM");
					NextId nextId  = nextIdRepository.getNextId("PR_NUM");
					 nextId.setIdValue((nextId.getIdValue()) + 1);
					 nextId.setIdName("PR_NUM");
					 nextIdRepository.save(nextId);
					 projNum = nextId.getIdValue();

					String projectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);

					/**
					 * Inserting records into PROJECT Table
					 */
					//projectId = db.getNextId("PROJECT_ID");
					 nextId  = nextIdRepository.getNextId("PROJECT_ID");
					 nextId.setIdValue((nextId.getIdValue()) + 1);
					 nextId.setIdName("PROJECT_ID");
					 nextIdRepository.save(nextId);
					 projectId = nextId.getIdValue();
					 
					logger.debug("The ID generated for new Project is " + projectId);
					projectSql = "insert into project(PROJ_ID, LSO_ID, PROJECT_NBR, NAME, DESCRIPTION, STATUS_ID, DEPT_ID, CREATED_BY, CREATED_DT) values(";
					projectSql = projectSql + projectId;
					logger.debug("got Project id " + projectId);
					projectSql = projectSql + ",";
					projectSql = projectSql + LsoId;
					logger.debug("got LSO id " + LsoId);
					projectSql = projectSql + ",";

					projectSql = projectSql + StringUtils.checkString(projectNumber);
					logger.debug("got Project Number " + StringUtils.checkString(projectNumber));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Name " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Description " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + 1;// STATUS OF ACTIVE
					logger.debug("got Status Id " + "1");
					projectSql = projectSql + ",";
					projectSql = projectSql + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES;
					logger.debug("got Department ID " + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES);
					projectSql = projectSql + ",";
					projectSql = projectSql + businessTaxActivity.getCreatedBy();
					logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
					projectSql = projectSql + ", current_timestamp)";
					logger.info(projectSql);
					jdbcTemplate.update(projectSql);
				}

				/**
				 * Inserting records into LKUP_PTYPE Table
				 */
				String subProjectName = (Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + businessTaxActivity.getBusinessName().trim()).toUpperCase();
				logger.debug("Generated subProjectName " + subProjectName);
				pTypeId = commonRepository.checklkupSubProjectName(subProjectName.trim());
				logger.debug("pTypeId Returned is  " + pTypeId);

				if (pTypeId == 0) {
					//pTypeId = db.getNextId("PTYPE_ID");
					 NextId nextId  = nextIdRepository.getNextId("PTYPE_ID");
					 nextId.setIdValue((nextId.getIdValue()) + 1);
					 nextId.setIdName("PTYPE_ID");
					 nextIdRepository.save(nextId);
					 pTypeId = nextId.getIdValue();
					 
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
					lkupPtypeSql = lkupPtypeSql + pTypeId;
					logger.debug("got pTypeId is " + pTypeId);
					lkupPtypeSql = lkupPtypeSql + ",";
					lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim()).toUpperCase());
					logger.debug("got Description " + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim()).toUpperCase()));
					lkupPtypeSql = lkupPtypeSql + ",'";
					lkupPtypeSql = lkupPtypeSql + "LC";
					logger.debug("got Department Code " + "LC");
					lkupPtypeSql = lkupPtypeSql + "',";
					lkupPtypeSql = lkupPtypeSql + "null";
					logger.debug("got Name " + "null");
					lkupPtypeSql = lkupPtypeSql + ")";

					logger.info(lkupPtypeSql);
					jdbcTemplate.update(lkupPtypeSql);
				}

				// subprojectId = checkSubProjectName(pTypeId, projectId);
				subprojectId = 0;
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				if (subprojectId == 0) {

					//sprojNum = db.getNextId("SP_NUM");
					NextId nextId  = nextIdRepository.getNextId("SP_NUM");
					nextId.setIdValue((nextId.getIdValue()) + 1);
					nextId.setIdName("SP_NUM");
					nextIdRepository.save(nextId);
					sprojNum = nextId.getIdValue();
					/**
					 * Sub Project Number Generation
					 */
					String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);
					/**
					 * Inserting records into SUB_PROJECT Table
					 */
					nextId  = nextIdRepository.getNextId("SUB_PROJECT_ID");
					nextId.setIdValue((nextId.getIdValue()) + 1);
					nextId.setIdName("SUB_PROJECT_ID");
					nextIdRepository.save(nextId);
					subprojectId = nextId.getIdValue();
					//subprojectId = db.getNextId("SUB_PROJECT_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
					subProjectSql = subProjectSql + subprojectId;
					logger.debug("got Subproject Id " + subprojectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + projectId;
					logger.debug("got Project id " + projectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + pTypeId;
					logger.debug("got Sub Project Type " + pTypeId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + businessTaxActivity.getCreatedBy();
					logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
					subProjectSql = subProjectSql + ", current_timestamp,";
					logger.debug("got Created ");
					subProjectSql = subProjectSql + 1;
					logger.debug("got Status " + 1);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
					logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
					subProjectSql = subProjectSql + ")";

					logger.info(subProjectSql);
					jdbcTemplate.update(subProjectSql);
				}

				// Checking activity existing or not
				String actBtType = businessTaxActivity.getActivityType().getType();
				activityId = commonRepository.checkActivityName(subprojectId, actBtType);
				if (activityId != 0) {

					String inactivateSql = "update activity set status =" + Constants.ACTIVITY_STATUS_BT_OUT_OF_BUSINESS + " where act_id = " + activityId;
					logger.info(inactivateSql);
					jdbcTemplate.update(inactivateSql);
				}

				// Activity Id Generation
				//activityId = db.getNextId("ACTIVITY_ID");
				NextId nextId  = nextIdRepository.getNextId("ACTIVITY_ID");
				nextId.setIdValue((nextId.getIdValue()) + 1);
				nextId.setIdName("ACTIVITY_ID");
				nextIdRepository.save(nextId);
				activityId = nextId.getIdValue();
				
				logger.debug("The ID generated for new Activity is " + activityId);

				// Activity Number generation
				//activityNum = db.getNextId("BT_NUM");
				nextId  = nextIdRepository.getNextId("BT_NUM");
				nextId.setIdValue((nextId.getIdValue()) + 1);
				nextId.setIdName("BT_NUM");
				nextIdRepository.save(nextId);
				activityNum = nextId.getIdValue();
				
				logger.debug("Obtained new activity number is " + activityNum);

				String strActNum = StringUtils.i2s(activityNum);
				strActNum = "00000".substring(strActNum.length()) + strActNum;
				logger.debug("Obtained new activity number string is " + strActNum);
				calendar = Calendar.getInstance();
				formatter = new SimpleDateFormat("yy");
				activityNumber = "BT" + formatter.format(calendar.getTime()) + strActNum.trim();

				/**
				 * Inserting records into ACTIVITY Table
				 * 
				 */
				activitySql = "insert into activity(ACT_ID, SPROJ_ID, ADDR_ID, ACT_NBR, PLAN_CHK_REQ, ACT_TYPE,  VALUATION, STATUS, START_DATE, APPLIED_DATE, ISSUED_DATE, PLAN_CHK_FEE_DATE, DEVELOPMENT_FEE_DATE, PERMIT_FEE_DATE, DEV_FEE_REQ, CREATED_BY, CREATED,APPLICATION_ONLINE) values(";

				activitySql = activitySql + activityId;
				logger.debug("got Activity id " + activityId);
				activitySql = activitySql + ",";
				activitySql = activitySql + subprojectId;
				logger.debug("got Sub Project Id " + subprojectId);
				activitySql = activitySql + ",";
				activitySql = activitySql + addressId;
				logger.debug("got Address Id " + addressId);
				activitySql = activitySql + ",";
				activitySql = activitySql + StringUtils.checkString(activityNumber);
				logger.debug("got Activity Number " + activityNumber);
				activitySql = activitySql + ",'N','";
				activitySql = activitySql + ((businessTaxActivity.getActivityType() != null) ? businessTaxActivity.getActivityType().getType() : "");
				logger.debug("got Activity Type " + ((businessTaxActivity.getActivityType() != null) ? businessTaxActivity.getActivityType().getType() : ""));
				activitySql = activitySql + "',";
				activitySql = activitySql + "0";
				logger.debug("got Valuation " + "0");
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null");
				logger.debug("got Activity Status " + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null"));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())) : null);
				logger.debug("got Start Date " + ((businessTaxActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())) : null);
				logger.debug("got Applied Date " + ((businessTaxActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null);
				logger.debug("got Issued Date " + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",'N',";
				activitySql = activitySql + businessTaxActivity.getCreatedBy();
				logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_timestamp";
				logger.debug("got Created " + "current_timestamp");
				activitySql = activitySql + ",";
				activitySql = activitySql + "'Y'";
				activitySql = activitySql + ")";
				logger.info(activitySql);
				jdbcTemplate.update(activitySql);

				/**
				 * Inserting records into BT_ACTIVITY Table
				 */
				//businessAccNo = db.getNextId("BUSINESS_ACC_NO");
				nextId  = nextIdRepository.getNextId("BUSINESS_ACC_NO");
				nextId.setIdValue((nextId.getIdValue()) + 1);
				nextId.setIdName("BUSINESS_ACC_NO");
				nextIdRepository.save(nextId);
				businessAccNo = nextId.getIdValue();
				logger.debug("Business Account No generated is " + businessAccNo);

				sql = "INSERT INTO BT_ACTIVITY(ACT_ID, BURBANK_BUSINESS, APPL_TYPE_ID, OOT_STR_NO, OOT_STR_NAME, OOT_UNIT, OOT_CITY, OOT_STATE,  OOT_ZIP, OOT_ZIP4, BUSINESS_NAME, BUSINESS_ACC_NO, CORPORATE_NAME, BUSINESS_PHONE, BUSINESS_PHONE_EXT, BUSINESS_FAX, SIC_CODE, MUNICIPAL_CODE, DESC_OF_BUSINESS, CLASS_CODE, HOME_OCCUPATION, DECAL_CODE, OTHER_BUSINESS_OCCUPANCY, OOB_DT, OWN_TYPE_ID, FEDEREAL_ID, EMAIL, SSN, MAIL_STR_NO, MAIL_STR_NAME, MAIL_UNIT, MAIL_CITY, MAIL_STATE, MAIL_ZIP, MAIL_ZIP4, PRE_STR_NO, PRE_STR_NAME, PRE_UNIT, PRE_CITY, PRE_STATE, PRE_ZIP, PRE_ZIP4, NAME1, TITLE1, OWNER1_STR_NO, OWNER1_STR_NAME, OWNER1_UNIT, OWNER1_CITY, OWNER1_STATE, OWNER1_ZIP, OWNER1_ZIP4, NAME2, TITLE2, OWNER2_STR_NO, OWNER2_STR_NAME, OWNER2_UNIT, OWNER2_CITY, OWNER2_STATE, OWNER2_ZIP, OWNER2_ZIP4, SQ_FOOTAGE, QTY_ID, QTY_OTHER, DRIVER_LICENSE, CREATED, CREATED_BY, ATTN) values(";
				sql = sql + activityId;
				logger.debug("got Activity id " + activityId);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isBusinessLocation()));
				logger.debug("got Business Location " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isBusinessLocation())));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getApplicationType() != null) ? StringUtils.i2s(businessTaxActivity.getApplicationType().getId()) : "null");
				logger.debug("got Application Type ID" + ((businessTaxActivity.getApplicationType() != null) ? StringUtils.i2s(businessTaxActivity.getApplicationType().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber());
				logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName());
				logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber());
				logger.debug("got out Of Town Unit Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownCity());
				logger.debug("got Out Of Town City " + StringUtils.checkString(businessTaxActivity.getOutOfTownCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownState());
				logger.debug("got Out Of Town State " + StringUtils.checkString(businessTaxActivity.getOutOfTownState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip());
				logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4());
				logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase());
				logger.debug("got Business Name " + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase()));
				sql = sql + ",";
				sql = sql + businessAccNo;
				logger.debug("got Business Account Number " + businessAccNo);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getCorporateName());
				logger.debug("got Corporate Name " + StringUtils.checkString(businessTaxActivity.getCorporateName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessPhone());
				logger.debug("got Business Phone " + StringUtils.checkString(StringUtils.phoneFormat(businessTaxActivity.getBusinessPhone())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessExtension());
				logger.debug("got Business Extension " + StringUtils.checkString(businessTaxActivity.getBusinessExtension()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessFax());
				logger.debug("got Business Fax " + StringUtils.checkString(businessTaxActivity.getBusinessFax()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSicCode());
				logger.debug("got Sic Code " + StringUtils.checkString(businessTaxActivity.getSicCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMuncipalCode());
				logger.debug("got Muncipal Code " + StringUtils.checkString(businessTaxActivity.getMuncipalCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDescOfBusiness());
				logger.debug("got Description Of Business " + StringUtils.checkString(businessTaxActivity.getDescOfBusiness()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getClassCode());
				logger.debug("got Class Code " + StringUtils.checkString(businessTaxActivity.getClassCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation()));
				logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode()));
				logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy()));
				logger.debug("got Other Business Occupancy " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy())));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate()));
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate())));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "null");
				logger.debug("got Ownership Type " + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "-1"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getFederalIdNumber());
				logger.debug("got Federal Id Number " + StringUtils.checkString(businessTaxActivity.getFederalIdNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getEmailAddress());
				logger.debug("got Email " + StringUtils.checkString(businessTaxActivity.getEmailAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber());
				logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber()));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetNumber());
				logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessTaxActivity.getMailStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetName());
				logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessTaxActivity.getMailStreetName())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailUnitNumber()));
				logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessTaxActivity.getMailUnitNumber())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailCity()));
				logger.debug("got Mail City 2: " + StringUtils.checkString((businessTaxActivity.getMailCity())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailState()));
				logger.debug("got Mail State 2: " + StringUtils.checkString((businessTaxActivity.getMailState())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip()));
				logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessTaxActivity.getMailZip())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip4()));
				logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessTaxActivity.getMailZip4())));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber());
				logger.debug("got Previous Street Number " + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetName());
				logger.debug("got Previous Street Name " + StringUtils.checkString(businessTaxActivity.getPrevStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber());
				logger.debug("got Previous Unit Number " + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevCity());
				logger.debug("got Previous City " + StringUtils.checkString(businessTaxActivity.getPrevCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevState());
				logger.debug("got Previous State " + StringUtils.checkString(businessTaxActivity.getPrevState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip());
				logger.debug("got Previous Zip " + StringUtils.checkString(businessTaxActivity.getPrevZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip4());
				logger.debug("got Previous Zip4 " + StringUtils.checkString(businessTaxActivity.getPrevZip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName1());
				logger.debug("got Name 1 " + StringUtils.checkString(businessTaxActivity.getName1()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle1());
				logger.debug("got Title 1 " + StringUtils.checkString(businessTaxActivity.getTitle1()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber());
				logger.debug("got Owner1 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetName());
				logger.debug("got Owner1 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner1StreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber());
				logger.debug("got Owner1 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1City());
				logger.debug("got Owner1 City " + StringUtils.checkString(businessTaxActivity.getOwner1City()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1State());
				logger.debug("got Owner1 State " + StringUtils.checkString(businessTaxActivity.getOwner1State()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip());
				logger.debug("got Owner1 Zip " + StringUtils.checkString(businessTaxActivity.getOwner1Zip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip4());
				logger.debug("got Owner1 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner1Zip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName2());
				logger.debug("got Name 2 " + StringUtils.checkString(businessTaxActivity.getName2()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle2());
				logger.debug("got Title 2 " + StringUtils.checkString(businessTaxActivity.getTitle2()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber());
				logger.debug("got Owner2 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetName());
				logger.debug("got Owner2 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner2StreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber());
				logger.debug("got Owner2 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2City());
				logger.debug("got Owner2 City " + StringUtils.checkString(businessTaxActivity.getOwner2City()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2State());
				logger.debug("got Owner2 State " + StringUtils.checkString(businessTaxActivity.getOwner2State()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip());
				logger.debug("got Owner2 Zip " + StringUtils.checkString(businessTaxActivity.getOwner2Zip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip4());
				logger.debug("got Owner2 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner2Zip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSquareFootage());
				logger.debug("got Square Footage " + StringUtils.checkString(businessTaxActivity.getSquareFootage()));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null");
				logger.debug("got Quantity " + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getQuantityNum());
				logger.debug("got Quantity Number " + StringUtils.checkString(businessTaxActivity.getQuantityNum()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDriverLicense());
				logger.debug("got Driver's License Number " + StringUtils.checkString(businessTaxActivity.getDriverLicense()));
				sql = sql + ", current_timestamp,";
				sql = sql + businessTaxActivity.getCreatedBy();
				logger.debug("got created by " + businessTaxActivity.getCreatedBy());
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getAttn());
				logger.debug("got ATTN " + StringUtils.checkString(businessTaxActivity.getAttn()));
				sql = sql + ")";

				logger.info(sql);
				jdbcTemplate.update(sql);


				/**
				 * Inserting records into PEOPLE Table
				 * 
				 */
				
				 nextId  = nextIdRepository.getNextId("PEOPLE_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("PEOPLE_ID");
				 nextIdRepository.save(nextId);
				 peopleId = nextId.getIdValue();
				
				peopleSql = "INSERT INTO PEOPLE (PEOPLE_ID, PEOPLE_TYPE_ID, NAME, PHONE, AGENT_NAME, AGENT_PH, ADDR, CITY, STATE, ZIP, EMAIL_ADDR, COMNTS, CREATED_BY, CREATED, UNIT) "
						+ "VALUES ("+peopleId+","+Constants.PEOPLE_TYPE_BUSINESS_OWNER+", '"+businessTaxActivity.getBusinessName()+"','"+businessTaxActivity.getBusinessPhone()+"','"+businessTaxActivity.getBusinessName()+"', '"+businessTaxActivity.getBusinessPhone()+"', '"+businessTaxActivity.getAddress()+"', "
						+ "'"+businessTaxActivity.getBusinessAddressCity()+"', '"+StringUtils.nullReplaceWithEmpty(businessTaxActivity.getBusinessAddressState())+"', '"+businessTaxActivity.getBusinessAddressZip()+"',"
						+ "'"+businessTaxActivity.getEmailAddress()+"', 'Created through BT online application', '',current_date, '"+businessTaxActivity.getBusinessAddressUnitNumber()+"')";
				logger.debug("peopleSql... "+peopleSql);
				jdbcTemplate.update(peopleSql);

				/**
				 * Inserting record into ACTIVITY PEOPLE Table
				 * 
				 */
				
				String actPeopleSql = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID, PSA_TYPE) VALUES ("+activityId+","+peopleId+",'A')";
				logger.debug("actPeopleSql... "+actPeopleSql);
				jdbcTemplate.update(actPeopleSql);
			}
			return activityId;
		} catch (Exception e) {
			logger.error(e.getMessage()+e);
			throw e;
		}
	
	}
	

	public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId){
		
		String actId="";
		
		try {		
			if(multiAddessList!=null){
			for(int j=0; j< multiAddessList.length; j++){
				MultiAddress rec = (MultiAddress) multiAddessList[j];
				int id = checkMultiAddress(rec.getId() , activityId);
				if(id==0){
					String insertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID,NAME,TITLE, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
							+ "VALUES (SEQUENCE_MULTI_ADDRESS_ID.NEXTVAL, '"+activityId+"', '"+getAddressTypeId(rec.getAddressType())+"', '"+rec.getName()+"', '"+rec.getTitle()+"', '"+rec.getStreetNumber()+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"', "
									+ "'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',  '"+rec.getAttn()+"', '"+rec.getUnit()+"', '"+rec.getCity()+"',"
											+ " '"+rec.getState()+"',  '"+rec.getZip()+"', '"+rec.getZip4()+"')";
					logger.debug("addressType InsertQuery  :"+insertQuery);
					jdbcTemplate.update(insertQuery);
				}else{
					String	updateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+rec.getStreetNumber()+"',STREET_NAME1= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"',STREET_NAME2= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',"
							+ "ATTN='"+rec.getAttn()+"',UNIT='"+rec.getUnit()+"',CITY='"+rec.getCity()+"',STATE='"+rec.getState()+"',"
							+ "ZIP= '"+rec.getZip()+"',ZIP4='"+rec.getZip4()+"',NAME='"+rec.getName()+"',TITLE='"+rec.getTitle()+"' "
							+ "WHERE ADDRESS_TYPE_ID="+getAddressTypeId(rec.getAddressType())+" AND ACT_ID="+activityId+" ";
					logger.debug("updateQuery :"+updateQuery);
					jdbcTemplate.update(updateQuery);	
				}
			}
			}		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public int checkMultiAddress(String addressTypeId,int actId){
		//Wrapper db = new Wrapper();
		//ResultSet rs = null;
		int id = 0;
		String sql = "select * from MULTI_ADDRESS where ADDRESS_TYPE_ID='"+addressTypeId+"' AND ACT_ID="+actId+" ";
		
		logger.debug("checkMultiAddress : "+sql);

		try {
			id = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
			    @Override
			    public Integer mapRow(ResultSet rs, int i) throws SQLException {
			    	int id = 0;
			    	//while (rs.next()) {
			    	id = rs.getInt("ID");
			    	//}
					return id;
			    }
			});	
		
		} catch (EmptyResultDataAccessException e) {
			logger.error("Unable to get id "+e);  
			return 0;
		}
		return id;
	}

	
	/**
	 * This method is to fetch the address id for address type. 
	 * This is to maintain for Multi_Address table
	 * @param addressType
	 * @return
	 */
	public String getAddressTypeId(String addressType) {
		String sql = "select * from LKUP_ADDRESS_TYPE where ADDRESS_TYPE='"+addressType+"'";
		logger.debug("getAddressTypeId sql :"+sql);
		String addressTypeId = "";
		try {
			addressTypeId=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				
			    @Override
			    public String mapRow(ResultSet rs, int i) throws SQLException {
					String addressTypeId=rs.getString("ID");
					return addressTypeId;	
			    }
			});
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			logger.error("",e);
		}	
		return addressTypeId;
	}
	
	/**
	 * Get Business Tax activity
	 * 
	 * @param activityId
	 *            , lsoId
	 * @return BusinessTaxActivity Object
	 * @throws Exception
	 * @author Gayathri
	 * 
	 */
	public BusinessTaxActivity getBusinessTaxActivity(int activityId, int lsoId) throws Exception {
		logger.info("getBusinessTaxActivity(" + activityId + ", " + lsoId + ")");

		BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
		logger.info("lso id is " + lsoId);

		try {
			//Wrapper db = new Wrapper();
			String sql = "";
			sql = "SELECT * FROM  ACTIVITY A, BT_ACTIVITY BA, V_ADDRESS_LIST VAL WHERE  A.ACT_ID = BA.ACT_ID  AND  VAL.LSO_ID=" + lsoId + " AND LSO_TYPE='O'  AND  A.ACT_ID = " + activityId;
			logger.info(sql);

			//RowSet rs = db.select(sql);			
		
		//	businessTaxActivity = getMultiActivityAddress(activityId);
			businessTaxActivity = jdbcTemplate.query(sql, new ResultSetExtractor<BusinessTaxActivity>() {                
		        @Override
		        public BusinessTaxActivity extractData(ResultSet rs) throws SQLException, DataAccessException {     
		        	BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
					
					while (rs.next()) {
						businessTaxActivity.setActivityNumber(rs.getString("ACT_NBR"));
						businessTaxActivity.setActivityStatus(commonService.getActivityStatus(rs.getInt("STATUS")));
						businessTaxActivity.setApplicationType(commonService.getApplicationType(rs.getInt("APPL_TYPE_ID")));
						businessTaxActivity.setActivityType(commonService.getActivityType(rs.getString("ACT_TYPE")));
						businessTaxActivity.setBusinessAddressStreetNumber(StringUtils.i2s(rs.getInt("STR_NO")));
						businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
						businessTaxActivity.setBusinessAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
						businessTaxActivity.setBusinessAddressUnitNumber(rs.getString("UNIT"));
						businessTaxActivity.setBusinessAddressCity(rs.getString("CITY"));
						businessTaxActivity.setBusinessAddressState(rs.getString("STATE"));
						businessTaxActivity.setBusinessAddressZip(rs.getString("ZIP"));
						businessTaxActivity.setBusinessAddressZip4(rs.getString("ZIP4"));
						businessTaxActivity.setBusinessLocation(StringUtils.s2b(rs.getString("BURBANK_BUSINESS")));
						businessTaxActivity.setOutOfTownStreetNumber(rs.getString("OOT_STR_NO"));
						if (rs.getString("OOT_STR_NO") == null) {
							businessTaxActivity.setOutOfTownStreetNumber("");
						}
						businessTaxActivity.setOutOfTownStreetName(rs.getString("OOT_STR_NAME"));
						businessTaxActivity.setOutOfTownUnitNumber(rs.getString("OOT_UNIT"));
						businessTaxActivity.setOutOfTownCity(rs.getString("OOT_CITY"));
						businessTaxActivity.setOutOfTownState(rs.getString("OOT_STATE"));
						businessTaxActivity.setOutOfTownZip(rs.getString("OOT_ZIP"));
						businessTaxActivity.setOutOfTownZip4(rs.getString("OOT_ZIP4"));
						businessTaxActivity.setBusinessName(rs.getString("BUSINESS_NAME").trim().toUpperCase());
						businessTaxActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));
						businessTaxActivity.setCorporateName(rs.getString("CORPORATE_NAME"));
						businessTaxActivity.setAttn(rs.getString("ATTN"));
						businessTaxActivity.setBusinessPhone(rs.getString("BUSINESS_PHONE"));
						businessTaxActivity.setBusinessExtension(rs.getString("BUSINESS_PHONE_EXT"));
						businessTaxActivity.setBusinessFax(rs.getString("BUSINESS_FAX"));
						businessTaxActivity.setClassCode(rs.getString("CLASS_CODE"));
						businessTaxActivity.setMuncipalCode(rs.getString("MUNICIPAL_CODE"));
						businessTaxActivity.setSicCode(rs.getString("SIC_CODE"));
						businessTaxActivity.setDecalCode(StringUtils.s2b(rs.getString("DECAL_CODE")));
						businessTaxActivity.setOtherBusinessOccupancy(StringUtils.s2b(rs.getString("OTHER_BUSINESS_OCCUPANCY")));
						businessTaxActivity.setDescOfBusiness(rs.getString("DESC_OF_BUSINESS"));
						businessTaxActivity.setHomeOccupation(StringUtils.s2b(rs.getString("HOME_OCCUPATION")));
						businessTaxActivity.setCreationDate(StringUtils.dbDate2cal(rs.getString("CREATED")));
						businessTaxActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("APPLIED_DATE")));
						businessTaxActivity.setIssueDate(StringUtils.dbDate2cal(rs.getString("ISSUED_DATE")));
						businessTaxActivity.setStartingDate(StringUtils.dbDate2cal(rs.getString("START_DATE")));
						businessTaxActivity.setOwnershipType(commonService.getOwnershipType(rs.getInt("OWN_TYPE_ID")));
						businessTaxActivity.setOutOfBusinessDate(StringUtils.dbDate2cal(rs.getString("OOB_DT")));
						businessTaxActivity.setFederalIdNumber(rs.getString("FEDEREAL_ID"));
						businessTaxActivity.setEmailAddress(rs.getString("EMAIL"));
						businessTaxActivity.setSocialSecurityNumber(rs.getString("SSN"));
						businessTaxActivity.setSquareFootage(rs.getString("SQ_FOOTAGE"));
						businessTaxActivity.setQuantity(commonService.getBTQuantityType(rs.getInt("QTY_ID")));
						businessTaxActivity.setQuantityNum(rs.getString("QTY_OTHER"));
						businessTaxActivity.setDriverLicense(rs.getString("DRIVER_LICENSE"));
						businessTaxActivity.setCreatedBy(rs.getInt("CREATED_BY"));
						
						logger.debug("Values setted to BusinessTax object");
					}
					return businessTaxActivity;
		        }
			});

			
			businessTaxActivity.setMultiAddress(commonService.getMultiAddress(activityId, "BT"));

			
			return businessTaxActivity;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Gets the BusinessLicenseActivity
	 * 
	 * @return Returns a BusinessLicenseActivity
	 */
	public BusinessTaxActivity getBusinessTaxActivity(BusinessTaxActivityForm businessTaxActivityForm) throws Exception {

		//businessTaxActivity = new BusinessTaxActivity();

		businessTaxActivity.setApplicationType(commonRepository.getApplicationType(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getApplicationType()))));
		businessTaxActivity.setActivityStatus(commonRepository.getActivityStatus(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getActivityStatus()))));
		businessTaxActivity.setOwnershipType(commonRepository.getOwnershipType(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwnershipType()))));
		businessTaxActivity.setActivityType(commonRepository.getActivityType(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getActivityType())));
		businessTaxActivity.setActivitySubType(commonRepository.getActivitySubType(businessTaxActivityForm.getActivitySubType()));
		businessTaxActivity.setQuantity(commonRepository.getBtQuantityType(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getQuantity()))));
		if(businessTaxActivityForm.getBusinessAddressStreetName() != "" && businessTaxActivityForm.getBusinessAddressStreetName() != null) {
		businessTaxActivity.setStreet(commonRepository.getStreet(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressStreetName())));
		}
		businessTaxActivity.setSubProjectId(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getSubProjectId())));
		businessTaxActivity.setBusinessLocation(businessTaxActivityForm.isBusinessLocation());
		businessTaxActivity.setBusinessAddressStreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressStreetNumber()));
		businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressStreetFraction()));
		businessTaxActivity.setBusinessAddressUnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressUnitNumber()));
		businessTaxActivity.setBusinessAddressCity(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressCity()));
		businessTaxActivity.setBusinessAddressState(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressState()));
		businessTaxActivity.setBusinessAddressZip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressZip()));
		businessTaxActivity.setBusinessAddressZip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAddressZip4()));
		businessTaxActivity.setOutOfTownStreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetNumber()));
		businessTaxActivity.setOutOfTownStreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetName()));
		businessTaxActivity.setOutOfTownUnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownUnitNumber()));
		businessTaxActivity.setOutOfTownCity(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownCity()));
		businessTaxActivity.setOutOfTownState(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownState()));
		businessTaxActivity.setOutOfTownZip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownZip()));
		businessTaxActivity.setOutOfTownZip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownZip4()));
		businessTaxActivity.setActivityNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getActivityNumber()));
		businessTaxActivity.setBusinessName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessName()));
		businessTaxActivity.setBusinessAccountNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessAccountNumber()));
		businessTaxActivity.setCorporateName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getCorporateName()));
		businessTaxActivity.setAttn(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getAttn()));
		businessTaxActivity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessPhone()));
		businessTaxActivity.setBusinessExtension(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessExtension()));
		businessTaxActivity.setBusinessFax(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getBusinessFax()));
		businessTaxActivity.setMuncipalCode(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMuncipalCode()));
		businessTaxActivity.setSicCode(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getSicCode()));
		businessTaxActivity.setDescOfBusiness(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getDescOfBusiness()));
		businessTaxActivity.setClassCode(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getClassCode()));
		businessTaxActivity.setHomeOccupation(businessTaxActivityForm.isHomeOccupation());
		businessTaxActivity.setDecalCode(businessTaxActivityForm.isDecalCode());
		businessTaxActivity.setOtherBusinessOccupancy(businessTaxActivityForm.isOtherBusinessOccupancy());
		businessTaxActivity.setFederalIdNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getFederalIdNumber()));
		businessTaxActivity.setEmailAddress(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getEmailAddress()));
		businessTaxActivity.setSocialSecurityNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getSocialSecurityNumber()));
		businessTaxActivity.setMailStreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailStreetNumber()));
		businessTaxActivity.setMailStreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailStreetName()));
		businessTaxActivity.setMailUnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailUnitNumber()));
		businessTaxActivity.setMailCity(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailCity()));
		businessTaxActivity.setMailState(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailState()));
		businessTaxActivity.setMailZip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailZip()));
		businessTaxActivity.setMailZip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailZip4()));
		businessTaxActivity.setPrevStreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevStreetNumber()));
		businessTaxActivity.setPrevStreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevStreetName()));
		businessTaxActivity.setPrevUnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevUnitNumber()));
		businessTaxActivity.setPrevCity(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevCity()));
		businessTaxActivity.setPrevState(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevState()));
		businessTaxActivity.setPrevZip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevZip()));
		businessTaxActivity.setPrevZip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPrevZip4()));
		businessTaxActivity.setName1(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getName1()));
		businessTaxActivity.setTitle1(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getTitle1()));
		businessTaxActivity.setOwner1StreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1StreetNumber()));
		businessTaxActivity.setOwner1StreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1StreetName()));
		businessTaxActivity.setOwner1UnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1UnitNumber()));
		businessTaxActivity.setOwner1City(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1City()));
		businessTaxActivity.setOwner1State(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1State()));
		businessTaxActivity.setOwner1Zip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1Zip()));
		businessTaxActivity.setOwner1Zip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1Zip4()));
		businessTaxActivity.setName2(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getName2()));
		businessTaxActivity.setTitle2(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getTitle2()));
		businessTaxActivity.setOwner2StreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2StreetNumber()));
		businessTaxActivity.setOwner2StreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2StreetName()));
		businessTaxActivity.setOwner2UnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2UnitNumber()));
		businessTaxActivity.setOwner2City(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2City()));
		businessTaxActivity.setOwner2State(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2State()));
		businessTaxActivity.setOwner2Zip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2Zip()));
		businessTaxActivity.setOwner2Zip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2Zip4()));
		businessTaxActivity.setSquareFootage(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getSquareFootage()));
		businessTaxActivity.setQuantityNum(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getQuantityNum()));
		businessTaxActivity.setDriverLicense(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getDriverLicense()));
		businessTaxActivity.setDisplayContent(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getDisplayContent()));
		businessTaxActivity.setCreationDate(StringUtils.str2cal(businessTaxActivityForm.getCreationDate()));
		businessTaxActivity.setApplicationDate(StringUtils.str2cal(businessTaxActivityForm.getApplicationDate()));
		businessTaxActivity.setIssueDate(StringUtils.str2cal(businessTaxActivityForm.getIssueDate()));
		businessTaxActivity.setStartingDate(StringUtils.str2cal(businessTaxActivityForm.getStartingDate()));
		businessTaxActivity.setOutOfBusinessDate(StringUtils.str2cal(businessTaxActivityForm.getOutOfBusinessDate()));
		businessTaxActivity.setMailAttn(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMailAttn()));
		businessTaxActivity.setPreAttn(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getPreAttn()));
		businessTaxActivity.setOwner1Attn(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner1Attn()));
		businessTaxActivity.setOwner2Attn(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOwner2Attn()));
		businessTaxActivity.setMultiAddress(businessTaxActivityForm.getMultiAddress());
		logger.debug("In getbusinessTaxActivity " + businessTaxActivity);

		return businessTaxActivity;
	}


//This method will calculate the fees based on the activity type and formulas 
public List<Fee> calculateFees(List<Fee> feeList, String screenName) {
	logger.debug("screenName :: " +screenName);

	double feeAmount = 0.00;
	int levelId = 0;			
	
	for(int i=0;i<feeList.size();i++){
		Fee fee = feeList.get(i);
		switch (fee.getFeeCalc1().charAt(0)) {
		case 'A': // This is for fees that are based on number of units
			logger.debug("fee_calc_1=A,This is for fees that are based on number of units");
			logger.debug("Fee getFeeUnits "+fee.getFeeUnits());
			logger.debug(fee.getFeeUnits() <1 );
			if (fee.getFeeFactor() > 0.00) {
				logger.debug("Fee factor is > 0.00, calculating fee amount");
				if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE)) {
					
					if(fee.getFeeUnits() >3000){
						fee.setFeeUnits(3000);
					}	
				}
				
				logger.debug("Fee getFeeUnits "+fee.getFeeUnits()+" fee btqty id "+fee.getBtQtyGroupId());
				if(fee.getPaidAmnt() > 0 && fee.getBalDue() >0) {
					feeAmount = fee.getBalDue();
				}else if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE) && fee.getFeeUnits() <= 0.0) {
					feeAmount = fee.getFeeFactor() * fee.getFeeUnits();
				}else {
					feeAmount = fee.getFeeFactor() * Math.max(1, fee.getFeeUnits());
				}
				logger.debug("calculated fee amount is " + feeAmount);
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			}
			break;

		case 'B': // This is for fees that are based on number of units
			logger.debug("fee_calc_1=B");
			logger.debug("volume..."+fee.getFeeUnits());
			double volume= fee.getFeeUnits();
			logger.debug("volume..."+volume);
			if(volume <= 50000) {
				feeAmount = 102.05;
				logger.debug("feeAmount...1.."+feeAmount);
			}else if (volume >= 50001 && volume <= 100000) {
				feeAmount = 204;
				logger.debug("feeAmount...2.."+feeAmount);
			}else if (volume >= 100001 && volume <= 200000) {
				feeAmount = 408.40;
				logger.debug("feeAmount...3.."+feeAmount);
			}else if (volume >= 200001 && volume <= 300000) {
				feeAmount = 608.25;
				logger.debug("feeAmount...4.."+feeAmount);
			}else if (volume >= 300001) {
				feeAmount = 1020.65;
				logger.debug("feeAmount...5.."+feeAmount);
			}
			logger.debug("feeAmount..."+feeAmount);	
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;				

		case 'C': // this is for a flat fee
			logger.debug("fee_calc_1=C,this is for a flat fee"+fee.getFeeUnits());
			feeAmount = fee.getFactor();
			if(fee.getActType().equalsIgnoreCase("CR") || fee.getActType().equalsIgnoreCase("CRS")) {
				if(fee.getFeeUnits() > 0) {
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
				    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));
				}else {
					feeList.get(i).setFeeTotal(0.00);
					feeList.get(i).setFeeTotalStr("0.00");
					feeList.get(i).setFeeDesc("");	
				}
			}else {
				logger.debug("in else ... "+fee.getFeeUnits()+" getFeeDesc ... "+fee.getFeeDesc());
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));
			}
			break;
		
		case 'D': // Max of factor and fee_factor*units
			logger.debug("fee_calc_1=D");
			feeAmount = Math.max(fee.getFactor(), fee.getFeeFactor()) * fee.getFeeUnits();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'E': // Min of factor and fee_factor*units
			logger.debug("fee_calc_1=E");
			feeAmount = Math.min(fee.getFactor(), fee.getFeeFactor()) * fee.getFeeUnits();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'F': // This is for fees that are based on number of units
			logger.debug("fee_calc_1=F");
			feeList.get(i).setFeeTotal(0.0);
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(0.0));       
			break;

		case 'G': // User Entering the Fee Amount
			logger.debug("fee_calc_1=G");
			feeAmount = fee.getFeeAmnt();
			
			if(fee.getActType().equalsIgnoreCase(Constants.VMO_ACT_TYPE)) {
				if(fee.getFeeDesc().equalsIgnoreCase(Constants.ONE_PERCENT_OF_GROSS_RECEIPTS_OVER_$5000)) {
					if(fee.getFeeUnits() > 5000) {
						double feeUnits = fee.getFeeUnits();
						feeUnits = feeUnits - 5000;
						feeAmount = feeUnits * 0.01;
						feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
						feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
					}else {
						feeList.get(i).setFeeTotal(0.00);	
						feeList.get(i).setFeeTotalStr("0.00");	
					}
				}
			}else {
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
			}
			break;

		case 'H': // this is for fee depending on valuation
			logger.debug("fee_calc_1=H, valuation * fee units");
			logger.debug("Fee units are " + fee.getFeeUnits());
			feeAmount = fee.getValuation() * fee.getFeeFactor();
			logger.debug("Calculated fee amount under formula H is " + feeAmount);
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'I': //
			logger.debug("fee_calc_1=I");
			feeAmount = fee.getValuation() * fee.getFactor();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'J':
			logger.debug("fee_calc_1=J");
			double[] subTotals = {};

			/**
			 * If feeInit > 0 then Mutilply the subtotal[feeInit] value by fee/unit Else Mutilply the subtotal[subTotalLevel] value by fee/unit
			 **/
			logger.debug("feeInit is : " + fee.getFeeInit());
			logger.debug("subTotalLevel is : " + fee.getSubtotalLevel());

			subTotals = fee.getSubTotals();
			logger.debug("sub totals are " + subTotals);

			if (fee.getFeeInit() > 0) {
				feeAmount = subTotals[fee.getFeeInit()];
			} else {
				logger.debug("in the else, subtotal level is " + fee.getSubtotalLevel());
				feeAmount = subTotals[fee.getSubtotalLevel()];
			}

			logger.debug("fee amount is :" + feeAmount);
			feeAmount *= fee.getFactor();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'R':
			logger.debug("fee_calc_1=R, Max (Factor & fee factor * valuation)");
			feeAmount = Math.max(fee.getFactor(), fee.getFeeFactor()) * fee.getValuation();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'S':
			logger.debug("fee_calc_1=S, not defined");
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
		case 'U':
			logger.debug("fee_calc_1=U");

			if (fee.getFeeInit() > 0) {
				levelId = fee.getFeeInit();
			} else {
				levelId = fee.getSubtotalLevel();
			}

			subTotals = fee.getSubTotals();
			logger.debug("LevelId : " + levelId);
			logger.debug("SubTotal : " + subTotals[levelId]);
			logger.debug("Factor : " + fee.getFactor());
			feeAmount = Math.max(subTotals[levelId], fee.getFactor());
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'V':
			logger.debug("fee_calc_1=V");

			if (fee.getFeeAmnt() > fee.getFactor()) {
				feeAmount = fee.getFactor();
			} else {
				feeAmount = fee.getFeeAmnt();
			}
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'Y':
			logger.debug("fee_calc_1=Y");

			/**
			 * Mutilply the subtotal value by Subtotallevel and compare the result to Factor Return the highest
			 */

			// add code here to select the subtotal from the arrey object based on feeInit of fee obj
			logger.debug("before setting up amount for calc Y : " + feeAmount);
			logger.debug("subtotallevel is : " + fee.getSubtotalLevel());
			subTotals = fee.getSubTotals();
			feeAmount = subTotals[fee.getSubtotalLevel()];
			logger.debug("after setting up amount for calc Y : " + feeAmount);
			logger.debug("fee factor " + fee.getFactor());

			feeAmount *= fee.getFeeFactor();

			if (feeAmount < fee.getFactor()) {
				feeAmount = fee.getFactor();
			}
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'y':
			logger.debug("fee_calc_1=y");

			/**
			 * Mutilply the subtotal value by fee/unit and compare the result to Factor Return the highest
			 */

			// add code here to select the subtotal from the arrey object based on feeInit of fee obj
			logger.debug("before setting up amount for calc y : " + feeAmount);
			logger.debug("feeInit is : " + fee.getFeeInit());
			subTotals = fee.getSubTotals();
			feeAmount = subTotals[fee.getFeeInit()];
			logger.debug("after setting up amount for calc y : " + feeAmount);
			logger.debug("fee factor " + fee.getFactor());

			feeAmount *= fee.getFeeFactor();

			if (feeAmount < fee.getFactor()) {
				feeAmount = fee.getFactor();
			}
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'a':
			logger.debug("fee_calc_1=a");

			/**
			 * Mutilply the subtotal value by units and compare the result to Factor Return the highest
			 */

			// add code here to select the subtotal from the arrey object based on feeInit of fee obj
			logger.debug("before setting up amount for calc y : " + feeAmount);
			logger.debug("feeInit is : " + fee.getFeeInit());
			subTotals = fee.getSubTotals();
			feeAmount = subTotals[fee.getFeeInit()];
			logger.debug("subtotal amount is : " + feeAmount);
			logger.debug("after setting up amount for calc y : " + feeAmount);
			feeAmount *= fee.getFeeUnits();

			if (feeAmount < fee.getFeeFactor()) {
				feeAmount = fee.getFeeFactor();
			}
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		case 'b': //
			logger.debug("fee_calc_1=b");
			feeAmount = fee.getValuation() * fee.getFeeFactor();
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;
		case 'c': // Added For BT
			logger.debug("fee_calc_1=c");
			logger.debug("getFeeUnits..."+fee.getFeeUnits()+"fee.getActType()... "+fee.getActType());
			if(fee.getActType().equalsIgnoreCase("CR") || fee.getActType().equalsIgnoreCase("CRS")) {
				if(fee.getFeeUnits() > 5000) {
					feeAmount = (((fee.getFeeUnits()-5000) /100) * fee.getFeeFactor());
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
				}else {
					feeList.get(i).setFeeTotal(0.00);
					feeList.get(i).setFeeTotalStr("0.00");
					feeList.get(i).setFeeDesc("");	
				}
			}else{
				feeAmount = (fee.getFeeFactor() * (fee.getFeeUnits() / 100)) + fee.getFactor();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
			}
			break;
		case 'd': // Added For BT
			logger.debug("fee_calc_1=d");
			feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.75;
			logger.debug("feeAmount..."+feeAmount);
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;
		case 'e': // Added For BT
			logger.debug("fee_calc_1=e");
			feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.50;
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;
		case 'f': // Added For BT
			logger.debug("fee_calc_1=f");
			feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.25;
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;
		case 'g': // Added For BT
			logger.debug("fee_calc_1=g");
			double total1 = pfTotal;
			double feeUnit = fee.getFeeFactor();
			if ((total1 * (fee.getFactor()) / 100) < feeUnit)
				feeAmount = feeUnit;
			else
				feeAmount = total1 * (fee.getFactor() / 100);

			double myDouble = feeAmount;
			double myDoubleMultiplied = myDouble * 100;
			double myDoubleRounded = Math.round(myDoubleMultiplied);
			feeAmount = myDoubleRounded / 100.0;

			logger.debug("Penalty fees  " + feeAmount);
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
		    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			break;

		default:
			break;
		}			
	}				
	
	return feeList;
}

public List<Fee> getFees(String actType,String tempId) {
	String sql ="SELECT (AF.FEE_AMNT - AF.FEE_PAID) AS FEE_BALANCE,((tad.TEMP_BT_QTY * FEE_FACTOR) - af.FEE_PAID) as total_feebalance,LQ.ID,BT.ACT_ID,F.ACT_TYPE,F.FEE_ID,AF.FEE_ID AF_FEE_ID,FEE_DESC,FEE_FACTOR,FACTOR,fee_calc_1,TEMP_BT_QTY,HOME_OCCUPATION ,tmp.type,AF.FEE_AMNT, AF.FEE_PAID" + 
			" FROM FEE f " + 
			"left join TEMP_ACTIVITY_DETAILS tad on tad.id = "+tempId+" "+ 
			"left join bt_activity bt on tad.renewal_code = bt.act_id "+
			"LEFT OUTER JOIN LKUP_ACT_TYPE LA ON f.ACT_TYPE=LA.TYPE " + 
			"LEFT OUTER JOIN REF_ACT_BT_QTY_TYPE RAB ON RAB.LKUP_ACT_TYPE_ID=LA.TYPE_ID " + 
			"LEFT OUTER JOIN LKUP_BT_QTY LQ ON LQ.ID=RAB.LKUP_BT_QTY_TYPE_ID " +
			"LEFT JOIN( " + 
			"                SELECT DISTINCT ACT_TYPE AS TYPE " + 
			"                FROM BT_ACTIVITY BA " + 
			"                JOIN ACTIVITY A ON BA.ACT_ID=A.ACT_ID  " + 
			"                WHERE BA.QTY_OTHER IS NOT NULL and ACT_TYPE not in ('APBC','BS','CBMTS','CR','CS','HMRU','LC','LSCO','RVSNU','TLS','VMO')" + 
			"         ) tmp " + 
			"ON TMP.TYPE = F.ACT_TYPE " + 
			"LEFT OUTER JOIN ACTIVITY_FEE AF ON (AF.FEE_ID=F.FEE_ID AND AF.ACTIVITY_ID=BT.ACT_ID ) "+
			"WHERE F.ACT_TYPE='"+actType+"' AND ONLINE_NEWBLBT_FLAG='Y' "
					+ "AND F.FEE_EXPIRATION_DT is null "+ 
			"AND 1= CASE WHEN TMP.TYPE = F.ACT_TYPE AND HOME_OCCUPATION = 'Y' " +
			"AND FEE_DESC  = '" +Constants.CA_STATE_DISABILITY_FEE  +"' THEN 0 ELSE 1 END ";
//			+ "AND  ((CASE WHEN FEE_DESC IN ('"+Constants.BT_ADMINISTRATION_FEE_DESC+"') THEN NULL ELSE (AF.FEE_AMNT - AF.FEE_PAID)  END )!=0 OR (CASE WHEN FEE_DESC IN ('"+Constants.BT_ADMINISTRATION_FEE_DESC+"') THEN NULL ELSE (AF.FEE_AMNT - AF.FEE_PAID)  END ) IS NULL)";
	
	logger.debug("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
			Fee fee;
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        		fee = new Fee();
	        	fee.setBtQtyGroupId(rs.getString("ID"));
	        	fee.setActType(rs.getString("ACT_TYPE"));
	        	fee.setFeeDesc(rs.getString("FEE_DESC"));
	        	fee.setFeeId(rs.getString("FEE_ID"));
	        	fee.setFeeFactor(StringUtils.s2d(rs.getString("FEE_FACTOR")));
	        	fee.setFactor(StringUtils.s2d(rs.getString("FACTOR")));
	        	fee.setFeeCalc1(rs.getString("fee_calc_1"));
				fee.setBalDue(StringUtils.s2d(rs.getString("total_feebalance")));
				fee.setPaidAmnt(StringUtils.s2d(rs.getString("FEE_PAID")));
	        	logger.debug("getBtQtyGroupId..."+fee.getBtQtyGroupId());        	
	        	if(rs.getString("TEMP_BT_QTY")!=null && StringUtils.s2i(rs.getString("TEMP_BT_QTY"))>0) {
	        		fee.setFeeUnits(StringUtils.s2d(rs.getString("TEMP_BT_QTY")));
	        	}else {
	        		if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE) ){
	            		fee.setFeeUnits(0.0);
	        		}else {
	        			fee.setFeeUnits(1.0);
	        		}
	        	}
	        	
	        	fee.setHomeOccupation(rs.getBoolean("HOME_OCCUPATION"));
        	logger.debug("REPO - Factor :: " +fee.getFactor() + " :: Fee Factor :: " +fee.getFeeFactor() +":: Fee Desc :: " +fee.getFeeDesc() );
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
}
	return feeList;
}

}