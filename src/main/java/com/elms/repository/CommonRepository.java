package com.elms.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.ActivityStatus;
import com.elms.model.ActivitySubType;
import com.elms.model.ActivityType;
import com.elms.model.ApplicationType;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.Condition;
import com.elms.model.ConditionLibraryForm;
import com.elms.model.ConditionLibraryRecord;
import com.elms.model.Department;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.model.ExemptionType;
import com.elms.model.Fee;
import com.elms.model.Group;
import com.elms.model.MultiAddress;
import com.elms.model.NextId;
import com.elms.model.OwnershipType;
import com.elms.model.ProcessTeamCollectionRecord;
import com.elms.model.ProcessTeamForm;
import com.elms.model.ProcessTeamRecord;
import com.elms.model.QuantityType;
import com.elms.model.Role;
import com.elms.model.Street;
import com.elms.model.User;
import com.elms.util.StringUtils;



@Repository
public class CommonRepository {

private static final Logger logger = Logger.getLogger(CommonRepository.class);
	
@Autowired
JdbcTemplate  jdbcTemplate;
@Autowired
NextIdRepository nextIdRepository;
@Autowired
PlatformTransactionManager transactionManager;
@Autowired
DataSource dataSource;
@Autowired
EmailTemplateAdminForm emailTemplateAdminForm;

public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
    this.jdbcTemplate = new JdbcTemplate(dataSource);
 }
 public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
 }

public String checkRenewalType(String renewalCode,String bussinessActNo)  throws BasicExceptionHandler{

	String sql = "SELECT ACT_NBR FROM ACTIVITY A LEFT JOIN BL_ACTIVITY BL ON BL.ACT_ID = A.ACT_ID LEFT JOIN BT_ACTIVITY BT ON BT.ACT_ID = A.ACT_ID WHERE A.ACT_ID = "+renewalCode+" "
			+ " AND (BT.BUSINESS_ACC_NO = "+bussinessActNo + " OR BL.BUSINESS_ACC_NO = " + bussinessActNo + ")";
	logger.debug("ActivityInfo sql :" +sql);

	String permitNumber = "";
	try {
		permitNumber = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String permitNbr;
		    	permitNbr=StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR"));
				return permitNbr;		
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
		logger.error("",e);
	}
	logger.debug("Before Return");
	return permitNumber;
}

/**
 * This method is to remove duplicate entries of same combination of Business Acc no & Renewal Code from 
 * TEMP_ACTIVITY_DETAILS Table. And also it insert a new record with all other details that user enters. 
 * having key- value pairs from List of Map objects
 * @param Activity Object
 * @throws BasicExceptionHandler
 * @return Activity Object 
 */
public Activity saveActivity(Activity activity){
	logger.debug("in save activity repository...");
	String deleteSql="";
	TransactionDefinition def = new DefaultTransactionDefinition();
	TransactionStatus status = transactionManager.getTransaction(def);
	    
	try{
		
		 String oldTempId = getOldTempActivityNumber(activity);
		 logger.debug("oldTemp Id  :: "+oldTempId);
		 
		 NextId nextId  = nextIdRepository.getNextId("TEMP_ID");
		 nextId.setIdValue((nextId.getIdValue()) + 1);
		 nextId.setIdName("TEMP_ID");
		 nextIdRepository.save(nextId);
		 int tempId = nextId.getIdValue();
		 activity.setTempId(StringUtils.i2s(tempId));
		 logger.debug("tempId  :"+tempId);
		 
		 deleteSql = "DELETE FROM TEMP_ACTIVITY_DETAILS WHERE RENEWAL_CODE = '" +activity.getRenewalCode()+ "' AND BUSINESS_ACC_NO = '"+StringUtils.nullReplaceWithEmpty(activity.getBusinessAccNo())+"'";
		 logger.debug("deleteSql  :: "+deleteSql);
		 jdbcTemplate.execute(deleteSql);
		
		logger.debug("saveActivity1"+activity.getRenewalCode());
		String insertQuery = "INSERT INTO TEMP_ACTIVITY_DETAILS (ID,RENEWAL_CODE,BUSINESS_ACC_NO) VALUES ("+tempId+",'"+activity.getRenewalCode()+"', '"+activity.getBusinessAccNo()+"')";
		
		logger.debug("insertQuery  :"+insertQuery);
		jdbcTemplate.update(insertQuery);
		
		if(oldTempId != null && oldTempId != "") {		
			String updateQuery = "UPDATE TEMP_ATTACHMENT SET TEMP_ID="+tempId+" WHERE TEMP_ID="+oldTempId ;
			logger.debug("updateQuery Temp Attachments :"+updateQuery);
			jdbcTemplate.update(updateQuery);
		}

		transactionManager.commit(status);
	}catch(Exception e){
		logger.error("",e);
		 transactionManager.rollback(status);
		 throw e;			
	}
	return activity;
}

/**
 * This method is to update the TEMP_ACTIVITY_DETAILS Table for every next button click.
 * @param activity
 * @throws BasicExceptionHandler
 * @return 
 * 
 */
public void updateUserDetails(Activity activity) throws BasicExceptionHandler{
	logger.debug("in updateUserDetails repository...");
	try{		
		String busniessName="";
		busniessName=activity.getBusinessName();
		if(busniessName != null) {
			busniessName=activity.getBusinessName().replaceAll("'", "''");	
		}

		String addr = activity.getAddress();
		if(activity.getAddress()!= null && activity.getAddress().contains("'")) {
		addr= addr.replaceAll("'", "''");
		}
		logger.debug("addr... "+addr);
		logger.debug("updateUserDetails"+activity.toString());
		String updateQuery = "UPDATE TEMP_ACTIVITY_DETAILS SET ACT_ID='"+activity.getRenewalCode()+"', ACT_NBR='"+activity.getPermitNumber()+"', EMAIL='"+activity.getEmail()+"',NAME ='"+activity.getName()+"', BUSINESS_PHONE='"+activity.getNewPhone()+"', STR_NO='"+activity.getStrNo()+"', ADDRESS='"+addr+"', UNIT='"+activity.getUnit()+"', CITY='"+activity.getCity()+"', STATE='"+activity.getState()+"', ZIP='"+activity.getZip()+"', BUSINESS_NAME='"+busniessName+"' WHERE ID="+activity.getTempId() ;
		logger.debug("updateQuery  :"+updateQuery);
		jdbcTemplate.update(updateQuery);
	
	}catch(Exception e){
		logger.error("",e);
		throw new BasicExceptionHandler(e);		
	}
}

/**
 * This method is to fetch the LKUP_SYSTEM Table in the form of single Map object 
 * having key- value pairs.
 * @param lkupSystemDataList
 * @throws Exception
 * @return Map<String,String>
 * 
 */
public Map<String, String> getLkupSystemDataMap(){        
    String sql = "SELECT * FROM LKUP_SYSTEM";
    logger.info(sql);
    Map<String, String> lkupSystemData = null;
    try {
    	lkupSystemData= jdbcTemplate.query(sql, new ResultSetExtractor<Map<String, String>>() {                
	        @Override
	        public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {    
	            Map<String, String> info = new HashMap<String, String>();
	            while(rs.next()) {
	                info.put(rs.getString("NAME"),rs.getString("VALUE"));                    
	            }            
	            return info;
	        }
    	});
    }catch(Exception e) {
    	logger.error("Error occured in getLkupSystemDataMap :: " +e.getMessage());
    	e.printStackTrace();
    }
    return lkupSystemData;       
}

/**
 * This method is to check if the Fee for BL/BT is payed or the Renewal application 
 * is submitted based on the renewal code or activity Id.
 * @param String
 * @throws Exception
 * @return String
 * 
 */
public String getBlOrBtActivityRenewalOnlineFlag(Activity activity) {
	String sql = "SELECT A.RENEWAL_ONLINE FROM ACTIVITY A LEFT JOIN BL_ACTIVITY BL ON BL.ACT_ID = A.ACT_ID LEFT JOIN BT_ACTIVITY BT ON BT.ACT_ID = A.ACT_ID WHERE A.ACT_ID = "+activity.getRenewalCode();
	
	if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)) {
		sql = sql + " AND BL.BUSINESS_ACC_NO = " +activity.getBusinessAccNo();
	}else {
		sql = sql + " AND BT.BUSINESS_ACC_NO = " +activity.getBusinessAccNo();
	}
			    
	logger.debug("getActivityRenewalOnline sql :"+sql);
	String renewalOnline = "N";
	try {
		renewalOnline=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String renewalOnline=rs.getString("RENEWAL_ONLINE");
				return renewalOnline;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		logger.error("",e1);
	}catch (Exception e) {
		logger.error("",e);
	}	
	return renewalOnline;
}

/**
 * This method is to fetch Fee list for BL/BT after payment of the Renewal application 
 * based on the renewal code or activity Id.
 * @param String, String
 * @throws Exception
 * @return List<Fee>
 * 
 */

public List<Fee> getFeesFromTempActFees(String actId,String tempId) {
	String sql ="SELECT DISTINCT F.FEE_ID,TAF.FEE_UNITS,TAF.FEE_AMNT,TAF.TOTAL_FEE_AMNT,F.FEE_DESC FROM TEMP_ACTIVITY_FEE TAF JOIN FEE F ON TAF.FEE_ID=F.FEE_ID WHERE TAF.ACTIVITY_ID = "+actId+" AND TAF.TEMP_ID = "+tempId;
	
	logger.debug("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        	Fee fee = new Fee();
        	fee.setFeeId(rs.getString("FEE_ID"));
        	fee.setFeeUnits(StringUtils.s2d(rs.getString("FEE_UNITS")));
        	fee.setFeeAmnt(StringUtils.s2d(rs.getString("FEE_AMNT")));
        	fee.setTotalFee(StringUtils.s2d(rs.getString("TOTAL_FEE_AMNT")));
        	fee.setFeeDesc(rs.getString("FEE_DESC"));        	
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return feeList;
}

/**
 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment of the Renewal application 
 * based on the fee Id.
 * @param String, String
 * @throws Exception
 * @return List<Fee>
 * 
 */
public String getFeesAccountForFeeId(String feeId) {
	String sql ="SELECT FEE_ACCOUNT FROM FEE WHERE FEE_ID = "+feeId;
	
	logger.debug("getFees sql :"+sql);
	String feeAccount = "";
	try {
		feeAccount = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
        @Override
        public String mapRow(ResultSet rs, int i) throws SQLException {
        	String feeAccount;
        	feeAccount = StringUtils.nullReplaceWithEmpty(rs.getString("FEE_ACCOUNT"));        	
		return  feeAccount;
        }
    });
	
     logger.debug("# feeAccount - " + feeAccount);
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
}
	return feeAccount;
}

/**
 * This method is to check if the payment is fully done or partially done for BL/BT Renewal application 
 * is submitted based on the renewal code or activity Id.
 * @param String
 * @throws Exception
 * @return String 
 */
public double getFullOrPartialPaymentFlag(Activity activity) {
	String sql = "select (SUM(FEE_AMNT)-SUM(FEE_PAID)) AS AVERAGE from activity_fee WHERE ACTIVITY_ID = "+activity.getRenewalCode();
				    
	logger.debug("getFullOrPartialPaymentFlag sql :"+sql);
	double partialPay=0.0;
	String partialPayFlag = null;
	try {
		partialPayFlag=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String partialPayFlag=rs.getString("AVERAGE");
				return partialPayFlag;	
		    }
		});
		logger.debug("partialPayFlag... "+partialPayFlag);
	} catch(EmptyResultDataAccessException e1) {
		logger.error("",e1);
	}catch (Exception e) {
		logger.error("",e);
	}	
	partialPay=StringUtils.s2d(partialPayFlag);
	return partialPay;
}

	/**
	 * This method is to fetch the online flag based on Activity Type for validating login
	 * @param Activity Form
	 * @throws 
	 * @return String 
	 */
	public String getActTypeOnlineFlag(Activity activityForm) {
		String sql ="SELECT B.ONLINE_YN FROM ACTIVITY A, LKUP_ACT_TYPE B WHERE A.ACT_NBR = '"+activityForm.getPermitNumber()+"' AND A.ACT_TYPE = B.TYPE";
		
		logger.debug("getActTypeOnlineFlag sql :"+sql);
		String actTypeOnlineFlag = "";
		try {
			actTypeOnlineFlag = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
	        @Override
	        public String mapRow(ResultSet rs, int i) throws SQLException {
	        	String flag;
	        	flag = StringUtils.nullReplaceWithEmpty(rs.getString("ONLINE_YN"));        	
			return  flag;
	        }
	    });
		
	     logger.debug("# ActTypeOnlineFlag - " + actTypeOnlineFlag);
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			logger.error("",e);
	}
		return actTypeOnlineFlag;
	}
	
	/**
	 * This method is to fetch the Email Templates based on email type
	 * @param String
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	public EmailTemplateAdminForm getEmailTempByTempTypeId(String tempType) {
		logger.info("getEmailTempByTempTypeId("+tempType+")");
		StringBuilder sql = new StringBuilder();
		EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
		try {
			sql.append("SELECT A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, B.EMAIL_SUBJECT, B.EMAIL_BODY"
	    			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID"
	    			+ " AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ");
	    	if(tempType!=null && !tempType.equalsIgnoreCase("")) {
	    		sql.append(" AND A.EMAIL_TEMP_TYPE = '" +tempType+"'");        		
	    	}
	    	sql.append(" ORDER BY A.EMAIL_TEMP_TYPE");
	    	logger.debug("Sql is :: " + sql.toString());       	
	
	    	emailTemplateAdminForm = jdbcTemplate.queryForObject(sql.toString(),new RowMapper<EmailTemplateAdminForm>() {
	            @Override
	            public EmailTemplateAdminForm mapRow(ResultSet rs, int i) throws SQLException {
	            	EmailTemplateAdminForm emailTempForm = new EmailTemplateAdminForm();
	            	emailTempForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTempForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
	            	emailTempForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
	            	emailTempForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());  
					return emailTempForm;
	            }
			});
			return emailTemplateAdminForm;
		} catch (Exception e) {
			logger.error("Unable to get email templates by id "+e+e.getMessage());			
		}
		return emailTemplateAdminForm;
	}
	public void resetBlUploadAttachments(String tempId) {
		String updateQuery = "delete from TEMP_ATTACHMENT where TEMP_ID = "+tempId;
		logger.debug("updateQuery  :"+updateQuery);
		jdbcTemplate.update(updateQuery);
	}

     /**
	 * This method is to fetch the Email Templates based on email type
	 * @param emailDetails
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	public EmailTemplateAdminForm getEmailData(EmailDetails emailDetails) {		
		try {		    
			String emailType = emailDetails.getEmailType();
			
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){			
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT)){
					logger.debug("in APPLICATION_SUBMIT_EMAIL_APPLICANT_BT");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
//				logger.debug("BT email subject :: " +emailTemplateAdminForm.getEmailSubject());
//				logger.debug("BT email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){			
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL)){
					logger.debug("in APPLICATION_SUBMIT_EMAIL_APPLICANT_BL");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("BL email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("BL email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			
			logger.debug("permit number "+emailDetails.getPermitNumberForAddBLBT());
			if(emailDetails.getBusinessAccNo() != null && emailDetails.getActId() != null && emailDetails.getPermitNumberForAddBLBT().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.BUSINESS_LICENSE_APPLICATION_SUBMIT)){
					logger.debug("in BUSINESS_LICENSE_APPLICATION_SUBMIT");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.BUSINESS_LICENSE_APPLICATION_SUBMIT);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("BL email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("BL email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			if(emailDetails.getBusinessAccNo() != null && emailDetails.getActId() != null&& emailDetails.getPermitNumberForAddBLBT().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.BUSINESS_TAX_APPLICATION_SUBMIT)){
					logger.debug("in BUSINESS_TAX_APPLICATION_SUBMIT");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.BUSINESS_TAX_APPLICATION_SUBMIT);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("BT email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("BT email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			if(emailDetails.getSecurityCode() != 0){
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.BL_SECURITY_CODE_VERIFICATION)){
					if(Constants.BL_SECURITY_CODE_VERIFICATION != null){
						emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.BL_SECURITY_CODE_VERIFICATION);
					}
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.BT_SECURITY_CODE_VERIFICATION)){
					if(Constants.BT_SECURITY_CODE_VERIFICATION != null){
						emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.BT_SECURITY_CODE_VERIFICATION);
					}
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("Email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("Email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			logger.error("Email message sent successfully");
			return emailTemplateAdminForm;
		} catch (EmptyResultDataAccessException e1) {
			logger.error("Empty Result Data Access Exception e1 :: "	+ e1.getMessage()+e1);
			e1.printStackTrace();
			return new EmailTemplateAdminForm();
		}catch (Exception e) {
			logger.error("Error in sending emails "	+ e.getMessage()+e);
			e.printStackTrace();
			return new EmailTemplateAdminForm();
		}
	}
	
	/**
		 * This method is to replace all the original values which has ## in the beginning & ending of the string.
		 * This will return complete email body language with values.
		 * @param String emailBody
		 * @param EmailDetails emailDetails
		 * @throws 
		 * @return String
	     * 
	 */
	private String getEmailBodyWithValues(String emailBody, EmailDetails emailDetails) {
		logger.error("emailBody :"+emailBody);
		logger.error("emailDetails.getBusinessName() :"+emailDetails.getBusinessName());
		logger.error("emailDetails.getNoOfEmp() :"+emailDetails.getNoOfEmployees());	
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				LocalDate currentDate = LocalDate.now();
				logger.debug(dtf.format(currentDate));
				
		if(emailBody!=null) {
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith("BL")) {
				if(emailDetails.getNoOfEmployees()==null && emailDetails.getNoOfEmployees().equalsIgnoreCase("0") && emailDetails.getNoOfEmployees().equalsIgnoreCase("")) emailDetails.setNoOfEmployees("1");
			}
			if(emailDetails.getNoOfEmployees()!=null && !emailDetails.getNoOfEmployees().trim().equalsIgnoreCase("")) {
				if(emailBody.contains("##TOTAL_COUNT##")) emailBody = emailBody.replaceAll("##TOTAL_COUNT##", emailDetails.getNoOfEmployees());
				if(emailBody.contains("##NUMBER_OF_PAYMENTS##")) emailBody = emailBody.replace("##NUMBER_OF_PAYMENTS##", emailDetails.getNoOfEmployees());
				logger.error("emailBody after total count :"+emailDetails.getNoOfEmployees());
			}
			if(currentDate != null) {
				logger.debug("currentDate :"+currentDate);
				if(emailBody.contains("##PAYMENT_DATE##")) emailBody = emailBody.replaceAll("##PAYMENT_DATE##", currentDate.toString());
				if(emailBody.contains("##SUBMIT_DATE##")) emailBody = emailBody.replaceAll("##SUBMIT_DATE##", currentDate.toString());
				if(emailBody.contains("##APPLICATION_DATE##")) emailBody = emailBody.replaceAll("##APPLICATION_DATE##", currentDate.toString());
				logger.error("currentDate :"+currentDate);
			}
			if(StringUtils.s2d(emailDetails.getTotalFee()) > 0) {
				if(emailBody!=null && emailBody.contains("##PAYMENT_AMOUNT##")) emailBody = emailBody.replace("##PAYMENT_AMOUNT##", ("$".concat(emailDetails.getTotalFee())));
				if(emailBody!=null && emailBody.contains("##TOTAL_SUM_OF_PAYMENTS##")) emailBody = emailBody.replace("##TOTAL_SUM_OF_PAYMENTS##", ("$".concat(emailDetails.getTotalFee())));
				logger.error("##PAYMENT_AMOUNT## :: " +StringUtils.s2d(emailDetails.getTotalFee()));			
				logger.error("##TOTAL_SUM_OF_PAYMENTS## :: " +StringUtils.s2d(emailDetails.getTotalFee()));
			}
			if(emailDetails.getBusinessName() != null && !emailDetails.getBusinessName().trim().equalsIgnoreCase("")) {
				if(emailBody.contains("##BUSINESS_NAME##")) emailBody = emailBody.replace("##BUSINESS_NAME##", emailDetails.getBusinessName());
				logger.error("##BUSINESS_NAME## :: " +emailDetails.getBusinessName());
			}
			if(StringUtils.s2d(emailDetails.getPaymentAmount()) > 0) {
				if(emailBody!=null && emailBody.contains("##PAYMENT_AMOUNT##")) emailBody = emailBody.replace("##PAYMENT_AMOUNT##", ("$".concat(emailDetails.getPaymentAmount())));
				logger.error("##PAYMENT_AMOUNT## :: " +emailDetails.getPaymentAmount());			
			}
			if(currentDate != null) {
				if(emailBody!=null && emailBody.contains("##APPLICATION_DATE##")) emailBody = emailBody.replace("##APPLICATION_DATE##", currentDate.toString());
				if(emailBody!=null && emailBody.contains("##APPLICATIONDATE##")) emailBody = emailBody.replace("##APPLICATIONDATE##", currentDate.toString());
				logger.error("##APPLICATION_DATE## :: " +currentDate.toString());
			}
			if(emailDetails.getBusinessAccNo() != null && emailDetails.getActId() != null) {
				if(emailBody!=null && emailBody.contains("##BUSINESS_ACC_NO##")) {
					emailBody = emailBody.replace("##BUSINESS_ACC_NO##", emailDetails.getBusinessAccNo());
				}
				if(emailBody!=null && emailBody.contains("##ACT_ID##")) {
					emailBody = emailBody.replace("##ACT_ID##", emailDetails.getActId());
				}						
			}
			if(emailDetails.getPermitNumber()!=null ) {
				if(emailBody.contains("##PERMIT_NUMBER##")) emailBody = emailBody.replaceAll("##PERMIT_NUMBER##", emailDetails.getPermitNumber());
			}
			if(emailDetails.getPermitNumberForAddBLBT()!=null) {
				if(emailBody.contains("##PERMIT_NUMBER##")) emailBody = emailBody.replaceAll("##PERMIT_NUMBER##", emailDetails.getPermitNumberForAddBLBT());
			}

			if(emailDetails.getBusinessAccNo()!=null) {
				if(emailBody.contains("##BUSINESS_ACC_NO##")) emailBody = emailBody.replaceAll("##BUSINESS_ACC_NO##", emailDetails.getBusinessAccNo());
			}
			if(emailDetails.getActId() != null) {
				if(emailBody.contains("##RENEWAL_CODE##")) emailBody = emailBody.replace("##RENEWAL_CODE##", emailDetails.getActId());
			}
			if(emailDetails.getBusinessName()!=null) {
				if(emailBody.contains("##BUSINESS_NAME##")) emailBody = emailBody.replace("##BUSINESS_NAME##", emailDetails.getBusinessName());
			}
			if(emailDetails.getSecurityCode()!=0) {
				if(emailBody.contains("##SECURITY_CODE##")) emailBody = emailBody.replace("##SECURITY_CODE##", StringUtils.i2s(emailDetails.getSecurityCode()));
			}
			if(emailDetails.getEmailId()!=null) {
				if(emailBody.contains("##EMAIL_ADDRESS##")) emailBody = emailBody.replace("##EMAIL_ADDRESS##", emailDetails.getEmailId());
			}
			
		}
		emailBody= emailBody.replace("##", "");
		emailBody= emailBody.replace("\\\"", "\"");
		logger.error("emailBody before method exit: "+emailBody);
		return emailBody;
	}
	
	/**
	 * This method is to fetch old temp id for updating the documents in TEMP_ATTACHMENT table.
	 * This is to not to re-upload the documents which they have done earlier.
	 * @param String Business Account number
	 * @param String Renewal Code
	 * @throws Exception
	 * @return String
	 * 
	 */
	public String getOldTempActivityNumber(Activity activity) {
		String selectSql = "SELECT ID FROM TEMP_ACTIVITY_DETAILS WHERE RENEWAL_CODE = '" +activity.getRenewalCode()+ "' AND BUSINESS_ACC_NO = '"+StringUtils.nullReplaceWithEmpty(activity.getBusinessAccNo())+"'";
		 logger.error("selectSql getOldTempActivityNumber() :: "+selectSql);
		
		 String oldTempId = "0";
		try {
			oldTempId=jdbcTemplate.query(selectSql,new ResultSetExtractor<String>() {				
				@Override
				public String extractData(ResultSet rs) throws SQLException, DataAccessException {
					String oldTempId="";
					if(rs.next()) {
						oldTempId=rs.getString("ID");						
					}
					return oldTempId;	
				}
			});
		} catch(EmptyResultDataAccessException e1) {
			logger.error("",e1);
		}catch (Exception e) {
			logger.error("",e);
		}	
		logger.error("Old Temp Id for updating the Temp Attchment Tabel :: " +oldTempId);
		return oldTempId;
	}
	
	/**
	 * For add new business license
	 * Gets the list of application type
	 * 
	 * @return
	 */
	public ApplicationType getApplicationType(int applicationTypeId) throws Exception {
		logger.debug("getApplicationType(" + applicationTypeId + ")");

		ApplicationType applicationType = null;

		try {
			String sql = "select * from lkup_appl_type where id=" + applicationTypeId;
			logger.debug(sql);
			
			applicationType = jdbcTemplate.query(sql, new ResultSetExtractor<ApplicationType>() {                
		        @Override
		        public ApplicationType extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	ApplicationType appType = new ApplicationType();
		            while(rs.next()) {
		            	appType.setId(rs.getInt("ID"));
		                appType.setDescription(rs.getString("DESCRIPTION"));                    
		            }            
		            return appType;
		        }
	    	});
	    	
	    }catch(Exception e) {
	    	logger.error("Error occured in getApplicationType :: " +e.getMessage());
	    	e.printStackTrace();
	    }
	    return applicationType;   
	}
	
	/**
	 * Gets the activity status for a status id.
	 * 
	 * @param statusId
	 * @return
	 * @throws Exception
	 */
	public ActivityStatus getActivityStatus(int statusId) throws Exception {
		logger.debug("getActivityStatus(" + statusId + ")");

		ActivityStatus activityStatus = null;

		try {
			String sql = "select * from lkup_act_st where status_id=" + statusId;
			logger.debug("Lookupagent - getActivityStatus -- " + sql);
			
			activityStatus = jdbcTemplate.query(sql, new ResultSetExtractor<ActivityStatus>() {                
		        @Override
		        public ActivityStatus extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	ActivityStatus actStatus = new ActivityStatus();
		            while(rs.next()) {
		            	actStatus.setStatus(rs.getInt("status_id"));
		            	actStatus.setCode(rs.getString("stat_code").trim());
		            	actStatus.setDescription(rs.getString("description").trim());	            	                  
		            }            
		            return actStatus;
		        }
	    	});
			
		} catch (Exception e) {
			logger.error("Error occured in getActivityStatus :: " +e.getMessage());
	    	e.printStackTrace();
		}
		return activityStatus;
	}
	
	/**
	 * Gets the list of ownership type
	 * 
	 * @return
	 */
	public OwnershipType getOwnershipType(int ownershipTypeId) throws Exception {
		logger.debug("getOwnershipType(" + ownershipTypeId + ")");

		OwnershipType ownershipType = null;

		try {
			String sql = "select * from lkup_own_type where id=" + ownershipTypeId;
			logger.debug(sql);
			
			ownershipType = jdbcTemplate.query(sql, new ResultSetExtractor<OwnershipType>() {                
		        @Override
		        public OwnershipType extractData(ResultSet rs) throws SQLException, DataAccessException { 
		        	OwnershipType ownershipType = null;
		        	if (rs != null && rs.next()) {
						int id = rs.getInt("id");
						String description = StringUtils.properCase(rs.getString("description"));
						ownershipType = new OwnershipType(id, description);
					}            
		            return ownershipType;
		        }
	    	});	
			
		} catch (Exception e) {
			logger.error("Error occured in getActivityStatus :: " +e.getMessage());
	    	e.printStackTrace();
		}
		return ownershipType;
	}
	
	/**
	 * Get the activity type for description for a type code
	 * 
	 * @param type
	 * @return
	 */
	public ActivityType getActivityType(String type) throws Exception {
		logger.debug("getActivityType(" + type + ")");

		ActivityType activityType = new ActivityType();

		if (type == null || type.equalsIgnoreCase("")) {
			return activityType;
		} else {
			try {
				String sql = "select * from lkup_act_type where type='" + type.toUpperCase() + "'";
				logger.debug(sql);
				
				
				activityType = jdbcTemplate.query(sql, new ResultSetExtractor<ActivityType>() {                
			        @Override
			        public ActivityType extractData(ResultSet rs) throws SQLException, DataAccessException {    
			        	ActivityType activityType = new ActivityType();
			            while(rs.next()) {
			            	//activityType.setType(rs.getString("type"));
			            	activityType.setDescription(StringUtils.properCase(rs.getString("description"))); 
			            	Department department = getDepartment(rs.getInt("dept_id"));
			            	
			            	if (department != null) {
								activityType.setDepartmentId(department.getDepartmentId());
								activityType.setDepartmentCode(department.getDepartmentCode());

								activityType.setTypeId(StringUtils.i2s(rs.getInt("type_id")));
								activityType.setType(type);
							} else {
								activityType.setDepartmentId(-1);
								activityType.setDepartmentCode("");
							}
		    
							activityType.setSubProjectType(rs.getInt("ptype_id"));
			            	
			            }            
			            return activityType;
			        }
		    	});			
				
			} catch (Exception e) {
				logger.error("Error occured in getActivityStatus :: " +e.getMessage());
		    	e.printStackTrace();
			}
			return activityType;

		}
	}
	
	/**
	 * Get the activity sub type for a sub type id
	 * 
	 * @param id
	 * @return
	 */
	public ActivitySubType getActivitySubType(String id) throws Exception {
		logger.debug("getActivitySubType(" + id + ")");

		ActivitySubType actSubType = new ActivitySubType();
		String sql = "select * from lkup_act_subtype where act_subtype_id=" + id;

		try {
			
			actSubType = jdbcTemplate.query(sql, new ResultSetExtractor<ActivitySubType>() {                
		        @Override
		        public ActivitySubType extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	ActivitySubType activitySubType = new ActivitySubType();
		        	if (rs != null && rs.next()) {
		        		activitySubType.setId(rs.getInt("act_subtype_id"));
		        		activitySubType.setDescription(rs.getString("act_subtype"));
					} else {
						activitySubType.setId(-1);
						activitySubType.setDescription("");
					}          
		            return activitySubType;
		        }
	    	});		
			
		} catch (Exception e) {
			logger.error("Error in getActivitySubType " + e.getMessage());
			throw e;
		}
		return actSubType;
	}
	
	/**
	 * Gets the list of Quantity type
	 * 
	 * @return
	 */
	public QuantityType getQuantityType(int quantityId) throws Exception {
		logger.debug("getQuantityType(" + quantityId + ")");

		QuantityType quantityType = null;

		try {
			String sql = "select * from lkup_qty where id=" + quantityId;
			logger.debug(sql);
			
			quantityType = jdbcTemplate.query(sql, new ResultSetExtractor<QuantityType>() {                
		        @Override
		        public QuantityType extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	QuantityType qtyType = null;
		        	if (rs != null && rs.next()) {
		        		int id = rs.getInt("id");
		        		String description = StringUtils.properCase(rs.getString("description"));
		        		qtyType = new QuantityType(id, description);
					}          
		            return qtyType;
		        }
	    	});
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return quantityType;
	}

	/**
	 * Gets the list of Exemption Type
	 * 
	 * @return
	 */
	public ExemptionType getExemptionType(int exemptionTypeId) throws Exception {
		logger.debug("getExemptionType(" + exemptionTypeId + ")");

		ExemptionType exemptionType = null;

		try {
			String sql = "select * from lkup_exem_type where id=" + exemptionTypeId;
			logger.debug(sql);
			
			exemptionType = jdbcTemplate.query(sql, new ResultSetExtractor<ExemptionType>() {                
		        @Override
		        public ExemptionType extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	ExemptionType exempType = null;
		        	if (rs != null && rs.next()) {
						int id = rs.getInt("id");
						String description = StringUtils.properCase(rs.getString("description"));
						exempType = new ExemptionType(id, description);
					}
		        	return exempType;
		        }
	    	});
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return exemptionType;
	}

	/**
	 * Get the list of streets
	 * 
	 * @param id
	 * @return description
	 */
	public Street getStreet(String streetName) throws Exception {
		logger.debug("getStreet(" + streetName + ")");

		Street street = new Street();
		String sql = "select * from street_list where street_id=" + streetName;
		logger.debug(" Street List is  " + sql);

		try {
			
			 street = jdbcTemplate.query(sql, new ResultSetExtractor<Street>() {                
		        @Override
		        public Street extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	Street st = new Street();
		        	if (rs != null && rs.next()) {
						st.setStreetId(rs.getInt("STREET_ID"));
						st.setStreetName(rs.getString("STR_NAME"));
					} else {
						st.setStreetId(-1);
						st.setStreetName("");
					}
		        	return st;
		        }
	    	});		

			return street;
		} catch (Exception e) {
			logger.error("Error in Street " + e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Gets the department for a given department id.
	 * 
	 * @param deptId
	 * @return
	 * @throws Exception
	 */
	public Department getDepartment(int deptId) {
		logger.debug("getDepartment(" + deptId + ")");

		Department department = new Department(0, "XX", "Unknown");

		try {
			String sql = "select * from department  where dept_id= " + deptId; // .
			// equalsIgnoreCase();
			logger.debug(sql);			
			
			department = jdbcTemplate.query(sql, new ResultSetExtractor<Department>() {                
		        @Override
		        public Department extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	Department dept = new Department();
		            while(rs.next()) {
		            	dept.setDepartmentId(rs.getInt("dept_id"));
		            	dept.setDepartmentCode(rs.getString("dept_code"));
		            	dept.setDescription(StringUtils.properCase(rs.getString("description")));		            	
		            }            
		            return dept;
		        }
	    	});	
			
		} catch (Exception e) {
			logger.error("Error occured in getDepartment :: " +e.getMessage());
	    	e.printStackTrace();
		}
		
		return department;
	}	
	
	/**
	 * Gets the list of Quantity type
	 * 
	 * @author Gayathri
	 * @return
	 */
	public QuantityType getBtQuantityType(int quantityId) throws Exception {
		logger.debug("getQuantityType(" + quantityId + ")");

		QuantityType quantityType = null;

		try {
			String sql = "select * from lkup_bt_qty where id=" + quantityId;
			logger.debug(sql);

			//RowSet rs = new Wrapper().select(sql);
			
			quantityType = jdbcTemplate.query(sql, new ResultSetExtractor<QuantityType>() {                
		        @Override
		        public QuantityType extractData(ResultSet rs) throws SQLException, DataAccessException {    
		        	QuantityType qtytype = new QuantityType();
		        	if (rs != null && rs.next()) {
						int id = rs.getInt("id");
						String description = StringUtils.properCase(rs.getString("description"));
						qtytype = new QuantityType(id, description);
					}      
		            return qtytype;
		        }
	    	});	
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return quantityType;
	}
	
	// Created by Gayathri to get Street name list order by Street id.
		public List getBTBLStreetArrayList() {
			String sql = "select * from street_list where street_id > 0 order by street_id";
			List streetList = new ArrayList();

			try {
				streetList = jdbcTemplate.query(sql,new RowMapper<Street>() {
			        @Override
			        public Street mapRow(ResultSet rs, int i) throws SQLException {			        	
			        Street street = null;
					String prefixDirection = (rs.getString("pre_dir") != null) ? rs.getString("pre_dir") : "";
					String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
					String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
					String suffixDirection = (rs.getString("suf_dir") != null) ? rs.getString("suf_dir") : "";
					String fullStreetName = "";

					if (!("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
						fullStreetName = prefixDirection + " " + streetName + " " + streetType;
					} else if (("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
						fullStreetName = streetName + " " + streetType;
					} else if (!("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
						fullStreetName = prefixDirection + " " + streetName;
					} else if (("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
						fullStreetName = streetName;
					}

					street = new Street(rs.getInt("street_id"), StringUtils.properCase(fullStreetName));
					
					return street;
				}
				});
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return streetList;
		}
		
		/**
		 * Gets the list of bl activity types
		 * 
		 * Created by Gayathri
		 * 
		 * @return
		 */
		public List getBlActivityTypes(int moduleId) throws Exception {
			logger.debug("getBlActivityTypes(" + moduleId + ")");

			List activityTypes = new ArrayList();
			//ActivityType activityType = new ActivityType();

			try {
				String sql = "select * from lkup_act_type where module_id=" + moduleId + " order by description ";
				logger.debug(sql);

				activityTypes = jdbcTemplate.query(sql,new RowMapper<ActivityType>() {
			        @Override
			        public ActivityType mapRow(ResultSet rs, int i) throws SQLException {	
			        ActivityType activityType = null;			        
					String type = rs.getString("type");
					String description = StringUtils.properCase(rs.getString("description"));
					activityType = new ActivityType(type, description);
					return activityType;
				}
				});
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return activityTypes;
		}
		
		/**
		 * Gets the list of application types
		 * 
		 * @return
		 */
		public List getApplicationTypes() throws Exception {
			logger.debug("getApplicationTypes()");

			List applicationTypes = new ArrayList();
			//ApplicationType applicationType = new ApplicationType();

			try {
				String sql = "select * from lkup_appl_type order by description";
				logger.debug(sql);

				applicationTypes = jdbcTemplate.query(sql,new RowMapper<ApplicationType>() {
			        @Override
			        public ApplicationType mapRow(ResultSet rs, int i) throws SQLException {
			        ApplicationType applicationType = null;			        
					int id = rs.getInt("id");
					String description = StringUtils.properCase(rs.getString("description"));
					applicationType = new ApplicationType(id, description);
					return applicationType;
				}
				});
				
				logger.debug("returning with application types list of size " + applicationTypes.size());
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return applicationTypes;
		}
		
		/**
		 * Get the list of activity statuses
		 * 
		 * @return
		 * @throws Exception
		 */
		public List getBLActivityStatuses(int moduleId) throws Exception {
			logger.debug("getActivityStatuses()");

			List activityStatuses = new ArrayList();
			//ActivityStatus activityStatus = null;

			try {
				String sql = "select * from lkup_act_st where module_id =" + moduleId + " order by  description ";
				logger.debug(sql);

				activityStatuses = jdbcTemplate.query(sql,new RowMapper<ActivityStatus>() {
			        @Override
			        public ActivityStatus mapRow(ResultSet rs, int i) throws SQLException {
			        ActivityStatus activityStatus = null;			        
					int statusId = rs.getInt("status_id");
					String code = rs.getString("stat_code").trim();
					String description = rs.getString("description").trim();
					activityStatus = new ActivityStatus(statusId, code, description);
					return activityStatus;
				}
				});	
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return activityStatuses;
		}
		
		/**
		 * Gets the list of ownership types
		 * 
		 * @return
		 */
		public List getOwnershipTypes() throws Exception {
			logger.debug("getOwnershipTypes()");

			List ownershipTypes = new ArrayList();
			//OwnershipType ownershipType = new OwnershipType();

			try {
				String sql = "select * from lkup_own_type order by description";
				logger.debug(sql);

				ownershipTypes = jdbcTemplate.query(sql,new RowMapper<OwnershipType>() {
			        @Override
			        public OwnershipType mapRow(ResultSet rs, int i) throws SQLException {
			        OwnershipType ownershipType = null;
					int id = rs.getInt("id");
					String description = StringUtils.properCase(rs.getString("description"));
					ownershipType = new OwnershipType(id, description);
					return ownershipType;
				}
				});
				
				logger.debug("returning with ownership types list of size " + ownershipTypes.size());
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return ownershipTypes;
		}
		
		/**
		 * Gets the list of BT Quantity types
		 * 
		 * @return
		 */
		public List getBtQuantityTypes() throws Exception {
			logger.debug("getBtQuantityTypes()");

			List quantityTypes = new ArrayList();
			//QuantityType quantityType = new QuantityType();

			try {
				String sql = "select * from lkup_bt_qty order by description";
				logger.debug(sql);

				quantityTypes = jdbcTemplate.query(sql,new RowMapper<QuantityType>() {
			        @Override
			        public QuantityType mapRow(ResultSet rs, int i) throws SQLException {
			        QuantityType quantityType = null;			        
					int id = rs.getInt("id");
					String description = StringUtils.properCase(rs.getString("description"));
					quantityType = new QuantityType(id, description);
					return quantityType;
				}
				});
				
				logger.debug("returning with BT quantity types list of size " + quantityTypes.size());
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return quantityTypes;
		}
		
		/**
		 * Gets the list of Quantity types
		 * 
		 * @return
		 */
		public List getQuantityTypes() throws Exception {
			logger.debug("getQuantityTypes()");

			List quantityTypes = new ArrayList();
			QuantityType quantityType = new QuantityType();

			try {
				String sql = "select * from lkup_qty order by description";
				logger.debug(sql);

				quantityTypes = jdbcTemplate.query(sql,new RowMapper<QuantityType>() {
			        @Override
			        public QuantityType mapRow(ResultSet rs, int i) throws SQLException {
			        	QuantityType quantityType = null;			        
					int id = rs.getInt("id");
					String description = StringUtils.properCase(rs.getString("description"));
					quantityType = new QuantityType(id, description);
					return quantityType;
				}
				});

				
				logger.debug("returning with quantity types list of size " + quantityTypes.size());
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return quantityTypes;
		}
		
		/**
		 * Gets the list of Exemption types
		 * 
		 * @return
		 */
		public List getExemptionTypes() throws Exception {
			logger.debug("getExemptionTypes()");

			List exemptionTypes = new ArrayList();
			//ExemptionType exemptionType = new ExemptionType();

			try {
				String sql = "select * from lkup_exem_type order by description";
				logger.debug(sql);

				exemptionTypes = jdbcTemplate.query(sql,new RowMapper<ExemptionType>() {
			        @Override
			        public ExemptionType mapRow(ResultSet rs, int i) throws SQLException {
			        ExemptionType exemptionType = null;
					int id = rs.getInt("id");
					String description = StringUtils.properCase(rs.getString("description"));
					exemptionType = new ExemptionType(id, description);
					return exemptionType;
				}
				});
				
				logger.debug("returning with exemption types list of size " + exemptionTypes.size());
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return exemptionTypes;
		}
		
		/**
		 * Get the list of activity sub types for an activity type.
		 * 
		 * @param activityType
		 * @return
		 */
		public List getActivitySubTypes(String activityType) throws Exception {
			logger.debug("getActivitySubTypes(" + activityType + ")");

			List activitySubTypes = new ArrayList();
			String sql = "select * from lkup_act_subtype where act_type='" + activityType + "' order by act_subtype";
			logger.debug(sql);

			ActivitySubType subType = new ActivitySubType();

			try {
				///RowSet rs = new Wrapper().select(sql);
				
				activitySubTypes = jdbcTemplate.query(sql,new RowMapper<ActivitySubType>() {
			        @Override
			        public ActivitySubType mapRow(ResultSet rs, int i) throws SQLException { 
			        	ActivitySubType activitySubType = new ActivitySubType(rs.getInt("act_subtype_id"), rs.getString("act_subtype"));      	
			        	return  activitySubType;
			        }
				});
				
				return activitySubTypes;
			} catch (Exception e) {
				logger.error("Error in getActivitySubType " + e.getMessage());
				throw e;
			}
		}
		
		/**
		 * get codes for activity type
		 * 
		 * @param activityType
		 * @return
		 * @throws Exception
		 */
		public Map getCodes(String activityType) throws Exception {
			logger.debug("getCodes(" + activityType + ")");

			Map codes = new HashMap();
			String sql = "";
			sql = "select * from lkup_act_type where type=" + StringUtils.checkString(activityType);
			logger.debug(sql);

			try {

				//RowSet rs = new Wrapper().select(sql);
				
				codes= jdbcTemplate.query(sql, new ResultSetExtractor<Map<String, String>>() {                
			        @Override
			        public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {    
			            Map<String, String> info = new HashMap<String, String>();
			            if (rs != null && rs.next()) {
			    			logger.debug("obtained muni code of " + rs.getString("MUNI_CODE"));
			    			info.put("MUNI_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
			    			info.put("SIC_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
			    			info.put("CLASS_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));
			    		}          
			            return info;
			        }
		    	});

				return codes;
			} catch (Exception e) {
				logger.error("Error in getCodes " + e.getMessage());
				throw e;
			}
		}
		
		public int checkAddress(String streetNumber, String streetFraction, int streetId, String unit, String zip, BusinessLicenseActivity businessLicenseActivity) throws Exception {
			logger.info("checkAddress(" + streetNumber + "," + streetFraction + " , " + streetId + ", " + unit + ", " + zip + ")");
			String sql = "";
			int lsoId = 0;
			int addressId = 0;
			sql = "SELECT * FROM V_ADDRESS_LIST WHERE ";

			if (!((streetNumber == null) || (streetNumber.equalsIgnoreCase("")))) {
				sql += (" STR_NO=" + streetNumber);
			}
			if (!((streetFraction == null) || (streetFraction.equalsIgnoreCase("")))) {
				sql += (" and STR_MOD=" + StringUtils.checkString(streetFraction));
			}
			if (!(streetId == 0)) {
				sql += (" and STREET_ID=" + streetId);
			}

			if (!((unit == null) || (unit.equalsIgnoreCase("")))) {
				sql += (" and UNIT=" + StringUtils.checkString(unit));
			}
			if (!((zip == null) || (zip.equalsIgnoreCase("")))) {
				sql += (" AND ZIP=" + StringUtils.checkString(zip));
			}

			sql += " and LSO_TYPE = 'O' order by addr_id";

			logger.info(sql);

			//RowSet rs = new Wrapper().select(sql);
			try {
				lsoId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
				    	int lsoId = 0;
				    	
						lsoId = rs.getInt("LSO_ID");
						logger.debug("LSO Id Is " + lsoId);
							//addressId = rs.getInt("ADDR_ID");
							//logger.debug("Address Id is " + addressId);
//						businessLicenseActivity.setAddressStreetNumber(rs.getString("STR_NO"));
//						businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
//						businessLicenseActivity.setAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
//						businessLicenseActivity.setAddressUnitNumber(rs.getString("UNIT"));
//						businessLicenseActivity.setAddressCity(rs.getString("CITY"));
//						businessLicenseActivity.setAddressState(rs.getString("STATE"));
//						businessLicenseActivity.setAddressZip(rs.getString("ZIP"));
//						businessLicenseActivity.setAddressZip4(rs.getString("ZIP4"));					
						return lsoId;
				    }
				});
			}catch(EmptyResultDataAccessException e) {
				logger.error("Unable to get the data from V_ADDRESS_LIST "+e);
				return 0;
			}
			return lsoId;
			
		}
		
		/**
		 * Get the lso id given the psa id and psa type.
		 * 
		 * @param psaId
		 * @param PsaType
		 * @return int
		 * @throws Exception
		 */
		public int getAddressIdForPsaId(String lsoId) throws Exception {
			logger.info("getAddressIdForPsaId(" + lsoId + ")");

			//RowSet rs = null;
			int addressId = -1;
			String sql = "";

			try {

				sql = "SELECT ADDR_ID FROM V_ADDRESS_LIST WHERE LSO_ID = " + lsoId;
				logger.debug(sql);
				//rs = new Wrapper().select(sql);
				
				addressId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
				    	int addrId = 0;				    	
				    	addrId = rs.getInt(1);
				    	
						return addrId;
				    }
				});

				
				logger.debug("The obtained address id is " + addressId);
				
			} catch (Exception e) {
				logger.error("Exception in getLsoIdForPsaId of LookupAgent - " + e.getMessage());

				throw e;
			}
			return addressId;
		}
		
		public boolean copyRequiredCondition(String typeId, int actId, User user) {
			//CommonAgent condAgent = new CommonAgent();
			ConditionLibraryForm conditionLibraryForm = new ConditionLibraryForm();
			boolean added = false;
			logger.debug("after init");

			try {
				conditionLibraryForm.setType(typeId);
				conditionLibraryForm.setConditionLevel("A");
				conditionLibraryForm.setLevelId(StringUtils.i2s(actId));
				conditionLibraryForm = getRequiredLibraryItems(conditionLibraryForm);
				logger.debug("updated conditionLibraryForm");

				ConditionLibraryRecord[] recList = conditionLibraryForm.getConditionLibraryRecords();
				logger.debug("got library record");

				// create Condition objects for every record and send them to condition agent
				for (int i = 0; i < recList.length; i++) {
					ConditionLibraryRecord rec = recList[i];
					logger.debug("inside For 4 new condition");

					Condition condition = new Condition();

					// condition.setConditionId((new Integer(condId)).intValue());
					condition.setConditionId(0); // when Id is 0 a new record is created
					condition.setConditionLevel("A");
					condition.setLevelId(actId);
					condition.setShortText(rec.getShort_text());
					condition.setText(rec.getCondition_text());
					condition.setInspectable(rec.getInspectability());
					condition.setWarning(rec.getWarning());
					condition.setComplete("N");
					condition.setLibraryId(rec.getLibraryId());
					condition.setCreatedBy(user.getUserId());
					condition.setUpdatedBy(user.getUserId());
					logger.debug("before save");
					saveCondition(condition);
					logger.debug("after save");
				}

				added = true;
			} catch (Exception e) {
				logger.debug("Exception occured in getActTypeId method of ConditionAgent. Message-" + e.getMessage());
			}

			return added;
		}
		
		/**
		 * Function to get items from library for a specific sub category
		 */
		public ConditionLibraryForm getRequiredLibraryItems(ConditionLibraryForm frm) {
			logger.debug("Inside getRequiredLibraryItems");

			String type = frm.getType();
			String level = frm.getConditionLevel(); // L,S,O,P,Q,A
			/* String sql = "";
			String levelType = "";
			String inspectability = "";
			String warning = "";
			String required = "";*/
			ConditionLibraryForm conditionLibraryForm = new ConditionLibraryForm();

			//CachedRowSet rs = null;

			try {
				//Wrapper db = new Wrapper();

				//rs = new CachedRowSet();

				String sql = "SELECT * FROM LKUP_CONDITIONS WHERE CONDITION_TYPE = " + type + " AND LEVEL_TYPE = '" + level + "' and REQUIRED = 'Y' AND EXPIRATION_DT IS null";

				logger.debug("sql for conditionItem " + sql);

				//rs = (CachedRowSet) db.select(sql);

				//ArrayList list = new ArrayList();
				conditionLibraryForm = jdbcTemplate.query(sql, new ResultSetExtractor<ConditionLibraryForm>() {                
			        @Override
			        public ConditionLibraryForm extractData(ResultSet rs) throws SQLException, DataAccessException {			        	
							String levelType = "";
							String inspectability = "";
							String warning = "";
							String required = "";
							ArrayList list = new ArrayList();
							ConditionLibraryForm frm = new ConditionLibraryForm();

					//if (rs != null) {
						//rs.beforeFirst();
	
						while (rs.next()) {
							String condTxt = new String(rs.getString("CONDITION_TEXT"));
	
							logger.debug("Condition Lib text #### : " + condTxt);
							logger.debug("condition code and library " + rs.getString("condition_code") + " : " + rs.getInt("condition_id"));
	
							levelType = rs.getString("LEVEL_TYPE");
	
							if (levelType == null) {
								levelType = "A";
							}
	
							inspectability = rs.getString("INSPECTABILITY");
	
							if (inspectability == null) {
								inspectability = "N";
							}
	
							warning = rs.getString("WARNING");
	
							if (warning == null) {
								warning = "N";
							}
	
							required = rs.getString("REQUIRED");
	
							if (required == null) {
								required = "N";
							}
	
							ConditionLibraryRecord rec = new ConditionLibraryRecord("off", levelType, "", inspectability, warning, "", required, rs.getString("SHORT_TEXT"), condTxt, rs.getInt("CONDITION_ID"), rs.getString("CONDITION_CODE"));
							list.add(rec);
						}
	
						ConditionLibraryRecord[] recArray = new ConditionLibraryRecord[list.size()];
						//list.toArray(recArray);
	
						frm.setConditionLibraryRecords(recArray);
						logger.debug("# of elements set in List - " + recArray.length);
					//}
						return frm;
			        }
				});
				
				
			} catch (Exception e) {
				logger.debug("Exception occured in getLibraryItems method of ConditionAgent. Message-" + e.getMessage());
			}

			return conditionLibraryForm;
		}
		
		/*
		 * saveCondition decides whether the record needs to be updated or added and branches to the appropriate procedure
		 */
		public void saveCondition(Condition aCondition) {
			logger.debug("inside saveCondition");

			if (aCondition.getConditionId() == 0) {
				addCondition(aCondition);
				logger.debug("inside if of add");
			} else {
				updateCondition(aCondition);
			}
		}

		private void updateCondition(Condition aCondition) {
			logger.debug("update method");

			try {
				String shortText = "";

				if ((aCondition.getShortText() != null) ^ (aCondition.getShortText() != "")) {
					shortText = StringUtils.checkString(aCondition.getShortText());
					logger.debug("shorttext 1 : " + shortText);
				} else {
					shortText = "SUBSTR(" + StringUtils.checkString(aCondition.getText()) + ",1,15)";
					logger.debug("shorttext 3 : " + shortText);
				}

				logger.debug("shortText #### : " + shortText);

				//Wrapper db = new Wrapper();
				String sql = "UPDATE CONDITIONS SET COND_LEVEL = " + StringUtils.checkString(aCondition.getConditionLevel()) + "," + "TEXT = " + StringUtils.checkString(aCondition.getText()) + "," + "INSPECTABLE =" + StringUtils.checkString(aCondition.getInspectable()) + "," + "WARNING = " + StringUtils.checkString(aCondition.getWarning()) + "," + "COMPLETE = " + StringUtils.checkString(aCondition.getComplete()) + "," + "UPDATE_DT =" + "CURRENT_TIMESTAMP " + "," + "UPDATED_BY = " + aCondition.getUpdatedBy() + " WHERE CONDITIONS.COND_ID = " + aCondition.getConditionId() + "  AND  CONDITIONS.LEVEL_ID = " + aCondition.getLevelId();
				logger.debug(sql);
				jdbcTemplate.update(sql);
			} catch (Exception e) {
				logger.error("Exception occured while updateCondition " + e.getMessage());
			}

			return;
		}

		private void addCondition(Condition aCondition) {
			logger.debug("add method");

			try {
				logger.debug("add try");

				//Wrapper db = new Wrapper();
				//int conditionId = db.getNextId("CONDITION_ID");
				 NextId nextId  = nextIdRepository.getNextId("CONDITION_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("CONDITION_ID");
				 nextIdRepository.save(nextId);
				 int conditionId = nextId.getIdValue();

				logger.debug("after getnextid : " + conditionId);

				String sql = "INSERT INTO CONDITIONS(COND_ID,LEVEL_ID,LIBRARY_ID,COND_LEVEL,TEXT,SHORT_TEXT,INSPECTABLE,WARNING,COMPLETE,CREATED_BY,CREATION_DT,UPDATED_BY,UPDATE_DT) VALUES ( " + conditionId + "," + aCondition.getLevelId() + "," + aCondition.getLibraryId() + "," + StringUtils.checkString(aCondition.getConditionLevel()) + "," + StringUtils.checkString(aCondition.getText()) + "," + StringUtils.checkString(aCondition.getShortText()) + "," + StringUtils.checkString(aCondition.getInspectable()) + "," + StringUtils.checkString(aCondition.getWarning()) + "," + StringUtils.checkString(aCondition.getComplete()) + "," + aCondition.getCreatedBy() + "," + "sysdate" + "," + aCondition.getUpdatedBy() + "," + "sysdate" + ")";
				logger.debug(sql);
				jdbcTemplate.update(sql);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return;
		}
		public int checkAddress(String businessAddressStreetNumber, String businessAddressStreetFraction, int streetId,
				String businessAddressUnitNumber, String businessAddressZip, String businessAddressZip4,
				BusinessTaxActivity businessTaxActivity) {
			logger.info("checkAddress(" + businessAddressStreetNumber + "," + businessAddressStreetFraction + " , " + streetId + ", " + businessAddressUnitNumber + ", " + businessAddressZip + ", " + businessAddressZip4 + ")");
			int lsoId = 0;
			String sql = "";
			int addressId = 0;

			sql = "SELECT * FROM V_ADDRESS_LIST WHERE ";
			
			if (!((businessAddressStreetNumber == null) || (businessAddressStreetNumber.equalsIgnoreCase("")))) {
				sql += (" STR_NO=" + businessAddressStreetNumber);
			}
			if (!((businessAddressStreetFraction == null) || (businessAddressStreetFraction.equalsIgnoreCase("")))) {
				sql += ("and STR_MOD=" + StringUtils.checkString(businessAddressStreetFraction));
			}			
			if (!(streetId == 0)) {
				sql += (" and STREET_ID=" + streetId);
			}
			if (!((businessAddressUnitNumber == null) || (businessAddressUnitNumber.equalsIgnoreCase("")))) {
				sql += (" and UNIT=" + StringUtils.checkString(businessAddressUnitNumber));
			} else {
				sql += (" and UNIT is null ");
			}
			if (!((businessAddressZip == null) || (businessAddressZip.equalsIgnoreCase("")))) {
				sql += (" AND ZIP=" + StringUtils.checkString(businessAddressZip));
			}
//			if (!((businessAddressZip4 == null) || (businessAddressZip4.equalsIgnoreCase("")))) {
//				sql += (" AND ZIP4=" + StringUtils.checkString(businessAddressZip4));
//			}

			sql += " and LSO_TYPE = 'O' order by addr_id";

			logger.debug("sql statement is " + sql);

			//RowSet rs = new Wrapper().select(sql);
			
			try {
				lsoId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
				    	int lsoId = 0;
				    	
						lsoId = rs.getInt("LSO_ID");
						logger.debug("LSO Id Is " + lsoId);
							//addressId = rs.getInt("ADDR_ID");
							//logger.debug("Address Id is " + addressId);
						businessTaxActivity.setBusinessAddressStreetNumber(rs.getString("STR_NO"));
						businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
						businessTaxActivity.setBusinessAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
						businessTaxActivity.setBusinessAddressUnitNumber(rs.getString("UNIT"));
						businessTaxActivity.setBusinessAddressCity(rs.getString("CITY"));
						businessTaxActivity.setBusinessAddressState(rs.getString("STATE"));
						businessTaxActivity.setBusinessAddressZip(rs.getString("ZIP"));
						businessTaxActivity.setBusinessAddressZip4(rs.getString("ZIP4"));					
						return lsoId;
				    }
				});
			}catch(EmptyResultDataAccessException e) {
				logger.error("Unable to get the data from V_ADDRESS_LIST "+e);
				return 0;
			}
			return lsoId;			
		}
		
		/**
		 * populate the team
		 * 
		 * @param form
		 * @return
		 */
		public ProcessTeamForm populateTeam(ProcessTeamForm form) throws Exception {
			String psaId = form.getPsaId();
			String psaTypeId = form.getPsaType();

			if ((psaId == null) || psaId.equals("")) {
				return new ProcessTeamForm();
			}

			if ((psaTypeId == null) || psaTypeId.equals("")) {
				return new ProcessTeamForm();
			}

			try {
				//Wrapper db = new Wrapper();
				List list = new ArrayList();

				String sql = "SELECT PT.LEAD, PT.USERID, PT.GROUP_ID, pt.created_by,pt.created,pt.updated_by,pt.updated,R.NAME, U.FIRST_NAME, U.LAST_NAME FROM PROCESS_TEAM PT, GROUPS R, USERS U WHERE PT.PSA_ID = " + psaId + " AND PT.PSA_TYPE = '" + psaTypeId + "' AND PT.USERID = U.USERID AND PT.GROUP_ID = R.GROUP_ID order by PT.LEAD desc,pt.created desc";
				logger.debug("Process Team SQL : " + sql);

				//RowSet rs = db.select(sql);
				list = jdbcTemplate.query(sql,new RowMapper<ProcessTeamRecord>() {
			        @Override
			        public ProcessTeamRecord mapRow(ResultSet rs, int i) throws SQLException {
			        	ProcessTeamRecord rec = new ProcessTeamRecord();
						rec.setLead(rs.getString("LEAD"));
						rec.setName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
						rec.setNameId(rs.getString("USERID"));
						rec.setTitle(rs.getString("NAME"));
						rec.setTitleId(rs.getString("GROUP_ID"));
						rec.setIsNew(false);
						rec.setCreatedBy(rs.getInt("created_by"));
						rec.setCreated(rs.getDate("created"));
						rec.setUpdatedBy(rs.getInt("updated_by"));
						rec.setUpdated(rs.getDate("updated"));
						return rec;			        	
			        }
				});
				

				ProcessTeamRecord[] tmp = new ProcessTeamRecord[list.size()];
				list.toArray(tmp);

				form.setProcessTeamRecord(tmp);

				ProcessTeamCollectionRecord[] tmpNew = new ProcessTeamCollectionRecord[0];
				form.setProcessTeamCollectionRecord(tmpNew);				
				
				return form;
			} catch (Exception e) {
				logger.warn("Exception in method populateTeam. message - " + e.getMessage());
				throw e;
			}

		}
		/**
		 * gets the list of project id for activity id
		 * 
		 * @return
		 * @throws Exception
		 */
		public int getProjectNameId(String psaType, int psaId) throws Exception {
			logger.debug("getProjectNameId(" + psaType + "," + psaId + ")");
	        String sql="";
			int projectNameId = -1;

			try {
				if (psaType.equalsIgnoreCase("P")) {
					sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(" + psaId + "))";
				} else if (psaType.equalsIgnoreCase("Q")) {
					sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (" + psaId + ")))";
				} else if (psaType.equalsIgnoreCase("A")) {
					sql = "select pname_id from lkup_pname where name in (select name from project where proj_id in(select proj_id from sub_project where sproj_id in (select sproj_id from activity where act_id=" + psaId + ")))";
				} else {
					logger.error("Level unidentified " + psaType);
					sql="";
				}

				logger.debug(sql);
	           if(!sql.equals(""))
	           {
					//RowSet rs = new Wrapper().select(sql);	        	   
	        	   projectNameId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
					    @Override
					    public Integer mapRow(ResultSet rs, int i) throws SQLException {
					    	int projectNameId = 0;					    	
					    	return projectNameId = rs.getInt("pname_id");
					    }
	        	   });		
					
					//rs.close();
	           }
	           
				return projectNameId;
			} catch (Exception e) {
				logger.error(e+e.getMessage());
				throw e;
			}
		}
		
		/**
		 * Gets the user object for the given user id.
		 * 
		 * @param userId
		 * @return
		 * @throws Exception
		 */
		public User getUser(String userName) throws Exception {
			User user = new User();

			try {
				logger.debug("entered into commonRepo getUser() with userName = " + userName);

				/*Wrapper db = new Wrapper();
				RowSet userRs = db.select(*/
				String sql = "select * from users where username = '" + userName+"'";
				
				logger.debug(sql);

				user = jdbcTemplate.query(sql, new ResultSetExtractor<User>() {                
			        @Override
			        public User extractData(ResultSet userRs) throws SQLException, DataAccessException {    
			        	User user = new User();
					            while(userRs.next()) {
							// user = new User();
							user.setUserId(userRs.getInt("userid"));
							logger.debug(" userid  is set to  " + user.getUserId());
							user.setUsername(userRs.getString("username"));
							logger.debug("  Username  is set to  " + user.getUsername());
							user.setFirstName(userRs.getString("first_name"));
							logger.debug("  FirstName  is set to  " + user.getFirstName());
							user.setLastName(userRs.getString("last_name"));
							logger.debug(" LastName  is set to  " + user.getLastName());
							user.setMiddleInitial(userRs.getString("mi"));
							logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
							user.setEmployeeNumber(userRs.getString("emp_no"));
							logger.debug(" EmployeeNumber  is set to  " + user.getEmployeeNumber());
							user.setTitle(userRs.getString("title"));
							logger.debug(" Title  is set to  " + user.getTitle());
							user.setPassword(userRs.getString("password"));
							// getting the Department of Created User
							int deptId = userRs.getInt("dept_id");
							user.setDepartment(getDepartment(deptId));
							logger.debug("set department to user");
		
							Role role = getRole(userRs.getInt("role_id"));
							logger.debug("obtained role object");
							user.setRole(role);
							logger.debug("set role to user  " + user.getRole().getRoleId());
							user.setGroups(getUserGroups(user.getUserId()));
							user.setIsSupervisor(isSupervisor(user.getUserId()));
							logger.debug("set role to user  " + user.getIsSupervisor());
							user.setActive(userRs.getString("active"));
							logger.debug("set active to user  " + user.getActive());
							user.setUserEmail(userRs.getString("email_id"));
							logger.debug("set active to user  " + user.getUserEmail());
							user.setHomePage(userRs.getString("homepage"));
							if (user.getHomePage() == null) {
								user.setHomePage("HOME");
							}
							logger.debug("set homepage of user to  " + user.getHomePage());
						}
						return user;
			        }
				});

			} catch (Exception e) {
				logger.error(" Exception in userAgent -- getUser() method " + e.getMessage());
				throw e;
			}	

			return user;
		}
		
		/**
		 * gets the role object for a given role id.
		 * 
		 * @param roleId
		 * @return
		 */
		public Role getRole(int roleId) {
			Role role = null;

			try {
				role = new Role();

				String sql = "select * from roles where role_id=" + roleId;
				logger.debug(sql);

				//RowSet roleRs = new Wrapper().select(sql);
				role = jdbcTemplate.query(sql, new ResultSetExtractor<Role>() {                
			        @Override
			        public Role extractData(ResultSet roleRs) throws SQLException, DataAccessException { 
			        	Role role = new Role();
			        	 while(roleRs.next()) {
				        	role.setRoleId(roleId);
							role.setDescription(roleRs.getString("description"));
							logger.debug("setting role description");
			        	 }
						return role;
			        }
				});
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return role;
		}
		
		/**
		 * Gets the user groups for a given user id.
		 * 
		 * @param userId
		 * @return
		 */
		public List getUserGroups(int userId) {
			List userGroups = new ArrayList();

			try {
				String sql = "select g.* from groups g,user_groups ug where g.group_id = ug.group_id and ug.user_id = " + userId;
				logger.debug(sql);

				//RowSet rs = new Wrapper().select(sql);
				try {
					///RowSet rs = new Wrapper().select(sql);
					
					userGroups = jdbcTemplate.query(sql,new RowMapper<Group>() {
				        @Override
				        public Group mapRow(ResultSet rs, int i) throws SQLException { 
				        	Group group = new Group();
							group.setGroupId(rs.getInt("group_id"));
							group.setName(rs.getString("name"));
							group.setDescription(rs.getString("description"));    	
				        	return group;
				        }
					});
					
					return userGroups;
				} catch (Exception e) {
					logger.error("Error in getActivitySubType " + e.getMessage());
					throw e;
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return userGroups;
		}
		
		/**
		 * checks if the given user id is a supervisor user id.
		 * 
		 * @param userId
		 * @return
		 */
		public boolean isSupervisor(int userId) {
			boolean isSupervisor = false;			
			try {				
				
				String sql = "SELECT COUNT(*) FROM user_groups where group_id=6 and user_id=" + userId;
				logger.debug("isEmailIdExists query = "+sql);				
			    
			    int count = jdbcTemplate.queryForObject(sql, new Object[] { }, Integer.class);
			    logger.debug("count = "+count);
			    if (count > 0) {
			    	isSupervisor = true;
			    }			    
				
			} catch (Exception e) {
				logger.error("Exception in getLsoIdForPsaId of LookupAgent - " + e.getMessage());

				throw e;
			}

			return isSupervisor;
		}

		public void saveProcessTeam(String psaId, String psaType, int userId, ProcessTeamRecord rec) {
			logger.debug("PsaId " + psaId + ",psaType " + psaType + ", userid " + userId);
			String sql;
			String projectId = "";
			int projCount = 0;

			try {
				//Wrapper db = new Wrapper();
				//db.beginTransaction();
				sql = processRecord(psaType, psaId, userId, rec);
				//db.addBatch(sql);

				/*if (psaType.equalsIgnoreCase("A")) {
					projectId = getProjectId(psaId);
					projCount = countProjTeam(projectId, rec.getNameId(), rec.getTitleId());
					logger.debug("The projcount is " + projCount);
				}*/
				
				jdbcTemplate.update(sql);
				//db.executeBatch();

			} catch (Exception e) {
				logger.error("Exception Occured in " + e.getMessage());

			}
		}
		
		/**
		 * Process the team record
		 * 
		 * @param psaType
		 * @param psaId
		 * @param userId
		 * @param rec
		 * @return
		 */
		public String processRecord(String psaType, String psaId, int userId, ProcessTeamRecord rec) {
			logger.info("processRecord(" + psaType + ", " + psaId + ", " + userId + ", ProcessTeamRecord )");

			String sql = "";

			if (!rec.getCheckRemove().equals("on")) {
				// insert if it is a new record
				if (rec.getIsNew()) {
					sql = "insert into process_team values (" + psaId + "," + StringUtils.checkString(psaType) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getNameId())) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getTitleId())) + "," + StringUtils.checkString(rec.getLead().trim()) + "," + userId + "," + "current_timestamp" + "," + userId + "," + "current_timestamp" + ")";
				} else {
					// update if it is an old record
					String lead = rec.getLead().trim();
					if (lead.equals(""))
						lead = "of";

					sql = "update process_team set lead=" + StringUtils.checkString(lead) + ", updated_by=" + userId + ", updated=current_timestamp where psa_id= " + psaId + " and userid=" + rec.getNameId() + " and group_id=" + rec.getTitleId();
				}
			} else {
				// case for records to be deleted
				sql = "delete from process_team where psa_id = " + psaId + " and userid = " + rec.getNameId() + " and group_id = " + rec.getTitleId();
			}

			logger.debug(sql);

			return sql;
		}
		
		/**
		 * gets the list of department Ids
		 * 
		 * @param userId
		 * @return dept Id
		 * @author Hemavathi
		 * @throws Exception
		 */
		public int getDepartmentId(int userId) throws Exception {
			logger.debug("getProjectNames()");

			//List projectNames = new ArrayList();
			String sql = "";
			int deptId = 0;
			try {
				sql = "SELECT dept_id FROM users where userid=" + userId;
				logger.debug(sql);
				//RowSet rs = new Wrapper().select(sql);
				deptId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
				    	int deptId = 0;				    	
				    	deptId = rs.getInt("dept_id");
				    	return deptId;
				    }
				});				

				
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
			return deptId;
		}
		
		/**
		 * records inserted into bl approval
		 * 
		 * @param userId
		 *            ,activity_id,deptId
		 * @author Hemavathi
		 * @throws Exception
		 */
		public void insertIntoBTapproval(int activityId, int deptId, int userId) throws Exception {

			String sql = "";
			//Wrapper db = new Wrapper();

			try {
				//int approvalId = db.getNextId("APPROVAL_ID");
				NextId nextId  = nextIdRepository.getNextId("APPROVAL_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("APPROVAL_ID");
				 nextIdRepository.save(nextId);
				 int approvalId = nextId.getIdValue();
				 
				logger.debug("approval id obtained is " + approvalId);
				sql = "INSERT INTO BL_APPROVAL (APPROVAL_ID,REFERRED_DT,BL_APPR_ST,APPROVED_BY,CREATED,CREATED_BY,ACT_ID,DEPT_ID) values (";
				sql += approvalId;
				sql += ",";
				sql += "current_date";
				sql += ",";
				sql += "100"; // pending status //TODO: Get this from the constants --AB/SV Oct 12,2007
				sql += ",";
				sql += userId;
				sql += ",";
				sql += "current_timestamp";
				sql += ",";
				sql += userId;
				sql += ",";
				sql += activityId;
				sql += ",";
				sql += deptId;
				sql += ")";
				logger.info(sql);
				jdbcTemplate.update(sql);
			} catch (Exception e) {
				logger.error("Exception occured while saving activity team" + e.getMessage());
				throw new Exception("Exception occured while saving activity team " + e.getMessage());
			}
		}
		
		/**
		 * Get APN for lso ID.
		 * 
		 * @param lso
		 *            id
		 * @return
		 * @throws Exception
		 */
		public String getApnForLsoId(String lsoId) throws Exception {
			logger.debug("getApnForLsoId(" + lsoId + ")");

			String apn = "";

			try {
				String sql = "select * from lso_apn where lso_id =" + lsoId;
				logger.info(sql);

				//RowSet rs = new Wrapper().select(sql);
				apn = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String apn = "";
				    	apn = StringUtils.apnWithHyphens(rs.getString("apn"));
				    	return apn;
				    }
				});				
			} catch (Exception e) {
				logger.error(e.getMessage());
				return apn;
			}
			return apn;
		}
		
		/**
		 * Get Inspector for APN.
		 * 
		 * @param apnNo
		 * @return inspetorId
		 * @throws Exception
		 */
		public int getInspectorForApn(String apnNo) throws Exception {
			logger.debug("getInspectorForApn(" + apnNo + ")");

			int inspectorId = 0;
			try {

				String sql = "select * from lkup_apn_inspector where apn =" + StringUtils.checkString(apnNo);
				logger.info(sql);

				//RowSet rs = new Wrapper().select(sql);
				inspectorId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
				    	int inspectorId = 0;
				    	inspectorId = rs.getInt("user_id");
				    	return inspectorId;
				    }
				});
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				return 0;
			}
			return inspectorId;
		}
		
		/**
		 * Function to get a user For given userId/inspectorId
		 */
		public String getInspectorUser(int inspetorId) {

			String inspectorUser = null;
			ResultSet rs = null;

			try {
				if (inspetorId != 0) {
					String sql = "SELECT USERID, FIRST_NAME, LAST_NAME FROM  USERS WHERE USERID = " + inspetorId;

					logger.debug("inspector user for userId " + sql);

					//rs = new Wrapper().select(sql);
					
					inspectorUser = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
					    @Override
					    public String mapRow(ResultSet rs, int i) throws SQLException {
					    	String inspectorUser = (rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
							logger.debug("User/Inspector Name " + inspectorUser);
							return inspectorUser;
					    }
					});
					
				}
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return inspectorUser;
		}
		
		/**
		 * Gets the list of BT Quantity type
		 * 
		 * @author Gayathri
		 * @return List of Quantity Types
		 */
		public QuantityType getBTQuantityType(int quantityId) throws Exception {
			logger.debug("getQuantityType(" + quantityId + ")");

			QuantityType quantityType = null;

			try {
				String sql = "select * from lkup_bt_qty where id=" + quantityId;
				logger.debug(sql);

				quantityType = jdbcTemplate.query(sql, new ResultSetExtractor<QuantityType>() {                
			        @Override
			        public QuantityType extractData(ResultSet rs) throws SQLException, DataAccessException {    
			        	QuantityType qtyType = null;
						if (rs != null && rs.next()) {
							int id = rs.getInt("id");
							String description = StringUtils.properCase(rs.getString("description"));
							qtyType = new QuantityType(id, description);
						}
						return qtyType;
			        }
				});

				//rs.close();

				return quantityType;
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
		}

		/**
		 * Gets the activity address for a given activity id
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public String getActivityAddressForId(int activityId) throws Exception {
			logger.info("getActivityAddressForId(" + activityId + ")");

			String address = "";

			int locationType = -1;
			try {
				locationType = getActivityNonLocationalType(activityId);
				if (locationType == 1) {
					address = getActivityAddressForAddress(activityId);
				} else if (locationType == 2) {
					address = getActivityAddressForCrossStreet(activityId);
				} else if (locationType == 3) {
					address = getActivityAddressForRange(activityId);
				} else if (locationType == 4) {
					address = getActivityAddressForLandmark(activityId);
				}
				return StringUtils.properCase(address);
			} catch (Exception e) {
				logger.debug("Exception thrown while getting activity address " + e.getMessage());
				throw e;
			}
		}
		
		/**
		 * Get Activity Non Location Type for Activity Id
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public int getActivityNonLocationalType(int activityId) throws Exception {
			logger.info("getActivityNonLocationalType(" + activityId + ")");

			String sql = "select location_type from activity where act_id=" + activityId;
			logger.debug(sql);
			int locationType = -1;
			try {
				logger.info(sql);
				locationType = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
				    @Override
				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
					    int locType = 0;
					    if (rs.next()) {
					    	locType = rs.getInt("location_type");
						}
						return locType;		    
				    }
				});		
				
			} catch (Exception e) {
				logger.debug("Exception thrown while executing getActivityNonLocationalType() " + e.getMessage());
				throw e;
			}
			return locationType;	
		}

		/**
		 * Gets the activity address for a given activity id address
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public String getActivityAddressForAddress(int activityId) throws Exception {
			logger.info("getActivityAddressForAddress(" + activityId + ")");

			String address = "";
			String sql = "select dl_address as address from v_activity_address where act_id=" + activityId;

			try {
				logger.info(sql);
				address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address = "";
						if (rs.next()) {
							address = rs.getString(1);
						}
						return address;
				    }
				});
				
			} catch (Exception e) {
				logger.debug("Exception thrown while getting getActivityAddressForIdAddress " + e.getMessage());
				throw e;
			}
			return address;
		}
		
		/**
		 * Gets the activity address for a given activity id cross street
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public String getActivityAddressForCrossStreet(int activityId) throws Exception {
			logger.info("getActivityAddressForCrossStreet(" + activityId + ")");

			String address = "";
			String sql = "select vsl1.street_name || '  & ' || vsl2.street_name as address from v_street_list vsl1,v_street_list vsl2 where vsl1.street_id = (select street_id1 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + ")) and vsl2.street_id=(select street_id2 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + "))";

			try {
				logger.info(sql);
				address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							address = rs.getString(1);
						}
						return address;
				    }
				});
				
			} catch (Exception e) {
				logger.debug("Exception thrown while getting activity getActivityAddressForCrossStreet " + e.getMessage());
				throw e;
			}
			return address;
		}
		
		/**
		 * Gets the activity address for a given activity id address range
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public String getActivityAddressForRange(int activityId) throws Exception {
			logger.info("getActivityAddressForRange(" + activityId + ")");

			String address = "";
			String sql = "";
			String sql1 = "select aar.FROM_STR_NO1 || ' - ' || aar.TO_STR_NO1 ||' '|| vsl.street_name as address,aar.street_id1 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id1 where a.act_id=" + activityId;
			String sql2 = "select aar.FROM_STR_NO2 || ' - ' || aar.TO_STR_NO2 ||' '|| vsl.street_name as address,aar.street_id2 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id2 where a.act_id=" + activityId;
			String sql3 = "select aar.FROM_STR_NO3 || ' - ' || aar.TO_STR_NO3 ||' '|| vsl.street_name as address,aar.street_id3 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id3 where a.act_id=" + activityId;
			String sql4 = "select aar.FROM_STR_NO4 || ' - ' || aar.TO_STR_NO4 ||' '|| vsl.street_name as address,aar.street_id4 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id4 where a.act_id=" + activityId;
			String sql5 = "select aar.FROM_STR_NO5 || ' - ' || aar.TO_STR_NO5 ||' '|| vsl.street_name as address,aar.street_id5 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id5 where a.act_id=" + activityId;
			//Wrapper db = new Wrapper();
			try {
				logger.info(sql);

				//RowSet rs = db.select(sql1);
				address = jdbcTemplate.queryForObject(sql1,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							address = rs.getString(1);
						}
						return address;
				    }
				});
				//rs = db.select(sql2);
				address = jdbcTemplate.queryForObject(sql2,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							if (!(rs.getString("street_id2").equalsIgnoreCase("-1"))) {
								address = address + " , " + rs.getString("address");
							}
						}
						return address;
				    }
				});
				//rs = db.select(sql3);
				address = jdbcTemplate.queryForObject(sql3,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							if (!(rs.getString("street_id3").equalsIgnoreCase("-1"))) {
								address = address + " , " + rs.getString("address");
							}
						}
						return address;
				    }
				});
				//rs = db.select(sql4);
				address = jdbcTemplate.queryForObject(sql4,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							if (!(rs.getString("street_id4").equalsIgnoreCase("-1"))) {
								address = address + " , " + rs.getString("address");
							}
						}
						return address;
				    }
				});
				//rs = db.select(sql5);
				address = jdbcTemplate.queryForObject(sql5,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							if (!(rs.getString("street_id5").equalsIgnoreCase("-1"))) {
								address = address + " , " + rs.getString("address");
							}
						}
						return address;
				    }
				});
				
			} catch (Exception e) {
				logger.debug("Exception thrown while getting activity getActivityAddressForRange " + e.getMessage());
				throw e;
			}
			return address;
		}

		/**
		 * Gets the activity address for a given activity id address landmark
		 * 
		 * @param activityId
		 * @return
		 * @throws Exception
		 */
		public String getActivityAddressForLandmark(int activityId) throws Exception {
			logger.info("getActivityAddressForLandmark(" + activityId + ")");

			String address = "";
			String sql = "select landmark_name as address from lkup_landmark where addr_id = (select addr_id from activity where act_id=" + activityId + ")";

			try {
				logger.info(sql);
				//RowSet rs = new Wrapper().select(sql);
				address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				    @Override
				    public String mapRow(ResultSet rs, int i) throws SQLException {
				    	String address="";
						if (rs.next()) {
							address = rs.getString(1);
						}
						return address;
				    }
				});
				
			} catch (Exception e) {
				logger.debug("Exception thrown while getting activity getActivityAddressForLandmark " + e.getMessage());
				throw e;
			}
			return StringUtils.properCase(address);
		}

		public MultiAddress[] getMultiAddress(int activityId,String flag) throws Exception {
			logger.info("getMultiAddress(" + activityId +  ")");
			MultiAddress[] multiAddressArray = null;
			//RowSet rs = null;
			//RowSet rs1 = null;
			List<MultiAddress> multiAddressList = new ArrayList<MultiAddress>();
			String sqlQuery ="select * from LKUP_ADDRESS_TYPE  where ";
			if(flag.equalsIgnoreCase("BT")){
				sqlQuery =sqlQuery+ "FLAG_BT='Y'  order by ID asc";
				}else if(flag.equalsIgnoreCase("BL")){
					sqlQuery =sqlQuery+ "FLAG_BL='Y'  order by ID asc";
				}
			
			try {		
				String sql = "";
				sql = "select MA.*,LAT.ADDRESS_TYPE,LAT.ID AS ADDRESS_TYPE_ID from MULTI_ADDRESS MA left outer join LKUP_ADDRESS_TYPE LAT on LAT.ID=MA.ADDRESS_TYPE_ID"
						+ " where ";
				//sql =sql+ "LAT.FLAG_"+flag+"='Y'";
				if(flag.equalsIgnoreCase("BT")){
				sql =sql+ "LAT.FLAG_BT='Y'";
				}else if(flag.equalsIgnoreCase("BL")){
				sql =sql+ "LAT.FLAG_BL='Y'";
				}
				sql =sql+ "and MA.ACT_ID="+ activityId+" order by MA.ID asc";
				logger.info(sql);
				
				
				List<MultiAddress> multiAddressListData = new ArrayList<MultiAddress>();
				multiAddressList = jdbcTemplate.query(sql,new RowMapper<MultiAddress>() {
			        @Override
			        public MultiAddress mapRow(ResultSet rs, int i) throws SQLException { 
			        	MultiAddress multiAddress = new MultiAddress();
						multiAddress.setId(rs.getString("ADDRESS_TYPE_ID"));
						multiAddress.setAddressType(rs.getString("ADDRESS_TYPE"));
						multiAddress.setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
						multiAddress.setTitle(StringUtils.nullReplaceWithEmpty(rs.getString("TITLE")));
						multiAddress.setStreetNumber(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NUMBER")));
						multiAddress.setStreetName1(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NAME1")));
						multiAddress.setStreetName2(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NAME2")));
						multiAddress.setAttn(StringUtils.nullReplaceWithEmpty(rs.getString("ATTN")));
						multiAddress.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
						multiAddress.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
						multiAddress.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
						multiAddress.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
						multiAddress.setZip4(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP4")));      	
			        	return  multiAddress;
			        }
				});

				
				if(multiAddressList !=null || !multiAddressList.isEmpty()){
					logger.debug("sql query :"+sqlQuery);
					boolean addressType =false;
					
					multiAddressList = jdbcTemplate.query(sqlQuery,new RowMapper<MultiAddress>() {
				        @Override
				        public MultiAddress mapRow(ResultSet rs1, int i) throws SQLException { 
				        	boolean addressType =false;
				        	MultiAddress multiAddress = null;
				        	List<MultiAddress> multiAddressList = new ArrayList<MultiAddress>();
				        	if(rs1 !=null){
								/*while (rs1.next()) {*/
									for(int j=0; j<multiAddressList.size(); j++){
								if(multiAddressList.get(i).getId().equalsIgnoreCase(rs1.getString("ID"))){
									addressType=true;
								}
								logger.debug(" ID 216 :"+rs1.getString("ID")+" addressType :"+addressType);
								}
									if(!addressType){	
									multiAddress = new MultiAddress();
									multiAddress.setId(rs1.getString("ID"));
									multiAddress.setAddressType(rs1.getString("ADDRESS_TYPE"));
									multiAddress.setName("");
									multiAddress.setTitle("");
									multiAddress.setStreetNumber("");
									multiAddress.setStreetName1("");
									multiAddress.setStreetName2("");
									multiAddress.setAttn("");
									multiAddress.setUnit("");
									multiAddress.setCity("");
									multiAddress.setState("");
									multiAddress.setZip("");
									multiAddress.setZip4("");
									//multiAddressList.add(multiAddress);
									}
									addressType=false;
								/*}*/
							}
							return multiAddress;
				        	
				        }
					});		
					
				}
				
				if(multiAddressList ==null || multiAddressList.isEmpty()){
				logger.debug("sql query :"+sqlQuery);
				
				multiAddressList = jdbcTemplate.query(sqlQuery,new RowMapper<MultiAddress>() {
			        @Override
			        public MultiAddress mapRow(ResultSet rs1, int i) throws SQLException {
			        	MultiAddress multiAddress = null;
					if(rs1 !=null){
						logger.debug("in loop");
						while (rs1.next()) {
							multiAddress = new MultiAddress();
							multiAddress.setId(rs1.getString("ID"));
							multiAddress.setAddressType(rs1.getString("ADDRESS_TYPE"));
							multiAddress.setStreetNumber("");
							multiAddress.setStreetName1("");
							multiAddress.setStreetName2("");
							multiAddress.setAttn("");
							multiAddress.setUnit("");
							multiAddress.setCity("");
							multiAddress.setState("");
							multiAddress.setZip("");
							multiAddress.setZip4("");
							//multiAddressList.add(multiAddress);
						}
					}
					return multiAddress;
			        }
				
			        });
				}
				multiAddressArray = new MultiAddress[multiAddressList.size()];
				for(int i=0;i<multiAddressList.size();i++){
					multiAddressArray[i] = multiAddressList.get(i);
				}
	 
			}catch (Exception e) {
				logger.debug("Error in getmulti address.. "+e+e.getMessage());
				e.printStackTrace();
			}
			return multiAddressArray;
		}
		/**
		 * get codes for Class Code
		 * 
		 * @param Class
		 *            Code
		 * @return
		 * @throws Exception
		 */
		public Map getCodesForClassCode(String classCode) throws Exception {
			logger.debug("getCodesForClassCode(" + classCode + ")");

			Map codes = new HashMap();
			String sql = "";
			sql = "select * from lkup_act_type where upper(CLASS_CODE)=" + StringUtils.checkString(classCode.toUpperCase());
			logger.debug(sql);

			try {

				//RowSet rs = new Wrapper().select(sql);
				
				codes= jdbcTemplate.query(sql, new ResultSetExtractor<Map<String, String>>() {                
			        @Override
			        public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {    
			            Map<String, String> info = new HashMap<String, String>();
			            if (rs != null && rs.next()) {
			            	info.put("TYPE", StringUtils.nullReplaceWithEmpty(rs.getString("TYPE")));
			            	info.put("MUNI_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
			            	info.put("SIC_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
			            	info.put("CLASS_CODE", StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));                   
			            }            
			            return info;
			        }
		    	});

				
			} catch (Exception e) {
				logger.error("Error in getCodes " + e.getMessage());
				throw e;
			}
			return codes;
		}

		/**
		 * This method is to get BlBtOnlineApplicationFlag   
		 * is submitted based on the renewal code or activity Id.
		 * @param String
		 * @throws Exception
		 * @return String
		 * 
		 */
		public String getBlBtOnlineApplicationFlag(Activity activity) {
			String sql = "SELECT A.APPLICATION_ONLINE FROM ACTIVITY A LEFT JOIN BL_ACTIVITY BL ON BL.ACT_ID = A.ACT_ID LEFT JOIN BT_ACTIVITY BT ON BT.ACT_ID = A.ACT_ID WHERE A.ACT_ID = "+activity.getRenewalCode()+" AND (BT.BUSINESS_ACC_NO = "+activity.getBusinessAccNo() + " OR BL.BUSINESS_ACC_NO = " + activity.getBusinessAccNo() + ")";
					    
			logger.debug("getActivityRenewalOnline sql :"+sql);
			String onlineFlag = "N";
			try {
				onlineFlag=jdbcTemplate.query(sql,new ResultSetExtractor<String>() {
				    @Override
				    public String extractData(ResultSet rs) throws SQLException {
						String onlineFlag = "N";
				    	if (rs.next()) {
				    		onlineFlag=rs.getString("APPLICATION_ONLINE");
				    	}
						return onlineFlag;	
				    }
				});
			} catch(EmptyResultDataAccessException e1) {
				logger.error("",e1);
			}catch (Exception e) {
				logger.error("",e);
			}	
			logger.debug("onlineFlag... "+onlineFlag);
			return onlineFlag;
		}


		//Generates a random int with n digits
		public static int generateRandomDigits(int n) {
		 int m = (int) Math.pow(10, n - 1);
		 return m + new Random().nextInt(9 * m);
		}

		public User saveUserIfnotExistBasedonEmailAddr(String emailAddr) throws Exception {
			logger.debug("In SaveUserIfnotExistBasedonEmailAddr");
			String sql1 = null;
			String sql2 = null;
			int extUserId = 0;
			User user = null;
			try {
				sql1 = "SELECT * FROM EXT_USER WHERE EXT_USERNAME='"+emailAddr+"'";
				logger.debug("sql1 "+sql1);
				
				
				user= jdbcTemplate.query(sql1, new ResultSetExtractor<User>() {                
			        @Override
			        public User extractData(ResultSet rs) throws SQLException, DataAccessException {    
			    		User user = new User();
						if (rs.next()) {
							user.setUserId(StringUtils.s2i(StringUtils.d2s(rs.getDouble("EXT_USER_ID"))));
							user.setAccountNbr(rs.getString("EXT_ACCT_NBR"));
							logger.debug(" userid  is set to  " + user.getUserId());
							user.setUsername(rs.getString("EXT_USERNAME"));
							logger.debug("  Username  is set to  " + user.getUsername());
							user.setFirstName(StringUtils.nullReplaceWithEmpty(rs.getString("FIRSTNAME")));
							logger.debug("  FirstName  is set to  " + user.getFirstName());
							user.setLastName(StringUtils.nullReplaceWithEmpty(rs.getString("LASTNAME")));
							logger.debug(" LastName  is set to  " + user.getLastName());
							user.setMiddleInitial("");
							logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
							user.setTitle(rs.getString("EXT_USER_TYPE"));
							user.setPassword(rs.getString("ext_password"));
							user.setIsObc("Y");
							user.setActive("Y");
			        }            
			            return user;
			        }
		    	});

				logger.debug("user... "+user);
				if(user.getUserId() == 0) {
					logger.debug("email address doesn't exist");

					 NextId nextId  = nextIdRepository.getNextId("EXT_USER_ID");
					 nextId.setIdValue((nextId.getIdValue()) + 1);
					 nextId.setIdName("EXT_USER_ID");
					 nextIdRepository.save(nextId);
					 extUserId = nextId.getIdValue();
					 
					sql2 =" INSERT INTO EXT_USER (EXT_USER_ID, EXT_ACCT_NBR, EXT_USERNAME, EXT_PASSWORD, PIN_EXP_DT, CREATED, CREATED_BY, EXT_USER_TYPE, FIRSTNAME, MIDDLENAME, LASTNAME, OBC, DOT, ACTIVEDOT, ACTIVEOBC, CITY, STATE, ZIP,EMAIL_VERIFIED) VALUES ("+extUserId+", 0, '"+emailAddr+"', 'X6ElgaKCqETvM', null, CURRENT_DATE, null, 0, 'ONLINE USER', null, '', 'Y', 'Y', 'Y', 'Y', 'Burbank', 'Ca', '91504','Y')";
					logger.debug("sql2... "+sql2);
					
					if (user == null) {
						user = new User();
						user.setUserId(StringUtils.s2i(StringUtils.d2s(extUserId)));
						user.setAccountNbr("0");
						logger.debug(" userid  is set to  " + user.getUserId());
						user.setUsername(emailAddr);
						logger.debug("  Username  is set to  " + user.getUsername());
						user.setFirstName(StringUtils.nullReplaceWithEmpty("Online"));
						logger.debug("  FirstName  is set to  " + user.getFirstName());
						user.setLastName(StringUtils.nullReplaceWithEmpty("User"));
						logger.debug(" LastName  is set to  " + user.getLastName());
						user.setMiddleInitial("");
						logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
						user.setTitle("0");
						user.setPassword("X6ElgaKCqETvM");
					}
					user.setIsObc("Y");
					user.setActive("Y");
					
					jdbcTemplate.execute(sql2);
				}
			} catch (Exception e) {
				logger.error("Exception thrown while trying to add online Permit  :" + e+e.getMessage());
				throw e;
			}
			return user;
		}
		
		public void updateEmailVerified(String emailAddress) throws Exception {
			logger.debug("In updateEmailVerified");
			String sql = null;
			String updateSql = null;
			try {
				sql = "SELECT * FROM EXT_USER WHERE EXT_USERNAME='"+emailAddress+"'";
				logger.debug("sql1 "+sql);

				 String userId = jdbcTemplate.query(sql,new ResultSetExtractor<String>() {
					@Override
					public String extractData(ResultSet rs) throws SQLException, DataAccessException {
						String userId="";
						if (rs.next()) {
							userId =rs.getString("EXT_USER_ID");
						}
						return userId;					}
				});
				logger.debug("userId.." +userId);

				if(userId != null) {
					updateSql="UPDATE EXT_USER SET EMAIL_VERIFIED='Y' WHERE EXT_USER_ID="+userId;
					logger.debug("updateSql... "+updateSql);
					jdbcTemplate.update(updateSql);
				}			
			} catch (Exception e) {
				logger.error("Exception thrown while trying to updateSql  :" + e+e.getMessage());
				throw e;
			}
		}

		public int checkProjectName(String ProjectName, int lsoId) throws Exception {
			logger.info("checkProjectName(" + ProjectName + ", " + lsoId + ")");
			String sql = "";
			int projectId = 0;
			sql = "SELECT PROJ_ID,DESCRIPTION  FROM PROJECT WHERE UPPER(NAME) =" + StringUtils.checkString(ProjectName).toUpperCase() + " AND LSO_ID = " + lsoId;
			logger.debug(" sql statement " + sql);		
			
			try {
				projectId = jdbcTemplate.query(sql,new ResultSetExtractor<Integer>() {
//				    @Override
//				    public Integer mapRow(ResultSet rs, int i) throws SQLException {
//				    	int projId = 0;	    	
//				    	projId = rs.getInt("PROJ_ID");
//				    	logger.debug("project Id is " + projId);
//				    	
//						return projId;
//				    }

					@Override
					public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
						int projId = 0;	    
						if(rs != null && rs.next()) {
					    	projId = rs.getInt("PROJ_ID");
					    	logger.debug("project Id is " + projId);	
						}
						return projId;
					}
				});
			}catch(EmptyResultDataAccessException e) {
				logger.debug("project id not available "+e.getMessage()+e);
				return 0;
			}

			return projectId;
		}


/**
 * @param subProjectName
 * @return pTypeId
 * @throws Exception
 * @author Gayathri & Manjuprasad
 */
public int checklkupSubProjectName(String subProjectName) throws Exception {
	logger.info("checklkupSubProjectName(" + subProjectName + ")");
	String sql = "";
	int pTypeId = 0;
	sql = "SELECT PTYPE_ID,DESCRIPTION  FROM LKUP_PTYPE WHERE UPPER(DESCRIPTION) =" + StringUtils.checkString(subProjectName);
	logger.debug(" sql statement " + sql);
	try {
		pTypeId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
		    @Override
		    public Integer mapRow(ResultSet rs, int i) throws SQLException {
		    	int pTypeId = 0;	    	
				pTypeId = rs.getInt("PTYPE_ID");
				logger.debug("pTypeId Is " + pTypeId);
		    	
				return pTypeId;
		    }
		});
	}catch(EmptyResultDataAccessException e) {
		logger.error("Unable to get the data from LKUP_PTYPE "+e.getMessage());
		return 0;
	}

	return pTypeId;
}

/**
 * @param pTypeId
 *            , projectId
 * @return subprojectId
 * @throws Exception
 * @author Gayathri
 */
public int checkActivityName(int subprojectId, String actBLType) throws Exception {
	logger.info("checkActivityName(" + subprojectId + ", " + actBLType + ")");
	String sql = "";
	int activityId = 0;

	sql = "SELECT ACT_ID FROM ACTIVITY WHERE SPROJ_ID = " + subprojectId + " AND ACT_TYPE ='" + actBLType + "'";
	logger.debug(" sql statement " + sql);
		try {
			activityId = jdbcTemplate.query(sql,new ResultSetExtractor<Integer>() {
				@Override
				public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
					int actId = 0;	    	
					if(rs.next()) {
						actId = rs.getInt("ACT_ID");
					}			
					return actId;
				}
			});
		
			return activityId;
		}catch(EmptyResultDataAccessException e) {
			logger.error("Unable to get activity id "+e);            
            return 0;
		}
	
}


/**
 * Gets the list of BL Quantity types based on activity type
 * 
 * @return
 */
public QuantityType getBLQuantityTypesBasedOnActType(String actType) throws Exception {
	logger.debug("In getBLQuantityTypesBasedOnActType..");
	QuantityType quantityType = new QuantityType();
	try {
		String sql = "SELECT * FROM REF_ACT_BL_QTY_TYPE RA LEFT JOIN LKUP_QTY LQ ON RA.LKUP_QTY_TYPE_ID=LQ.ID WHERE RA.LKUP_ACT_TYPE_ID=(SELECT TYPE_ID FROM LKUP_ACT_TYPE WHERE TYPE='"+actType+"')";
		logger.debug(sql);		
		quantityType = jdbcTemplate.query(sql,new ResultSetExtractor<QuantityType>() {
			@Override
			public QuantityType extractData(ResultSet rs) throws SQLException, DataAccessException {
			   	QuantityType quantityType = new QuantityType();
			   	if(rs.next()) {
			   		quantityType = new QuantityType(rs.getInt("LKUP_QTY_TYPE_ID"), StringUtils.properCase(rs.getString("DESCRIPTION")));
			   	}
	        	return quantityType;
			}
		});
	} catch (Exception e) {
		logger.error("Error in getBLQuantityTypesBasedOnActType"+e.getMessage()+e);
		throw e;
	}
	return quantityType;
}


/**
 * Gets the list of BT Quantity types based on activity type
 * 
 * @return
 */
public List getBTQuantityTypesBasedOnActType(String actType) throws Exception {
	logger.debug("In getBTQuantityTypesBasedOnActType..");

	List quantityTypes = new ArrayList();
	QuantityType quantityType = new QuantityType();
	try {
		String sql = "SELECT * FROM REF_ACT_BT_QTY_TYPE RA LEFT JOIN LKUP_BT_QTY LQ ON RA.LKUP_BT_QTY_TYPE_ID=LQ.ID WHERE RA.LKUP_ACT_TYPE_ID= (SELECT TYPE_ID FROM LKUP_ACT_TYPE WHERE TYPE='"+actType+"')";
		logger.debug(sql);
		quantityTypes = jdbcTemplate.query(sql,new RowMapper<QuantityType>() {
	        @Override
	        public QuantityType mapRow(ResultSet rs, int i) throws SQLException {
	        	QuantityType quantityType = null;			        
			int id = rs.getInt("id");
			String description = StringUtils.properCase(rs.getString("description"));
			quantityType = new QuantityType(id, description);
			return quantityType;
		}
		});
		logger.debug("Exiting getBtQuantityTypesBasedOnActType with quantity types list of size " + quantityTypes.size());
	} catch (Exception e) {
		logger.error(e.getMessage());
		throw e;
	}
	return quantityTypes;
}

/**
 * Gets the activity status desc for a status id.
 * 
 * @param statusId
 * @return
 * @throws Exception
 */
public String getActivityStatusDesc(String statusId) throws Exception {
	logger.debug("getActivityStatusDesc(" + statusId + ")");

	String activityStatus = null;

	try {
		String sql = "select * from lkup_act_st where status_id=" + statusId;
		logger.debug("getActivityStatusDesc -- " + sql);
		
		activityStatus = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException {    
	        	String actStatusDesc = null;
	            if(rs.next()) {
	            	actStatusDesc =rs.getString("description");	            	                  
	            }            
	            return actStatusDesc;
	        }
    	});
	} catch (Exception e) {
		logger.error("Error occured in getActivityStatusDesc :: " +e.getMessage());
    	e.printStackTrace();
	}
	return activityStatus;
}



/**
 * Gets the ownership type Desc
 * 
 * @return
 */
public String getOwnershipTypeDesc(String ownershipTypeId) throws Exception {
	logger.debug("getOwnershipTypeDesc(" + ownershipTypeId + ")");

	String ownershipType = null;

	try {
		String sql = "select * from lkup_own_type where id=" + ownershipTypeId;
		logger.debug(sql);
		
		ownershipType = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException { 
	        	String description = null;
	        	if (rs != null && rs.next()) {
					description = StringUtils.properCase(rs.getString("description"));
				}            
	            return description;
	        }
    	});	
		
	} catch (Exception e) {
		logger.error("Error occured in getOwnershipTypeDesc :: " +e.getMessage());
    	e.printStackTrace();
	}
	return ownershipType;
}

/**
 * Get the activity type description for a type code
 * 
 * @param type
 * @return
 */
public String getActivityTypeDesc(String type) throws Exception {
	logger.debug("getActivityTypeDesc(" + type + ")");
	String activityType = null;
	try {
		String sql = "select * from lkup_act_type where type='" + type.toUpperCase() + "'";
		logger.debug(sql);
		
		activityType = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException {    
	        	String desc=null;
	            if(rs.next()) {
	            	desc = StringUtils.properCase(rs.getString("description")); 
	            }            
	            return desc;
	        }
    	});			
		
	} catch (Exception e) {
		logger.error("Error occured in getActivityTypeDesc :: " +e.getMessage()+e);
    	e.printStackTrace();
	}
	return activityType;
}

/**
 * Get the activity sub type desc for a sub type id
 * 
 * @param id
 * @return
 */
public String getActivitySubTypeDesc(String id) throws Exception {
	logger.debug("getActivitySubTypeDesc(" + id + ")");

	String actSubType = null;
	String sql = "select * from lkup_act_subtype where act_subtype_id=" + id;
	try {
		actSubType = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException {    
	        	String activitySubType = null;
	        	if (rs != null && rs.next()) {
	        		activitySubType=rs.getString("act_subtype");
				}       
	            return activitySubType;
	        }
    	});		
		
	} catch (Exception e) {
		logger.error("Error in getActivitySubTypeDesc " + e.getMessage()+e);
		throw e;
	}
	return actSubType;
}



/**
 * Gets the list of Exemption Type
 * 
 * @return
 */
public String getExemptionTypeDesc(String exemptionTypeId) throws Exception {
	logger.debug("getExemptionTypeDesc(" + exemptionTypeId + ")");

	String exemptionType = null;

	try {
		String sql = "select * from lkup_exem_type where id=" + exemptionTypeId;
		logger.debug(sql);
		
		exemptionType = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException {    
	        	String description = null;
	        	if (rs != null && rs.next()) {
					description = StringUtils.properCase(rs.getString("description"));
				}
	        	return description;
	        }
    	});
		
	} catch (Exception e) {
		logger.error(e.getMessage());
		throw e;
	}
	return exemptionType;
}

/**
 * Get the list of streets
 * 
 * @param id
 * @return description
 */
public String getStreetName(String streetName) throws Exception {
	logger.debug("getStreetName(" + streetName + ")");

	String street = null;
	String sql = "select * from street_list where street_id=" + streetName;
	logger.debug(" Street List is  " + sql);

	try {
		 street = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {                
	        @Override
	        public String extractData(ResultSet rs) throws SQLException, DataAccessException {    
	        	String st = null;
	        	if (rs != null && rs.next()) {
	        		

					String prefixDirection = (rs.getString("pre_dir") != null) ? rs.getString("pre_dir") : "";
					String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
					String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
					String suffixDirection = (rs.getString("suf_dir") != null) ? rs.getString("suf_dir") : "";
					String fullStreetName = "";

					if (!("").equalsIgnoreCase(prefixDirection)) {
						fullStreetName = prefixDirection ;
					} 
					if (!("").equalsIgnoreCase(streetName)) {
						fullStreetName = fullStreetName+ " " + streetName;
					} 
					if (!("").equalsIgnoreCase(streetType)) {
						fullStreetName = fullStreetName+ " " + streetType;
					} 
					if (!("").equalsIgnoreCase(suffixDirection)) {
						fullStreetName = fullStreetName+" "+suffixDirection;
					}
					logger.debug("streetName.. "+streetName);
					st = StringUtils.properCase(fullStreetName);
				}
	        	return st;
	        }
    	});		

		return street;
	} catch (Exception e) {
		logger.error("Error in Street " + e.getMessage());
		throw e;
	}
}


public String getDefaultLkupActStatus(String moduleId) {
	String sql = "SELECT STATUS_ID FROM LKUP_ACT_ST WHERE MODULE_ID="+moduleId+" AND ONLINE_APPLICATION_STATUS='Y'";
	
	logger.debug("sql :"+sql);
	String statusId="";
	try {
		statusId = jdbcTemplate.query(sql,new ResultSetExtractor<String>() {
		@Override
		public String extractData(ResultSet rs) throws SQLException, DataAccessException {
			String statusId="";
			if(rs.next()) {
				statusId=rs.getString("STATUS_ID");	
			}
			return statusId;
		}
    });
	
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return statusId;
}

}