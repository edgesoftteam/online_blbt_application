package com.elms.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;

import com.elms.common.Constants;
import com.elms.model.ActivityType;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.NextId;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;

/**
 * @author Siva Kumari
 *
 */


@Repository
public class OnlineApplicationBLRepository {

private static final Logger logger = Logger.getLogger(OnlineApplicationBLRepository.class);
	
@Autowired
JdbcTemplate  jdbcTemplate;
@Autowired
NextIdRepository nextIdRepository;
@Autowired
PlatformTransactionManager transactionManager;
@Autowired
DataSource dataSource;
@Autowired
CommonService commonService;
@Autowired
CommonRepository commonRepository;

	public void setDataSource(DataSource dataSource) {
	    this.dataSource = dataSource;
	    this.jdbcTemplate = new JdbcTemplate(dataSource);
	 }
	 public void setTransactionManager(PlatformTransactionManager transactionManager) {
	    this.transactionManager = transactionManager;
	 }


	/**
	 * Check class codes is exists or not
	 * 
	 * @param Class Code
	 * @return
	 * @throws Exception
	 */
	public boolean isClassCodeExists(String classCode) throws Exception {
		logger.debug("isClassCodeExists(" + classCode + ")");

		boolean isClassCodeExists=false;
	
		String sql = "SELECT COUNT(*) FROM lkup_act_type where  upper(CLASS_CODE)=" + StringUtils.checkString(classCode.toUpperCase());
		logger.debug("isClassCodeExists query = "+sql);				

		try {
		    int count = jdbcTemplate.queryForObject(sql, new Object[] { }, Integer.class);
		    logger.debug("count = "+count);
		    if (count > 0) {
		    	isClassCodeExists = true;
		    }						
		} catch (Exception e) {
			logger.error("Error in getCodes " + e.getMessage());
			throw e;
		}
		return isClassCodeExists;
	}

/**
 * SaveBusinessLicenseActivity()
 * 
 * @param businesslicenseactivity
 *            , lsoId
 * @return activityId
 * @param businessLicenseActivity
 * @throws Exception
 */
public int saveBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity) throws Exception {
	logger.info("saveBusinessLicenseActivity(" + businessLicenseActivity.toString() + ")");

	int projNum = -1;
	int sprojNum = -1;
	int activityId = -1;
	int activityNum = -1;
	int subprojectId = 0;
	int projectId = 0;
	int addressId = 0;
	String activityNumber = "";
	Calendar calendar = Calendar.getInstance();
	SimpleDateFormat formatter = new SimpleDateFormat("yy");
	addressId=commonService.getAddressIdForPsaId(businessLicenseActivity.getLsoId()+"");
	try {
		//Wrapper db = new Wrapper();
		String sql = "";
		String projectSql = "";
		String subProjectSql = "";
		String activitySql = "";
		String peopleSql = "";
		String lkupPtypeSql = "";
		int businessAccNo = 0;
		int peopleId = 0;
		int pTypeId = 0;
		String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
		projectId = commonRepository.checkProjectName(projName,StringUtils.s2i(businessLicenseActivity.getLsoId()));
		logger.debug("Project Id Returned is  " + projectId);

		if (businessLicenseActivity != null) {
			if (projectId == 0) {
				/**
				 * Project Number Generation
				 */
				//projNum = db.getNextId("PR_NUM");
				
				NextId nextId  = nextIdRepository.getNextId("PR_NUM");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("PR_NUM");
				 nextIdRepository.save(nextId);
				 projNum = nextId.getIdValue();

				String projectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);

				/**
				 * Inserting records into PROJECT Table
				 */
				//projectId = db.getNextId("PROJECT_ID");
				 nextId  = nextIdRepository.getNextId("PROJECT_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("PROJECT_ID");
				 nextIdRepository.save(nextId);
				 projectId = nextId.getIdValue();
				 
				logger.debug("The ID generated for new Project is " + projectId);
				projectSql = "insert into project(PROJ_ID, LSO_ID, PROJECT_NBR, NAME, DESCRIPTION, STATUS_ID, DEPT_ID, CREATED_BY, CREATED_DT) values(";
				projectSql = projectSql + projectId;
				logger.debug("got Project id " + projectId);
				projectSql = projectSql + ",";
				projectSql = projectSql + businessLicenseActivity.getLsoId();
				logger.debug("got LSO id " + businessLicenseActivity.getLsoId());
				projectSql = projectSql + ",";
				projectSql = projectSql + StringUtils.checkString(projectNumber);
				logger.debug("got Project Number " + StringUtils.checkString(projectNumber));
				projectSql = projectSql + ",";
				projectSql = projectSql + StringUtils.checkString(projName);
				logger.debug("got Project Name " + StringUtils.checkString(projName));
				projectSql = projectSql + ",";
				projectSql = projectSql + StringUtils.checkString(projName);
				logger.debug("got Project Description " + StringUtils.checkString(projName));
				projectSql = projectSql + ",";
				projectSql = projectSql + 1;// STATUS OF ACTIVE
				logger.debug("got Status Id " + "1");
				projectSql = projectSql + ",";
				projectSql = projectSql + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES;
				logger.debug("got Department ID " + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES);
				projectSql = projectSql + ",";
				projectSql = projectSql + businessLicenseActivity.getCreatedBy();
				logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
				projectSql = projectSql + ", current_timestamp)";
				logger.info(projectSql);
				jdbcTemplate.update(projectSql);
			}

			/**
			 * Inserting records into LKUP_PTYPE Table
			 */
			String subProjectName = (Constants.SUB_PROJECT_NAME_STARTS_WITH + businessLicenseActivity.getBusinessName().trim()).toUpperCase();
			logger.debug("Generated subProjectName " + subProjectName);
			pTypeId = commonRepository.checklkupSubProjectName(subProjectName);
			logger.debug("pTypeId Returned is  " + pTypeId);

			if (pTypeId == 0) {
				//pTypeId = db.getNextId("PTYPE_ID");
				 NextId nextId  = nextIdRepository.getNextId("PTYPE_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("PTYPE_ID");
				 nextIdRepository.save(nextId);
				 pTypeId = nextId.getIdValue();				
				
				logger.debug("The ID generated for new Sub Project is " + subprojectId);
				lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
				lkupPtypeSql = lkupPtypeSql + pTypeId;
				logger.debug("got pTypeId is " + pTypeId);
				lkupPtypeSql = lkupPtypeSql + ",";
				lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase());
				logger.debug("got Description " + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase()));
				lkupPtypeSql = lkupPtypeSql + ",'";
				lkupPtypeSql = lkupPtypeSql + "LC";
				logger.debug("got Department Code " + "LC");
				lkupPtypeSql = lkupPtypeSql + "',";
				lkupPtypeSql = lkupPtypeSql + "null";
				logger.debug("got Name " + "null");
				lkupPtypeSql = lkupPtypeSql + ")";

				logger.info(lkupPtypeSql);
				jdbcTemplate.update(lkupPtypeSql);
			}

			// subprojectId = checkSubProjectName(pTypeId, projectId);
			subprojectId = 0;
			logger.debug("Sub Project Id Returned is  " + subprojectId);

			if (subprojectId == 0) {

				//sprojNum = db.getNextId("SP_NUM");
				
				 NextId nextId  = nextIdRepository.getNextId("SP_NUM");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("SP_NUM");
				 nextIdRepository.save(nextId);
				 sprojNum = nextId.getIdValue();		
				/**
				 * Sub Project Number Generation
				 */
				String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);
				/**
				 * Inserting records into SUB_PROJECT Table
				 */
				//subprojectId = db.getNextId("SUB_PROJECT_ID");
				 nextId  = nextIdRepository.getNextId("SUB_PROJECT_ID");
				 nextId.setIdValue((nextId.getIdValue()) + 1);
				 nextId.setIdName("SUB_PROJECT_ID");
				 nextIdRepository.save(nextId);
				 subprojectId = nextId.getIdValue();
				 
				logger.debug("The ID generated for new Sub Project is " + subprojectId);
				subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
				subProjectSql = subProjectSql + subprojectId;
				logger.debug("got Subproject Id " + subprojectId);
				subProjectSql = subProjectSql + ",";
				subProjectSql = subProjectSql + projectId;
				logger.debug("got Project id " + projectId);
				subProjectSql = subProjectSql + ",";
				subProjectSql = subProjectSql + pTypeId;
				logger.debug("got Sub Project Type " + pTypeId);
				subProjectSql = subProjectSql + ",";
				subProjectSql = subProjectSql + businessLicenseActivity.getCreatedBy();
				logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
				subProjectSql = subProjectSql + ", current_timestamp,";
				logger.debug("got Created ");
				subProjectSql = subProjectSql + 1;
				logger.debug("got Status " + 1);
				subProjectSql = subProjectSql + ",";
				subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
				logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
				subProjectSql = subProjectSql + ")";

				logger.info(subProjectSql);
				jdbcTemplate.update(subProjectSql);
			}

			// Checking activity existing or not
			String actBLType = businessLicenseActivity.getActivityType().getType();
			activityId = commonRepository.checkActivityName(subprojectId, actBLType);
			if (activityId != 0) {

				String inactivateSql = "update activity set status =" + Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS + " where act_id = " + activityId;
				logger.info(inactivateSql);
				jdbcTemplate.update(inactivateSql);
			}

			// Activity Id Generation
			//activityId = db.getNextId("ACTIVITY_ID");
			NextId nextId  = nextIdRepository.getNextId("ACTIVITY_ID");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("ACTIVITY_ID");
			 nextIdRepository.save(nextId);
			 activityId = nextId.getIdValue();
			 
			logger.debug("The ID generated for new Activity is " + activityId);

			// Activity Number generation
			//activityNum = db.getNextId("ACT_NUM");
			 nextId  = nextIdRepository.getNextId("ACT_NUM");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("ACT_NUM");
			 nextIdRepository.save(nextId);
			 activityNum = nextId.getIdValue();
			logger.debug("Obtained new activity number is " + activityNum);

			String strActNum = StringUtils.i2s(activityNum);
			strActNum = "00000".substring(strActNum.length()) + strActNum;
			logger.debug("Obtained new activity number string is " + strActNum);
			calendar = Calendar.getInstance();
			formatter = new SimpleDateFormat("yy");
			activityNumber = "BL" + formatter.format(calendar.getTime()) + strActNum.trim();

			/**
			 * Inserting records into ACTIVITY Table
			 * 
			 */
			activitySql = "insert into activity(ACT_ID, SPROJ_ID, ADDR_ID, ACT_NBR, PLAN_CHK_REQ, ACT_TYPE,  VALUATION, STATUS, START_DATE, APPLIED_DATE, ISSUED_DATE, PLAN_CHK_FEE_DATE, PERMIT_FEE_DATE, DEV_FEE_REQ, CREATED_BY, CREATED,APPLICATION_ONLINE) values(";

			activitySql = activitySql + activityId;
			logger.debug("got Activity id " + activityId);
			activitySql = activitySql + ",";
			activitySql = activitySql + subprojectId;
			logger.debug("got Sub Project Id " + subprojectId);
			activitySql = activitySql + ",";
			activitySql = activitySql + addressId;
			logger.debug("got Address Id " + addressId);
			activitySql = activitySql + ",";
			activitySql = activitySql + StringUtils.checkString(activityNumber);
			logger.debug("got Activity Number " + activityNumber);
			activitySql = activitySql + ",'N','";
			activitySql = activitySql + ((businessLicenseActivity.getActivityType() != null) ? businessLicenseActivity.getActivityType().getType() : "");
			logger.debug("got Activity Type " + ((businessLicenseActivity.getActivityType() != null) ? businessLicenseActivity.getActivityType().getType() : ""));
			activitySql = activitySql + "',";
			activitySql = activitySql + "0";
			logger.debug("got Valuation " + "0");
			activitySql = activitySql + ",";
			activitySql = activitySql + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null");
			logger.debug("got Activity Status " + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null"));
			activitySql = activitySql + ",";
			activitySql = activitySql + ((businessLicenseActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())) : "current_timestamp");
			logger.debug("got Start Date " + ((businessLicenseActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())) : "current_timestamp"));
			activitySql = activitySql + ",";
			activitySql = activitySql + ((businessLicenseActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())) : null);
			logger.debug("got Applied Date " + ((businessLicenseActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())) : null));
			activitySql = activitySql + ",";
			if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1053")) {
				activitySql = activitySql + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "current_timestamp");
				logger.debug("got Issue Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
			} else if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
				activitySql = activitySql + null;
				logger.debug("got Issue Date null");
			} else {
				activitySql = activitySql + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "null");
				logger.debug("got Issue Date null");
			}
			activitySql = activitySql + ",";
			activitySql = activitySql + "current_date";
			activitySql = activitySql + ",";
			activitySql = activitySql + "current_date";
			activitySql = activitySql + ",'N',";
			activitySql = activitySql + businessLicenseActivity.getCreatedBy();
			logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
			activitySql = activitySql + ",";
			activitySql = activitySql + "current_timestamp";
			logger.debug("got Created " + "current_timestamp");
			activitySql = activitySql + ",'Y'";
			activitySql = activitySql + ")";
			logger.info(activitySql);
			jdbcTemplate.update(activitySql);

			/**
			 * Inserting records into BL_ACTIVITY Table
			 */
			//businessAccNo = db.getNextId("BUSINESS_ACC_NO");
			nextId  = nextIdRepository.getNextId("BUSINESS_ACC_NO");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("BUSINESS_ACC_NO");
			 nextIdRepository.save(nextId);
			 businessAccNo = nextId.getIdValue();
			 
			logger.debug("Business Account No generated is " + businessAccNo);
			sql = "insert into bl_activity(ACT_ID, BURBANK_BUSINESS, APPL_TYPE_ID, OOT_STR_NO, OOT_STR_NAME, OOT_UNIT, OOT_CITY, OOT_STATE, OOT_ZIP, OOT_ZIP4, RENEWAL_DT, BUSINESS_NAME, BUSINESS_ACC_NO, CORPORATE_NAME, BUSINESS_PHONE, BUSINESS_PHONE_EXT, BUSINESS_FAX, MUNICIPAL_CODE, SIC_CODE, DESC_OF_BUSINESS, CLASS_CODE, HOME_OCCUPATION, DECAL_CODE, OOB_DT, OWN_TYPE_ID, FEDEREAL_ID, EMAIL, SSN, MAIL_STR_NO, MAIL_STR_NAME, MAIL_UNIT, MAIL_CITY, MAIL_STATE, MAIL_ZIP, MAIL_ZIP4, PRE_STR_NO, PRE_STR_NAME, PRE_CITY, PRE_STATE, PRE_ZIP, PRE_ZIP4, PRE_UNIT, INS_EXP_DT, BOND_EXP_DT, DOJ_EXP_DT, FFL_EXP_DT, SQ_FOOTAGE, QTY_ID, QTY_OTHER, EXEM_TYPE_ID, CREATED, CREATED_BY) values(";
			sql = sql + activityId;
			logger.debug("got Activity id " + activityId);
			sql = sql + ",";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isBusinessLocation()));
			logger.debug("got Business Location " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isBusinessLocation())));
			sql = sql + ",";
			sql = sql + ((businessLicenseActivity.getApplicationType() != null) ? StringUtils.i2s(businessLicenseActivity.getApplicationType().getId()) : "null");
			logger.debug("got Application Type ID" + ((businessLicenseActivity.getApplicationType() != null) ? StringUtils.i2s(businessLicenseActivity.getApplicationType().getId()) : "null"));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber());
			logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName());
			logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber());
			logger.debug("got out Of Town Unit Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity());
			logger.debug("got Out Of Town City " + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownState());
			logger.debug("got Out Of Town State " + StringUtils.checkString(businessLicenseActivity.getOutOfTownState()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip());
			logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4());
			logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4()));
			sql = sql + ",";
			sql = sql + ((businessLicenseActivity.getRenewalDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())) : "null");
			logger.debug("got Renewal Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase());
			logger.debug("got Business Name " + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase()));
			sql = sql + ",";
			sql = sql + businessAccNo;
			businessLicenseActivity.setBusinessAccountNumber(StringUtils.i2s(businessAccNo));
			logger.debug("got Business Account Number " + businessAccNo);
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getCorporateName());
			logger.debug("got Corporate Name " + StringUtils.checkString(businessLicenseActivity.getCorporateName()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessPhone());
			logger.debug("got Business Phone " + StringUtils.checkString(StringUtils.phoneFormat(businessLicenseActivity.getBusinessPhone())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessExtension());
			logger.debug("got Business Extension " + StringUtils.checkString(businessLicenseActivity.getBusinessExtension()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessFax());
			logger.debug("got Business Fax " + StringUtils.checkString(businessLicenseActivity.getBusinessFax()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getMuncipalCode());
			logger.debug("got Muncipal Code " + StringUtils.checkString(businessLicenseActivity.getMuncipalCode()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getSicCode());
			logger.debug("got Sic Code " + StringUtils.checkString(businessLicenseActivity.getSicCode()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness());
			logger.debug("got Description Of Business " + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getClassCode());
			logger.debug("got Class Code " + StringUtils.checkString(businessLicenseActivity.getClassCode()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation()));
			logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode()));
			logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode())));
			sql = sql + ",";
			if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
				sql = sql + ((businessLicenseActivity.getOutOfBusinessDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())) : "current_timestamp");
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
			} else {
				sql = sql + null;
				logger.debug("got Out Of Business Date null");
			}
			sql = sql + ",";
			sql = sql + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "null");
			logger.debug("got Ownership Type " + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "-1"));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber());
			logger.debug("got Federal Id Number " + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getEmailAddress());
			logger.debug("got Email " + StringUtils.checkString(businessLicenseActivity.getEmailAddress()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber());
			logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber()));
			sql = sql + ",";

			sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber());
			logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetName());
			logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessLicenseActivity.getMailStreetName())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber()));
			logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailCity()));
			logger.debug("got Mail City 2: " + StringUtils.checkString((businessLicenseActivity.getMailCity())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailState()));
			logger.debug("got Mail State 2: " + StringUtils.checkString((businessLicenseActivity.getMailState())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip()));
			logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip4()));
			logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip4())));
			sql = sql + ",";

			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber());
			logger.debug("got Previous Street Number " + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetName());
			logger.debug("got Previous Street Name " + StringUtils.checkString(businessLicenseActivity.getPrevStreetName()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevCity());
			logger.debug("got Previous City " + StringUtils.checkString(businessLicenseActivity.getPrevCity()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevState());
			logger.debug("got Previous State " + StringUtils.checkString(businessLicenseActivity.getPrevState()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip());
			logger.debug("got Previous Zip " + StringUtils.checkString(businessLicenseActivity.getPrevZip()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip4());
			logger.debug("got Previous Zip4 " + StringUtils.checkString(businessLicenseActivity.getPrevZip4()));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber());
			logger.debug("got Previous Unit Number " + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber()));
			sql = sql + ",";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate()));
			logger.debug("got Insurance Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate())));
			sql = sql + ",";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate()));
			logger.debug("got Bond Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate())));
			sql = sql + ",";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate()));
			logger.debug("got Department of Justice Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate())));
			sql = sql + ",";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate()));
			logger.debug("got Federal Firearms License Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate())));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getSquareFootage());
			logger.debug("got Square Footage " + StringUtils.checkString(businessLicenseActivity.getSquareFootage()));
			sql = sql + ",";
			sql = sql + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null");
			logger.debug("got Quantity " + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null"));
			sql = sql + ",";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getQuantityNum());
			logger.debug("got Quantity Number " + StringUtils.checkString(businessLicenseActivity.getQuantityNum()));
			sql = sql + ",";
			sql = sql + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null");
			logger.debug("got Type Of Exemptions " + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null"));
			sql = sql + ", current_timestamp,";
			sql = sql + businessLicenseActivity.getCreatedBy();
			logger.debug("got created by " + businessLicenseActivity.getCreatedBy());
			sql = sql + ")";

			logger.info(sql);
			jdbcTemplate.update(sql);

			/**
			 * Inserting record into PEOPLE Table
			 * 
			 */
			
			 nextId  = nextIdRepository.getNextId("PEOPLE_ID");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("PEOPLE_ID");
			 nextIdRepository.save(nextId);
			 peopleId = nextId.getIdValue();
			
			peopleSql = "INSERT INTO PEOPLE (PEOPLE_ID, PEOPLE_TYPE_ID,NAME,PHONE, AGENT_NAME, AGENT_PH, ADDR, CITY, STATE, ZIP, EMAIL_ADDR, COMNTS, CREATED_BY, CREATED, UNIT) "
					+ "VALUES ("+peopleId+","+Constants.PEOPLE_TYPE_BUSINESS_OWNER+", '"+businessLicenseActivity.getBusinessName().trim().toUpperCase()+"','"+businessLicenseActivity.getBusinessPhone()+"','"+businessLicenseActivity.getBusinessName().trim().toUpperCase()+"', '"+businessLicenseActivity.getBusinessPhone()+"', '"+businessLicenseActivity.getAddress()+"', "
					+ "'"+businessLicenseActivity.getAddressCity()+"', '"+businessLicenseActivity.getAddressState()+"', '"+businessLicenseActivity.getAddressZip()+"',"
					+ "'"+businessLicenseActivity.getEmailAddress()+"', 'Created through BL online appliaction', '',current_date, '"+businessLicenseActivity.getAddressUnitNumber()+"')";
			logger.debug("peopleSql... "+peopleSql);
			jdbcTemplate.update(peopleSql);
			
			/**
			 * Inserting record into ACTIVITY PEOPLE Table
			 * 
			 */
			
			String actPeopleSql = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID, PSA_TYPE) VALUES ("+activityId+","+peopleId+",'A')";
			logger.debug("actPeopleSql... "+actPeopleSql);
			jdbcTemplate.update(actPeopleSql);
				
		}
		return activityId;
	} catch (Exception e) {
		logger.error(e.getMessage(),e);
		throw e;
	}
}


/**
 * Gets the list of bl activity types
 * 
 * Created by Gayathri
 * 
 * @return
 */
public List getBlActivityTypes(int moduleId) throws Exception {
	logger.debug("getBlActivityTypes(" + moduleId + ")");

	List activityTypes = new ArrayList();
	//ActivityType activityType = new ActivityType();

	try {
		String sql = "select * from lkup_act_type where module_id=" + moduleId + " order by description ";
		logger.debug(sql);		

		activityTypes = jdbcTemplate.query(sql,new RowMapper<ActivityType>() {
	        @Override
	        public ActivityType mapRow(ResultSet rs, int i) throws SQLException { 
	        	ActivityType activityType = null;
	        	String type = rs.getString("type");
				String description = StringUtils.properCase(rs.getString("description"));
				activityType = new ActivityType(type, description);        	
	        	return  activityType;
	        }
		});	
		
	} catch (Exception e) {
		logger.error(e.getMessage());
		throw e;
	}
	return activityTypes;
}


/**
 * Get business license activity
 * 
 * @param activityId
 *            , lsoId
 * @return
 * @throws Exception
 * 
 */
public BusinessLicenseActivity getBusinessLicenseActivity(int activityId, int lsoId) throws Exception {
	logger.info("getBusinessLicenseActivity(" + activityId + ", " + lsoId + ")");

	BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
	logger.info("lso id is " + lsoId);
	try {
		//Wrapper db = new Wrapper();
		String sql = "";
		sql = "SELECT * FROM  ACTIVITY A, BL_ACTIVITY BA, V_ADDRESS_LIST VAL WHERE  A.ACT_ID = BA.ACT_ID  AND  VAL.LSO_ID=" + lsoId + " AND LSO_TYPE='O'  AND  A.ACT_ID = " + activityId;
		logger.info(sql);
logger.debug("in sql..");
		//RowSet rs = db.select(sql);
		businessLicenseActivity = jdbcTemplate.query(sql, new ResultSetExtractor<BusinessLicenseActivity>() {                
	        @Override
	        public BusinessLicenseActivity extractData(ResultSet rs) throws SQLException, DataAccessException {     
	        	BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
				while (rs.next()) {
					businessLicenseActivity.setActivityNumber(rs.getString("ACT_NBR"));
					businessLicenseActivity.setActivityStatus(commonService.getActivityStatus(rs.getInt("STATUS")));
					businessLicenseActivity.setApplicationType(commonService.getApplicationType(rs.getInt("APPL_TYPE_ID")));
					businessLicenseActivity.setActivityType(commonService.getActivityType(rs.getString("ACT_TYPE")));
					businessLicenseActivity.setAddressStreetNumber(StringUtils.i2s(rs.getInt("STR_NO")));
					businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
					businessLicenseActivity.setAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
					businessLicenseActivity.setAddressUnitNumber(rs.getString("UNIT"));
					businessLicenseActivity.setAddressCity(rs.getString("CITY"));
					businessLicenseActivity.setAddressState(rs.getString("STATE"));
					businessLicenseActivity.setAddressZip(rs.getString("ZIP"));
					businessLicenseActivity.setAddressZip4(rs.getString("ZIP4"));
					businessLicenseActivity.setBusinessLocation(StringUtils.s2b(rs.getString("BURBANK_BUSINESS")));
					businessLicenseActivity.setOutOfTownStreetNumber(rs.getString("OOT_STR_NO"));
					if (rs.getString("OOT_STR_NO") == null) {
						businessLicenseActivity.setOutOfTownStreetNumber("");
					}
					businessLicenseActivity.setOutOfTownStreetName(rs.getString("OOT_STR_NAME"));
					businessLicenseActivity.setOutOfTownUnitNumber(rs.getString("OOT_UNIT"));
					businessLicenseActivity.setOutOfTownCity(rs.getString("OOT_CITY"));
					businessLicenseActivity.setOutOfTownState(rs.getString("OOT_STATE"));
					businessLicenseActivity.setOutOfTownZip(rs.getString("OOT_ZIP"));
					businessLicenseActivity.setOutOfTownZip4(rs.getString("OOT_ZIP4"));
					businessLicenseActivity.setBusinessName(rs.getString("BUSINESS_NAME").trim().toUpperCase());
					businessLicenseActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));
					businessLicenseActivity.setCorporateName(rs.getString("CORPORATE_NAME"));
					businessLicenseActivity.setBusinessPhone(rs.getString("BUSINESS_PHONE"));
					businessLicenseActivity.setBusinessExtension(rs.getString("BUSINESS_PHONE_EXT"));
					businessLicenseActivity.setBusinessFax(rs.getString("BUSINESS_FAX"));
					businessLicenseActivity.setClassCode(rs.getString("CLASS_CODE"));
					businessLicenseActivity.setMuncipalCode(rs.getString("MUNICIPAL_CODE"));
					businessLicenseActivity.setSicCode(rs.getString("SIC_CODE"));
					businessLicenseActivity.setDecalCode(StringUtils.s2b(rs.getString("DECAL_CODE")));
					businessLicenseActivity.setDescOfBusiness(rs.getString("DESC_OF_BUSINESS"));
					businessLicenseActivity.setHomeOccupation(StringUtils.s2b(rs.getString("HOME_OCCUPATION")));
					businessLicenseActivity.setCreationDate(StringUtils.dbDate2cal(rs.getString("CREATED")));
					businessLicenseActivity.setRenewalDate(StringUtils.dbDate2cal(rs.getString("RENEWAL_DT")));
					businessLicenseActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("APPLIED_DATE")));
					businessLicenseActivity.setIssueDate(StringUtils.dbDate2cal(rs.getString("ISSUED_DATE")));
					businessLicenseActivity.setStartingDate(StringUtils.dbDate2cal(rs.getString("START_DATE")));
					businessLicenseActivity.setOwnershipType(commonService.getOwnershipType(rs.getInt("OWN_TYPE_ID")));
					businessLicenseActivity.setOutOfBusinessDate(StringUtils.dbDate2cal(rs.getString("OOB_DT")));
					businessLicenseActivity.setFederalIdNumber(rs.getString("FEDEREAL_ID"));
					businessLicenseActivity.setEmailAddress(rs.getString("EMAIL"));
					businessLicenseActivity.setSocialSecurityNumber(rs.getString("SSN"));
					businessLicenseActivity.setMailStreetNumber(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NO")));
					businessLicenseActivity.setMailStreetName(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NAME")));
					businessLicenseActivity.setMailUnitNumber(rs.getString("MAIL_UNIT"));
					businessLicenseActivity.setMailCity(rs.getString("MAIL_CITY"));
					businessLicenseActivity.setMailState(rs.getString("MAIL_STATE"));
					businessLicenseActivity.setMailZip(rs.getString("MAIL_ZIP"));
					businessLicenseActivity.setMailZip4(rs.getString("MAIL_ZIP4"));
					businessLicenseActivity.setPrevStreetNumber(rs.getString("PRE_STR_NO"));
					businessLicenseActivity.setPrevStreetName(rs.getString("PRE_STR_NAME"));
					businessLicenseActivity.setPrevUnitNumber(rs.getString("PRE_UNIT"));
					businessLicenseActivity.setPrevCity(rs.getString("PRE_CITY"));
					businessLicenseActivity.setPrevState(rs.getString("PRE_STATE"));
					businessLicenseActivity.setPrevZip(rs.getString("PRE_ZIP"));
					businessLicenseActivity.setPrevZip4(rs.getString("PRE_ZIP4"));
					businessLicenseActivity.setInsuranceExpDate(StringUtils.dbDate2cal(rs.getString("INS_EXP_DT")));
					businessLicenseActivity.setBondExpDate(StringUtils.dbDate2cal(rs.getString("BOND_EXP_DT")));
					businessLicenseActivity.setDeptOfJusticeExpDate(StringUtils.dbDate2cal(rs.getString("DOJ_EXP_DT")));
					businessLicenseActivity.setFederalFirearmsLiscExpDate(StringUtils.dbDate2cal(rs.getString("FFL_EXP_DT")));
					businessLicenseActivity.setSquareFootage(rs.getString("SQ_FOOTAGE"));
					businessLicenseActivity.setQuantity(commonService.getQuantityType(rs.getInt("QTY_ID")));
					businessLicenseActivity.setQuantityNum(rs.getString("QTY_OTHER"));
					businessLicenseActivity.setTypeOfExemptions(commonService.getExemptionType(rs.getInt("EXEM_TYPE_ID")));
					businessLicenseActivity.setCreatedBy(rs.getInt("CREATED_BY"));
					
					if (rs.getString("DEV_FEE_REQ") == null || rs.getString("PLAN_CHK_REQ") == null) {
						String devFeeSql = "UPDATE ACTIVITY SET DEV_FEE_REQ = 'N',PLAN_CHK_REQ = 'N' WHERE ACT_ID = " + activityId;
						logger.info("*** Development Fee Query **** " + devFeeSql);
						jdbcTemplate.update(devFeeSql);
					}
					logger.debug("Values setted to BusinessLicense object");
				}
				return businessLicenseActivity;
	        }
		});
//		businessLicenseActivity.setMultiAddress(commonService.getMultiAddress(activityId, "BL"));
		
	} catch (Exception e) {
		logger.error("Error in getactivity... "+e.getMessage()+e);
		throw new Exception();
	}
	return businessLicenseActivity;
}


public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId){
	
	try {
		if(multiAddessList!=null){
			for(int j=0; j< multiAddessList.length; j++){
				MultiAddress rec = (MultiAddress) multiAddessList[j];
				logger.debug("in updateMultiAddress"+rec);
				logger.debug("in updateMultiAddress"+rec.getId());
//				int id = checkMultiAddress(getAddressTypeId(rec.getAddressType()), activityId);
				int id = checkMultiAddress(rec.getId() , activityId);
				
				if(id==0){
					String insertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID,NAME,TITLE, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
							+ "VALUES (SEQUENCE_MULTI_ADDRESS_ID.NEXTVAL, '"+activityId+"', '"+getAddressTypeId(rec.getAddressType())+"', '"+rec.getName()+"', '"+rec.getTitle()+"', '"+rec.getStreetNumber()+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"', "
									+ "'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',  '"+rec.getAttn()+"', '"+rec.getUnit()+"', '"+rec.getCity()+"',"
											+ " '"+rec.getState()+"',  '"+rec.getZip()+"', '"+rec.getZip4()+"')";
					logger.debug("addressType InsertQuery  :"+insertQuery);
					jdbcTemplate.update(insertQuery);
				}else{
					String	updateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+rec.getStreetNumber()+"',STREET_NAME1= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"',STREET_NAME2= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',"
							+ "ATTN='"+rec.getAttn()+"',UNIT='"+rec.getUnit()+"',CITY='"+rec.getCity()+"',STATE='"+rec.getState()+"',"
							+ "ZIP= '"+rec.getZip()+"',ZIP4='"+rec.getZip4()+"',NAME='"+rec.getName()+"',TITLE='"+rec.getTitle()+"' "
							+ "WHERE ADDRESS_TYPE_ID="+getAddressTypeId(rec.getAddressType())+" AND ACT_ID="+activityId+" ";
					logger.debug("updateQuery :"+updateQuery);
					jdbcTemplate.update(updateQuery);	
				}
			}
		}	
	} catch (Exception e) {
		logger.debug("Error in Multiaddress... "+e+e.getMessage());
		e.printStackTrace();
	}
	
}

public int checkMultiAddress(String addressTypeId,int actId){
	int id = 0;
	String sql = "select * from MULTI_ADDRESS where ADDRESS_TYPE_ID='"+addressTypeId+"' AND ACT_ID="+actId+" ";
	logger.debug("checkMultiAddress : "+sql);

	try {
		id = jdbcTemplate.query(sql,new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				int id = 0;
		    	if (rs.next()) {
		    		id = rs.getInt("ID");
		    	}
				return id;
			}
		});	
	
	} catch (EmptyResultDataAccessException e) {
		logger.error("Unable to get id "+e);  
		return 0;
	}
	return id;
}


/**
 * This method is to fetch the address id for address type. 
 * This is to maintain for Multi_Address table
 * @param addressType
 * @return
 */
public String getAddressTypeId(String addressType) {
	String sql = "select * from LKUP_ADDRESS_TYPE where ADDRESS_TYPE='"+addressType+"'";
	logger.debug("getAddressTypeId sql :"+sql);
	String addressTypeId = "";
	try {
		addressTypeId=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String addressTypeId=rs.getString("ID");
				return addressTypeId;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return addressTypeId;
}

//save fees into activity_fee table for new BL Activity 
public List<Fee> saveActivityFee(List<Fee> fees,String actId,String peopleId) {
	
	try {
		for(int i=0;i<fees.size();i++) {
			String sql = " INSERT INTO ACTIVITY_FEE (ACTIVITY_ID,FEE_ID,PEOPLE_ID,FEE_UNITS,FEE_VALUATION,FEE_AMNT,FEE_PAID,COMMENTS) VALUES("+actId+","+fees.get(i).getFeeId()+","+peopleId+","+fees.get(i).getFeeUnits()+","+fees.get(i).getTotalFee()+","+fees.get(i).getTotalFee()+",0,'Added from online')";
			jdbcTemplate.update(sql);
		}
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return fees;
}

public List<Fee> getFees(String actType,String tempId) {
	String sql = "select f.fee_factor ,CAST((CASE WHEN TAD.NO_OF_EMPLOYEEES IS NULL THEN '1' ELSE TAD.NO_OF_EMPLOYEEES END) as int) as qty , CAST((CASE WHEN F.FEE_FACTOR IS NULL THEN F.FACTOR ELSE F.FEE_FACTOR END)  as int) as fctr, "
			+ "LAT.TYPE,F.FACTOR,F.FEE_DESC,F.FEE_FACTOR,F.FEE_ID,CASE WHEN (LAT.BL_QTY_FLAG ='Y' AND F.FEE_DESC NOT IN('CA State Disability Fee')) "
			+ "THEN ((CAST((CASE WHEN TAD.NO_OF_EMPLOYEEES IS NULL THEN '1' ELSE TAD.NO_OF_EMPLOYEEES END) as int) *  CAST((CASE WHEN F.FEE_FACTOR IS NULL THEN F.FACTOR ELSE F.FEE_FACTOR END)  as DECIMAL(6,2)))) "
			+ "else (CASE WHEN f.factor IS NULL THEN f.fee_factor ELSE f.factor END) end AS FEE_TOTAL from fee F join LKUP_ACT_TYPE LAT ON F.ACT_TYPE=LAT.TYPE "
			+ "join TEMP_ACTIVITY_DETAILS TAD ON TAD.ID="+tempId+" where F.act_type IN('"+actType+"') and  F.FEE_EXPIRATION_DT is null "
			+ "and F.ONLINE_NEWBLBT_FLAG='Y' ORDER BY F.FEE_ID DESC";
	
	logger.debug("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        	Fee fee = new Fee();
        	fee.setActType(rs.getString("TYPE"));
        	fee.setFeeDesc(rs.getString("FEE_DESC"));
        	fee.setFeeId(rs.getString("FEE_ID"));
        	fee.setFeeFactor(StringUtils.s2d(rs.getString("FEE_FACTOR")));
        	fee.setFactor(StringUtils.s2d(rs.getString("FACTOR")));
        	fee.setNoOfQuantity(rs.getString("QTY"));
        	fee.setFeeTotal(StringUtils.s2d(rs.getString("FEE_TOTAL")));
        	fee.setFeeTotalStr(StringUtils.roundOffDouble(rs.getDouble("FEE_TOTAL")));
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return feeList;
}


}