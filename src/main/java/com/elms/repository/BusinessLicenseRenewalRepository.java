package com.elms.repository;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.ActivityType;
import com.elms.model.Attachment;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.NextId;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;


@Repository
public class BusinessLicenseRenewalRepository {


private static final Logger logger = Logger.getLogger(BusinessLicenseRenewalRepository.class);
	
@Autowired
JdbcTemplate  jdbcTemplate;
@Autowired
NextIdRepository nextIdRepository;
@Autowired
CommonService commonService;
@Autowired
PlatformTransactionManager transactionManager;

@Autowired
DataSource dataSource;

public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
    this.jdbcTemplate = new JdbcTemplate(dataSource);
 }
 public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
 }
/**
 * These are keys and values from lookup system table
 * 
 * @param keyName
 * @return
 * @throws Exception
 */
public Activity getKeyValue(String keyName) { 
	String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(keyName);
	logger.debug("config sql :"+sql);
	Activity activityList = new Activity();
	try {
		activityList=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
		    @Override
		    public Activity mapRow(ResultSet rs, int i) throws SQLException {
				
		    	Activity activity = new Activity();
		    	activity.setActTypeDesc(rs.getString("VALUE"));	
		    	return activity;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return activityList;
}


public boolean saveStreamToFile(Activity activity,String attachmentTypeId) throws MultipartException,Exception {

	boolean didConvert = false;
	OutputStream bos = null;

	TransactionDefinition def = new DefaultTransactionDefinition();
	TransactionStatus status = transactionManager.getTransaction(def);
	
	String sql = "SELECT * from lkup_system WHERE NAME='TEMP_FILE_LOC'";
	logger.debug("TEMP file location sql :"+sql);
	
		String location=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	
				return rs.getString("VALUE");		
		    }
		});

	try {
		
		 List<MultipartFile> filesList=activity.getTheFile();
		
		if(null != filesList && filesList.size() > 0) {
			for (MultipartFile multipartFile : filesList) {
				if(multipartFile != null && multipartFile.getOriginalFilename() !=null && !multipartFile.getOriginalFilename().equalsIgnoreCase("")) {
			
					String fileName1 = multipartFile.getOriginalFilename();
					
					File f=new File(fileName1);
					String tempFileName=activity.getRenewalCode()+"_"+f.getName().replaceAll("[^\\d\\w _.-]", "-");
					logger.debug("file name 127.. "+tempFileName);
					
					//create directory if we don't have

					File fileSaveDir = new File(location);
			        if (!fileSaveDir.exists()) {
			            fileSaveDir.mkdir();
			        }
			        /*location=location+activity.getRenewalCode();
					File permitNumDir = new File(location);
			        if (!permitNumDir.exists()) {
			        	permitNumDir.mkdir();
			        }*/
					 // Get the file and save it somewhere
					byte[] bytes = multipartFile.getBytes();
					Path path = Paths.get(location +tempFileName);	
			        
					Files.write(path, bytes);

					Attachment attachmentList = new Attachment();
					String selectQuery = "select * from TEMP_ATTACHMENT where TEMP_ID="+activity.getTempId()+" and LKUP_ATTACHMENT_TYPE_ID="+attachmentTypeId;
					logger.debug("saveAttachmentList Query : "+selectQuery);
					try {					
						attachmentList= jdbcTemplate.queryForObject(selectQuery,new RowMapper<Attachment>() {
						    @Override
						    public Attachment mapRow(ResultSet rs,int i) throws SQLException {
								Attachment attachment = new Attachment();
						    	attachment.setAttachmentTypeId(rs.getInt("LKUP_ATTACHMENT_TYPE_ID"));
						    	attachment.setAttachmentId(rs.getInt("TEMP_ID"));
						    	return attachment;	
						    }
						});
					} catch (EmptyResultDataAccessException e) {
						logger.error(e.getMessage());
					} catch (Exception e) {
						logger.error("",e);
					}
						if(attachmentList != null && attachmentList.getAttachmentTypeId() != 0 && attachmentList.getAttachmentId() != 0 ) {

							String updateQuery = "update TEMP_ATTACHMENT set FILENAME='"+tempFileName+"',FILE_LOCATION='"+location+"' where TEMP_ID="+activity.getTempId()+" and LKUP_ATTACHMENT_TYPE_ID="+attachmentTypeId;
				    		logger.debug("saveAttachmentList  :"+updateQuery);
				    		jdbcTemplate.update(updateQuery);
						}else {
				    		String insertQuery = "INSERT INTO TEMP_ATTACHMENT (TEMP_ID,LKUP_ATTACHMENT_TYPE_ID,FILENAME,FILE_LOCATION) values("+activity.getTempId()+","+attachmentTypeId+",'"+tempFileName+"','"+location+"')";
				    		logger.debug("saveAttachmentList  :"+insertQuery);
				    		jdbcTemplate.update(insertQuery);	
						}
					}
	
				}
			}

		transactionManager.commit(status);
		return didConvert;
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch(MultipartException e) {
		didConvert = false;
		logger.error("");
	} catch (Exception e) {
		logger.error("problem while saving file " + e.getMessage()+e);
		didConvert = false;
		e.printStackTrace();
		 transactionManager.rollback(status);
		 throw new Exception("Problem while saving file " + e.getMessage()+e);
	} finally {
		if (bos != null) {
			// flush the stream
	         bos.flush();

	         // close the stream but it does nothing
	         bos.close();
		}
	}
	return didConvert;
}

public Activity updateEmp(Activity activity) {
	logger.debug("in updateEmp repository...");
	try{
		logger.debug("updateEmp"+activity.toString());
		if(StringUtils.nullReplaceWithEmpty(activity.getNoOfEmp()).equals("") && StringUtils.s2i(activity.getQtyOther())>0){
			activity.setNoOfEmp(activity.getQtyOther());
		}
		
		String updateQuery = "UPDATE TEMP_ACTIVITY_DETAILS SET NO_OF_EMPLOYEEES ='"+StringUtils.nullReplaceWithEmpty(activity.getNoOfEmp())+"', "
				+ "TEMP_BL_QTY_FLAG ='" +activity.getTempBlQtyFlag()+ "' WHERE ID ="+activity.getTempId();
		logger.debug("updateQuery  :"+updateQuery);
		jdbcTemplate.update(updateQuery);	
	}catch(Exception e){
		logger.error("",e);
		
	}
	return activity;
}


public void saveBLFeeDetails(Activity activity) {
	logger.debug("in saveFeeDetails repository...");
	String qty = activity.getNoOfEmp();
	if(qty==null || qty.equalsIgnoreCase("")) qty="1";
	List<Fee> feeList=new ArrayList<Fee>();
	feeList=activity.getFeeList();
	String insertQuery = "";
	TransactionDefinition def = new DefaultTransactionDefinition();
	TransactionStatus status = transactionManager.getTransaction(def);
	    
	try{
		String deleteQuery ="DELETE FROM TEMP_ACTIVITY_FEE WHERE TEMP_ID="+activity.getTempId();			
		logger.debug("Feelist.size()  :"+feeList.size());
		jdbcTemplate.update(deleteQuery);	
		for(int i=0; i<feeList.size();i++) {
			if(feeList.get(i).getFeeTotal()>0.0) {
				insertQuery = "INSERT INTO TEMP_ACTIVITY_FEE (TEMP_ID,ACTIVITY_ID,FEE_ID,FEE_UNITS,FEE_AMNT,TOTAL_FEE_AMNT) values "
				+ "("+activity.getTempId()+","+activity.getRenewalCode()+","+feeList.get(i).getFeeId()+","+qty+","+feeList.get(i).getFeeTotal()+", "+activity.getTotalFee()+" )";
				
				logger.debug("saveFeeDetails  :"+insertQuery);
				jdbcTemplate.update(insertQuery);
			}			
		}
		transactionManager.commit(status);
	}catch(Exception e){
		logger.error("",e);
		 transactionManager.rollback(status);
		 throw e;			
	}
}

public List<Fee> getFees(String actType,String tempId) {
	String sql = "select f.fee_factor ,CAST((CASE WHEN TAD.NO_OF_EMPLOYEEES IS NULL THEN '1' ELSE TAD.NO_OF_EMPLOYEEES END) as int) as qty , CAST((CASE WHEN F.FEE_FACTOR IS NULL THEN F.FACTOR ELSE F.FEE_FACTOR END)  as int) as fctr, "
			+ "LAT.TYPE,F.FACTOR,F.FEE_DESC,F.FEE_FACTOR,F.FEE_ID,CASE WHEN (LAT.BL_QTY_FLAG ='Y' AND F.FEE_DESC NOT IN('CA State Disability Fee')) "
			+ "THEN ((CAST((CASE WHEN TAD.NO_OF_EMPLOYEEES IS NULL THEN '1' ELSE TAD.NO_OF_EMPLOYEEES END) as int) *  CAST((CASE WHEN F.FEE_FACTOR IS NULL THEN F.FACTOR ELSE F.FEE_FACTOR END)  as DECIMAL(6,2)))) "
			+ "else (CASE WHEN f.factor IS NULL THEN f.fee_factor ELSE f.factor END) end AS FEE_TOTAL from fee F join LKUP_ACT_TYPE LAT ON F.ACT_TYPE=LAT.TYPE "
			+ "join TEMP_ACTIVITY_DETAILS TAD ON TAD.ID="+tempId+" where F.act_type IN('"+actType+"') and  F.FEE_EXPIRATION_DT is null "
			+ "and F.ONLINE_RENEWABLE='Y' ORDER BY F.FEE_ID DESC";
	
	logger.debug("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        	Fee fee = new Fee();
        	fee.setActType(rs.getString("TYPE"));
        	fee.setFeeDesc(rs.getString("FEE_DESC"));
        	fee.setFeeId(rs.getString("FEE_ID"));
        	fee.setFeeFactor(StringUtils.s2d(rs.getString("FEE_FACTOR")));
        	fee.setFactor(StringUtils.s2d(rs.getString("FACTOR")));
        	fee.setNoOfQuantity(rs.getString("QTY"));
        	fee.setFeeTotal(StringUtils.s2d(rs.getString("FEE_TOTAL")));
        	fee.setFeeTotalStr(StringUtils.roundOffDouble(rs.getDouble("FEE_TOTAL")));

        	logger.debug("REPO - Factor :: " +fee.getFactor() + " :: Fee Factor :: " +fee.getFeeFactor() +" :: Fee Total :: " +fee.getFeeTotal());
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return feeList;
}

public Activity getBLQuantityInfo(String busAccNo, int renewalCode) {
	String sql = "SELECT "
			+ "NVL(bl.BUSINESS_ACC_NO,bt.BUSINESS_ACC_NO) as BUSINESS_ACC_NO,a.act_id,a.act_nbr,la.DESCRIPTION,NVL(bl.BURBANK_BUSINESS,bt.BURBANK_BUSINESS) as BURBANK_BUSINESS,NVL(bl.BUSINESS_FAX,bt.BUSINESS_FAX) as BUSINESS_FAX,NVL(bl.BUSINESS_NAME,bt.BUSINESS_NAME) as BUSINESS_NAME,NVL(bl.BUSINESS_PHONE,bt.BUSINESS_PHONE) BUSINESS_PHONE,NVL(bl.BUSINESS_PHONE_EXT,bt.BUSINESS_PHONE_EXT) as BUSINESS_PHONE_EXT,NVL(bl.email,bt.email) as email,NVL(bl.MAIL_CITY,bt.MAIL_CITY) as MAIL_CITY,NVL(bl.MAIL_STATE,bt.MAIL_STATE) MAIL_STATE,NVL(bl.MAIL_STR_NAME,bt.MAIL_STR_NAME) as MAIL_STR_NAME,NVL(bl.MAIL_STR_NO,bt.MAIL_STR_NO) MAIL_STR_NO,NVL(bl.MAIL_UNIT,bt.MAIL_UNIT) as MAIL_UNIT,NVL(bl.MAIL_ZIP,bt.MAIL_ZIP) as MAIL_ZIP,tad.id as temp_id,vaa.STR_NO,vaa.ADDRESS,vaa.CITYSTATEZIP FROM activity A LEFT OUTER JOIN lkup_act_type LA ON A.act_type=LA.type left join bl_activity bl on bl.act_id =a.act_id left join bt_activity bt on bt.act_id=a.act_id left join V_ACTIVITY_ADDRESS vaa on a.act_id=vaa.act_id and a.addr_id=vaa.addr_id left join TEMP_ACTIVITY_DETAILS tad on a.ACT_ID=tad.RENEWAL_CODE " + 
			" WHERE bl.BUSINESS_ACC_NO='"+busAccNo+"' and a.act_id="+renewalCode;
	logger.debug("ActivityInfo sql :"+sql);
	Activity activityList=new Activity();
	try {
		activityList=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
			
		    @Override
		    public Activity mapRow(ResultSet rs, int i) throws SQLException {
		    	Activity activity = new Activity();
		    	activity.setRenewalCode(StringUtils.i2s(rs.getInt("act_id")));
		    	activity.setPermitNumber(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR")));
		    	activity.setBurbankBusiness(StringUtils.nullReplaceWithEmpty(rs.getString("BURBANK_BUSINESS")));
		    	activity.setBusinessFax(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_FAX")));
		    	activity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));
				return activity;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return activityList;
}

public Activity getQtyFlag(String renewalCode) { 
	String sql = "SELECT * from lkup_act_type lat join activity a on lat.type = a.act_type WHERE a.act_id="+renewalCode;
	logger.debug("getQtyFlag sql :"+sql);
	Activity activityList = new Activity();
	try {
		activityList=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
		    @Override
		    public Activity mapRow(ResultSet rs, int i) throws SQLException {
		    	Activity activity = new Activity();
		    	activity.setBlQtyFlag(rs.getString("BL_QTY_FLAG"));	
				return activity;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return activityList;
}


/**
 * This method is to fetch the TEMP_ACTIVITY_DETAILS table record on click of
 * Back Button on each screen / Print Preview Screen before Payment success
 * @param tempId
 * @throws Exception
 * @return Activity
 * 
 */
public Activity getTempActivityDetails(String tempId) {
	Activity activity = new Activity();
	String sql = "SELECT ACT_ID,ACT_NBR, RENEWAL_CODE, RENEWAL_DT, BUSINESS_NAME, EMAIL, STR_NO, ADDRESS, UNIT, CITY, STATE, ZIP, BUSINESS_PHONE, NO_OF_EMPLOYEEES, "
			+ "BUSINESS_ACC_NO, DESCRIPTION, LOCATION, ATTACH_SIZE, CREATED_BY, CREATED, DELETED, STATUS, KEYWORD1, KEYWORD2, KEYWORD3, KEYWORD4, TEMP_BT_QTY "
			+ "FROM TEMP_ACTIVITY_DETAILS WHERE ID = "+tempId;
	
	logger.debug("ActivityInfo sql :"+sql);
	try {
		logger.debug(jdbcTemplate.getFetchSize());
		jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() { 
			
		    @Override
		    public Activity mapRow(ResultSet rs, int i) throws SQLException {
				activity.setPermitNumber(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR")));
		    	activity.setRenewalCode(StringUtils.i2s(rs.getInt("RENEWAL_CODE")));
		    	activity.setStartDate(StringUtils.date2str(rs.getDate("RENEWAL_DT")));
		    	activity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));
		    	activity.setEmail(StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL")));
		    	activity.setStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")));
		    	activity.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));		    	
		    	activity.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
		    	activity.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
		    	activity.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
		    	activity.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));		    	
		    	activity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_PHONE")));
		    	activity.setNoOfEmp(StringUtils.nullReplaceWithEmpty(rs.getString("NO_OF_EMPLOYEEES")));
		    	activity.setBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ACC_NO")));
		    	activity.setDescription(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
		    	activity.setTempBtQty(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BT_QTY")));
				return activity;				
		    }
		});	
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	}catch (Exception e) {
		logger.error("",e);
		e.printStackTrace();
		return new Activity();
	}	
	return activity;	
}
/**
 * This method is to fetch the Activity Information to be displayed on all the screens 
 * like Business Address, Activity Number, Business Mailing Address
 * @param busAccNo, renewalCode
 * @throws Exception
 * @return Activity
 */
public Activity getBLActivityInfo(String busAccNo, String renewalCode) {
	 String sql =  "SELECT BL.QTY_OTHER,CASE WHEN LQ.ID IS NULL THEN LQR.ID ELSE LQ.ID END AS LKUP_BL_QTY_ID, A.APPLICATION_ONLINE,"
			    + "CASE WHEN LQ.DESCRIPTION IS NULL THEN LQR.DESCRIPTION ELSE LQ.DESCRIPTION END AS QTY_DESC, "
			    + "CASE WHEN LQ.PLACEHOLDER_DESCRIPTION IS NULL THEN LQR.PLACEHOLDER_DESCRIPTION ELSE LQ.PLACEHOLDER_DESCRIPTION END AS PLACEHOLDER_DESCRIPTION, "
			    + "CASE WHEN LQ.LINE_PATTERN IS NULL THEN LQR.LINE_PATTERN ELSE LQ.LINE_PATTERN END AS LINE_PATTERN, "
			    + "CASE WHEN LQ.LINE1_DESC IS NULL THEN LQR.LINE1_DESC ELSE LQ.LINE1_DESC END AS LINE1_DESC, "
			    + "CASE WHEN LQ.LINE2_DESC IS NULL THEN LQR.LINE2_DESC ELSE LQ.LINE2_DESC END AS LINE2_DESC, "
			    + "CASE WHEN LQ.LINE3_DESC IS NULL THEN LQR.LINE3_DESC ELSE LQ.LINE3_DESC END AS LINE3_DESC, "
			    + "TAD.BUSINESS_NAME AS TEMP_BUSINESS_NAME, TAD.EMAIL AS TEMP_EMAIL, TAD.BUSINESS_PHONE AS TEMP_BUSINESS_PHONE,TAD.STR_NO, "
			    + "TAD.ADDRESS, TAD.UNIT, TAD.CITY, TAD.STATE, TAD.ZIP,  TAD.NO_OF_EMPLOYEEES, TAD.BUSINESS_ACC_NO AS TEMP_BUSINESS_ACC_NO,"
			    + "TAD.TEMP_BL_QTY_FLAG, BL.BUSINESS_ACC_NO AS BUSINESS_ACC_NO,A.ACT_ID, "
			    + "A.ACT_TYPE, A.ACT_NBR,LA.DESCRIPTION, BL.BURBANK_BUSINESS AS BURBANK_BUSINESS, "
			    + "BL.BUSINESS_FAX AS BUSINESS_FAX,BL.BUSINESS_NAME AS BUSINESS_NAME, BL.BUSINESS_PHONE AS BUSINESS_PHONE, "
			    + "BL.BUSINESS_PHONE_EXT AS BUSINESS_PHONE_EXT,BL.EMAIL AS EMAIL,MA.CITY AS MAIL_CITY, "
			    + "MA.STATE AS MAIL_STATE,MA.STREET_NAME1 AS MAIL_STR_NAME,MA.STREET_NUMBER AS MAIL_STR_NO,MA.UNIT AS MAIL_UNIT,MA.ZIP AS MAIL_ZIP,"
			    + "TAD.ID AS TEMP_ID,"
			    + "CASE WHEN BL.OOT_STR_NO IS NULL THEN CAST(VAA.STR_NO AS VARCHAR(20)) ELSE CAST(BL.OOT_STR_NO AS VARCHAR(100)) END  AS BUSINESS_STR_NO,"
			    + "CASE WHEN BL.OOT_STR_NAME IS NULL THEN VAA.BL_ADDRESS ELSE (OOT_STR_NO || ' ' || BL.OOT_STR_NAME) END AS BUSINESS_STR_NAME, "
			    + "CASE WHEN BL.OOT_CITY IS NULL THEN 'BURBANK' ELSE COALESCE(BL.OOT_CITY,'') END  AS BUSINESS_CITY, "
			    + "CASE WHEN BL.OOT_STATE IS NULL THEN ' CA' ELSE COALESCE(' ' || BL.OOT_STATE,'') END AS BUSINESS_STATE, "
			    + "CASE WHEN BL.OOT_ZIP IS NULL THEN  CAST(91505 AS VARCHAR(5)) ELSE COALESCE(' ' || BL.OOT_ZIP ,'') END AS BUSINESS_ZIP "
			    + "FROM ACTIVITY A "
			    + "LEFT OUTER JOIN LKUP_ACT_TYPE LA ON A.ACT_TYPE=LA.TYPE "
			    + "LEFT OUTER JOIN BL_ACTIVITY BL ON BL.ACT_ID =A.ACT_ID "
			    + "LEFT OUTER JOIN V_ACTIVITY_ADDRESS VAA ON BL.ACT_ID=VAA.ACT_ID "
			    + "LEFT OUTER JOIN LKUP_QTY LQ ON BL.QTY_ID=LQ.ID "
			    + "LEFT OUTER JOIN REF_ACT_BL_QTY_TYPE  RAQ ON LA.TYPE_ID = RAQ.LKUP_ACT_TYPE_ID "
			    + "LEFT OUTER JOIN LKUP_QTY LQR ON RAQ.LKUP_QTY_TYPE_ID=LQR.ID "
			    + "LEFT OUTER JOIN MULTI_ADDRESS MA ON A.ACT_ID=MA.ACT_ID AND MA.ADDRESS_TYPE_ID=1 "
			    + "LEFT OUTER JOIN TEMP_ACTIVITY_DETAILS TAD ON A.ACT_ID=TAD.RENEWAL_CODE  "
			    + "WHERE BL.BUSINESS_ACC_NO ='"+busAccNo.trim()+"' AND A.ACT_ID = "+renewalCode+" AND A.RENEWAL_ONLINE='N'";
	
	logger.debug("ActivityInfo sql :"+sql);
	Activity activityList=new Activity();
	Activity activity = new Activity();
	
	try {   
		activityList=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
			
		    @Override
		    public Activity mapRow(ResultSet rs, int i) throws SQLException {
	    	
		    	activity.setRenewalCode(StringUtils.i2s(rs.getInt("ACT_ID")));
		    	activity.setActType(rs.getString("ACT_TYPE"));
		    	activity.setPermitNumber(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR")));
		    	activity.setDescription(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));		    	
		    	activity.setEmail(StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL")));
		    	activity.setOnlineBLBTApplication(rs.getString("APPLICATION_ONLINE"));
		    	activity.setQtyOther(StringUtils.nullReplaceWithEmpty(rs.getString("QTY_OTHER")));
		    	activity.setQtyDesc(StringUtils.nullReplaceWithEmpty(rs.getString("QTY_DESC")));
		    	
		    	activity.setPlaceholderDesc(StringUtils.nullReplaceWithEmpty(rs.getString("PLACEHOLDER_DESCRIPTION")));
		    	activity.setLine1Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE1_DESC")));
		    	activity.setLine2Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE2_DESC")));
		    	activity.setLine3Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE3_DESC")));
		    	activity.setLinePattern(StringUtils.nullReplaceWithEmpty(rs.getString("LINE_PATTERN")));
						    	
		    	activity.setTempBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BUSINESS_NAME")));
		    	activity.setTempEmail(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_EMAIL")));
		    	String tempBusinessPhone = rs.getString("TEMP_BUSINESS_PHONE");
		    	if(tempBusinessPhone != null) {
		    		tempBusinessPhone = tempBusinessPhone.replaceAll("\\W+","");
		    	}
		    	activity.setTempBusinessPhone(StringUtils.nullReplaceWithEmpty(StringUtils.phoneFormat(StringUtils.nullReplaceWithEmpty(tempBusinessPhone))));
		    	activity.setTempBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BUSINESS_ACC_NO")));
		    	activity.setTempId(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_ID")));
		    	activity.setTempBlQtyFlag(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BL_QTY_FLAG")));
		    			    	
		    	activity.setStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")));
		    	activity.setStrName(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));
		    	activity.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));
		    	activity.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
		    	activity.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
		    	activity.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
		    	activity.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
		    	activity.setCityStateZip(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STATE")) +" "+ StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
		    	
		    	activity.setNoOfEmp(StringUtils.nullReplaceWithEmpty(rs.getString("NO_OF_EMPLOYEEES")));
		    	activity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));		    	
		    	
		    	activity.setBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ACC_NO")));
		    	activity.setBusinessFax(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_FAX")));		
		    	String businessPhone = rs.getString("BUSINESS_PHONE");
		    	if(businessPhone != null) {
		    		businessPhone = businessPhone.replaceAll("\\W+","");
		    	}
		    	activity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(StringUtils.phoneFormat(StringUtils.nullReplaceWithEmpty(businessPhone))));
		    	activity.setBusinessPhoneExt(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_PHONE_EXT")));
		    	activity.setBusinessStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STR_NO")));
		    	activity.setBusinessStrName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STR_NAME")));
		    	activity.setBusinessCity(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_CITY")));
		    	activity.setBusinessState(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STATE")));
		    	activity.setBusinessZip(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ZIP")));
		    	activity.setBusinessCityStateZip(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_CITY")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STATE")) +" "+ StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ZIP")));
		    	
		    	activity.setBurbankBusiness(StringUtils.nullReplaceWithEmpty(rs.getString("BURBANK_BUSINESS")));
		    	
		    	activity.setMailStrName(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NAME")));
		    	activity.setMailStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NO")));
		    	activity.setMailUnit(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_UNIT")));
		    	activity.setMailCity(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_CITY")));
		    	activity.setMailState(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STATE")));
		    	activity.setMailZip(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_ZIP")));
				return activity;				
		    }
		});
	
	} catch(EmptyResultDataAccessException er) {
		logger.error("",er);		
	}catch (Exception e) {
		logger.error("",e);		
	}	
	return activityList;	
}

public Activity saveApplicationDetails(Activity activity) {
	logger.debug("in saveApplicationDetails repository..."+activity.toString());
	/*TransactionDefinition def = new DefaultTransactionDefinition();
    TransactionStatus status = transactionManager.getTransaction(def);*/
    String userName="";
    int userId=0;
    if(activity.getPermitNumber() != null && activity.getPermitNumber().startsWith("BL")) {
    	userName="BLUsers";
	}else if(activity.getPermitNumber() != null && activity.getPermitNumber().startsWith("BT")) {
		userName="BTUsers";		
	}
    
    userId = getUserInfo(userName);

	String peopleID=null;
	try{
		if(activity.getTempId() != null) {

		String updateActivityQuery = "";
		String qtyOther="";
		//UPDATE BL ACTIVITY TABLE
		if(activity.getPermitNumber() != null && activity.getPermitNumber().startsWith("BL")) {
			
			//UPDATE ACTIVITY TABLE
			updateActivityQuery = "UPDATE ACTIVITY SET STATUS ="+Constants.PAID_PENDING_FOR_APPROVAL_CODE+",RENEWAL_ONLINE='Y',UPDATED_BY="+userId+",UPDATED=current_date  WHERE ACT_ID ="+activity.getRenewalCode();
			logger.debug("updateActivityQuery..."+updateActivityQuery);
			jdbcTemplate.update(updateActivityQuery);

			String updateBLActivityQuery = "UPDATE BL_ACTIVITY SET BUSINESS_PHONE = '"+activity.getBusinessPhone()+"', UPDATED_BY="+userId+",UPDATED=current_date,EMAIL ='"+activity.getEmail()+"'";
				
			qtyOther=activity.getNoOfEmp();
			if(activity.getNoOfEmp() != null && !activity.getNoOfEmp().equals("")) {
				updateBLActivityQuery = updateBLActivityQuery +	 ",QTY_OTHER='"+activity.getNoOfEmp()+"'";
			}

			updateBLActivityQuery = updateBLActivityQuery +" WHERE ACT_ID ="+activity.getRenewalCode();
			logger.debug("updateBLActivityQuery... "+updateBLActivityQuery);
			jdbcTemplate.update(updateBLActivityQuery);
	
		}else if(activity.getPermitNumber() != null && activity.getPermitNumber().startsWith("BT")) {
			updateActivityQuery = "";
			//UPDATE ACTIVITY TABLE
			updateActivityQuery = "UPDATE ACTIVITY SET STATUS ="+Constants.PAID_OR_CURRENT_CODE+",RENEWAL_ONLINE='Y',UPDATED_BY="+userId+",UPDATED=current_date WHERE ACT_ID ="+activity.getRenewalCode();
			logger.debug("updateActivityQuery..."+updateActivityQuery);
			jdbcTemplate.update(updateActivityQuery);
			
			//UPDATE BT ACTIVITY TABLE 
			// WE NEED TO UPDATE THE CURRENT DATE IN UPDATED COLUMN IN EVERY TABLE
			String updateBTActivityQuery = "UPDATE BT_ACTIVITY SET BUSINESS_PHONE = '"+activity.getBusinessPhone()+"',UPDATED_BY="+userId+",UPDATED=current_date, EMAIL ='"+activity.getEmail()+"'";
				
			qtyOther=activity.getTempBtQty();
			if(activity.getTempBtQty() != null && !activity.getTempBtQty().equals("")) {
				updateBTActivityQuery = updateBTActivityQuery +	 ",QTY_OTHER='"+activity.getTempBtQty()+"'";
			}

			updateBTActivityQuery = updateBTActivityQuery +" WHERE ACT_ID ="+activity.getRenewalCode();
			logger.debug("updateBTActivityQuery... "+updateBTActivityQuery);
			jdbcTemplate.update(updateBTActivityQuery);
			
		}		
		//UPDATE MULTI ADDRESS TABLE
		//check and if there update it not there insert in it
		if(activity.getStrNo() != null) {
			String updateMultiAddressQuery = "UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+activity.getStrNo()+"', STREET_NAME1='"+activity.getAddress()+"',UNIT='"+activity.getUnit()+"',CITY='"+activity.getCity()+"',STATE='"+activity.getState()+"',ZIP='"+activity.getZip()+"' WHERE ADDRESS_TYPE_ID='"+getAddressTypeId(Constants.MULTI_ADDRESS_MAILING_ADDRESS) +"' AND ACT_ID="+activity.getRenewalCode();
			logger.debug("updateMultiAddressQuery... "+updateMultiAddressQuery);
			jdbcTemplate.update(updateMultiAddressQuery);			
		}else {
			String insertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID, NAME, STREET_NUMBER, STREET_NAME1, UNIT, CITY, STATE, ZIP) "
					+ "VALUES (SEQUENCE_MULTI_ADDRESS_ID.NEXTVAL, '"+activity.getRenewalCode()+"', '"+getAddressTypeId(Constants.MULTI_ADDRESS_MAILING_ADDRESS)+"', '"+userName+"', '"+activity.getStrNo()+"', '"+StringUtils.nullReplaceWithEmpty(activity.getAddress())+"', "
					+ "'"+activity.getUnit()+"', '"+activity.getCity()+"',  '"+activity.getState()+"', '"+activity.getZip()+"')";

			logger.debug("Insert MultiAddressQuery... "+insertQuery);
			jdbcTemplate.update(insertQuery);			
		}
		//CHECKING PEOPLE IS EXIST OR NOT WITH PEOPLE TYPE APPLICANT(PEOPLE_TYPE_ID=2)
		if(activity.getEmail() != null && activity.getEmail().equalsIgnoreCase("")) {
			String peopleCheck="select * from people where EMAIL_ADDR='"+activity.getEmail()+"' and PEOPLE_TYPE_ID =2 ";
			logger.debug("peopleCheck.. "+peopleCheck);
			 //Checking people is exist or not
			
			 peopleID=jdbcTemplate.query(peopleCheck,new ResultSetExtractor<String>() {					
				    @Override
				    public String extractData(ResultSet rs) throws SQLException,DataAccessException {					
				    	String peopleID=null;
				    	if(rs!=null && rs.next()) {
				    		peopleID=rs.getString("ACTIVITY_ID");
				    	}					    	
						logger.debug("inside resultset $$$$ ..."+peopleID);
						return peopleID;						
				    }
				});	
		}		 		 
		 
		 logger.debug("peopleID... "+peopleID);

		 //insert into ACTIVITY_PEOPLE table after people insertion not in updation
		String finalPeopleId=null;
		if(peopleID == null) {

			 NextId nextId  = nextIdRepository.getNextId("PEOPLE_ID");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("PEOPLE_ID");
			 nextIdRepository.save(nextId);
			 int peopleId = nextId.getIdValue();

			//INSERTING INTO PEOPLE TABLE IF RECORD DOESNT EXIST
			 //add email and phone number
			 String insertPeople="INSERT INTO PEOPLE (PEOPLE_ID, PEOPLE_TYPE_ID, NAME, ADDR, CITY, STATE, ZIP, PHONE, EMAIL_ADDR,CREATED,CREATED_BY) VALUES ("+peopleId+", 2,'"+activity.getName()+"', '"+activity.getStrNo()+ " "+activity.getAddress()+"', '"+StringUtils.nullReplaceWithEmpty(activity.getCity())+"', '"+StringUtils.nullReplaceWithEmpty(activity.getState())+"', '"+activity.getZip()+"', '"+activity.getBusinessPhone()+"',  '"+activity.getEmail()+"',current_date,"+userId+")";
			 logger.debug("insertPeople..."+insertPeople);
			
			 //INSERTING INTO activity PEOPLE TABLE IF RECORD DOESNT EXIST
			 String insertActivityPeople="INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID, PSA_TYPE) VALUES ("+activity.getRenewalCode()+","+peopleId+",'A')";
			 logger.debug("insertActivityPeople..."+insertActivityPeople);
			
			jdbcTemplate.update(insertPeople);			 
			jdbcTemplate.update(insertActivityPeople);
			
			 finalPeopleId=StringUtils.i2s(peopleId);
		}else {
			 //INSERTING INTO activity PEOPLE TABLE IF RECORD DOESNT EXIST
			 String insertActivityPeople="INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID, PSA_TYPE) VALUES ("+activity.getRenewalCode()+","+peopleID+",'A')";
			 logger.debug("insertActivityPeople..."+insertActivityPeople);
			 jdbcTemplate.update(insertActivityPeople);				
		}

		//FEE UPDATES

		logger.debug("activity type..."+activity.toString());
		 NextId pymentId  = nextIdRepository.getNextId("PAYMENT_ID");
		 pymentId.setIdValue((pymentId.getIdValue()) + 1);
		 pymentId.setIdName("PAYMENT_ID");
		 nextIdRepository.save(pymentId);
		 int pymntId = pymentId.getIdValue();
		 logger.debug("pymntId..."+pymntId);
		 
		List<Fee> feeList =  commonService.getFeesFromTempActFees(activity.getRenewalCode(),activity.getTempId());
		
		double totalWithoutAdmistrativeFee=0.0;
		for(int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
				totalWithoutAdmistrativeFee=totalWithoutAdmistrativeFee+feeList.get(i).getFeeAmnt();	
				logger.debug("totalWithoutAdmistrativeFee.....############"+totalWithoutAdmistrativeFee);
			}
		}	
		logger.debug("totalWithoutAdmistrativeFee.....############"+totalWithoutAdmistrativeFee);
		
		//INSERT INTO PAYMENT TABLE
		 String insertPayment="INSERT INTO PAYMENT (PYMNT_ID, PYMNT_METHOD, PYMNT_TYPE, PYMNT_DT, PEOPLE_ID, PAYEE, PYMNT_AMNT,ENTER_BY_ID, AUTHORIZATION_ID, DEPT_CODE, ONLINETXNID) "
		 		+ "select distinct "+pymntId+",'creditcard','',current_date,"+finalPeopleId+",'"+activity.getName()+"', "+activity.getTotalFee()+","+userId+",'"+activity.getAuthCode()+"','LC','"+activity.getOnlineTransactionId()+"' "
		 		+ "from TEMP_ACTIVITY_DETAILS tad left join TEMP_ACTIVITY_FEE taf on tad.act_id=taf.activity_id and tad.id=taf.temp_id where "
		 		+ "tad.id="+activity.getTempId();
		 logger.debug("insertPayment..."+insertPayment);
		 jdbcTemplate.update(insertPayment);

		logger.debug("activity type..."+activity.toString());
		logger.debug("feeList..."+feeList.size());
		logger.debug("qtyOther..."+qtyOther+"test");
		
		if(StringUtils.nullReplaceWithEmpty(qtyOther).equalsIgnoreCase("") || qtyOther.equalsIgnoreCase("0")) {
			qtyOther="1";
		}
		

		String actID=null;		
		String feeAccount=null;
		for(int i=0;i<feeList.size();i++) {
			 NextId transId  = nextIdRepository.getNextId("TRANS_ID");
			 transId.setIdValue((transId.getIdValue()) + 1);
			 transId.setIdName("TRANS_ID");
			 nextIdRepository.save(transId);
			 int transsId = transId.getIdValue();
			 logger.debug("transsId..."+transsId);
			 
			 feeAccount = commonService.getFeesAccountForFeeId(feeList.get(i).getFeeId());
			 
			 logger.debug("fee total 630..."+feeList.get(i).getFeeTotal());
			 logger.debug("fee total 631..."+feeList.get(i).getTotalFee());
			 logger.debug("fee factor 632..."+feeList.get(i).getFeeFactor());
			 
			//INSERT INTO PAYMENT DETAIL TABLE
			String insertPaymentDetail="INSERT INTO PAYMENT_DETAIL (TRANS_ID, PYMNT_ID, ACT_ID, FEE_ID, PEOPLE_ID, AMNT, COMMENTS, FEE_ACCOUNT) "
					+ "values( "+transsId+","+pymntId+","+activity.getRenewalCode()+","+feeList.get(i).getFeeId()+","+finalPeopleId+","+feeList.get(i).getFeeAmnt()+", 'Transaction done by Online Renewal Portal','"+ feeAccount +"')";
			logger.debug("insertPaymentDetail..."+insertPaymentDetail);
			jdbcTemplate.update(insertPaymentDetail);

			//CHECKING IN ACTIVITY_FEE TABLE ROW IS EXIST OR NOT
			if(StringUtils.s2i(activity.getRenewalCode()) != 0 && !feeList.get(i).getFeeId().equalsIgnoreCase("")) {

				String activityFeeCheck="select * from ACTIVITY_FEE where ACTIVITY_ID="+activity.getRenewalCode()+" and FEE_ID = "+feeList.get(i).getFeeId();
				logger.debug("activityFeeCheck.. "+activityFeeCheck);
				 //Checking in activity fee table row is exist or not
				
				logger.debug("actID ::  "+actID);
				 actID=null;				 
				 actID=jdbcTemplate.query(activityFeeCheck,new ResultSetExtractor<String>() {						
					    @Override
					    public String extractData(ResultSet rs) throws SQLException,DataAccessException {
					    	String actID=null;
					    	if(rs!=null && rs.next()) {
					    		actID=rs.getString("ACTIVITY_ID");
					    	}					    	
							logger.debug("inside resultset $$$$ ..."+actID);
							return actID;
							
					    }
					});
				 logger.debug("activityFeeCheck outside jdbctemplate.. "+actID);
				 
			}		 		 
			 
			
			if(actID != null) {
				String updateActFee="";
				
				//UPDATE INTO ACTIVITY_FEE TABLE
				if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
					 updateActFee = "UPDATE ACTIVITY_FEE SET FEE_PAID="+feeList.get(i).getFeeAmnt()+" ,FEE_ID="+feeList.get(i).getFeeId()+",PEOPLE_ID="+finalPeopleId+",FEE_UNITS="+totalWithoutAdmistrativeFee+",FEE_AMNT="+feeList.get(i).getFeeAmnt()+" where ACTIVITY_ID="+activity.getRenewalCode() + " AND FEE_ID="+feeList.get(i).getFeeId();
					 logger.debug("update if condition of BT_ADMINISTRATION_FEE_DESC..."+updateActFee);
				}else {
					updateActFee = "UPDATE ACTIVITY_FEE SET FEE_PAID="+feeList.get(i).getFeeAmnt()+" ,FEE_ID="+feeList.get(i).getFeeId()+",PEOPLE_ID="+finalPeopleId+",FEE_UNITS="+qtyOther+",FEE_AMNT="+feeList.get(i).getFeeAmnt()+" where ACTIVITY_ID="+activity.getRenewalCode() + " AND FEE_ID="+feeList.get(i).getFeeId();
					logger.debug("update else condition of :: "+updateActFee);
				}
				logger.debug("updateActFee..."+updateActFee);
				jdbcTemplate.update(updateActFee);
			}else { 
				//INSERT INTO ACTIVITY_FEE TABLE
				 String insertActFee="";
				 if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
					 insertActFee="INSERT INTO ACTIVITY_FEE (ACTIVITY_ID,FEE_ID,PEOPLE_ID,FEE_UNITS,FEE_AMNT,FEE_PAID) values( "+activity.getRenewalCode()+","+feeList.get(i).getFeeId()+","+finalPeopleId+","+totalWithoutAdmistrativeFee+","+feeList.get(i).getFeeAmnt()+","+feeList.get(i).getFeeAmnt()+" )";
					 logger.debug("insert if condition of BT_ADMINISTRATION_FEE_DESC..."+insertActFee);
				 }else {
					 insertActFee="INSERT INTO ACTIVITY_FEE (ACTIVITY_ID,FEE_ID,PEOPLE_ID,FEE_UNITS,FEE_AMNT,FEE_PAID) values( "+activity.getRenewalCode()+","+feeList.get(i).getFeeId()+","+finalPeopleId+","+qtyOther+","+feeList.get(i).getFeeAmnt()+","+feeList.get(i).getFeeAmnt()+" )";
					 logger.debug("insert else condition "+insertActFee);
				 }
				 jdbcTemplate.update(insertActFee);	
			}			
		}
		 //payment table 1 record, payment_detail table will have 2 records

		 //delete data in all temporary tables(3)

		//INSERT INTO ATTACHMENT TABLE
		if(activity.getPermitNumber() != null && activity.getPermitNumber().startsWith("BL")) {

			List<Attachment> attachmentList =getAttachmentLists(activity.getTempId());
			for (Attachment attachment : attachmentList) {	//for(int i=0;i<attachmentList.size();i++) {
				 NextId attachmentId  = nextIdRepository.getNextId("ATTACHMENT_ID");
				 attachmentId.setIdValue((attachmentId.getIdValue()) + 1);
				 attachmentId.setIdName("ATTACHMENT_ID");
				 nextIdRepository.save(attachmentId);
				 int attachmentsId = attachmentId.getIdValue();
				 String insertAttachmentDetail="INSERT INTO ATTACHMENTS (ATTACH_ID, LEVEL_ID, ATTACH_LEVEL, FILE_NAME, DESCRIPTION, LKUP_ATTACHMENT_TYPE_ID, DELETED, CREATED, CREATED_BY) "
						+ "values( "+attachmentsId+","+activity.getRenewalCode()+",'A','"+attachment.getFileName()+"','"+attachment.getFileLoc()+"', 'RENEWAL ATTACHMENTS',"+attachment.getAttachmentTypeId()+",'N',CURRENT_DATE,"+userId+")";

				logger.debug("insertAttachmentDetail..."+insertAttachmentDetail);
				jdbcTemplate.update(insertAttachmentDetail);
				
			}	

		}
		}
		/*transactionManager.commit(status);*/
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	}catch(Exception e){
		logger.error("",e);
	}
	
	return activity;
}
/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity type
 * @param renewalCode
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentList(int renewalCode,String tempId) {
	String sql = "SELECT DISTINCT rownum, LATT.TYPE_ID, LATT.DESCRIPTION, LATT.ONLINE_AVAILABLE,LATT.DOCUMENT_URL, ta.FILENAME, ta.FILE_LOCATION,LATT.COMMENTS,"
			+ "LATT.UPLOAD_OR_DOWNLOAD_TYPE,LATT.BMC_POPUP_WINDOW_URL,to_char(LATT.BMC_DESCRIPTION) as BMC_DESCRIPTION FROM LKUP_ATTACHMENT_TYPE LATT "
			+ "JOIN REF_ACT_ATTACHMENT_TYPE RAAT ON RAAT.LKUP_ATTACHMENT_TYPE_ID = LATT.TYPE_ID JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE_ID = RAAT.LKUP_ACT_TYPE_ID "
			+ "JOIN ACTIVITY A ON A.ACT_TYPE = LAT.TYPE left join TEMP_ATTACHMENT ta on raat.LKUP_ATTACHMENT_TYPE_ID =ta.LKUP_ATTACHMENT_TYPE_ID and "
			+ "ta.temp_id="+tempId+" WHERE A.ACT_ID='"+renewalCode+"' AND LATT.ONLINE_AVAILABLE = 'Y' ORDER BY ROWNUM"; 
			
	logger.debug("Repo: 556 - getAttachmentList sql :"+sql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	List<String> commentsList = new ArrayList<String>(); 
	Map<String,String> bmcLinkDescMap = new HashMap<String, String>(); 
	
	try {
		attachmentList = jdbcTemplate.query(sql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	String comments = null;
        	Attachment attachment = new Attachment();        	
        	attachment.setAttachmentId(rs.getInt("rownum"));
        	attachment.setAttachmentTypeId(rs.getInt("TYPE_ID"));
        	attachment.setAttachmentDesc(rs.getString("DESCRIPTION"));
        	attachment.setDocumentURL(rs.getString("DOCUMENT_URL"));
        	attachment.setFileName(rs.getString("FILENAME"));
        	attachment.setComments(rs.getString("COMMENTS"));
        	attachment.setDownloadOrUploadType(rs.getString("UPLOAD_OR_DOWNLOAD_TYPE"));
        	attachment.setBmcDescription(rs.getString("BMC_DESCRIPTION"));  
        	if(rs.getString("BMC_DESCRIPTION") != null) {
        		bmcLinkDescMap.put(rs.getString("BMC_POPUP_WINDOW_URL"),rs.getString("BMC_DESCRIPTION"));
        	}        	
        	if(comments == null && rs.getString("COMMENTS") != null && !"".equals(rs.getString("COMMENTS"))) {
        		comments = rs.getString("COMMENTS");
        	}
        	if(comments != null && commentsList.size() == 0) {
        		commentsList.add(comments);
        	}
        	attachment.setBmcLinkDescMap(bmcLinkDescMap);
        	attachment.setCommentsList(commentsList);
        	attachment.setBmcPopUpWindowUrl(rs.getString("BMC_POPUP_WINDOW_URL"));
        	return  attachment;
        }
    });
	
     logger.debug("# of elements set in List - " + attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}

/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity
 * @param tempId
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentListForActivity(String tempId) {
	String sql = "select unique TYPE_ID,description,FILENAME,FILE_LOCATION from TEMP_ATTACHMENT ta left join REF_ACT_ATTACHMENT_TYPE raat on ta.LKUP_ATTACHMENT_TYPE_ID =raat.LKUP_ATTACHMENT_TYPE_ID left join LKUP_ATTACHMENT_TYPE lat on raat.LKUP_ATTACHMENT_TYPE_ID = lat.TYPE_ID "
			+ "WHERE ta.temp_id="+tempId;
	
	logger.debug("getAttachmentListForActivity sql :"+sql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	try {
		attachmentList = jdbcTemplate.query(sql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	Attachment attachment = new Attachment();
        	attachment.setAttachmentTypeId(rs.getInt("TYPE_ID"));
        	attachment.setAttachmentDesc(rs.getString("DESCRIPTION"));
        	attachment.setFileName(rs.getString("FILENAME"));
        	attachment.setFileLoc(rs.getString("FILE_LOCATION"));
        	return  attachment;
        }
    });
	
     logger.debug("# of elements set in List - " + attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}


/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity
 * @param tempId
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentListForTemp(int actId,String tempId) {
		String sql = "SELECT DISTINCT LATT.TYPE_ID, LATT.DESCRIPTION,LATT.ONLINE_AVAILABLE,LATT.DOCUMENT_URL,FILENAME,FILE_LOCATION FROM  TEMP_ATTACHMENT TA ,LKUP_ATTACHMENT_TYPE LATT JOIN REF_ACT_ATTACHMENT_TYPE RAAT ON RAAT.LKUP_ATTACHMENT_TYPE_ID = LATT.TYPE_ID JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE_ID = RAAT.LKUP_ACT_TYPE_ID JOIN ACTIVITY A ON A.ACT_TYPE = LAT.TYPE "
				+ "WHERE A.ACT_ID="+actId+" AND LATT.ONLINE_AVAILABLE = 'Y' and latt.TYPE_ID not in (select unique TYPE_ID from TEMP_ATTACHMENT ta left join REF_ACT_ATTACHMENT_TYPE raat on ta.LKUP_ATTACHMENT_TYPE_ID =raat.LKUP_ATTACHMENT_TYPE_ID left join LKUP_ATTACHMENT_TYPE lat on raat.LKUP_ATTACHMENT_TYPE_ID = lat.TYPE_ID WHERE ta.temp_id="+tempId+") AND  ta.temp_id="+tempId;
	
	logger.debug("getAttachmentListForActivity sql :"+sql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	try {
		attachmentList = jdbcTemplate.query(sql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	Attachment attachment = new Attachment();
        	attachment.setAttachmentTypeId(rs.getInt("TYPE_ID"));
        	attachment.setAttachmentDesc(rs.getString("DESCRIPTION"));
        	attachment.setFileName(rs.getString("FILENAME"));
        	attachment.setFileLoc(rs.getString("FILE_LOCATION"));
		return  attachment;
        }
    });
	
	 logger.debug("# of elements set in List - " + attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}

/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity
 * @param tempId
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentListForUpdatedAttachments(String tempId, String backButtonFlag) {
	String attachmentSql = "select distinct TYPE_ID,description,TA.FILENAME,ta.FILE_LOCATION from TEMP_ATTACHMENT ta left join REF_ACT_ATTACHMENT_TYPE raat on ta.LKUP_ATTACHMENT_TYPE_ID =raat.LKUP_ATTACHMENT_TYPE_ID left join LKUP_ATTACHMENT_TYPE lat on raat.LKUP_ATTACHMENT_TYPE_ID = lat.TYPE_ID "
			+ "WHERE ta.temp_id="+tempId;
	
	if(backButtonFlag==null) backButtonFlag = "";
	
	if(backButtonFlag.equals("")) {
		attachmentSql = attachmentSql + " AND TA.FILENAME is not NULL";
	}
	
	logger.debug("getAttachmentListForActivity sql :"+attachmentSql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	try {
		attachmentList = jdbcTemplate.query(attachmentSql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	Attachment attachment = new Attachment();
        	attachment.setAttachmentTypeId(rs.getInt("TYPE_ID"));
        	attachment.setAttachmentDesc(rs.getString("DESCRIPTION"));
        	attachment.setFileName(rs.getString("FILENAME"));
        	attachment.setFileLoc(rs.getString("FILE_LOCATION"));
        	return  attachment;
        }
    });
	
	 logger.debug("# of elements set in List - " + attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}

public void deleteAttachmentList(Activity activity) {
	logger.debug("in deleteAttachmentList repository..."+activity.getTempId());
	String sql="";
	try{	
		String deleteQuery ="DELETE FROM TEMP_ATTACHMENT WHERE TEMP_ID="+activity.getTempId();

		jdbcTemplate.update(deleteQuery);
		logger.debug("deleteAttachmentList... "+activity.getTempId());
		
	}catch(Exception e){
		logger.error("sql::" + sql);
		logger.error("",e);
		
	}
//	return activity;
} 
/**
* This method is to delete the TEMP_ACTIVITY_DETAILS table record on click of
* On Load of Permit Renewal Screen / Cancel Button / Payment success
* @param tempId
* @throws Exception
* @return Activity
*
*/

public int deleteTempActivity(String tempId) {
    logger.debug("Entered deleteTempActivity("+ tempId +"+) repository...");
    String sql="";
    TransactionDefinition def = new DefaultTransactionDefinition();
    TransactionStatus status = transactionManager.getTransaction(def);
    try{        
        sql = "DELETE FROM TEMP_ATTACHMENT WHERE TEMP_ID="+StringUtils.s2i(tempId);
        logger.debug("Delete Query  :"+sql);
        jdbcTemplate.execute(sql);
        
        sql = "DELETE FROM TEMP_ACTIVITY_FEE WHERE TEMP_ID="+StringUtils.s2i(tempId);
        logger.debug("Delete Query  :"+sql);
        jdbcTemplate.execute(sql);
        
        sql = "DELETE FROM TEMP_ACTIVITY_DETAILS WHERE ID="+StringUtils.s2i(tempId);
        logger.debug("Delete TEMP_ACTIVITY_DETAILS Query  :"+sql);
        jdbcTemplate.execute(sql);

		transactionManager.commit(status);    
        return 1;    
    }catch(Exception e){
        logger.error("Exception Occured ::" + sql);
        e.printStackTrace();
		 transactionManager.rollback(status);		
        return 0;
    }    
}
/**
 * This method is to fetch the USER ID from Users Table 
 * @param 
 * @throws Exception
 * @return Int
 * 
 */

public int getUserInfo(String userName) {
	String userSql = "SELECT USERID FROM USERS WHERE upper(USERNAME) ='"+userName.toUpperCase()+"'";
	
	logger.debug("getUserInfo sql :"+userSql);
	int userId = 0;
	
	try {
//			Object[] inputs = new Object[] {Constants.ONLINE_USER};
		 	userId = (int) jdbcTemplate.queryForObject(userSql,Integer.class);
	        return userId;
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	}catch(Exception e) {
		e.printStackTrace();
		return 0;
	}
	return userId;
}

/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity
 * @param tempId
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentLists(String tempId) {
	String sql = "select unique LKUP_ATTACHMENT_TYPE_ID,FILENAME,FILE_LOCATION from TEMP_ATTACHMENT ta left join TEMP_ACTIVITY_DETAILS tad on ta.temp_id=tad.id "
			+ "WHERE ta.temp_id="+tempId;
	
	logger.debug("getAttachmentLists sql :"+sql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	try {
		attachmentList = jdbcTemplate.query(sql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	Attachment attachment = new Attachment();
        	attachment.setAttachmentTypeId(rs.getInt("LKUP_ATTACHMENT_TYPE_ID"));
        	attachment.setFileName(rs.getString("FILENAME"));
        	attachment.setFileLoc(rs.getString("FILE_LOCATION"));
		return  attachment;
        }
    });
	
     logger.debug("# of elements set in List - " + attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}


/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity
 * @param tempId
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Fee> getFeeListForActivity(String tempId) {
	String sql = "select unique tf.ACTIVITY_ID,tf.FEE_ID,tf.FEE_UNITS,tf.FEE_AMNT,tf.TOTAL_FEE_AMNT from TEMP_ACTIVITY_FEE TF left join  TEMP_ACTIVITY_DETAILS TAF ON TF.TEMP_ID=TAF.ID "
			+ "WHERE tf.temp_id="+tempId;
	
	logger.debug("getFeeListForActivity sql :"+sql);
	
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        	Fee fee = new Fee();
        	fee.setFeeId(rs.getString("FEE_ID"));
        	fee.setFeeAmnt(rs.getDouble("FEE_AMNT"));
        	fee.setFeeUnits(rs.getDouble("FEE_UNITS"));
        	fee.setTotalFee(rs.getDouble("TOTAL_FEE_AMNT"));
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return feeList;
}

public List<Fee> getFeelistForCalculations(List<Fee> feeList) throws BasicExceptionHandler{
	
	Map<String, String> lkupSystemDataMap =   commonService.getLkupSystemDataMap();
	String lateFeeFromDate = "";
	String lateFeeToDate = "";
	logger.debug("lkupSystemDataMap.size() :: " +lkupSystemDataMap.size());
		if(lkupSystemDataMap.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
			lateFeeFromDate = lkupSystemDataMap.get(Constants.BL_LATE_FEE_FROM_DATE);
		}
		
		if(lkupSystemDataMap.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
			lateFeeToDate = lkupSystemDataMap.get(Constants.BL_LATE_FEE_TO_DATE);
		}
	
	double totalFee = 0;
	double lateFeeFactor = 0;
	double licenseFeeAnnualTotalFactor = 0;
	

	SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");  
	 String strDate = formatter.format(new Date());  
     
	for (int i=0;i<feeList.size();i++) {
		if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
			logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

			   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
				lateFeeFactor = feeList.get(i).getFeeFactor();
				logger.debug("lateFeeFactor ::  " +lateFeeFactor);				
			}
		}
		
		if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
			licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
			logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
		}
	}
	
	for (int i=0;i<feeList.size();i++) {
		if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
			logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

			   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {

				totalFee = totalFee + (lateFeeFactor * licenseFeeAnnualTotalFactor);// When it is Late Fee, get Factor instead of feeFactor	 
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor)));
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor));
				logger.debug("totalFee... " +totalFee);					
			}else {
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeDesc("");
			}
		}
		logger.debug("feeList.get(i).getFeeDesc() :: " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
		
		if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
			logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
			feeList.get(i).setFeeTotal(StringUtils.s2d(""));
			feeList.get(i).setFeeTotalStr("");
			feeList.get(i).setFeeDesc("");
		}
		if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC) && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
			totalFee = totalFee + feeList.get(i).getFeeTotal();// When it is other than Late Fee, get FeeFactor instead of Factor					
			logger.debug("Line 324  Fee details.... " +feeList.get(i).getFeeTotal() + " :: Total Fee :: " +totalFee);
		}
	}
		logger.debug("feeList.size()..."+feeList.size());
		return feeList;	
}

public String get(String renewalCode) {
	String sql = "SELECT * FROM activity WHERE act_id="+renewalCode;
	logger.debug("getActivityRenewalOnline sql :"+sql);
	String renewalOnline = "N";
	try {
		renewalOnline=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String renewalOnline=rs.getString("RENEWAL_ONLINE");
				return renewalOnline;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return renewalOnline;
}

/**
 * This method is to fetch the Attachment type description to show list of 
 * attachments are mapped to the activity type
 * @param renewalCode
 * @throws Exception
 * @return List<Attachment>
 * 
 */

public List<Attachment> getAttachmentListForReUpload(String renewalCode) {
	
	String sql = "SELECT rownum,LATT.TYPE_ID, LATT.DESCRIPTION,LATT.ONLINE_AVAILABLE,LATT.DOCUMENT_URL,LATT.BMC_POPUP_WINDOW_URL,LATT.COMMENTS, "
			+ "to_char(LATT.BMC_DESCRIPTION) as BMC_DESCRIPTION,ATT.FILE_NAME FROM  LKUP_ATTACHMENT_TYPE LATT JOIN "
			+ "REF_ACT_ATTACHMENT_TYPE RAAT ON RAAT.LKUP_ATTACHMENT_TYPE_ID = LATT.TYPE_ID JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE_ID = RAAT.LKUP_ACT_TYPE_ID "
			+ "JOIN ACTIVITY A ON A.ACT_TYPE = LAT.TYPE left join BL_activity ba on a.act_id=ba.act_id LEFT JOIN Attachments ATT ON ATT.level_id=A.ACT_ID "
			+ "and LATT.TYPE_ID=ATT.LKUP_ATTACHMENT_TYPE_ID WHERE A.ACT_ID="+renewalCode+"  AND LATT.ONLINE_AVAILABLE = 'Y' ORDER BY ROWNUM";
			
	logger.debug("getAttachmentListForReUpload sql :"+sql);
	
	List<Attachment> attachmentList = new ArrayList<Attachment>(); 
	List<String> commentsList = new ArrayList<String>(); 
	Map<String,String> bmcLinkDescMap = new HashMap<String, String>(); 
	try {
		attachmentList = jdbcTemplate.query(sql,new RowMapper<Attachment>() {
        @Override
        public Attachment mapRow(ResultSet rs, int i) throws SQLException {
        	String comments = null;
        	Attachment attachment = new Attachment();
        	attachment.setAttachmentId(rs.getInt("rownum"));
        	attachment.setAttachmentTypeId(rs.getInt("TYPE_ID"));
        	attachment.setAttachmentDesc(rs.getString("DESCRIPTION"));
        	attachment.setDocumentURL(rs.getString("DOCUMENT_URL"));
        	attachment.setFileName(rs.getString("FILE_NAME"));
        	attachment.setBmcDescription(rs.getString("BMC_DESCRIPTION"));
        	attachment.setBmcPopUpWindowUrl(rs.getString("BMC_POPUP_WINDOW_URL"));
        	attachment.setComments(rs.getString("COMMENTS"));
        	if(rs.getString("BMC_DESCRIPTION") != null) {
        		bmcLinkDescMap.put(rs.getString("BMC_POPUP_WINDOW_URL"),rs.getString("BMC_DESCRIPTION"));
        	}        	
        	if(comments == null && rs.getString("COMMENTS") != null && !"".equals(rs.getString("COMMENTS"))) {
        		comments = rs.getString("COMMENTS");
        	}
        	if(comments != null && commentsList.size() == 0) {
        		commentsList.add(comments);
        	}
        	attachment.setBmcLinkDescMap(bmcLinkDescMap);
        	attachment.setCommentsList(commentsList);
        	
		return  attachment;
        }
    });
	
	 logger.debug("attachmentList size  :"+attachmentList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return attachmentList;
}


public boolean saveStreamToAttachment(Activity activity,String attachmentTypeId) throws MultipartException,Exception {

	boolean didConvert = false;
	OutputStream bos = null;

	String sql = "SELECT * from lkup_system WHERE NAME='TEMP_FILE_LOC'";
	logger.debug("TEMP file location sql :"+sql);
	
		logger.debug(jdbcTemplate.getFetchSize());
		
		String location=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	logger.debug(jdbcTemplate.getFetchSize());
				return rs.getString("VALUE");		
		    }
		});
		
	try {
		
		 List<MultipartFile> filesList=activity.getTheFile();
		if(null != filesList && filesList.size() > 0) {
			for (MultipartFile multipartFile : filesList) {
				if(multipartFile != null && multipartFile.getOriginalFilename() !=null && !multipartFile.getOriginalFilename().equalsIgnoreCase("")) {

					String fileName1 = multipartFile.getOriginalFilename();
					
					File f=new File(fileName1);
					String tempFileName=activity.getRenewalCode()+"_"+f.getName().replaceAll("[^\\d\\w _.-]", "-");
					logger.debug("file name 1266.. "+tempFileName);
					
					//create directory if we don't have

					File fileSaveDir = new File(location);
			        if (!fileSaveDir.exists()) {
			            fileSaveDir.mkdir();
			        }
			        /*location=location+activity.getRenewalCode();
					File permitNumDir = new File(location);
			        if (!permitNumDir.exists()) {
			        	permitNumDir.mkdir();
			        }*/
					 // Get the file and save it somewhere
					byte[] bytes = multipartFile.getBytes();
					logger.debug("length..."+bytes.length);
					Path path = Paths.get(location+tempFileName);					
			        
					Files.write(path, bytes);

					 NextId nextId  = nextIdRepository.getNextId("ATTACHMENT_ID");
					 nextId.setIdValue((nextId.getIdValue()) + 1);
					 nextId.setIdName("ATTACHMENT_ID");
					 nextIdRepository.save(nextId);
					 int attachmentId = nextId.getIdValue();
					
				    		String insertQuery ="INSERT INTO ATTACHMENTS (ATTACH_ID, LEVEL_ID, ATTACH_LEVEL, FILE_NAME, LOCATION, CREATED, DESCRIPTION, LKUP_ATTACHMENT_TYPE_ID, DELETED) "
				    				+ "values( "+attachmentId+","+activity.getRenewalCode()+",'A','"+tempFileName+"','"+location+"', CURRENT_DATE,'RENEWAL ATTACHMENTS',"+attachmentTypeId+",'N')" ;
				    		
				    		logger.debug("saveAttachmentList  :"+insertQuery);
				    		jdbcTemplate.update(insertQuery);	
					}
	
				}
			}
		return didConvert;
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch(MultipartException e) {
		didConvert = false;
		logger.error("");
	} catch (Exception e) {
		logger.error("problem while saving file " + e.getMessage());
		didConvert = false;
		e.printStackTrace();
		throw new Exception("Problem while saving file " + e.getMessage()+e);
	} finally {
		if (bos != null) {
			// flush the stream
	         bos.flush();

	         // close the stream but it does nothing
	         bos.close();
		}
	}
	return didConvert;
}

/**
 * This method is for checking whether the particular business type file is uploaded or not
 * 
 * @throws IOException
 * @return response object, renewalCode
 * @Parameters
 */
public JSONObject checkFileUploadedOrNot(int renewalCode,String tempId) {
	List<Attachment> attachments = getAttachmentList(renewalCode, tempId);
	List<Integer> typeIds = new ArrayList<Integer>();
	List<Integer> uploadedTypeIds = new ArrayList<Integer>();
	List<Integer> remainingIds = new ArrayList<Integer>();
	JSONObject obj = new JSONObject();
	
	
	for(int i = 0; i< attachments.size(); i++) {
		typeIds.add(attachments.get(i).getAttachmentTypeId());
	}	
	
	try {
		String sql = "select LKUP_ATTACHMENT_TYPE_ID from TEMP_ATTACHMENT a,TEMP_ACTIVITY_DETAILS b where a.TEMP_ID = b.ID and b.ACT_ID = "+renewalCode;
		logger.debug("temp attachments = "+sql);
		jdbcTemplate.query(sql, new RowCallbackHandler() {
	    	 public void processRow(ResultSet rs) throws SQLException {
	    	 	if (rs != null) {
	    	 		uploadedTypeIds.add(rs.getInt("LKUP_ATTACHMENT_TYPE_ID"));
	    	 	}
	    	 }
	    });	     
		
		obj.put("uploadedTypeIds", uploadedTypeIds);
		obj.put("typeIds", typeIds);
		
		
	} catch(EmptyResultDataAccessException e) {		
		logger.error("",e);
	}catch (Exception e1) {
		logger.error("",e1);
	}	
	return obj;
}

/**
 * This method is to fetch the address id for address type. 
 * This is to maintain for Multi_Address table
 * @param addressType
 * @return
 */
public String getAddressTypeId(String addressType) {
	String sql = "select * from LKUP_ADDRESS_TYPE where ADDRESS_TYPE='"+addressType+"'";
	logger.debug("getAddressTypeId sql :"+sql);
	String addressTypeId = "";
	try {
		addressTypeId=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String addressTypeId=rs.getString("ID");
				return addressTypeId;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}	
	return addressTypeId;
}



public int checkProjectName(String ProjectName, int lsoId) throws Exception {
	logger.info("checkProjectName(" + ProjectName + ", " + lsoId + ")");
	String sql = "";
	int projectId = 0;
	sql = "SELECT PROJ_ID,DESCRIPTION  FROM PROJECT WHERE UPPER(NAME) =" + StringUtils.checkString(ProjectName).toUpperCase() + " AND LSO_ID = " + lsoId;
	logger.debug(" sql statement " + sql);

	//RowSet rs = new Wrapper().select(sql);
	try {
		projectId = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
		    @Override
		    public Integer mapRow(ResultSet rs, int i) throws SQLException {
		    	int projId = 0;	    	
		    	projId = rs.getInt("PROJ_ID");
		    	logger.debug("project Id is " + projId);
		    	
				return projId;
		    }
		});	
	}catch(EmptyResultDataAccessException e) {
		logger.debug("No project id "+e.getMessage());
		return 0;
	}

	return projectId;
}

/**
 * Get Activity Non Location Type for Activity Id
 * 
 * @param activityId
 * @return
 * @throws Exception
 */
public int getActivityNonLocationalType(int activityId) throws Exception {
	logger.info("getActivityNonLocationalType(" + activityId + ")");

	String sql = "select location_type from activity where act_id=" + activityId;
	logger.debug(sql);
	int locationType = -1;
	try {
		logger.info(sql);
		locationType = jdbcTemplate.queryForObject(sql,new RowMapper<Integer>() {
		    @Override
		    public Integer mapRow(ResultSet rs, int i) throws SQLException {
			    int locType = 0;
			    if (rs.next()) {
			    	locType = rs.getInt("location_type");
				}
				return locType;		    
		    }
		});		
		
	} catch (Exception e) {
		logger.debug("Exception thrown while executing getActivityNonLocationalType() " + e.getMessage());
		throw e;
	}
	return locationType;	
}

/**
 * Gets the activity address for a given activity id address
 * 
 * @param activityId
 * @return
 * @throws Exception
 */
public String getActivityAddressForAddress(int activityId) throws Exception {
	logger.info("getActivityAddressForAddress(" + activityId + ")");

	String address = "";
	String sql = "select dl_address as address from v_activity_address where act_id=" + activityId;

	try {
		logger.info(sql);
		address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address = "";
				if (rs.next()) {
					address = rs.getString(1);
				}
				return address;
		    }
		});
		
	} catch (Exception e) {
		logger.debug("Exception thrown while getting getActivityAddressForIdAddress " + e.getMessage());
		throw e;
	}
	return address;
}

/**
 * Gets the activity address for a given activity id cross street
 * 
 * @param activityId
 * @return
 * @throws Exception
 */
public String getActivityAddressForCrossStreet(int activityId) throws Exception {
	logger.info("getActivityAddressForCrossStreet(" + activityId + ")");

	String address = "";
	String sql = "select vsl1.street_name || '  & ' || vsl2.street_name as address from v_street_list vsl1,v_street_list vsl2 where vsl1.street_id = (select street_id1 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + ")) and vsl2.street_id=(select street_id2 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + "))";

	try {
		logger.info(sql);
		address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					address = rs.getString(1);
				}
				return address;
		    }
		});
		
	} catch (Exception e) {
		logger.debug("Exception thrown while getting activity getActivityAddressForCrossStreet " + e.getMessage());
		throw e;
	}
	return address;
}

/**
 * Gets the activity address for a given activity id address range
 * 
 * @param activityId
 * @return
 * @throws Exception
 */
public String getActivityAddressForRange(int activityId) throws Exception {
	logger.info("getActivityAddressForRange(" + activityId + ")");

	String address = "";
	String sql = "";
	String sql1 = "select aar.FROM_STR_NO1 || ' - ' || aar.TO_STR_NO1 ||' '|| vsl.street_name as address,aar.street_id1 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id1 where a.act_id=" + activityId;
	String sql2 = "select aar.FROM_STR_NO2 || ' - ' || aar.TO_STR_NO2 ||' '|| vsl.street_name as address,aar.street_id2 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id2 where a.act_id=" + activityId;
	String sql3 = "select aar.FROM_STR_NO3 || ' - ' || aar.TO_STR_NO3 ||' '|| vsl.street_name as address,aar.street_id3 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id3 where a.act_id=" + activityId;
	String sql4 = "select aar.FROM_STR_NO4 || ' - ' || aar.TO_STR_NO4 ||' '|| vsl.street_name as address,aar.street_id4 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id4 where a.act_id=" + activityId;
	String sql5 = "select aar.FROM_STR_NO5 || ' - ' || aar.TO_STR_NO5 ||' '|| vsl.street_name as address,aar.street_id5 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id5 where a.act_id=" + activityId;
	//Wrapper db = new Wrapper();
	try {
		logger.info(sql);

		//RowSet rs = db.select(sql1);
		address = jdbcTemplate.queryForObject(sql1,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					address = rs.getString(1);
				}
				return address;
		    }
		});
		//rs = db.select(sql2);
		address = jdbcTemplate.queryForObject(sql2,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					if (!(rs.getString("street_id2").equalsIgnoreCase("-1"))) {
						address = address + " , " + rs.getString("address");
					}
				}
				return address;
		    }
		});
		//rs = db.select(sql3);
		address = jdbcTemplate.queryForObject(sql3,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					if (!(rs.getString("street_id3").equalsIgnoreCase("-1"))) {
						address = address + " , " + rs.getString("address");
					}
				}
				return address;
		    }
		});
		//rs = db.select(sql4);
		address = jdbcTemplate.queryForObject(sql4,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					if (!(rs.getString("street_id4").equalsIgnoreCase("-1"))) {
						address = address + " , " + rs.getString("address");
					}
				}
				return address;
		    }
		});
		//rs = db.select(sql5);
		address = jdbcTemplate.queryForObject(sql5,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					if (!(rs.getString("street_id5").equalsIgnoreCase("-1"))) {
						address = address + " , " + rs.getString("address");
					}
				}
				return address;
		    }
		});
		
	} catch (Exception e) {
		logger.debug("Exception thrown while getting activity getActivityAddressForRange " + e.getMessage());
		throw e;
	}
	return address;
}

/**
 * Gets the activity address for a given activity id address landmark
 * 
 * @param activityId
 * @return
 * @throws Exception
 */
public String getActivityAddressForLandmark(int activityId) throws Exception {
	logger.info("getActivityAddressForLandmark(" + activityId + ")");

	String address = "";
	String sql = "select landmark_name as address from lkup_landmark where addr_id = (select addr_id from activity where act_id=" + activityId + ")";

	try {
		logger.info(sql);
		//RowSet rs = new Wrapper().select(sql);
		address = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String address="";
				if (rs.next()) {
					address = rs.getString(1);
				}
				return address;
		    }
		});
		
	} catch (Exception e) {
		logger.debug("Exception thrown while getting activity getActivityAddressForLandmark " + e.getMessage());
		throw e;
	}
	return StringUtils.properCase(address);
}


}