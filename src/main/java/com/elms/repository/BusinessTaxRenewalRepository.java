/**
 * 
 */
package com.elms.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.NextId;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;

/**
 * @author Gayathri Turlapati
 *
 */
@Repository
public class BusinessTaxRenewalRepository {
	

private static final Logger logger = Logger.getLogger(BusinessTaxRenewalRepository.class);
	
	@Autowired
	JdbcTemplate  jdbcTemplate;

	@Autowired
	NextIdRepository nextIdRepository;
	
	@Autowired
	CommonRepository commonRepository;

	@Autowired
	PlatformTransactionManager transactionManager;
	
	@Autowired
	DataSource dataSource;
	@Autowired
	CommonService commonService;
	@Autowired
	BusinessTaxActivity businessTaxActivity;
	
	public void setDataSource(DataSource dataSource) {
	    this.dataSource = dataSource;
	    this.jdbcTemplate = new JdbcTemplate(dataSource);
	 }
	 public void setTransactionManager(PlatformTransactionManager transactionManager) {
	    this.transactionManager = transactionManager;
	 }

	private double pfTotal;
	
	/**
	 * This method is to fetch the Activity Information to be displayed on all the screens 
	 * like Business Address, Activity Number, Business Mailing Address
	 * @param busAccNo, renewalCode
	 * @throws Exception
	 * @return Activity
	 * 
	 */
	public Activity getBTActivityInfo(String busAccNo, String renewalCode) {
		String sql =  "SELECT BT.QTY_OTHER,CASE WHEN LQ.ID IS NULL THEN LQR.ID ELSE LQ.ID END AS LKUP_BT_QTY_ID,A.APPLICATION_ONLINE, "
				+ "CASE WHEN LQ.DESCRIPTION IS NULL THEN LQR.DESCRIPTION ELSE LQ.DESCRIPTION END AS QTY_DESC, "
				+ "CASE WHEN LQ.PLACEHOLDER_DESCRIPTION IS NULL THEN LQR.PLACEHOLDER_DESCRIPTION ELSE LQ.PLACEHOLDER_DESCRIPTION END AS PLACEHOLDER_DESCRIPTION, "
				+ "CASE WHEN LQ.LINE_PATTERN IS NULL THEN LQR.LINE_PATTERN ELSE LQ.LINE_PATTERN END AS LINE_PATTERN, "
				+ "CASE WHEN LQ.LINE1_DESC IS NULL THEN LQR.LINE1_DESC ELSE LQ.LINE1_DESC END AS LINE1_DESC, "
				+ "CASE WHEN LQ.LINE2_DESC IS NULL THEN LQR.LINE2_DESC ELSE LQ.LINE2_DESC END AS LINE2_DESC, "
				+ "CASE WHEN LQ.LINE3_DESC IS NULL THEN LQR.LINE3_DESC ELSE LQ.LINE3_DESC END AS LINE3_DESC, "
				+ "TAD.BUSINESS_NAME AS TEMP_BUSINESS_NAME, "
			    + "TAD.EMAIL AS TEMP_EMAIL, TAD.BUSINESS_PHONE AS TEMP_BUSINESS_PHONE,TAD.STR_NO, "
			    + "TAD.ADDRESS, TAD.UNIT, TAD.CITY, TAD.STATE, TAD.ZIP,  TAD.TEMP_BT_QTY, TAD.BUSINESS_ACC_NO AS TEMP_BUSINESS_ACC_NO,"
			    + "TAD.TEMP_BT_QTY_FLAG, BT.BUSINESS_ACC_NO AS BUSINESS_ACC_NO,A.ACT_ID, "
			    + "A.ACT_TYPE, A.ACT_NBR,LA.DESCRIPTION, BT.BURBANK_BUSINESS AS BURBANK_BUSINESS, "
			    + "BT.BUSINESS_FAX AS BUSINESS_FAX,BT.BUSINESS_NAME AS BUSINESS_NAME, BT.BUSINESS_PHONE AS BUSINESS_PHONE, "
			    + "BT.BUSINESS_PHONE_EXT AS BUSINESS_PHONE_EXT,BT.EMAIL AS EMAIL,BT.SQ_FOOTAGE,MA.CITY AS MAIL_CITY, "
			    + "MA.STATE AS MAIL_STATE,MA.STREET_NAME1 AS MAIL_STR_NAME,MA.STREET_NUMBER AS MAIL_STR_NO,MA.UNIT AS MAIL_UNIT,MA.ZIP AS MAIL_ZIP,"
			    + "TAD.ID AS TEMP_ID,"
			    + "CASE WHEN BT.OOT_STR_NO IS NULL THEN CAST(VAA.STR_NO AS VARCHAR(20)) ELSE CAST(BT.OOT_STR_NO AS VARCHAR(100)) END  AS BUSINESS_STR_NO,"
			    + "CASE WHEN BT.OOT_STR_NAME IS NULL THEN VAA.BL_ADDRESS ELSE (OOT_STR_NO || ' ' || BT.OOT_STR_NAME) END AS BUSINESS_STR_NAME, "
			    + "CASE WHEN BT.OOT_CITY IS NULL THEN 'BURBANK' ELSE COALESCE(BT.OOT_CITY,'') END  AS BUSINESS_CITY, "
			    + "CASE WHEN BT.OOT_STATE IS NULL THEN 'CA' ELSE COALESCE(' ' || BT.OOT_STATE,'') END AS BUSINESS_STATE"
			    + ", CASE WHEN BT.OOT_ZIP IS NULL THEN  CAST(91505 AS VARCHAR(5)) ELSE COALESCE(' ' || BT.OOT_ZIP ,'') END AS BUSINESS_ZIP "
			    + "FROM ACTIVITY A "
			    + "LEFT OUTER JOIN LKUP_ACT_TYPE LA ON A.ACT_TYPE=LA.TYPE "
			    + "LEFT OUTER JOIN BT_ACTIVITY BT ON BT.ACT_ID =A.ACT_ID "
			    + "LEFT OUTER JOIN V_ACTIVITY_ADDRESS VAA ON BT.ACT_ID=VAA.ACT_ID "
			    + "LEFT OUTER JOIN LKUP_BT_QTY LQ ON BT.QTY_ID=LQ.ID "
			    + "LEFT OUTER JOIN REF_ACT_BT_QTY_TYPE RAQ ON LA.TYPE_ID = RAQ.LKUP_ACT_TYPE_ID "
			    + "LEFT OUTER JOIN LKUP_BT_QTY LQR ON RAQ.LKUP_BT_QTY_TYPE_ID=LQR.ID "
			    + "LEFT OUTER JOIN MULTI_ADDRESS MA ON A.ACT_ID=MA.ACT_ID AND MA.ADDRESS_TYPE_ID=1 "
			    + "LEFT OUTER JOIN TEMP_ACTIVITY_DETAILS TAD ON A.ACT_ID=TAD.RENEWAL_CODE  "
			    + "WHERE BT.BUSINESS_ACC_NO ='"+busAccNo.trim()+"' AND A.ACT_ID = "+renewalCode+"";
				
		logger.debug("ActivityInfo sql :"+sql);
		Activity activityList=new Activity();
		Activity activity = new Activity();
		
		try {   
			activityList=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
				
			    @Override
			    public Activity mapRow(ResultSet rs, int i) throws SQLException {
		    	
			    	activity.setRenewalCode(StringUtils.i2s(rs.getInt("ACT_ID")));
			    	activity.setActType(rs.getString("ACT_TYPE"));
			    	activity.setPermitNumber(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR")));
			    	activity.setDescription(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));		    	
			    	activity.setEmail(StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL")));			    	
			    	activity.setQtyDesc(StringUtils.nullReplaceWithEmpty(rs.getString("QTY_DESC")));
			    	activity.setOnlineBLBTApplication(rs.getString("APPLICATION_ONLINE"));
			    	String qtyOther = "";   	
			    	
			    	if((activity.getActType().equalsIgnoreCase("CR") || activity.getActType().equalsIgnoreCase("CRS")) && rs.getInt("LKUP_BT_QTY_ID") == Constants.SQUARE_FOOTAGE) {
			    		qtyOther = StringUtils.nullReplaceWithEmpty(rs.getString("SQ_FOOTAGE"));
			    	}else {
			    		qtyOther = StringUtils.nullReplaceWithEmpty(rs.getString("QTY_OTHER"));
			    	}
			    	
			    	if(!qtyOther.equals("")) {
			    		activity.setQtyOther(qtyOther);			    		
			    	}else{
			    		if(rs.getInt("LKUP_BT_QTY_ID") == StringUtils.s2i(Constants.NO_OF_EMPLOYEES_CODE)) {
			    			activity.setQtyOther("0");
			    		}else {
			    			activity.setQtyOther("1");
			    		}
			    	}
			    	
			    	
			    	activity.setPlaceholderDesc(StringUtils.nullReplaceWithEmpty(rs.getString("PLACEHOLDER_DESCRIPTION")));
			    	activity.setLine1Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE1_DESC")));
			    	activity.setLine2Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE2_DESC")));
			    	activity.setLine3Desc(StringUtils.nullReplaceWithEmpty(rs.getString("LINE3_DESC")));
			    	activity.setLinePattern(StringUtils.nullReplaceWithEmpty(rs.getString("LINE_PATTERN")));
			    	
			    	activity.setTempBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BUSINESS_NAME")));
			    	activity.setTempEmail(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_EMAIL")));
			    	String tempBusinessPhone = rs.getString("TEMP_BUSINESS_PHONE");
			    	if(tempBusinessPhone != null) {
			    		tempBusinessPhone = tempBusinessPhone.replaceAll("\\W+","");
			    	}
			    	activity.setTempBusinessPhone(StringUtils.nullReplaceWithEmpty(StringUtils.phoneFormat(StringUtils.nullReplaceWithEmpty(tempBusinessPhone))));
			    	activity.setTempBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BUSINESS_ACC_NO")));
			    	activity.setTempId(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_ID")));
			    	activity.setTempBtQtyFlag(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BT_QTY_FLAG")));
			    	
			    	if(activity.getTempBtQtyFlag()!=null && activity.getTempBtQtyFlag().equalsIgnoreCase("Yes")) {
			    		activity.setTempBtQtyFlag("Y");
					}else{
						activity.setTempBtQtyFlag("N");
					}
			    			    	
			    	activity.setStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")));
			    	activity.setStrName(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));
			    	activity.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));
			    	activity.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
			    	activity.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
			    	activity.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
			    	activity.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
			    	activity.setCityStateZip(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STATE")) +" "+ StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
			    	
			    	activity.setTempBtQty(StringUtils.nullReplaceWithEmpty(rs.getString("TEMP_BT_QTY")));
			    	
			    	activity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));		    	
			    	
			    	activity.setBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ACC_NO")));
			    	activity.setBusinessFax(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_FAX")));		    	
			    	String businessPhone = rs.getString("BUSINESS_PHONE");
			    	if(businessPhone != null) {
			    		businessPhone = businessPhone.replaceAll("\\W+","");
			    	}
			    	activity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(StringUtils.phoneFormat(StringUtils.nullReplaceWithEmpty(businessPhone))));
			    	activity.setBusinessPhoneExt(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_PHONE_EXT")));
			    	activity.setBusinessStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STR_NO")));
			    	activity.setBusinessStrName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STR_NAME")));
			    	activity.setBusinessCity(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_CITY")));
			    	activity.setBusinessState(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STATE")));
			    	activity.setBusinessZip(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ZIP")));
			    	activity.setBusinessCityStateZip(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_CITY")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_STATE")) +" "+ StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ZIP")));
			    	
			    	activity.setBurbankBusiness(StringUtils.nullReplaceWithEmpty(rs.getString("BURBANK_BUSINESS")));
			    	
			    	activity.setMailStrName(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NAME")));
			    	activity.setMailStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NO")));
			    	activity.setMailUnit(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_UNIT")));
			    	activity.setMailCity(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_CITY")));
			    	activity.setMailState(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STATE")));
			    	activity.setMailZip(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_ZIP")));
					return activity;				
			    }
			});
		
		} catch(EmptyResultDataAccessException er) {
			logger.error("",er);		
		}catch (Exception e) {
			logger.error("",e);		
		}	
		return activityList;	
	}
	
	public Activity updateUserDetails(Activity activity) throws BasicExceptionHandler {
		logger.debug("in updateUserDetails repository...");
		try{		
			String busniessName=activity.getBusinessName();
			busniessName=activity.getBusinessName().replaceAll("'", "''");
			logger.debug("updateUserDetails"+activity.toString());

			String addr = activity.getAddress();
			if(activity.getAddress().contains("'")) {
			addr= addr.replaceAll("'", "''");
			}
			logger.debug("addr... "+addr);

			String updateQuery = "update TEMP_ACTIVITY_DETAILS set act_id='"+activity.getRenewalCode()+"', ACT_NBR='"+activity.getPermitNumber().trim()+"', EMAIL='"+activity.getEmail()+"',NAME ='"+activity.getName()+"', BUSINESS_PHONE='"+activity.getNewPhone()+"', STR_NO='"+activity.getStrNo()+"', ADDRESS='"+addr+"', UNIT='"+activity.getUnit()+"', CITY='"+activity.getCity()+"', STATE='"+activity.getState()+"', ZIP='"+activity.getZip()+"', BUSINESS_NAME='"+busniessName+"' WHERE ID="+activity.getTempId() ;

			logger.debug("updateQuery  :"+updateQuery);
			jdbcTemplate.update(updateQuery);		
		
		}catch(Exception e){
			logger.error("",e);
			throw new BasicExceptionHandler(e);			
		}
		return activity;
	}	
	

	public void saveBtFeeDetails(Activity activity) {
		logger.debug("in saveFeeDetails repository...");
		String qty = activity.getTempBtQty();
		if(qty==null || qty.equalsIgnoreCase("")) qty="1";
		List<Fee> feeList=new ArrayList<Fee>();
		feeList=activity.getFeeList();
		String insertQuery = "";
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		    
		try{
			String deleteQuery ="DELETE FROM TEMP_ACTIVITY_FEE WHERE TEMP_ID="+activity.getTempId();			
			logger.debug("Feelist.size()  :"+feeList.size());
			jdbcTemplate.update(deleteQuery);	
			for(int i=0; i<feeList.size();i++) {
				if(feeList.get(i).getFeeTotal()>0.0) {
					insertQuery = "INSERT INTO TEMP_ACTIVITY_FEE (TEMP_ID,ACTIVITY_ID,FEE_ID,FEE_UNITS,FEE_AMNT,TOTAL_FEE_AMNT) values "
					+ "("+activity.getTempId()+","+activity.getRenewalCode()+","+feeList.get(i).getFeeId()+","+qty+","+feeList.get(i).getFeeTotal()+", "+activity.getTotalFee()+" )";
					
					logger.debug("saveFeeDetails  :"+insertQuery);
					jdbcTemplate.update(insertQuery);
				}
				
			}

			transactionManager.commit(status);
		}catch(Exception e){
			logger.error("",e);
			 transactionManager.rollback(status);
			 throw e;			
		}
	}

public List<Fee> getFees(String actType,String tempId) {
	String sql ="SELECT (AF.FEE_AMNT - AF.FEE_PAID) AS FEE_BALANCE,((tad.TEMP_BT_QTY * FEE_FACTOR) - af.FEE_PAID) as total_feebalance,LQ.ID,BT.ACT_ID,F.ACT_TYPE,F.FEE_ID,AF.FEE_ID AF_FEE_ID,FEE_DESC,FEE_FACTOR,FACTOR,fee_calc_1,TEMP_BT_QTY,HOME_OCCUPATION ,tmp.type,AF.FEE_AMNT, AF.FEE_PAID" + 
			" FROM FEE f " + 
			"LEFT JOIN TEMP_ACTIVITY_DETAILS tad on tad.id = "+tempId+" "+ 
			"LEFT JOIN bt_activity bt on tad.renewal_code = bt.act_id "+
			"LEFT OUTER JOIN LKUP_ACT_TYPE LA ON f.ACT_TYPE=LA.TYPE " + 
			"LEFT OUTER JOIN REF_ACT_BT_QTY_TYPE RAB ON RAB.LKUP_ACT_TYPE_ID=LA.TYPE_ID " + 
			"LEFT OUTER JOIN LKUP_BT_QTY LQ ON LQ.ID=RAB.LKUP_BT_QTY_TYPE_ID " +
			"LEFT JOIN( " + 
			"                SELECT DISTINCT ACT_TYPE AS TYPE " + 
			"                FROM BT_ACTIVITY BA " + 
			"                JOIN ACTIVITY A ON BA.ACT_ID=A.ACT_ID  " + 
			"                WHERE BA.QTY_OTHER IS NOT NULL and ACT_TYPE not in ('APBC','BS','CBMTS','CR','CS','HMRU','LC','LSCO','RVSNU','TLS','VMO')" + 
			"         ) tmp " + 
			"ON TMP.TYPE = F.ACT_TYPE " + 
			"LEFT OUTER JOIN ACTIVITY_FEE AF ON (AF.FEE_ID=F.FEE_ID AND AF.ACTIVITY_ID=BT.ACT_ID ) "+
			"WHERE F.ACT_TYPE='"+actType+"' AND ONLINE_RENEWABLE='Y' "
					+ "AND F.FEE_EXPIRATION_DT is null "+ 
			"AND 1= CASE WHEN TMP.TYPE = F.ACT_TYPE AND HOME_OCCUPATION = 'Y' " +
			"AND FEE_DESC  = '" +Constants.CA_STATE_DISABILITY_FEE  +"' THEN 0 ELSE 1 END "
					+ "AND  ((CASE WHEN FEE_DESC IN ('"+Constants.BT_ADMINISTRATION_FEE_DESC+"') THEN NULL ELSE (AF.FEE_AMNT - AF.FEE_PAID)  END )!=0 OR (CASE WHEN FEE_DESC IN ('"+Constants.BT_ADMINISTRATION_FEE_DESC+"') THEN NULL ELSE (AF.FEE_AMNT - AF.FEE_PAID)  END ) IS NULL)";
	
	logger.debug("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
			Fee fee;
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        		fee = new Fee();
	        	fee.setBtQtyGroupId(rs.getString("ID"));
	        	fee.setActType(rs.getString("ACT_TYPE"));
	        	fee.setFeeDesc(rs.getString("FEE_DESC"));
	        	fee.setFeeId(rs.getString("FEE_ID"));
	        	fee.setFeeFactor(StringUtils.s2d(rs.getString("FEE_FACTOR")));
	        	fee.setFactor(StringUtils.s2d(rs.getString("FACTOR")));
	        	fee.setFeeCalc1(rs.getString("fee_calc_1"));
				fee.setBalDue(StringUtils.s2d(rs.getString("total_feebalance")));
				fee.setPaidAmnt(StringUtils.s2d(rs.getString("FEE_PAID")));
	        	logger.debug("getBtQtyGroupId..."+fee.getBtQtyGroupId());        	
	        	if(rs.getString("TEMP_BT_QTY")!=null && StringUtils.s2i(rs.getString("TEMP_BT_QTY"))>0) {
	        		fee.setFeeUnits(StringUtils.s2d(rs.getString("TEMP_BT_QTY")));
	        	}else {
	        		if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE) ){
	            		fee.setFeeUnits(0.0);
	        		}else {
	        			fee.setFeeUnits(1.0);
	        		}
	        	}
	        	
	        	fee.setHomeOccupation(rs.getBoolean("HOME_OCCUPATION"));
        	logger.debug("REPO - Factor :: " +fee.getFactor() + " :: Fee Factor :: " +fee.getFeeFactor() +":: Fee Desc :: " +fee.getFeeDesc() );
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
}
	return feeList;
}

	/**
	 * This method is to fetch the TEMP_ACTIVITY_DETAILS table record on click of
	 * Back Button on each screen / Print Preview Screen before Payment success
	 * @param tempId
	 * @throws Exception
	 * @return Activity
	 * 
	 */
	public Activity getTempActivityDetails(String tempId) {
		Activity activity = new Activity();
		String sql = "SELECT ACT_ID,ACT_NBR, RENEWAL_CODE, RENEWAL_DT, BUSINESS_NAME, EMAIL, STR_NO, ADDRESS, UNIT, CITY, STATE, ZIP, BUSINESS_PHONE, NO_OF_EMPLOYEEES, "
				+ "BUSINESS_ACC_NO, DESCRIPTION, LOCATION, ATTACH_SIZE, CREATED_BY, CREATED, DELETED, STATUS, KEYWORD1, KEYWORD2, KEYWORD3, KEYWORD4 "
				+ "FROM TEMP_ACTIVITY_DETAILS WHERE ID = "+tempId;
		
		logger.debug("ActivityInfo sql :"+sql);
		try {
			logger.debug(jdbcTemplate.getFetchSize());
			jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() { 
				
			    @Override
			    public Activity mapRow(ResultSet rs, int i) throws SQLException {
					activity.setRenewalCode(StringUtils.i2s(rs.getInt("ACT_ID")));
			    	activity.setPermitNumber(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR")));
			    	activity.setRenewalCode(StringUtils.i2s(rs.getInt("RENEWAL_CODE")));
			    	activity.setStartDate(StringUtils.date2str(rs.getDate("RENEWAL_DT")));
			    	activity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));
			    	activity.setEmail(StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL")));
			    	activity.setStrNo(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")));
			    	activity.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS")));		    	
			    	activity.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
			    	activity.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
			    	activity.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
			    	activity.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));		    	
			    	activity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_PHONE")));
			    	activity.setNoOfEmp(StringUtils.nullReplaceWithEmpty(rs.getString("NO_OF_EMPLOYEEES")));
			    	activity.setBusinessAccNo(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ACC_NO")));
			    	activity.setDescription(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
					return activity;				
			    }
			});	
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		}catch (Exception e) {
			logger.error("",e);
			e.printStackTrace();
			return new Activity();
		}	
		return activity;	
	}

	/**
	 * This method is to save BT quantity details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */	
	public void updateBtQtyValue(Activity activity) {
		logger.debug("in updateEmp repository...");
		try{
			logger.debug("updateEmp"+activity.toString());
			if(StringUtils.nullReplaceWithEmpty(activity.getTempBtQty()).equals("") && StringUtils.s2i(activity.getQtyOther())>0){
				activity.setTempBtQty(activity.getQtyOther());
			}
			
			String updateQuery = "UPDATE TEMP_ACTIVITY_DETAILS SET TEMP_BT_QTY ='"+StringUtils.nullReplaceWithEmpty(activity.getTempBtQty())+"', "
					+ "TEMP_BT_QTY_FLAG ='" +activity.getTempBtQtyFlag()+ "' WHERE ID ="+activity.getTempId();
			logger.debug("updateQuery  :"+updateQuery);
			jdbcTemplate.update(updateQuery);	
		}catch(Exception e){
			logger.error("",e);			
		}
	}
	
	public Activity getBtQtyFlag(String renewalCode) { 
		String sql = "SELECT * from lkup_act_type lat join activity a on lat.type = a.act_type WHERE a.act_id="+renewalCode;
		logger.debug("getBtQtyFlag sql :: "+sql);
		Activity activity = new Activity();
		try {
			activity=jdbcTemplate.queryForObject(sql,new RowMapper<Activity>() {
			    @Override
			    public Activity mapRow(ResultSet rs, int i) throws SQLException {
			    	Activity activity = new Activity();
			    	activity.setBtQtyFlag(rs.getString("BT_QTY_FLAG"));	
					return activity;	
			    }
			});
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("",e);
		}
		logger.debug("activity.getBtQtyFlag() :: "+activity.getBtQtyFlag());
		return activity;
	}
	
	//This method will calculate the fees based on the activity type and formulas 
	public List<Fee> calculateFees(List<Fee> feeList, String screenName) {
		logger.debug("screenName :: " +screenName);

		double feeAmount = 0.00;
		int levelId = 0;			
		
		for(int i=0;i<feeList.size();i++){
			Fee fee = feeList.get(i);
			switch (fee.getFeeCalc1().charAt(0)) {
			case 'A': // This is for fees that are based on number of units
				logger.debug("fee_calc_1=A,This is for fees that are based on number of units");
				logger.debug("Fee getFeeUnits "+fee.getFeeUnits());
				logger.debug(fee.getFeeUnits() <1 );
				if (fee.getFeeFactor() > 0.00) {
					logger.debug("Fee factor is > 0.00, calculating fee amount");
					if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE)) {
						
						if(fee.getFeeUnits() >3000){
							fee.setFeeUnits(3000);
						}	
					}
					
					logger.debug("Fee getFeeUnits "+fee.getFeeUnits()+" fee btqty id "+fee.getBtQtyGroupId());
					if(fee.getPaidAmnt() > 0 && fee.getBalDue() >0) {
						feeAmount = fee.getBalDue();
					}else if(fee.getBtQtyGroupId().equalsIgnoreCase(Constants.NO_OF_EMPLOYEES_CODE) && fee.getFeeUnits() <= 0.0) {
						feeAmount = fee.getFeeFactor() * fee.getFeeUnits();
					}else {
						feeAmount = fee.getFeeFactor() * Math.max(1, fee.getFeeUnits());
					}
					logger.debug("calculated fee amount is " + feeAmount);
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
				    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				}
				break;

			case 'B': // This is for fees that are based on number of units
				logger.debug("fee_calc_1=B");
				logger.debug("volume..."+fee.getFeeUnits());
				double volume= fee.getFeeUnits();
				logger.debug("volume..."+volume);
				if(volume <= 50000) {
					feeAmount = 102.05;
					logger.debug("feeAmount...1.."+feeAmount);
				}else if (volume >= 50001 && volume <= 100000) {
					feeAmount = 204;
					logger.debug("feeAmount...2.."+feeAmount);
				}else if (volume >= 100001 && volume <= 200000) {
					feeAmount = 408.40;
					logger.debug("feeAmount...3.."+feeAmount);
				}else if (volume >= 200001 && volume <= 300000) {
					feeAmount = 608.25;
					logger.debug("feeAmount...4.."+feeAmount);
				}else if (volume >= 300001) {
					feeAmount = 1020.65;
					logger.debug("feeAmount...5.."+feeAmount);
				}
				logger.debug("feeAmount..."+feeAmount);	
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;				

			case 'C': // this is for a flat fee
				logger.debug("fee_calc_1=C,this is for a flat fee"+fee.getFeeUnits());
				feeAmount = fee.getFactor();
				if(fee.getActType().equalsIgnoreCase("CR") || fee.getActType().equalsIgnoreCase("CRS")) {
					if(fee.getFeeUnits() > 0) {
						feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
					    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));
					}else {
						feeList.get(i).setFeeTotal(0.00);
						feeList.get(i).setFeeTotalStr("0.00");
						feeList.get(i).setFeeDesc("");	
					}
				}else {
					logger.debug("in else ... "+fee.getFeeUnits()+" getFeeDesc ... "+fee.getFeeDesc());
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
				    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));
				}
				break;
			
			case 'D': // Max of factor and fee_factor*units
				logger.debug("fee_calc_1=D");
				feeAmount = Math.max(fee.getFactor(), fee.getFeeFactor()) * fee.getFeeUnits();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'E': // Min of factor and fee_factor*units
				logger.debug("fee_calc_1=E");
				feeAmount = Math.min(fee.getFactor(), fee.getFeeFactor()) * fee.getFeeUnits();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'F': // This is for fees that are based on number of units
				logger.debug("fee_calc_1=F");
				feeList.get(i).setFeeTotal(0.0);
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(0.0));       
				break;

			case 'G': // User Entering the Fee Amount
				logger.debug("fee_calc_1=G");
				feeAmount = fee.getFeeAmnt();
				
				if(fee.getActType().equalsIgnoreCase(Constants.VMO_ACT_TYPE)) {
					if(fee.getFeeDesc().equalsIgnoreCase(Constants.ONE_PERCENT_OF_GROSS_RECEIPTS_OVER_$5000)) {
						if(fee.getFeeUnits() > 5000) {
							double feeUnits = fee.getFeeUnits();
							feeUnits = feeUnits - 5000;
							feeAmount = feeUnits * 0.01;
							feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
							feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
						}else {
							feeList.get(i).setFeeTotal(0.00);	
							feeList.get(i).setFeeTotalStr("0.00");	
						}
					}
				}else {
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
				}
				break;

			case 'H': // this is for fee depending on valuation
				logger.debug("fee_calc_1=H, valuation * fee units");
				logger.debug("Fee units are " + fee.getFeeUnits());
				feeAmount = fee.getValuation() * fee.getFeeFactor();
				logger.debug("Calculated fee amount under formula H is " + feeAmount);
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'I': //
				logger.debug("fee_calc_1=I");
				feeAmount = fee.getValuation() * fee.getFactor();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'J':
				logger.debug("fee_calc_1=J");
				double[] subTotals = {};

				/**
				 * If feeInit > 0 then Mutilply the subtotal[feeInit] value by fee/unit Else Mutilply the subtotal[subTotalLevel] value by fee/unit
				 **/
				logger.debug("feeInit is : " + fee.getFeeInit());
				logger.debug("subTotalLevel is : " + fee.getSubtotalLevel());

				subTotals = fee.getSubTotals();
				logger.debug("sub totals are " + subTotals);

				if (fee.getFeeInit() > 0) {
					feeAmount = subTotals[fee.getFeeInit()];
				} else {
					logger.debug("in the else, subtotal level is " + fee.getSubtotalLevel());
					feeAmount = subTotals[fee.getSubtotalLevel()];
				}

				logger.debug("fee amount is :" + feeAmount);
				feeAmount *= fee.getFactor();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'R':
				logger.debug("fee_calc_1=R, Max (Factor & fee factor * valuation)");
				feeAmount = Math.max(fee.getFactor(), fee.getFeeFactor()) * fee.getValuation();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'S':
				logger.debug("fee_calc_1=S, not defined");
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
			case 'U':
				logger.debug("fee_calc_1=U");

				if (fee.getFeeInit() > 0) {
					levelId = fee.getFeeInit();
				} else {
					levelId = fee.getSubtotalLevel();
				}

				subTotals = fee.getSubTotals();
				logger.debug("LevelId : " + levelId);
				logger.debug("SubTotal : " + subTotals[levelId]);
				logger.debug("Factor : " + fee.getFactor());
				feeAmount = Math.max(subTotals[levelId], fee.getFactor());
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'V':
				logger.debug("fee_calc_1=V");

				if (fee.getFeeAmnt() > fee.getFactor()) {
					feeAmount = fee.getFactor();
				} else {
					feeAmount = fee.getFeeAmnt();
				}
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'Y':
				logger.debug("fee_calc_1=Y");

				/**
				 * Mutilply the subtotal value by Subtotallevel and compare the result to Factor Return the highest
				 */

				// add code here to select the subtotal from the arrey object based on feeInit of fee obj
				logger.debug("before setting up amount for calc Y : " + feeAmount);
				logger.debug("subtotallevel is : " + fee.getSubtotalLevel());
				subTotals = fee.getSubTotals();
				feeAmount = subTotals[fee.getSubtotalLevel()];
				logger.debug("after setting up amount for calc Y : " + feeAmount);
				logger.debug("fee factor " + fee.getFactor());

				feeAmount *= fee.getFeeFactor();

				if (feeAmount < fee.getFactor()) {
					feeAmount = fee.getFactor();
				}
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'y':
				logger.debug("fee_calc_1=y");

				/**
				 * Mutilply the subtotal value by fee/unit and compare the result to Factor Return the highest
				 */

				// add code here to select the subtotal from the arrey object based on feeInit of fee obj
				logger.debug("before setting up amount for calc y : " + feeAmount);
				logger.debug("feeInit is : " + fee.getFeeInit());
				subTotals = fee.getSubTotals();
				feeAmount = subTotals[fee.getFeeInit()];
				logger.debug("after setting up amount for calc y : " + feeAmount);
				logger.debug("fee factor " + fee.getFactor());

				feeAmount *= fee.getFeeFactor();

				if (feeAmount < fee.getFactor()) {
					feeAmount = fee.getFactor();
				}
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'a':
				logger.debug("fee_calc_1=a");

				/**
				 * Mutilply the subtotal value by units and compare the result to Factor Return the highest
				 */

				// add code here to select the subtotal from the arrey object based on feeInit of fee obj
				logger.debug("before setting up amount for calc y : " + feeAmount);
				logger.debug("feeInit is : " + fee.getFeeInit());
				subTotals = fee.getSubTotals();
				feeAmount = subTotals[fee.getFeeInit()];
				logger.debug("subtotal amount is : " + feeAmount);
				logger.debug("after setting up amount for calc y : " + feeAmount);
				feeAmount *= fee.getFeeUnits();

				if (feeAmount < fee.getFeeFactor()) {
					feeAmount = fee.getFeeFactor();
				}
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			case 'b': //
				logger.debug("fee_calc_1=b");
				feeAmount = fee.getValuation() * fee.getFeeFactor();
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;
			case 'c': // Added For BT
				logger.debug("fee_calc_1=c");
				logger.debug("getFeeUnits..."+fee.getFeeUnits()+"fee.getActType()... "+fee.getActType());
				if(fee.getActType().equalsIgnoreCase("CR") || fee.getActType().equalsIgnoreCase("CRS")) {
					if(fee.getFeeUnits() > 5000) {
						feeAmount = (((fee.getFeeUnits()-5000) /100) * fee.getFeeFactor());
						feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
						feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));	
					}else {
						feeList.get(i).setFeeTotal(0.00);
						feeList.get(i).setFeeTotalStr("0.00");
						feeList.get(i).setFeeDesc("");	
					}
				}else{
					feeAmount = (fee.getFeeFactor() * (fee.getFeeUnits() / 100)) + fee.getFactor();
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));	
				}
				break;
			case 'd': // Added For BT
				logger.debug("fee_calc_1=d");
				feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.75;
				logger.debug("feeAmount..."+feeAmount);
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;
			case 'e': // Added For BT
				logger.debug("fee_calc_1=e");
				feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.50;
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;
			case 'f': // Added For BT
				logger.debug("fee_calc_1=f");
				feeAmount = ((fee.getFeeUnits() * fee.getFeeFactor())) + fee.getFactor() * 0.25;
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;
			case 'g': // Added For BT
				logger.debug("fee_calc_1=g");
				double total1 = pfTotal;
				double feeUnit = fee.getFeeFactor();
				if ((total1 * (fee.getFactor()) / 100) < feeUnit)
					feeAmount = feeUnit;
				else
					feeAmount = total1 * (fee.getFactor() / 100);

				double myDouble = feeAmount;
				double myDoubleMultiplied = myDouble * 100;
				double myDoubleRounded = Math.round(myDoubleMultiplied);
				feeAmount = myDoubleRounded / 100.0;

				logger.debug("Penalty fees  " + feeAmount);
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeAmount)));
			    feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeAmount));       
				break;

			default:
				break;
			}			
		}				
		
		return feeList;
	}
	
	/**
	 * This method is to calculate BT Late Fee 
	 * As it the service fee needs to get 
	 * like Business Address, Activity Number, Business Mailing Address
	 * @param busAccNo, renewalCode
	 * @throws Exception
	 * @return Activity
	 * 
	 */

	public List<Fee> getBtLateFee(List<Fee> feeList, double totalFee) {
		
		Map<String, String> lkupSystemDataMap = null;
		String lateFeeFromDate = "";
		String lateFeeToDate = "";
			try {
				lkupSystemDataMap =   commonRepository.getLkupSystemDataMap();
				logger.debug("lkupSystemDataMap.size() :: " +lkupSystemDataMap.size());
				if(lkupSystemDataMap.get(Constants.BT_LATE_FEE_FROM_DATE)!=null) {
						lateFeeFromDate = lkupSystemDataMap.get(Constants.BT_LATE_FEE_FROM_DATE);
						logger.debug("BT_LATE_FEE_FROM_DATE"+lateFeeFromDate);
				}
				if(lkupSystemDataMap.get(Constants.BT_LATE_FEE_TO_DATE)!=null) {
					lateFeeToDate = lkupSystemDataMap.get(Constants.BT_LATE_FEE_TO_DATE);
				}

				 SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
	             sdf.setLenient(false);
	          
	             SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");  
	 		     String strDate = formatter.format(new Date());  
	 		     System.out.println("Date Format with ddMMMyyyy : "+strDate);  
	 		    System.out.println("Date Format with ddMMMyyyy : "+lateFeeFromDate);  
	 		   System.out.println("Date Format with ddMMMyyyy : "+lateFeeToDate);  
		 		  
	             Date btFromDate = sdf.parse(lateFeeFromDate);
	             Date currentDate = sdf.parse(strDate);
	             Date btToDate = sdf.parse(lateFeeToDate);
		         logger.debug("btFromDate.."+btFromDate);
		         logger.debug("btToDate..."+btToDate);
			    
			    
				for(int j=0;j<feeList.size();j++) {
					if(feeList.get(j).getFeeDesc()!=null && feeList.get(j).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
						   if (currentDate.compareTo(btFromDate) >= 0 && currentDate.compareTo(btToDate) <= 0) {
								feeList.get(j).setFeeTotal(feeList.get(j).getFactor() * totalFee);
								feeList.get(j).setFeeTotalStr(StringUtils.roundOffDouble(feeList.get(j).getFeeTotal()));
								logger.debug("LATE_FEE getFeeFactor :: " +feeList.get(j).getFeeTotal());
								logger.debug("LATE_FEE getTotalFee :: " +feeList.get(j).getTotalFee());	
						}else {
							feeList.get(j).setFeeTotal(0.00);
							feeList.get(j).setFeeTotalStr("");
							feeList.get(j).setFeeDesc("");
						}
					}
				}
				logger.debug("LATE_FEE totalFee :: " +totalFee);			
			}catch(Exception e) {
				e.printStackTrace();
			}
			return feeList;
		}


	/**
	 * This method is to calculate Service Fee 
	 * As it the service fee needs to get 
	 * like Business Address, Activity Number, Business Mailing Address
	 * @param busAccNo, renewalCode
	 * @throws Exception
	 * @return Activity
	 */
	public List<Fee> getBtServiceFee(List<Fee> feeList, double totalFee, String screenName) {

		logger.debug("Entered getBtServiceFee( feeList, " + totalFee + ", " +screenName);
			try {
				for (int i=0;i<feeList.size();i++) {
					if(!StringUtils.nullReplaceWithEmpty(screenName).equals("") && screenName.equalsIgnoreCase(Constants.FEE_DETAILS_SCREEN)){
						if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {

							feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(totalFee * (feeList.get(i).getFactor())))); // To show the calculated value as fee factor on preview screen
							feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(totalFee * (feeList.get(i).getFactor())));
						}
					}
				}
				logger.debug("Exiting getBtServiceFee()");			
			}catch(Exception e) {
				e.printStackTrace();
			}
			return feeList;
		}	
	

    public static void main(String[] args) throws ParseException {
    	Double d1=0.0;
    	Double d2=0.00;
    	System.out.println(d1 > d2); 
    	System.out.println(d1 < d2); 
    	System.out.println(d1 == d2); 
    	Date date = new Date();  
		    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");  
		    String strDate = formatter.format(date);  
		    System.out.println("Date Format with MM/dd/yyyy : "+strDate);  
		  
		    formatter = new SimpleDateFormat("dd MMMM yyyy");  
		    strDate = formatter.format(date);  
		    System.out.println("Date Format with dd MMMM yyyy : "+strDate);  
		  
		    formatter = new SimpleDateFormat("dd MMMM yyyy zzzz");  
		    strDate = formatter.format(date);  
		    System.out.println("Date Format with dd MMMM yyyy zzzz : "+strDate);  
		  
		    formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");  
		    strDate = formatter.format(date);  
		    System.out.println("Date Format with E, dd MMM yyyy HH:mm:ss z : "+strDate);
    	 }
    		
	

	
}
