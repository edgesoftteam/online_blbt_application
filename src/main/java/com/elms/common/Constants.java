package com.elms.common;

/**
 * @author Gayathri Turlapati
 *
 */

public final class Constants {
	public static final String ACTIVITY_NUMBER_STARTS_WITH_BL = "BL";
	public static final String ACTIVITY_NUMBER_STARTS_WITH_BT = "BT";
		
	public static final String USER_DETAILS_SCREEN = "USER_DETAILS_SCREEN";
	public static final String QUANTITY_DETAILS_SCREEN = "QUANTITY_DETAILS_SCREEN";
	public static final String ATTACHMENT_DETAILS_UPLOAD_SCREEN = "ATTACHMENT_DETAILS_UPLOAD_SCREEN";
	public static final String ATTACHMENT_DETAILS_SCREEN = "ATTACHMENT_DETAILS_SCREEN";
	public static final String FEE_DETAILS_SCREEN = "FEE_DETAILS_SCREEN";
	public static final String PREVIEW_DETAILS_SCREEN = "PREVIEW_DETAILS_SCREEN";
	
	//public static final String SERVICE_FEE_DESC = "2% Credit card service fee"; 
	public static final String BT_ADMINISTRATION_FEE_DESC = "Administration Fee";
	public static final String BL_ADMINISTRATION_FEE_DESC = "Administration Fee";
	public static final String LATE_FEE = "Late Fee";
	public static final String LICENSE_FEE_ANNUAL = "License Fee";
	public static final String CA_STATE_DISABILITY_FEE = "CA State Disability Fee";
	//Bt

	public static final String UNIT_AREA_OVER_5000_SQFT = "Unit Area Over 5000 sq.ft.";
	
	public static final String BL_LATE_FEE_TO_DATE = "BL_LATE_FEE_TO_DATE";
	public static final String BL_LATE_FEE_FROM_DATE = "BL_LATE_FEE_FROM_DATE";	
	
	public static final String BT_LATE_FEE_FROM_DATE = "BT_LATE_FEE_FROM_DATE";
	public static final String BT_LATE_FEE_TO_DATE = "BT_LATE_FEE_TO_DATE";		
	
	public static final String ONLINE_USER = "ONLINE USER";
	public static final String PROD_SERVER_FLAG = "PROD_SERVER_FLAG";
	
	public static final String EMPTY_FLAG = "";
	
	public static final String EMAIL_SERVICE_URL = "EMAIL_SERVICE_URL";
	public static final String EMAIL_SERVER_URL = "EMAIL_SERVER_URL";
	public static final String EMAIL_AUTHENTICATION_USERNAME = "EMAIL_AUTHENTICATION_USERNAME";
	public static final String EMAIL_AUTHENTICATION_PASSWORD = "EMAIL_AUTHENTICATION_PASSWORD";
	public static final String EMAIL_FROM_ADDRESS = "EMAIL_FROM_ADDRESS";
	public static final String EMAIL_FOR_STAFF = "EMAIL_FOR_STAFF";	
	
	public static final String RENEWAL_DISCLAIMER_TEXT_MESSAGE = "RENEWAL_DISCLAIMER_TEXT_MESSAGE";
	
	public static final String PAID_PENDING_FOR_APPROVAL_CODE = "201078";
	public static final String PAID_OR_CURRENT_CODE = "1068";
	
	public static final String ONLINE_BOA_URL = "BOA_URL";
	public static final String ONLINE_BOA_X_LOGIN = "BOA_X_LOGIN";
	public static final String ONLINE_BOA_TRANSACTIONKEY = "BOA_TRANSACTIONKEY";
	public static final String ONLINE_BOA_X_FP_HASH = "BOA_X_FP_HASH";	
	public static final String ONLINE_BOA_FORM_ID = "BOA_FORM_ID";
	public static final String ONLINE_BOA_BUTTON_CODE = "BOA_BUTTON_CODE";
	public static final String BL_PAYMENT_RECEIPT_URL = "BL_PAYMENT_RECEIPT_URL";
	public static final String BT_PAYMENT_RECEIPT_PROD_URL = "BT_PAYMENT_RECEIPT_PROD_URL";
	public static final String BT_PAYMENT_RECEIPT_NON_PROD_URL = "BT_PAYMENT_RECEIPT_NON_PROD_URL";
	
	public static final String ONLINE_BOA_URL_FOR_TEST_ENV = "TEST_BOA_URL";
	public static final String ONLINE_BOA_X_LOGIN_FOR_TEST_ENV = "TEST_BOA_X_LOGIN";
	public static final String ONLINE_BOA_TRANSACTIONKEY_FOR_TEST_ENV = "TEST_BOA_TRANSACTIONKEY";
	public static final String ONLINE_BOA_X_FP_HASH_FOR_TEST_ENV = "TEST_BOA_X_FP_HASH";	
	public static final String ONLINE_BOA_FORM_ID_FOR_TEST_ENV = "TEST_BOA_FORM_ID";
	public static final String ONLINE_BOA_BUTTON_CODE_FOR_TEST_ENV = "TEST_BOA_BUTTON_CODE";
	
	public static final String TEMP_FILE_LOC = "TEMP_FILE_LOC";
	public static final String VMO_ACT_TYPE = "VMO";
	public static final String ONE_PERCENT_OF_GROSS_RECEIPTS_OVER_$5000 = "1% of Gross Receipts (Over $5000)";
	
	public static final String NO_OF_EMPLOYEES_CODE = "101";
	public static final int SQUARE_FOOTAGE = 105;	
	
	public static final String UPLOAD_ATTACHMENT_DETAILS_SCREEN = "UPLOAD_ATTACHMENT_DETAILS_SCREEN";
	public static final String DOWNLOAD_ATTACHMENT_DETAILS_SCREEN = "DOWNLOAD_ATTACHMENT_DETAILS_SCREEN";
	
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";
	public static final String SUCCESS_MESSAGE = "All uploads are mandatory. Please upload ";
	public static final String FAILURE_MESSAGE = "All uploads are mandatory.";
	
	public static final String MULTI_ADDRESS_MAILING_ADDRESS = "Mailing Address";
	public static final String PENDING_UPLOADS = "pendingUploads";
	
	public static final String RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT = "RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT";   
    public static final String RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL = "RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL";
    public static final String BUSINESS_TAX_APPLICATION_SUBMIT  = "BUSINESS_TAX_APPLICATION_SUBMIT";
	public static final String BUSINESS_LICENSE_APPLICATION_SUBMIT  = "BUSINESS_LICENSE_APPLICATION_SUBMIT";
	public static final String BL_SECURITY_CODE_VERIFICATION  = "BL_SECURITY_CODE_VERIFICATION";
	public static final String BT_SECURITY_CODE_VERIFICATION  = "BT_SECURITY_CODE_VERIFICATION";
	public static final String EDGESOFT_SUPPORT_EMAIL = "EDGESOFT_SUPPORT_EMAIL";
	public static final String SECURITY_CODE_VERIFICATION = "SECURITY_CODE_VERIFICATION";
	
    public static final String NO_DOCS_TO_UPLOAD_OR_DOWNLOAD = "No documents to upload and download";

	public static final int MODULE_NAME_BUSINESS_TAX = 8;
    public static final int MODULE_NAME_BUSINESS_LICENSE = 4;
	
    public static final int ACTIVITY_SUB_TYPE = -1;

	public static final int ACTIVITY_STATUS_BT_PENDING_APPROVAL = 1069;
	public static final int ACTIVITY_STATUS_BL_PENDING = 1050;
	public static final int ACTIVITY_STATUS_BT_OUT_OF_BUSINESS = 1067;
	public static final int ACTIVITY_STATUS_BL_OUT_OF_BUSINESS = 1054;
	
	public static final int APPLICATION_TYPE_BUSINESSLICENSE = 101;
	public static final int APPLICATION_TYPE_BUSINESSTAX = 102;
	
	public static final String USER_KEY = "user";
	public static final int GROUPS_BUSINESS_LICENSE_USER = 48;
	public static final int GROUPS_BUSINESS_LICENSE_APPROVAL = 47;
	public static final int GROUPS_BUSINESS_TAX_APPROVAL = 50;
	public static final int GROUPS_BUSINESS_TAX_USER = 51;
	
	public static final String PROJECT_NAME_LICENSE_AND_CODE_SERVICES = "License and Code Services";
	public static final int DEPARTMENT_LICENSE_AND_CODE_SERVICES = 22;
	public static final int PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID = 4;
	
	public static final int BL_USER_ID = 200348;
	public static final int BT_USER_ID = 200357;

	public static final String SUB_PROJECT_NAME_STARTS_WITH = "BL - ";
	public static final String BT_SUB_PROJECT_NAME_STARTS_WITH = "BT - ";
	
	public static final String USER_UNASSIGNED = "unassigned";
	public static final String USER_ED_KHOURDADJIAN = "ekhourdadjian";
	public static final String USER_EULA_EWARREN = "ewarren";
		
	public static final String PEOPLE_TYPE_BUSINESS_OWNER = "23";	
}