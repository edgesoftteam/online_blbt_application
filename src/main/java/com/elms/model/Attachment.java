/**
 * 
 */
package com.elms.model;

import java.io.Serializable;
import java.sql.Clob;
import java.util.List;
import java.util.Map;

/**
 * @author Gayathri Turlapati
 *
 */
public class Attachment implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3280207766202799693L;
	
	private int attachmentId;
	private int attachmentTypeId;
	private String attachmentDesc;
	private String onlineAvailable;
	private String fileName;
	private String fileLoc;
	private String documentURL;
	private String comments;
	private String downloadOrUploadType;
	private String bmcDescription;
	private String bmcPopUpWindowUrl;
	private Map<String, String> bmcLinkDescMap;
	private List<String> commentsList;
	
	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}
	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}
	/**
	 * @return the attachmentDesc
	 */
	public String getAttachmentDesc() {
		return attachmentDesc;
	}
	/**
	 * @param attachmentDesc the attachmentDesc to set
	 */
	public void setAttachmentDesc(String attachmentDesc) {
		this.attachmentDesc = attachmentDesc;
	}
	/**
	 * @return the onlineAvailable
	 */
	public String getOnlineAvailable() {
		return onlineAvailable;
	}
	/**
	 * @param onlineAvailable the onlineAvailable to set
	 */
	public void setOnlineAvailable(String onlineAvailable) {
		this.onlineAvailable = onlineAvailable;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileLoc() {
		return fileLoc;
	}
	public void setFileLoc(String fileLoc) {
		this.fileLoc = fileLoc;
	}
	/**
	 * @return the documentURL
	 */
	public String getDocumentURL() {
		return documentURL;
	}
	/**
	 * @param documentURL the documentURL to set
	 */
	public void setDocumentURL(String documentURL) {
		this.documentURL = documentURL;
	}
	public int getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDownloadOrUploadType() {
		return downloadOrUploadType;
	}
	public void setDownloadOrUploadType(String downloadOrUploadType) {
		this.downloadOrUploadType = downloadOrUploadType;
	}	
	public String getBmcPopUpWindowUrl() {
		return bmcPopUpWindowUrl;
	}
	public void setBmcPopUpWindowUrl(String bmcPopUpWindowUrl) {
		this.bmcPopUpWindowUrl = bmcPopUpWindowUrl;
	}
	public String getBmcDescription() {
		return bmcDescription;
	}
	public void setBmcDescription(String bmcDescription) {
		this.bmcDescription = bmcDescription;
	}	
	public List<String> getCommentsList() {
		return commentsList;
	}
	public void setCommentsList(List<String> commentsList) {
		this.commentsList = commentsList;
	}
	public Map<String, String> getBmcLinkDescMap() {
		return bmcLinkDescMap;
	}
	public void setBmcLinkDescMap(Map<String, String> bmcLinkDescMap) {
		this.bmcLinkDescMap = bmcLinkDescMap;
	}
}

