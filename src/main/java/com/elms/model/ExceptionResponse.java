/**
 * 
 */
package com.elms.model;

/**
 * @author Siva Kumari
 *
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class ExceptionResponse implements Serializable{
		
		private String errorMessage;
		private String requestedURI;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(final String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public String getRequestedURI() {
			return requestedURI;
		}

		public void callerURL(final String requestedURI) {
			this.requestedURI = requestedURI;
		}
}