package com.elms.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
public class User implements Serializable {
	public int userId;
	public String username;
	public String firstName;
	public String lastName;
	public String middleInitial;
	public Department department;
	public String employeeNumber;
	public String title;
	public String password;
	public Role role;
	public List groups;
	public boolean isSupervisor;
	public String active;
	public String homePage;
	public String userEmail;
	public String isDot = "N";
	
	private List modules;
    public String hasCIP;
    public String phone;

	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHasCIP() {
		return hasCIP;
	}

	public void setHasCIP(String hasCIP) {
		this.hasCIP = hasCIP;
	}

	/**
	 * @return Returns the accountNbr.
	 */
	public String getAccountNbr() {
		return accountNbr;
	}

	/**
	 * @param accountNbr
	 *            The accountNbr to set.
	 */
	public void setAccountNbr(String accountNbr) {
		this.accountNbr = accountNbr;
	}

	/**
	 * @return Returns the isDot.
	 */
	public String getIsDot() {
		return isDot;
	}

	/**
	 * @param isDot
	 *            The isDot to set.
	 */
	public void setIsDot(String isDot) {
		this.isDot = isDot;
	}

	/**
	 * @return Returns the isObc.
	 */
	public String getIsObc() {
		return isObc;
	}

	/**
	 * @param isObc
	 *            The isObc to set.
	 */
	public void setIsObc(String isObc) {
		this.isObc = isObc;
	}

	public String isObc = "N";
	public String accountNbr;

	public String toString() {
		String returnString = "userid :" + userId + ",username" + username + ",firstname" + firstName;
		return returnString;
	}

	public User(int aUserId, String aUsername, String aFirstName, String aLastName, String aMiddleInitial, Department aDepartment, String aEmployeeNumber, String aTitle, String aPassword) {
		this.userId = aUserId;
		this.username = aUsername;
		this.firstName = aFirstName;
		this.lastName = aLastName;
		this.middleInitial = aMiddleInitial;
		this.department = aDepartment;
		this.employeeNumber = aEmployeeNumber;
		this.title = aTitle;
		this.password = aPassword;
	}

	public User() {
		role = new Role();
		department = new Department();

	}

	public User(int aUserId) {
		this.userId = aUserId;
	}

	/**
	 * Access method for the userId property.
	 * 
	 * @return the current value of the userId property
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the value of the userId property.
	 * 
	 * @param aUserId
	 *            the new value of the userId property
	 */
	public void setUsername(String aUserId) {
		if (aUserId == null)
			username = "";
		else
			username = aUserId;
	}

	/**
	 * Access method for the firstName property.
	 * 
	 * @return the current value of the firstName property
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param aFirstName
	 *            the new value of the firstName property
	 */
	public void setFirstName(String aFirstName) {
		if (aFirstName == null)
			firstName = "";
		else
			firstName = aFirstName;
	}

	/**
	 * Access method for the lastName property.
	 * 
	 * @return the current value of the lastName property
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the value of the lastName property.
	 * 
	 * @param aLastName
	 *            the new value of the lastName property
	 */
	public void setLastName(String aLastName) {
		if (aLastName == null)
			lastName = "";
		else
			lastName = aLastName;
	}

	/**
	 * Access method for the middleInitial property.
	 * 
	 * @return the current value of the middleInitial property
	 */
	public String getMiddleInitial() {
		return middleInitial;
	}

	/**
	 * Sets the value of the middleInitial property.
	 * 
	 * @param aMiddleInitial
	 *            the new value of the middleInitial property
	 */
	public void setMiddleInitial(String aMiddleInitial) {
		if (aMiddleInitial == null)
			middleInitial = "";
		else
			middleInitial = aMiddleInitial;
	}

	/**
	 * Access method for the department property.
	 * 
	 * @return the current value of the department property
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * Sets the value of the department property.
	 * 
	 * @param aDepartment
	 *            the new value of the department property
	 */
	public void setDepartment(Department aDepartment) {
		department = aDepartment;
	}

	/**
	 * Access method for the employeeNumber property.
	 * 
	 * @return the current value of the employeeNumber property
	 */
	public String getEmployeeNumber() {
		return employeeNumber;
	}

	/**
	 * Sets the value of the employeeNumber property.
	 * 
	 * @param aEmployeeNumber
	 *            the new value of the employeeNumber property
	 */
	public void setEmployeeNumber(String aEmployeeNumber) {
		if (aEmployeeNumber == null)
			employeeNumber = "";
		else
			employeeNumber = aEmployeeNumber;
	}

	/**
	 * Access method for the title property.
	 * 
	 * @return the current value of the title property
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param aTitle
	 *            the new value of the title property
	 */
	public void setTitle(String aTitle) {
		if (aTitle == null)
			title = "";
		else
			title = aTitle;
		title = aTitle;
	}

	/**
	 * Access method for the password property.
	 * 
	 * @return the current value of the password property
	 */
	public String getPassword() {
		return password;
	}

	public boolean getIsSupervisor() {
		return isSupervisor;
	}

	/**
	 * Sets the value of the password property.
	 * 
	 * @param aPassword
	 *            the new value of the password property
	 */
	public void setPassword(String aPassword) {
		if (aPassword == null)
			password = "";
		else
			password = aPassword;
	}

	/**
	 * Gets the userId
	 * 
	 * @return Returns a int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the userId
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the userId
	 * 
	 * @return Returns a int
	 */
	public Role getRole() {
		return role;
	}

	public void setRole(Role aRole) {
		this.role = aRole;
	}

	/**
	 * Gets the fullName
	 * 
	 * @return Returns a String
	 */
	public String getFullName() {
		return getFirstName() +" "+getLastName();
	}



	/**
	 * Sets the isSupervisor.
	 * 
	 * @param isSupervisor
	 *            The isSupervisor to set
	 */
	public void setIsSupervisor(boolean isSupervisor) {
		this.isSupervisor = isSupervisor;
	}

	/**
	 * Returns the groups.
	 * 
	 * @return List
	 */
	public List getGroups() {
		return groups;
	}

	/**
	 * Sets the groups.
	 * 
	 * @param groups
	 *            The groups to set
	 */
	public void setGroups(List groups) {
		this.groups = groups;
	}

	/**
	 * Returns the active.
	 * 
	 * @return String
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * @return
	 */
	public String getHomePage() {
		return homePage;
	}

	/**
	 * @param string
	 */
	public void setHomePage(String string) {
		homePage = string;
	}

	/**
	 * @param b
	 */
	public void setSupervisor(boolean b) {
		isSupervisor = b;
	}

	/**
	 * @return Returns the userEmail.
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            The userEmail to set.
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	public List getModules() {
		return modules;
	}

	public void setModules(List modules) {
		this.modules = modules;
	}

}