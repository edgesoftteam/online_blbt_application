package com.elms.model;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Shekhar Jain
 */
public class ConditionLibraryForm {

	protected String type;// corresponding to CONDITION_TYPE
	protected String search;
	protected String condId;
	protected String levelId;// lso_id or psa_id
	protected String conditionLevel;// L, S, O, P, Q, A
	protected ConditionLibraryRecord[] conditionLibraryRecords;
	private String checkDate;
	private String searchProgType;
	 

	public void setSearchProgType(String searchProgType) {
		this.searchProgType = searchProgType;
	}

	public ConditionLibraryForm() {
		this.type = "";
		this.search = "";
		this.condId = "";
		this.levelId = "";
		this.conditionLevel = "";
		this.conditionLibraryRecords = new ConditionLibraryRecord[0];
	}

	/*public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}*/

	/*public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 
		return errors;
	}*/

	/**
	 * Gets the conditionLibraryRecords
	 * 
	 * @return Returns a ConditionLibraryRecord[]
	 */
	public ConditionLibraryRecord[] getConditionLibraryRecords() {
		return conditionLibraryRecords;
	}

	/**
	 * Sets the conditionLibraryRecords
	 * 
	 * @param conditionLibraryRecords
	 *            The conditionLibraryRecords to set
	 */
	public void setConditionLibraryRecords(ConditionLibraryRecord[] conditionLibraryRecords) {
		this.conditionLibraryRecords = conditionLibraryRecords;
	}

	/**
	 * Gets the search
	 * 
	 * @return Returns a String
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * Sets the search
	 * 
	 * @param search
	 *            The search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * Gets the condId
	 * 
	 * @return Returns a String
	 */
	public String getCondId() {
		return condId;
	}

	/**
	 * Sets the condId
	 * 
	 * @param condId
	 *            The condId to set
	 */
	public void setCondId(String condId) {
		this.condId = condId;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a String
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the level
	 * 
	 * @return Returns a String
	 */
	public String getConditionLevel() {
		return conditionLevel;
	}

	/**
	 * Sets the level
	 * 
	 * @param level
	 *            The level to set
	 */
	public void setConditionLevel(String level) {
		this.conditionLevel = level;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return Returns the checkDate.
	 */
	public String getCheckDate() {
		return checkDate;
	}

	/**
	 * @param checkDate
	 *            The checkDate to set.
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	

	/**
	 * @return the searchProgType
	 */
	public String getSearchProgType() {
		return searchProgType;
	}

	/**
	 * @param searchProgType the searchProgType to set
	 */
}
