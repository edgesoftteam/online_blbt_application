package com.elms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "NEXTID")
@Component
public class NextId {
	@Id
		
	@Column(name = "IDNAME")	
	 protected String idName;
	@Column(name = "IDVALUE")
    protected Integer idValue;
	public NextId() {
		super();
	}
	public NextId(String idName, Integer idValue) {
		super();
		this.idName = idName;
		this.idValue = idValue;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	public Integer getIdValue() {
		return idValue;
	}
	public void setIdValue(Integer idValue) {
		this.idValue = idValue;
	}
			
}