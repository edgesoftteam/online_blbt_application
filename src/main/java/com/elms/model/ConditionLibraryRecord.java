package com.elms.model;

public class ConditionLibraryRecord {
	/**
	 * Member variable declaration
	 */
	protected String select;
	protected String levelType;
	protected String condition_desc;
	protected String inspectability;
	protected String warning;
	protected String conditionType;
	protected String required;
	protected String short_text;
	protected String condition_text;
	protected int libraryId;
	protected String conditionCode;
	protected String addUpdateFlag;
	protected String removable;
	protected String conditionId; 
	private String conditionGroup;
    protected int noOfViolation;
   
	public ConditionLibraryRecord() {
		this.select = "";
		this.levelType = "";
		this.condition_desc = "";
		this.inspectability = "";
		this.warning = "";
		this.conditionType = "";
		this.required = "";
		this.short_text = "";
		this.condition_text = "";
		this.libraryId = 0;
		this.conditionCode = "";
	}

	public ConditionLibraryRecord(String select, String levelType, String condition_desc, String inspectability, String warning, String conditionType, String required, String short_text, String condition_text, int libraryId, String conditionCode) {
		this.select = select;
		this.levelType = levelType;
		this.condition_desc = condition_desc;
		this.inspectability = inspectability;
		this.warning = warning;
		this.conditionType = conditionType;
		this.required = required;
		this.short_text = short_text;
		this.condition_text = condition_text;
		this.libraryId = libraryId;
		this.conditionCode = conditionCode;

	}

	/**
	 * Gets the select
	 * 
	 * @return Returns a String
	 */
	public String getSelect() {
		return select;
	}

	/**
	 * Sets the select
	 * 
	 * @param select
	 *            The select to set
	 */
	public void setSelect(String select) {
		this.select = select;
	}

	/**
	 * Gets the levelType
	 * 
	 * @return Returns a String
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * Sets the levelType
	 * 
	 * @param levelType
	 *            The levelType to set
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * Gets the condition_desc
	 * 
	 * @return Returns a String
	 */
	public String getCondition_desc() {
		return condition_desc;
	}

	/**
	 * Sets the condition_desc
	 * 
	 * @param condition_desc
	 *            The condition_desc to set
	 */
	public void setCondition_desc(String condition_desc) {
		this.condition_desc = condition_desc;
	}

	/**
	 * Gets the inspectability
	 * 
	 * @return Returns a String
	 */
	public String getInspectability() {
		return inspectability;
	}

	/**
	 * Sets the inspectability
	 * 
	 * @param inspectability
	 *            The inspectability to set
	 */
	public void setInspectability(String inspectability) {
		this.inspectability = inspectability;
	}

	/**
	 * Gets the warning
	 * 
	 * @return Returns a String
	 */
	public String getWarning() {
		return warning;
	}

	/**
	 * Sets the warning
	 * 
	 * @param warning
	 *            The warning to set
	 */
	public void setWarning(String warning) {
		this.warning = warning;
	}

	/**
	 * Gets the conditionType
	 * 
	 * @return Returns a String
	 */
	public String getConditionType() {
		return conditionType;
	}

	/**
	 * Sets the conditionType
	 * 
	 * @param conditionType
	 *            The conditionType to set
	 */
	public void setPermit_type(String conditionType) {
		this.conditionType = conditionType;
	}

	/**
	 * Gets the required
	 * 
	 * @return Returns a String
	 */
	public String getRequired() {
		return required;
	}

	/**
	 * Sets the required
	 * 
	 * @param required
	 *            The required to set
	 */
	public void setRequired(String required) {
		this.required = required;
	}

	/**
	 * Gets the short_text
	 * 
	 * @return Returns a String
	 */
	public String getShort_text() {
		return short_text;
	}

	/**
	 * Sets the short_text
	 * 
	 * @param short_text
	 *            The short_text to set
	 */
	public void setShort_text(String short_text) {
		this.short_text = short_text;
	}

	/**
	 * Gets the condition_text
	 * 
	 * @return Returns a String
	 */
	public String getCondition_text() {
		return condition_text;
	}

	/**
	 * Sets the condition_text
	 * 
	 * @param condition_text
	 *            The condition_text to set
	 */
	public void setCondition_text(String condition_text) {
		this.condition_text = condition_text;
	}

	/**
	 * Gets the libraryId
	 * 
	 * @return Returns a int
	 */
	public int getLibraryId() {
		return libraryId;
	}

	/**
	 * Sets the libraryId
	 * 
	 * @param libraryId
	 *            The libraryId to set
	 */
	public void setLibraryId(int libraryId) {
		this.libraryId = libraryId;
	}

	/**
	 * Gets the conditionCode
	 * 
	 * @return Returns a String
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * Sets the conditionCode
	 * 
	 * @param conditionCode
	 *            The conditionCode to set
	 */
	public void setConditionCode(String conditionCode) {
		this.conditionCode = conditionCode;
	}

	/**
	 * Returns the addUpdateFlag.
	 * 
	 * @return String
	 */
	public String getAddUpdateFlag() {
		return addUpdateFlag;
	}

	/**
	 * Sets the addUpdateFlag.
	 * 
	 * @param addUpdateFlag
	 *            The addUpdateFlag to set
	 */
	public void setAddUpdateFlag(String addUpdateFlag) {
		this.addUpdateFlag = addUpdateFlag;
	}

	/**
	 * Sets the conditionType.
	 * 
	 * @param conditionType
	 *            The conditionType to set
	 */
	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	/**
	 * Returns the removable.
	 * 
	 * @return String
	 */
	public String getRemovable() {
		return removable;
	}

	/**
	 * Sets the removable.
	 * 
	 * @param removable
	 *            The removable to set
	 */
	public void setRemovable(String removable) {
		this.removable = removable;
	}

	/**
	 * @return
	 */
	public String getConditionId() {
		return conditionId;
	}

	/**
	 * @param string
	 */
	public void setConditionId(String string) {
		conditionId = string;
	}
	/**
	 * @return the conditionGroup
	 */
	public String getConditionGroup() {
		return conditionGroup;
	}

	/**
	 * @param conditionGroup the conditionGroup to set
	 */
	public void setConditionGroup(String conditionGroup) {
		this.conditionGroup = conditionGroup;
	}

	 
		/**
		 * @return the noOfViolation
		 */
		public int getNoOfViolation() {
			return noOfViolation;
		}

		/**
		 * @param noOfViolation the noOfViolation to set
		 */
		public void setNoOfViolation(int noOfViolation) {
			this.noOfViolation = noOfViolation;
		}

}