package com.elms.model;

import java.io.Serializable;

public class ActivitySubType implements Serializable {
	
	private int id;
	private String description;
	private String actType;
	private String label;

	public ActivitySubType() {

	}

	/*
	 * public ActivitySubType(int id) { this.id = id; //this.description = description; }
	 */

	public ActivitySubType(String description) {

		this.description = description;
	}

	public ActivitySubType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	
	
	public ActivitySubType(int id, String description, String label) {
		super();
		this.id = id;
		this.description = description;
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the id.
	 * 
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {

		this.description = description;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return Returns the actType.
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @param actType
	 *            The actType to set.
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}
}
