package com.elms.model;

/**
 * @author Shekhar Jain
 */
public class ProcessTeamForm {

	protected String psaId;
	protected String insertRow;
	protected String save;
	protected String psaType;
	protected String backUrl;
	protected boolean supervisor;
	protected ProcessTeamRecord[] processTeamRecord;
	protected ProcessTeamCollectionRecord[] processTeamCollectionRecord;

	public ProcessTeamForm() {

		insertRow = "";
		save = "";
		psaType = "";
		processTeamRecord = new ProcessTeamRecord[0];
		processTeamCollectionRecord = new ProcessTeamCollectionRecord[0];
	}

	/*public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.supervisor = false;
		if (this.processTeamRecord != null)
			for (int i = 0; i < this.processTeamRecord.length; i++) {
				this.processTeamRecord[i].setCheckRemove("");
				this.processTeamRecord[i].setLead("");
			}
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}*/

	/**
	 * Gets the processTeamRecord
	 * 
	 * @return Returns a ProcessTeamRecord[]
	 */
	public ProcessTeamRecord[] getProcessTeamRecord() {
		return processTeamRecord;
	}

	/**
	 * Sets the processTeamRecord
	 * 
	 * @param processTeamRecord
	 *            The processTeamRecord to set
	 */
	public void setProcessTeamRecord(ProcessTeamRecord[] processTeamRecord) {
		this.processTeamRecord = processTeamRecord;
	}

	/**
	 * Gets the processTeamCollectionRecord
	 * 
	 * @return Returns a ProcessTeamCollectionRecord[]
	 */
	public ProcessTeamCollectionRecord[] getProcessTeamCollectionRecord() {
		return processTeamCollectionRecord;
	}

	/**
	 * Sets the processTeamCollectionRecord
	 * 
	 * @param processTeamCollectionRecord
	 *            The processTeamCollectionRecord to set
	 */
	public void setProcessTeamCollectionRecord(ProcessTeamCollectionRecord[] processTeamCollectionRecord) {
		this.processTeamCollectionRecord = processTeamCollectionRecord;
	}

	/**
	 * Gets the insertRow
	 * 
	 * @return Returns a String
	 */
	public String getInsertRow() {
		return insertRow;
	}

	/**
	 * Sets the insertRow
	 * 
	 * @param insertRow
	 *            The insertRow to set
	 */
	public void setInsertRow(String insertRow) {
		this.insertRow = insertRow;
	}

	/**
	 * Gets the save
	 * 
	 * @return Returns a String
	 */
	public String getSave() {
		return save;
	}

	/**
	 * Sets the save
	 * 
	 * @param save
	 *            The save to set
	 */
	public void setSave(String save) {
		this.save = save;
	}

	/**
	 * Gets the psaId
	 * 
	 * @return Returns a String
	 */
	public String getPsaId() {
		return psaId;
	}

	/**
	 * Sets the psaId
	 * 
	 * @param psaId
	 *            The psaId to set
	 */
	public void setPsaId(String psaId) {
		this.psaId = psaId;
	}

	/**
	 * Gets the psaType
	 * 
	 * @return Returns a String
	 */
	public String getPsaType() {
		return psaType;
	}

	/**
	 * Sets the psaType
	 * 
	 * @param psaType
	 *            The psaType to set
	 */
	public void setPsaType(String psaType) {
		this.psaType = psaType;
	}

	/**
	 * @return
	 */
	public String getBackUrl() {
		return backUrl;
	}

	/**
	 * @param string
	 */
	public void setBackUrl(String string) {
		backUrl = string;
	}

	/**
	 * @return
	 */
	public boolean isSupervisor() {
		return supervisor;
	}

	/**
	 * @param b
	 */
	public void setSupervisor(boolean b) {
		supervisor = b;
	}

}
