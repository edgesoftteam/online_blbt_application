package com.elms.model;

public class ProcessTeamNameRecord {

	protected String nameValue;
	protected String nameLabel;

	/**
	 * Member variable declaration
	 */

	public ProcessTeamNameRecord() {
		nameValue = "";
		nameLabel = "";
	}

	public ProcessTeamNameRecord(String nameValue, String nameLabel) {
		this.nameValue = nameValue;
		this.nameLabel = nameLabel;
	}

	/**
	 * Gets the nameLabel
	 * 
	 * @return Returns a String
	 */
	public String getNameLabel() {
		return nameLabel;
	}

	/**
	 * Sets the nameLabel
	 * 
	 * @param nameLabel
	 *            The nameLabel to set
	 */
	public void setNameLabel(String nameLabel) {
		this.nameLabel = nameLabel;
	}

	/**
	 * Gets the nameValue
	 * 
	 * @return Returns a String
	 */
	public String getNameValue() {
		return nameValue;
	}

	/**
	 * Sets the nameValue
	 * 
	 * @param nameValue
	 *            The nameValue to set
	 */
	public void setNameValue(String nameValue) {
		this.nameValue = nameValue;
	}

}