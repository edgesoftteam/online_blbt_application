package com.elms.model;

import java.io.Serializable;
import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component
public class Fee implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7777938657730516063L;
	
	private int renewalCode;
	private String feeId;
	private String actType;
	private String feeDesc;
	private double factor;
	private double feeFactor;
	private double feeAmnt;
	private double feeUnits;
	private double totalFee;
	private double paidAmnt;
	private double balDue;
	private double feeTotal;//This is for calculating amount double value to hold the precision values(Ex:52.23 or xx.xx )
	private String feeTotalStr;//This is used for printing the value roundoff to 2 digits after period.
	
	private double valuation;
	private String noOfQuantity;
	private String feeCalc1;
	
	private int feeInit = 0;
	protected int subtotalLevel;
	double[] subTotals = {};
	private boolean homeOccupation;
	private String btQtyGroupId;
	
	public Fee() {
		super();
	}

	/**
	 * @return the renewalCode
	 */
	public int getRenewalCode() {
		return renewalCode;
	}

	/**
	 * @param renewalCode the renewalCode to set
	 */
	public void setRenewalCode(int renewalCode) {
		this.renewalCode = renewalCode;
	}

	/**
	 * @return the feeId
	 */
	public String getFeeId() {
		return feeId;
	}

	/**
	 * @param feeId the feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

	/**
	 * @return the actType
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @param actType the actType to set
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}

	/**
	 * @return the feeDesc
	 */
	public String getFeeDesc() {
		return feeDesc;
	}

	/**
	 * @param feeDesc the feeDesc to set
	 */
	public void setFeeDesc(String feeDesc) {
		this.feeDesc = feeDesc;
	}

	/**
	 * @return the factor
	 */
	public double getFactor() {
		return factor;
	}

	/**
	 * @param factor the factor to set
	 */
	public void setFactor(double factor) {
		this.factor = factor;
	}

	/**
	 * @return the feeFactor
	 */
	public double getFeeFactor() {
		return feeFactor;
	}

	/**
	 * @param feeFactor the feeFactor to set
	 */
	public void setFeeFactor(double feeFactor) {
		this.feeFactor = feeFactor;
	}

	/**
	 * @return the feeAmnt
	 */
	public double getFeeAmnt() {
		return feeAmnt;
	}

	/**
	 * @param feeAmnt the feeAmnt to set
	 */
	public void setFeeAmnt(double feeAmnt) {
		this.feeAmnt = feeAmnt;
	}

	/**
	 * @return the feeUnits
	 */
	public double getFeeUnits() {
		return feeUnits;
	}

	/**
	 * @param feeUnits the feeUnits to set
	 */
	public void setFeeUnits(double feeUnits) {
		this.feeUnits = feeUnits;
	}

	/**
	 * @return the totalFee
	 */
	public double getTotalFee() {
		return totalFee;
	}

	/**
	 * @param totalFee the totalFee to set
	 */
	public void setTotalFee(double totalFee) {
		this.totalFee = totalFee;
	}

	/**
	 * @return the feeTotal
	 */
	public double getFeeTotal() {
		return feeTotal;
	}

	/**
	 * @param feeTotal the feeTotal to set
	 */
	public void setFeeTotal(double feeTotal) {
		this.feeTotal = feeTotal;
	}

	/**
	 * @return the feeTotalStr
	 */
	public String getFeeTotalStr() {
		return feeTotalStr;
	}

	/**
	 * @param feeTotalStr the feeTotalStr to set
	 */
	public void setFeeTotalStr(String feeTotalStr) {
		this.feeTotalStr = feeTotalStr;
	}

	/**
	 * @return the valuation
	 */
	public double getValuation() {
		return valuation;
	}

	/**
	 * @param valuation the valuation to set
	 */
	public void setValuation(double valuation) {
		this.valuation = valuation;
	}

	/**
	 * @return the noOfQuantity
	 */
	public String getNoOfQuantity() {
		return noOfQuantity;
	}

	/**
	 * @param noOfQuantity the noOfQuantity to set
	 */
	public void setNoOfQuantity(String noOfQuantity) {
		this.noOfQuantity = noOfQuantity;
	}

	/**
	 * @return the feeCalc1
	 */
	public String getFeeCalc1() {
		return feeCalc1;
	}

	/**
	 * @param feeCalc1 the feeCalc1 to set
	 */
	public void setFeeCalc1(String feeCalc1) {
		this.feeCalc1 = feeCalc1;
	}

	/**
	 * @return the feeInit
	 */
	public int getFeeInit() {
		return feeInit;
	}

	/**
	 * @param feeInit the feeInit to set
	 */
	public void setFeeInit(int feeInit) {
		this.feeInit = feeInit;
	}

	/**
	 * @return the subtotalLevel
	 */
	public int getSubtotalLevel() {
		return subtotalLevel;
	}

	/**
	 * @param subtotalLevel the subtotalLevel to set
	 */
	public void setSubtotalLevel(int subtotalLevel) {
		this.subtotalLevel = subtotalLevel;
	}

	/**
	 * @return the subTotals
	 */
	public double[] getSubTotals() {
		return subTotals;
	}

	/**
	 * @param subTotals the subTotals to set
	 */
	public void setSubTotals(double[] subTotals) {
		this.subTotals = subTotals;
	}

	/**
	 * @return the homeOccupation
	 */
	public boolean isHomeOccupation() {
		return homeOccupation;
	}

	/**
	 * @param homeOccupation the homeOccupation to set
	 */
	public void setHomeOccupation(boolean homeOccupation) {
		this.homeOccupation = homeOccupation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Fee [renewalCode=" + renewalCode + ", feeId=" + feeId + ", actType=" + actType + ", feeDesc=" + feeDesc
				+ ", factor=" + factor + ", feeFactor=" + feeFactor + ", feeAmnt=" + feeAmnt + ", feeUnits=" + feeUnits
				+ ", totalFee=" + totalFee + ", feeTotal=" + feeTotal + ", feeTotalStr=" + feeTotalStr + ", valuation="
				+ valuation + ", noOfQuantity=" + noOfQuantity + ", feeCalc1=" + feeCalc1 + ", feeInit=" + feeInit
				+ ", subtotalLevel=" + subtotalLevel + ", subTotals=" + Arrays.toString(subTotals) + ", homeOccupation="
				+ homeOccupation + "]";
	}

	public String getBtQtyGroupId() {
		return btQtyGroupId;
	}

	public void setBtQtyGroupId(String btQtyGroupId) {
		this.btQtyGroupId = btQtyGroupId;
	}

	public double getPaidAmnt() {
		return paidAmnt;
	}

	public void setPaidAmnt(double paidAmnt) {
		this.paidAmnt = paidAmnt;
	}

	public double getBalDue() {
		return balDue;
	}

	public void setBalDue(double balDue) {
		this.balDue = balDue;
	}
}