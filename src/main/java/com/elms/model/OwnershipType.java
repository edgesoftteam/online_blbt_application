package com.elms.model;

/**
 * @author Manjuprasad & Gayathri
 * 
 *         Lookup object for business license ownership type
 */
public class OwnershipType {

	public int id;
	public String description;

	/**
	 *
	 */
	public OwnershipType() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public OwnershipType(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}
