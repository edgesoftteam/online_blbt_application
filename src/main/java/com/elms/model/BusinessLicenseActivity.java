package com.elms.model;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.elms.util.StringUtils;

/**
 * @author krishna
 * 
 *         Business License Activity Form Copy
 */
@Component
public class BusinessLicenseActivity {

	static Logger logger = Logger.getLogger(BusinessLicenseActivity.class.getName());

	protected String activityId;
	protected boolean businessLocation;
	protected ApplicationType applicationType;
	protected Street street;
	protected String address;
	protected String addressStreetNumber;
	protected String addressStreetFraction;
	protected String addressStreetName;
	protected String addressUnitNumber;
	protected String addressCity;
	protected String addressState;
	protected String addressZip;
	protected String addressZip4;
	protected String outOfTownStreetNumber;
	protected String outOfTownStreetName;
	protected String outOfTownUnitNumber;
	protected String outOfTownCity;
	protected String outOfTownState;
	protected String outOfTownZip;
	protected String outOfTownZip4;
	protected String activityNumber;
	protected ActivityStatus activityStatus;
	protected Calendar creationDate;
	protected Calendar renewalDate;
	protected Calendar renewalDt;
	protected String businessName;
	protected String businessAccountNumber;
	protected String businessOwnerName;
	protected String corporateName;
	protected String businessPhone;
	protected String businessExtension;
	protected String businessFax;
	protected ActivityType activityType;
	protected ActivitySubType activitySubType;
	protected String muncipalCode;
	protected String sicCode;
	protected String descOfBusiness;
	protected String classCode;
	protected boolean homeOccupation;
	protected boolean decalCode;
	protected Calendar applicationDate;
	protected String applicationDateString;
	protected Calendar issueDate;
	protected Calendar startingDate;
	protected Calendar outOfBusinessDate;
	protected OwnershipType ownershipType;
	protected String federalIdNumber;
	protected String emailAddress;
	protected String socialSecurityNumber;
	protected String mailStreetNumber;
	protected String mailStreetName;
	protected String mailUnitNumber;
	protected String mailCity;
	protected String mailState;
	protected String mailZip;
	protected String mailZip4;
	protected String prevStreetNumber;
	protected String prevStreetName;
	protected String prevUnitNumber;
	protected String prevCity;
	protected String prevState;
	protected String prevZip;
	protected String prevZip4;
	protected Calendar insuranceExpDate;
	protected Calendar bondExpDate;
	protected Calendar deptOfJusticeExpDate;
	protected Calendar federalFirearmsLiscExpDate;
	protected String squareFootage;
	protected QuantityType quantity;
	protected String quantityNum;
	protected ExemptionType typeOfExemptions;
	protected int subProjectId;
	protected String copyBusinessLicenseActivity = "N";
	protected String displayContent;
	protected int createdBy;
	protected int updatedBy;
	protected int departmentId;// to be used only for BL approval
	protected int userId;// approver id , to be used only for BL approval
	protected String departmentName;
	protected String userName;
	protected String approvalStatus;
	protected String activityStatusDescription;
	protected int approvalId;
	protected List businessLicenseApprovals;
	protected String departmentCheck;
	protected MultiAddress[] multiAddress;
	protected String lsoId;

	protected String activityStatusDesc;
	protected String addressStreetNameDesc;
	protected String activitySubTypeDesc;
	protected String ownershipTypeDesc;
	
	public String getMailingAddress() {
		return StringUtils.nullReplaceWithEmpty(mailStreetNumber) + " " + StringUtils.nullReplaceWithEmpty(mailStreetName) + " " + StringUtils.nullReplaceWithEmpty(mailUnitNumber);
	}

	/**
	 * @return Returns the businessLicenseApprovals.
	 */
	public List getBusinessLicenseApprovals() {
		return businessLicenseApprovals;
	}

	/**
	 * @param businessLicenseApprovals
	 *            The businessLicenseApprovals to set.
	 */
	public void setBusinessLicenseApprovals(List businessLicenseApprovals) {
		this.businessLicenseApprovals = businessLicenseApprovals;
	}

	/**
	 * @return Returns the approvalId.
	 */
	public int getApprovalId() {
		return approvalId;
	}

	/**
	 * @param approvalId
	 *            The approvalId to set.
	 */
	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	/**
	 * @return Returns the departmentName.
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName
	 *            The departmentName to set.
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the activityId.
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            The activityId to set.
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return Returns the createdBy.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the updatedBy.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            The updatedBy to set.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return Returns the addressStreetName.
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * @return Returns the addressCity.
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * @param addressCity
	 *            The addressCity to set.
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * @return Returns the departmentCheck.
	 */
	public String getDepartmentCheck() {
		return departmentCheck;
	}

	/**
	 * @param departmentCheck
	 *            The departmentCheck to set.
	 */
	public void setDepartmentCheck(String departmentCheck) {
		this.departmentCheck = departmentCheck;
	}

	/**
	 * @return Returns the addressState.
	 */
	public String getAddressState() {
		return addressState;
	}

	/**
	 * @param addressState
	 *            The addressState to set.
	 */
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	/**
	 * @return Returns the addressStreetNumber.
	 */
	public String getAddressStreetNumber() {
		return addressStreetNumber;
	}

	/**
	 * @param addressStreetNumber
	 *            The addressStreetNumber to set.
	 */
	public void setAddressStreetNumber(String addressStreetNumber) {
		this.addressStreetNumber = addressStreetNumber;
	}

	/**
	 * @return Returns the addressStreetFraction.
	 */
	public String getAddressStreetFraction() {
		return addressStreetFraction;
	}

	/**
	 * @param addressStreetFraction
	 *            The addressStreetFraction to set.
	 */
	public void setAddressStreetFraction(String addressStreetFraction) {
		this.addressStreetFraction = addressStreetFraction;
	}

	/**
	 * @return Returns the addressUnitNumber.
	 */
	public String getAddressUnitNumber() {
		return addressUnitNumber;
	}

	/**
	 * @param addressUnitNumber
	 *            The addressUnitNumber to set.
	 */
	public void setAddressUnitNumber(String addressUnitNumber) {
		this.addressUnitNumber = addressUnitNumber;
	}

	/**
	 * @return Returns the addressZip.
	 */
	public String getAddressZip() {
		return addressZip;
	}

	/**
	 * @param addressZip
	 *            The addressZip to set.
	 */
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}

	/**
	 * @return Returns the addressZip4.
	 */
	public String getAddressZip4() {
		return addressZip4;
	}

	/**
	 * @param addressZip4
	 *            The addressZip4 to set.
	 */
	public void setAddressZip4(String addressZip4) {
		this.addressZip4 = addressZip4;
	}

	/**
	 * @return Returns the logger.
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            The logger to set.
	 */
	public static void setLogger(Logger logger) {
		BusinessLicenseActivity.logger = logger;
	}

	/**
	 * @return Returns the activityNumber.
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * @param activityNumber
	 *            The activityNumber to set.
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * @return Returns the activityStatus.
	 */
	public ActivityStatus getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(ActivityStatus activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the activitySubType.
	 */
	public ActivitySubType getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(ActivitySubType activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the activityType.
	 */
	public ActivityType getActivityType() {

		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;

	}

	/**
	 * @return Returns the departmentId.
	 */
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            The departmentId to set.
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return Returns the userId.
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @param applicationDateString
	 *            The applicationDateString to set.
	 */
	public void setApplicationDateString(String applicationDateString) {
		this.applicationDateString = applicationDateString;
	}

	/**
	 * @return Returns the applicationDate.
	 */
	public Calendar getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate
	 *            The applicationDate to set.
	 */
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return Returns the applicationType.
	 */
	public ApplicationType getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType
	 *            The applicationType to set.
	 */
	public void setApplicationType(ApplicationType applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return Returns the bondExpDate.
	 */
	public Calendar getBondExpDate() {
		return bondExpDate;
	}

	/**
	 * @param bondExpDate
	 *            The bondExpDate to set.
	 */
	public void setBondExpDate(Calendar bondExpDate) {
		this.bondExpDate = bondExpDate;
	}

	/**
	 * @return Returns the businessAccountNumber.
	 */
	public String getBusinessAccountNumber() {
		return businessAccountNumber;
	}

	/**
	 * @param businessAccountNumber
	 *            The businessAccountNumber to set.
	 */
	public void setBusinessAccountNumber(String businessAccountNumber) {
		this.businessAccountNumber = businessAccountNumber;
	}

	/**
	 * @return Returns the businessOwnerName.
	 */
	public String getBusinessOwnerName() {
		return businessOwnerName;
	}

	/**
	 * @param businessOwnerName
	 *            The businessOwnerName to set.
	 */
	public void setBusinessOwnerName(String businessOwnerName) {
		this.businessOwnerName = businessOwnerName;
	}

	/**
	 * @return Returns the businessExtension.
	 */
	public String getBusinessExtension() {
		return businessExtension;
	}

	/**
	 * @param businessExtension
	 *            The businessExtension to set.
	 */
	public void setBusinessExtension(String businessExtension) {
		this.businessExtension = businessExtension;
	}

	/**
	 * @return Returns the businessFax.
	 */
	public String getBusinessFax() {
		return businessFax;
	}

	/**
	 * @param businessFax
	 *            The businessFax to set.
	 */
	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}

	/**
	 * @return Returns the businessLocation.
	 */
	public boolean isBusinessLocation() {
		return businessLocation;
	}

	/**
	 * @param businessLocation
	 *            The businessLocation to set.
	 */
	public void setBusinessLocation(boolean businessLocation) {
		this.businessLocation = businessLocation;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the businessPhone.
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}

	/**
	 * @param businessPhone
	 *            The businessPhone to set.
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the copyBusinessLicenseActivity.
	 */
	public String getCopyBusinessLicenseActivity() {
		return copyBusinessLicenseActivity;
	}

	/**
	 * @param copyBusinessLicenseActivity
	 *            The copyBusinessLicenseActivity to set.
	 */
	public void setCopyBusinessLicenseActivity(String copyBusinessLicenseActivity) {
		this.copyBusinessLicenseActivity = copyBusinessLicenseActivity;
	}

	/**
	 * @return Returns the corporateName.
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * @param corporateName
	 *            The corporateName to set.
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * @return Returns the creationDate.
	 */
	public Calendar getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            The creationDate to set.
	 */
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return Returns the decalCode.
	 */
	public boolean isDecalCode() {
		return decalCode;
	}

	/**
	 * @param decalCode
	 *            The decalCode to set.
	 */
	public void setDecalCode(boolean decalCode) {
		this.decalCode = decalCode;
	}

	/**
	 * @return Returns the deptOfJusticeExpDate.
	 */
	public Calendar getDeptOfJusticeExpDate() {
		return deptOfJusticeExpDate;
	}

	/**
	 * @param deptOfJusticeExpDate
	 *            The deptOfJusticeExpDate to set.
	 */
	public void setDeptOfJusticeExpDate(Calendar deptOfJusticeExpDate) {
		this.deptOfJusticeExpDate = deptOfJusticeExpDate;
	}

	/**
	 * @return Returns the descOfBusiness.
	 */
	public String getDescOfBusiness() {
		return descOfBusiness;
	}

	/**
	 * @param descOfBusiness
	 *            The descOfBusiness to set.
	 */
	public void setDescOfBusiness(String descOfBusiness) {
		this.descOfBusiness = descOfBusiness;
	}

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the federalFirearmsLiscExpDate.
	 */
	public Calendar getFederalFirearmsLiscExpDate() {
		return federalFirearmsLiscExpDate;
	}

	/**
	 * @param federalFirearmsLiscExpDate
	 *            The federalFirearmsLiscExpDate to set.
	 */
	public void setFederalFirearmsLiscExpDate(Calendar federalFirearmsLiscExpDate) {
		this.federalFirearmsLiscExpDate = federalFirearmsLiscExpDate;
	}

	/**
	 * @return Returns the federalIdNumber.
	 */
	public String getFederalIdNumber() {
		return federalIdNumber;
	}

	/**
	 * @param federalIdNumber
	 *            The federalIdNumber to set.
	 */
	public void setFederalIdNumber(String federalIdNumber) {
		this.federalIdNumber = federalIdNumber;
	}

	/**
	 * @return Returns the homeOccupation.
	 */
	public boolean isHomeOccupation() {
		return homeOccupation;
	}

	/**
	 * @param homeOccupation
	 *            The homeOccupation to set.
	 */
	public void setHomeOccupation(boolean homeOccupation) {
		this.homeOccupation = homeOccupation;
	}

	/**
	 * @return Returns the insuranceExpDate.
	 */
	public Calendar getInsuranceExpDate() {
		return insuranceExpDate;
	}

	/**
	 * @param insuranceExpDate
	 *            The insuranceExpDate to set.
	 */
	public void setInsuranceExpDate(Calendar insuranceExpDate) {
		this.insuranceExpDate = insuranceExpDate;
	}

	/**
	 * @return Returns the issueDate.
	 */
	public Calendar getIssueDate() {
		return issueDate;
	}

	/**
	 * @param issueDate
	 *            The issueDate to set.
	 */
	public void setIssueDate(Calendar issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * @return Returns the mailCity.
	 */
	public String getMailCity() {
		return mailCity;
	}

	/**
	 * @param mailCity
	 *            The mailCity to set.
	 */
	public void setMailCity(String mailCity) {
		this.mailCity = mailCity;
	}

	/**
	 * @return Returns the mailState.
	 */
	public String getMailState() {
		return mailState;
	}

	/**
	 * @param mailState
	 *            The mailState to set.
	 */
	public void setMailState(String mailState) {
		this.mailState = mailState;
	}

	/**
	 * @return Returns the mailStreetName.
	 */
	public String getMailStreetName() {
		return mailStreetName;
	}

	/**
	 * @param mailStreetName
	 *            The mailStreetName to set.
	 */
	public void setMailStreetName(String mailStreetName) {
		this.mailStreetName = mailStreetName;
	}

	/**
	 * @return Returns the mailStreetNumber.
	 */
	public String getMailStreetNumber() {
		return mailStreetNumber;
	}

	/**
	 * @param mailStreetNumber
	 *            The mailStreetNumber to set.
	 */
	public void setMailStreetNumber(String mailStreetNumber) {
		this.mailStreetNumber = mailStreetNumber;
	}

	/**
	 * @return Returns the mailUnitNumber.
	 */
	public String getMailUnitNumber() {
		return mailUnitNumber;
	}

	/**
	 * @param mailUnitNumber
	 *            The mailUnitNumber to set.
	 */
	public void setMailUnitNumber(String mailUnitNumber) {
		this.mailUnitNumber = mailUnitNumber;
	}

	/**
	 * @return Returns the mailZip.
	 */
	public String getMailZip() {
		return mailZip;
	}

	/**
	 * @param mailZip
	 *            The mailZip to set.
	 */
	public void setMailZip(String mailZip) {
		this.mailZip = mailZip;
	}

	/**
	 * @return Returns the mailZip4.
	 */
	public String getMailZip4() {
		return mailZip4;
	}

	/**
	 * @param mailZip4
	 *            The mailZip4 to set.
	 */
	public void setMailZip4(String mailZip4) {
		this.mailZip4 = mailZip4;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the outOfBusinessDate.
	 */
	public Calendar getOutOfBusinessDate() {
		return outOfBusinessDate;
	}

	/**
	 * @param outOfBusinessDate
	 *            The outOfBusinessDate to set.
	 */
	public void setOutOfBusinessDate(Calendar outOfBusinessDate) {
		this.outOfBusinessDate = outOfBusinessDate;
	}

	/**
	 * @return Returns the outOfTownCity.
	 */
	public String getOutOfTownCity() {
		return outOfTownCity;
	}

	/**
	 * @param outOfTownCity
	 *            The outOfTownCity to set.
	 */
	public void setOutOfTownCity(String outOfTownCity) {
		this.outOfTownCity = outOfTownCity;
	}

	/**
	 * @return Returns the outOfTownState.
	 */
	public String getOutOfTownState() {
		return outOfTownState;
	}

	/**
	 * @param outOfTownState
	 *            The outOfTownState to set.
	 */
	public void setOutOfTownState(String outOfTownState) {
		this.outOfTownState = outOfTownState;
	}

	/**
	 * @return Returns the outOfTownStreetName.
	 */
	public String getOutOfTownStreetName() {
		return outOfTownStreetName;
	}

	/**
	 * @param outOfTownStreetName
	 *            The outOfTownStreetName to set.
	 */
	public void setOutOfTownStreetName(String outOfTownStreetName) {
		this.outOfTownStreetName = outOfTownStreetName;
	}

	/**
	 * @return Returns the outOfTownStreetNumber.
	 */
	public String getOutOfTownStreetNumber() {
		return outOfTownStreetNumber;
	}

	/**
	 * @param outOfTownStreetNumber
	 *            The outOfTownStreetNumber to set.
	 */
	public void setOutOfTownStreetNumber(String outOfTownStreetNumber) {
		this.outOfTownStreetNumber = outOfTownStreetNumber;
	}

	/**
	 * @return Returns the outOfTownUnitNumber.
	 */
	public String getOutOfTownUnitNumber() {
		return outOfTownUnitNumber;
	}

	/**
	 * @param outOfTownUnitNumber
	 *            The outOfTownUnitNumber to set.
	 */
	public void setOutOfTownUnitNumber(String outOfTownUnitNumber) {
		this.outOfTownUnitNumber = outOfTownUnitNumber;
	}

	/**
	 * @return Returns the outOfTownZip.
	 */
	public String getOutOfTownZip() {
		return outOfTownZip;
	}

	/**
	 * @param outOfTownZip
	 *            The outOfTownZip to set.
	 */
	public void setOutOfTownZip(String outOfTownZip) {
		this.outOfTownZip = outOfTownZip;
	}

	/**
	 * @return Returns the outOfTownZip4.
	 */
	public String getOutOfTownZip4() {
		return outOfTownZip4;
	}

	/**
	 * @param outOfTownZip4
	 *            The outOfTownZip4 to set.
	 */
	public void setOutOfTownZip4(String outOfTownZip4) {
		this.outOfTownZip4 = outOfTownZip4;
	}

	/**
	 * @return Returns the ownershipType.
	 */
	public OwnershipType getOwnershipType() {
		return ownershipType;
	}

	/**
	 * @param ownershipType
	 *            The ownershipType to set.
	 */
	public void setOwnershipType(OwnershipType ownershipType) {
		this.ownershipType = ownershipType;
	}

	/**
	 * @return Returns the prevCity.
	 */
	public String getPrevCity() {
		return prevCity;
	}

	/**
	 * @param prevCity
	 *            The prevCity to set.
	 */
	public void setPrevCity(String prevCity) {
		this.prevCity = prevCity;
	}

	/**
	 * @return Returns the prevState.
	 */
	public String getPrevState() {
		return prevState;
	}

	/**
	 * @param prevState
	 *            The prevState to set.
	 */
	public void setPrevState(String prevState) {
		this.prevState = prevState;
	}

	/**
	 * @return Returns the prevStreetName.
	 */
	public String getPrevStreetName() {
		return prevStreetName;
	}

	/**
	 * @param prevStreetName
	 *            The prevStreetName to set.
	 */
	public void setPrevStreetName(String prevStreetName) {
		this.prevStreetName = prevStreetName;
	}

	/**
	 * @return Returns the prevStreetNumber.
	 */
	public String getPrevStreetNumber() {
		return prevStreetNumber;
	}

	/**
	 * @param prevStreetNumber
	 *            The prevStreetNumber to set.
	 */
	public void setPrevStreetNumber(String prevStreetNumber) {
		this.prevStreetNumber = prevStreetNumber;
	}

	/**
	 * @return Returns the prevUnitNumber.
	 */
	public String getPrevUnitNumber() {
		return prevUnitNumber;
	}

	/**
	 * @param prevUnitNumber
	 *            The prevUnitNumber to set.
	 */
	public void setPrevUnitNumber(String prevUnitNumber) {
		this.prevUnitNumber = prevUnitNumber;
	}

	/**
	 * @return Returns the prevZip.
	 */
	public String getPrevZip() {
		return prevZip;
	}

	/**
	 * @param prevZip
	 *            The prevZip to set.
	 */
	public void setPrevZip(String prevZip) {
		this.prevZip = prevZip;
	}

	/**
	 * @return Returns the prevZip4.
	 */
	public String getPrevZip4() {
		return prevZip4;
	}

	/**
	 * @param prevZip4
	 *            The prevZip4 to set.
	 */
	public void setPrevZip4(String prevZip4) {
		this.prevZip4 = prevZip4;
	}

	/**
	 * @return Returns the quantity.
	 */
	public QuantityType getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            The quantity to set.
	 */
	public void setQuantity(QuantityType quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return Returns the quantityNum.
	 */
	public String getQuantityNum() {
		return quantityNum;
	}

	/**
	 * @param quantityNum
	 *            The quantityNum to set.
	 */
	public void setQuantityNum(String quantityNum) {
		this.quantityNum = quantityNum;
	}

	/**
	 * @return Returns the renewalDate.
	 */
	public Calendar getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @param renewalDate
	 *            The renewalDate to set.
	 */
	public void setRenewalDate(Calendar renewalDate) {
		this.renewalDate = renewalDate;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	/**
	 * @return Returns the socialSecurityNumber.
	 */
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	/**
	 * @param socialSecurityNumber
	 *            The socialSecurityNumber to set.
	 */
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @return Returns the squareFootage.
	 */
	public String getSquareFootage() {
		return squareFootage;
	}

	/**
	 * @param squareFootage
	 *            The squareFootage to set.
	 */
	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	/**
	 * @return Returns the startingDate.
	 */
	public Calendar getStartingDate() {
		return startingDate;
	}

	/**
	 * @param startingDate
	 *            The startingDate to set.
	 */
	public void setStartingDate(Calendar startingDate) {
		this.startingDate = startingDate;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the typeOfExemptions.
	 */
	public ExemptionType getTypeOfExemptions() {
		return typeOfExemptions;
	}

	/**
	 * @param typeOfExemptions
	 *            The typeOfExemptions to set.
	 */
	public void setTypeOfExemptions(ExemptionType typeOfExemptions) {
		this.typeOfExemptions = typeOfExemptions;
	}

	/**
	 * @return Returns the street.
	 */
	public Street getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            The street to set.
	 */
	public void setStreet(Street street) {
		this.street = street;
	}

	/**
	 * @param addressStreetName
	 *            The addressStreetName to set.
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

	/**
	 * @return Returns the renewalDt.
	 */
	public Calendar getRenewalDt() {
		return renewalDt;
	}

	/**
	 * @param renewalDt
	 *            The renewalDt to set.
	 */
	public void setRenewalDt(Calendar renewalDt) {
		this.renewalDt = renewalDt;
	}

	/**
	 * @return Returns the displayContent.
	 */
	public String getDisplayContent() {
		return displayContent;
	}

	/**
	 * @param displayContent
	 *            The displayContent to set.
	 */
	public void setDisplayContent(String displayContent) {
		this.displayContent = displayContent;
	}

	/**
	 * @return Returns the applicationDateString.
	 */
	public String getApplicationDateString() {
		return StringUtils.cal2str(applicationDate);
	}

	/**
	 * @return Returns the approvalStatus.
	 */
	public String getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * @param approvalStatus
	 *            The approvalStatus to set.
	 */
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * @return Returns the activityStatusDescription.
	 */
	public String getActivityStatusDescription() {
		return activityStatusDescription;
	}

	/**
	 * @param activityStatusDescription
	 *            The activityStatusDescription to set.
	 */
	public void setActivityStatusDescription(String activityStatusDescription) {
		this.activityStatusDescription = activityStatusDescription;
	}

	public MultiAddress[] getMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(MultiAddress[] multiAddress) {
		this.multiAddress = multiAddress;
	}

	public String getLsoId() {
		return lsoId;
	}

	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	@Override
	public String toString() {
		return "BusinessLicenseActivity [activityId=" + activityId + ", businessLocation=" + businessLocation
				+ ", applicationType=" + applicationType + ", street=" + street + ", address=" + address
				+ ", addressStreetNumber=" + addressStreetNumber + ", addressStreetFraction=" + addressStreetFraction
				+ ", addressStreetName=" + addressStreetName + ", addressUnitNumber=" + addressUnitNumber
				+ ", addressCity=" + addressCity + ", addressState=" + addressState + ", addressZip=" + addressZip
				+ ", addressZip4=" + addressZip4 + ", outOfTownStreetNumber=" + outOfTownStreetNumber
				+ ", outOfTownStreetName=" + outOfTownStreetName + ", outOfTownUnitNumber=" + outOfTownUnitNumber
				+ ", outOfTownCity=" + outOfTownCity + ", outOfTownState=" + outOfTownState + ", outOfTownZip="
				+ outOfTownZip + ", outOfTownZip4=" + outOfTownZip4 + ", activityNumber=" + activityNumber
				+ ", activityStatus=" + activityStatus + ", creationDate=" + creationDate + ", renewalDate="
				+ renewalDate + ", renewalDt=" + renewalDt + ", businessName=" + businessName
				+ ", businessAccountNumber=" + businessAccountNumber + ", businessOwnerName=" + businessOwnerName
				+ ", corporateName=" + corporateName + ", businessPhone=" + businessPhone + ", businessExtension="
				+ businessExtension + ", businessFax=" + businessFax + ", activityType=" + activityType
				+ ", activitySubType=" + activitySubType + ", muncipalCode=" + muncipalCode + ", sicCode=" + sicCode
				+ ", descOfBusiness=" + descOfBusiness + ", classCode=" + classCode + ", homeOccupation="
				+ homeOccupation + ", decalCode=" + decalCode + ", applicationDate=" + applicationDate
				+ ", applicationDateString=" + applicationDateString + ", issueDate=" + issueDate + ", startingDate="
				+ startingDate + ", outOfBusinessDate=" + outOfBusinessDate + ", ownershipType=" + ownershipType
				+ ", federalIdNumber=" + federalIdNumber + ", emailAddress=" + emailAddress + ", socialSecurityNumber="
				+ socialSecurityNumber + ", mailStreetNumber=" + mailStreetNumber + ", mailStreetName=" + mailStreetName
				+ ", mailUnitNumber=" + mailUnitNumber + ", mailCity=" + mailCity + ", mailState=" + mailState
				+ ", mailZip=" + mailZip + ", mailZip4=" + mailZip4 + ", prevStreetNumber=" + prevStreetNumber
				+ ", prevStreetName=" + prevStreetName + ", prevUnitNumber=" + prevUnitNumber + ", prevCity=" + prevCity
				+ ", prevState=" + prevState + ", prevZip=" + prevZip + ", prevZip4=" + prevZip4 + ", insuranceExpDate="
				+ insuranceExpDate + ", bondExpDate=" + bondExpDate + ", deptOfJusticeExpDate=" + deptOfJusticeExpDate
				+ ", federalFirearmsLiscExpDate=" + federalFirearmsLiscExpDate + ", squareFootage=" + squareFootage
				+ ", quantity=" + quantity + ", quantityNum=" + quantityNum + ", typeOfExemptions=" + typeOfExemptions
				+ ", subProjectId=" + subProjectId + ", copyBusinessLicenseActivity=" + copyBusinessLicenseActivity
				+ ", displayContent=" + displayContent + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ ", departmentId=" + departmentId + ", userId=" + userId + ", departmentName=" + departmentName
				+ ", userName=" + userName + ", approvalStatus=" + approvalStatus + ", activityStatusDescription="
				+ activityStatusDescription + ", approvalId=" + approvalId + ", businessLicenseApprovals="
				+ businessLicenseApprovals + ", departmentCheck=" + departmentCheck + ", multiAddress="
				+ Arrays.toString(multiAddress) + ", lsoId=" + lsoId + "]";
	}

	public String getActivityStatusDesc() {
		return activityStatusDesc;
	}

	public void setActivityStatusDesc(String activityStatusDesc) {
		this.activityStatusDesc = activityStatusDesc;
	}

	public String getAddressStreetNameDesc() {
		return addressStreetNameDesc;
	}

	public void setAddressStreetNameDesc(String addressStreetNameDesc) {
		this.addressStreetNameDesc = addressStreetNameDesc;
	}

	public String getActivitySubTypeDesc() {
		return activitySubTypeDesc;
	}

	public void setActivitySubTypeDesc(String activitySubTypeDesc) {
		this.activitySubTypeDesc = activitySubTypeDesc;
	}

	public String getOwnershipTypeDesc() {
		return ownershipTypeDesc;
	}

	public void setOwnershipTypeDesc(String ownershipTypeDesc) {
		this.ownershipTypeDesc = ownershipTypeDesc;
	}
}