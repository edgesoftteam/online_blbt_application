package com.elms.model;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class ActivityType implements Serializable {
	private String typeId;
	private String type;
	private String description;
	private String departmentCode;
	private int departmentId;
	private int subProjectType;
	private int moduleId;
	private String muncipalCode;
	private String sicCode;
	private String classCode;
	protected String departmentDescription;

	static Logger logger = Logger.getLogger(ActivityType.class.getName());

	/**
	 * @param type
	 */
	public ActivityType(String type) {
		super();
		this.type = type;
	}

	public ActivityType() {

	}

	/**
	 * @return Returns the moduleId.
	 */
	public int getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            The moduleId to set.
	 */
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public ActivityType(String type, String description) {

		this.type = type;
		this.description = description;
	}

	public ActivityType(String type, String description, String departmentCode, String departmentDescription) {
		this.type = type;
		this.description = description;
		this.departmentCode = departmentCode;
		this.departmentDescription = departmentDescription;
	}

	public String getDepartmentDescription() {
		return departmentDescription;
	}

	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {

		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;

	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the departmentCode
	 * 
	 * @return Returns a String
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * Sets the departmentCode
	 * 
	 * @param departmentCode
	 *            The departmentCode to set
	 */
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/**
	 * Returns the departmentId.
	 * 
	 * @return int
	 */
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * Returns the subProjectType.
	 * 
	 * @return int
	 */
	public int getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the departmentId.
	 * 
	 * @param departmentId
	 *            The departmentId to set
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Sets the subProjectType.
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(int subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Returns the typeId.
	 * 
	 * @return String
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * Sets the typeId.
	 * 
	 * @param typeId
	 *            The typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}
}
