package com.elms.model;

import java.util.ArrayList;
import java.util.List;

public class ProcessTeamCollectionRecord {

	protected String checkRemove;
	protected String name;
	protected List nameList;
	protected String title;
	protected List titleList;
	protected String lead;

	/**
	 * Member variable declaration
	 */

	public ProcessTeamCollectionRecord() {
		checkRemove = "of";
		name = "";
		title = "";
		lead = "of";
		nameList = new ArrayList();
		titleList = new ArrayList();

		nameList.add(new ProcessTeamNameRecord());
		titleList.add(new ProcessTeamTitleRecord());

	}

	public ProcessTeamCollectionRecord(String checkRemove, String name, String title, String lead) {
		this.checkRemove = checkRemove;
		this.name = name;
		this.title = title;
		this.lead = lead;
		nameList = new ArrayList();
		titleList = new ArrayList();

		nameList.add(new ProcessTeamNameRecord());
		titleList.add(new ProcessTeamTitleRecord());
	}

	/**
	 * Gets the checkRemove
	 * 
	 * @return Returns a String
	 */
	public String getCheckRemove() {
		return checkRemove;
	}

	/**
	 * Sets the checkRemove
	 * 
	 * @param checkRemove
	 *            The checkRemove to set
	 */
	public void setCheckRemove(String checkRemove) {
		this.checkRemove = checkRemove;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the title
	 * 
	 * @return Returns a String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the lead
	 * 
	 * @return Returns a String
	 */
	public String getLead() {
		return lead;
	}

	/**
	 * Sets the lead
	 * 
	 * @param lead
	 *            The lead to set
	 */
	public void setLead(String lead) {
		this.lead = lead;
	}

	/**
	 * Gets the nameList
	 * 
	 * @return Returns a List
	 */
	public List getNameList() {
		return nameList;
	}

	/**
	 * Sets the nameList
	 * 
	 * @param nameList
	 *            The nameList to set
	 */
	public void setNameList(List nameList) {
		this.nameList = nameList;
	}

	/**
	 * Gets the titleList
	 * 
	 * @return Returns a List
	 */
	public List getTitleList() {
		return titleList;
	}

	/**
	 * Sets the titleList
	 * 
	 * @param titleList
	 *            The titleList to set
	 */
	public void setTitleList(List titleList) {
		this.titleList = titleList;
	}

}