//Source file: C:\\datafile\\source\\elms\\app\\lso\\Street.java

package com.elms.model;

/**
 * @author Shekhar Jain
 */
public class Street {

	protected int streetId;
	protected String streetName = "";
	protected String streetPrefix;
	protected String streetSuffix;
	protected String streetType;
	 protected String streetNamewithType;

	public Street(int streetId, String streetName, String streetPrefix, String streetSuffix, String streetType) {
		this.streetId = streetId;
		this.streetName = streetName;
		this.streetPrefix = streetPrefix;
		this.streetSuffix = streetSuffix;
		this.streetType = streetType;
	}

	public Street(int streetId, String streetName, String streetType) {
		this.streetId = streetId;
		this.streetName = streetName;
		this.streetType = streetType;
	}

	public Street(int streetId, String streetName) {
		this.streetId = streetId;
		this.streetName = streetName;
	}

	public Street(int streetId) {
		this.streetId = streetId;
	}

	/**
	 * 3CCDCBA7036F
	 */
	public Street() {

	}

	/**
	 * Gets the streetId
	 * 
	 * @return Returns a int
	 */
	public int getStreetId() {
		return streetId;
	}

	/**
	 * Sets the streetId
	 * 
	 * @param streetId
	 *            The streetId to set
	 */
	public void setStreetId(int streetId) {
		this.streetId = streetId;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the streetPrefix
	 * 
	 * @return Returns a String
	 */
	public String getStreetPrefix() {
		return streetPrefix;
	}

	/**
	 * Sets the streetPrefix
	 * 
	 * @param streetPrefix
	 *            The streetPrefix to set
	 */
	public void setStreetPrefix(String streetPrefix) {
		this.streetPrefix = streetPrefix;
	}

	/**
	 * Gets the streetSuffix
	 * 
	 * @return Returns a String
	 */
	public String getStreetSuffix() {
		return streetSuffix;
	}

	/**
	 * Sets the streetSuffix
	 * 
	 * @param streetSuffix
	 *            The streetSuffix to set
	 */
	public void setStreetSuffix(String streetSuffix) {
		this.streetSuffix = streetSuffix;
	}

	/**
	 * Gets the streetType
	 * 
	 * @return Returns a String
	 */
	public String getStreetType() {
		return streetType;
	}

	/**
	 * Sets the streetType
	 * 
	 * @param streetType
	 *            The streetType to set
	 */
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}
	
	/**
	 * @return the streetNamewithType
	 */
	public String getStreetNamewithType() {
		return streetNamewithType;
	}


	/**
	 * @param streetNamewithType the streetNamewithType to set
	 */
	public void setStreetNamewithType(String streetNamewithType) {
		this.streetNamewithType = streetNamewithType;
	}

	
	 public Street(String streetName,String streetNameWithType ){
	    	this.streetName = streetName;
	    	this.streetNamewithType = streetNameWithType;
	    	
	    }

}
