//Source file: C:\\datafile\\source\\elms\\security\\Department.java
package com.elms.model;

import java.io.Serializable;

public class Department implements Serializable {
	private int departmentId;
	private String departmentCode;
	private String description;

	public Department() {

	}

	public Department(int deptId, String deptCode, String desc) {
		this.departmentId = deptId;
		this.departmentCode = deptCode;
		this.description = desc;
	}

	/**
	 * Gets the departmentId
	 * 
	 * @return Returns a int
	 */
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * Sets the departmentId
	 * 
	 * @param departmentId
	 *            The departmentId to set
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Gets the departmentCode
	 * 
	 * @return Returns a String
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * Sets the departmentCode
	 * 
	 * @param departmentCode
	 *            The departmentCode to set
	 */
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
