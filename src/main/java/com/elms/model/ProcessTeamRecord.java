package com.elms.model;

import java.util.Calendar;
import java.util.Date;

public class ProcessTeamRecord {

	protected String checkRemove;
	protected String name;
	protected String nameId;
	protected String title;
	protected String titleId;
	protected String lead;
	protected int createdBy;
	protected Calendar created;
	protected int updatedBy;
	protected Calendar updated;
	protected boolean isNew;

	/**
	 * Member variable declaration
	 */

	public ProcessTeamRecord() {
		checkRemove = "of";
		name = "";
		title = "";
		lead = "of";
	}

	public ProcessTeamRecord(String checkRemove, String name, String title, String lead) {
		this.checkRemove = checkRemove;
		this.name = name;
		this.title = title;
		this.lead = lead;
	}

	/**
	 * Gets the checkRemove
	 * 
	 * @return Returns a String
	 */
	public String getCheckRemove() {
		return checkRemove;
	}

	/**
	 * Sets the checkRemove
	 * 
	 * @param checkRemove
	 *            The checkRemove to set
	 */
	public void setCheckRemove(String checkRemove) {
		this.checkRemove = checkRemove;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the title
	 * 
	 * @return Returns a String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the lead
	 * 
	 * @return Returns a String
	 */
	public String getLead() {
		return lead;
	}

	/**
	 * Sets the lead
	 * 
	 * @param lead
	 *            The lead to set
	 */
	public void setLead(String lead) {
		this.lead = lead;
	}

	/**
	 * Gets the isNew
	 * 
	 * @return Returns a boolean
	 */
	public boolean getIsNew() {
		return isNew;
	}

	/**
	 * Sets the isNew
	 * 
	 * @param isNew
	 *            The isNew to set
	 */
	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}

	/**
	 * Gets the nameId
	 * 
	 * @return Returns a String
	 */
	public String getNameId() {
		return nameId;
	}

	/**
	 * Sets the nameId
	 * 
	 * @param nameId
	 *            The nameId to set
	 */
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}

	/**
	 * Gets the titleId
	 * 
	 * @return Returns a String
	 */
	public String getTitleId() {
		return titleId;
	}

	/**
	 * Sets the titleId
	 * 
	 * @param titleId
	 *            The titleId to set
	 */
	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}
	}

}