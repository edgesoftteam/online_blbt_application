package com.elms.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Activity implements Serializable {
	
	/**
	 * System generate serial UID
	 */
	private static final long serialVersionUID = 3555109083041770462L;
	
	

	private int renewalCodeIntValue; // Activity Id is considered as renewalCode in complete app of BL/BT Renewals 
	private String renewalCode; // Activity Id is considered as renewalCode in complete app of BL/BT Renewals 
	private String businessAccNo;
	private String permitNumber; // Activity Number is considered as permitNumber in complete app of BL/BT Renewals
		
	private String actType;
	private String actTypeDesc;
	
	private String tempId;
	private String tempBusinessName;
	private String tempEmail;
	private String tempBusinessPhone;
	private String tempBusinessAccNo;
	private String tempUserDetailsCheckbox;
	private String tempBlQtyFlag;
	private String tempBtQtyFlag;
	
	private String startDate;
	private String appliedDate;
	private String issueDate;
	private String expirationDate;
	private String finaledDate;
	private String created;
	private String updated;
	
	private String burbankBusiness;
	private String business_fax;
	private String businessName;
	private String businessPhone;
	private String businessPhoneExt;
	private String businessFax;
	
	private String businessStrNo;
	private String businessStrName;
	private String businessCity;
	private String businessState;
	private String businessZip;
	private String businessCityStateZip;
	
	private String mailStrName;
	private String mailStrNo;
	private String mailUnit;
	private String mailCity;
	private String mailState;
	private String mailZip;
	
	private String strNo;
	private String strName;
	private String address;
	private String cityStateZip;
	private String unit;
	private String city;
	private String state;
	private String zip;
	
	private String name;
	private String email;	
	private String description;
	private String qtyDesc;
    private String totalFee;
	private double serviceFee;
	private String newEmail;
	private String cancel;
	private String newPhone;

	private String checkBox;
	private String selectNoOfEmp;
	
	private List<Fee> feeList;
	private String blQtyFlag;
	private String btQtyFlag;
	private String qtyOther;
    private List<MultipartFile> theFile;
    
	private String responseCode;
	private String authCode;
	private String onlineTransactionId;
	private List<Attachment> attachmentList;
	
	private String noOfEmployeees;
	private String noOfEmp;
	private String tempBtQty;
	private String linePattern;
	private String placeholderDesc;
	private String line1Desc;
	private String line2Desc;
	private String line3Desc;
	
	private String displayErrorMsg;
	private String displaySuccessMsg;
	private String attachFlag;
	
	private String screenName;
	private String tempFileLoc;
	private String viewName;
	
	private String onlineBLBTApplication;
	
	/**
	 * @return the renewalCodeIntValue
	 */
	public int getRenewalCodeIntValue() {
		return renewalCodeIntValue;
	}
	/**
	 * @param renewalCodeIntValue the renewalCodeIntValue to set
	 */
	public void setRenewalCodeIntValue(int renewalCodeIntValue) {
		this.renewalCodeIntValue = renewalCodeIntValue;
	}
	/**
	 * @return the renewalCode
	 */
	public String getRenewalCode() {
		return renewalCode;
	}
	/**
	 * @param renewalCode the renewalCode to set
	 */
	public void setRenewalCode(String renewalCode) {
		this.renewalCode = renewalCode;
	}
	/**
	 * @return the businessAccNo
	 */
	public String getBusinessAccNo() {
		return businessAccNo;
	}
	/**
	 * @param businessAccNo the businessAccNo to set
	 */
	public void setBusinessAccNo(String businessAccNo) {
		this.businessAccNo = businessAccNo;
	}
	/**
	 * @return the permitNumber
	 */
	public String getPermitNumber() {
		return permitNumber;
	}
	/**
	 * @param permitNumber the permitNumber to set
	 */
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}
	/**
	 * @return the actType
	 */
	public String getActType() {
		return actType;
	}
	/**
	 * @param actType the actType to set
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}
	/**
	 * @return the actTypeDesc
	 */
	public String getActTypeDesc() {
		return actTypeDesc;
	}
	/**
	 * @param actTypeDesc the actTypeDesc to set
	 */
	public void setActTypeDesc(String actTypeDesc) {
		this.actTypeDesc = actTypeDesc;
	}
	/**
	 * @return the tempId
	 */
	public String getTempId() {
		return tempId;
	}
	/**
	 * @param tempId the tempId to set
	 */
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	/**
	 * @return the tempBusinessName
	 */
	public String getTempBusinessName() {
		return tempBusinessName;
	}
	/**
	 * @param tempBusinessName the tempBusinessName to set
	 */
	public void setTempBusinessName(String tempBusinessName) {
		this.tempBusinessName = tempBusinessName;
	}
	/**
	 * @return the tempEmail
	 */
	public String getTempEmail() {
		return tempEmail;
	}
	/**
	 * @param tempEmail the tempEmail to set
	 */
	public void setTempEmail(String tempEmail) {
		this.tempEmail = tempEmail;
	}
	/**
	 * @return the tempBusinessPhone
	 */
	public String getTempBusinessPhone() {
		return tempBusinessPhone;
	}
	/**
	 * @param tempBusinessPhone the tempBusinessPhone to set
	 */
	public void setTempBusinessPhone(String tempBusinessPhone) {
		this.tempBusinessPhone = tempBusinessPhone;
	}
	/**
	 * @return the tempBusinessAccNo
	 */
	public String getTempBusinessAccNo() {
		return tempBusinessAccNo;
	}
	/**
	 * @param tempBusinessAccNo the tempBusinessAccNo to set
	 */
	public void setTempBusinessAccNo(String tempBusinessAccNo) {
		this.tempBusinessAccNo = tempBusinessAccNo;
	}
	/**
	 * @return the tempUserDetailsCheckbox
	 */
	public String getTempUserDetailsCheckbox() {
		return tempUserDetailsCheckbox;
	}
	/**
	 * @param tempUserDetailsCheckbox the tempUserDetailsCheckbox to set
	 */
	public void setTempUserDetailsCheckbox(String tempUserDetailsCheckbox) {
		this.tempUserDetailsCheckbox = tempUserDetailsCheckbox;
	}
	/**
	 * @return the tempBlQtyFlag
	 */
	public String getTempBlQtyFlag() {
		return tempBlQtyFlag;
	}
	/**
	 * @param tempBlQtyFlag the tempBlQtyFlag to set
	 */
	public void setTempBlQtyFlag(String tempBlQtyFlag) {
		this.tempBlQtyFlag = tempBlQtyFlag;
	}
	/**
	 * @return the tempBtQtyFlag
	 */
	public String getTempBtQtyFlag() {
		return tempBtQtyFlag;
	}
	/**
	 * @param tempBtQtyFlag the tempBtQtyFlag to set
	 */
	public void setTempBtQtyFlag(String tempBtQtyFlag) {
		this.tempBtQtyFlag = tempBtQtyFlag;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the appliedDate
	 */
	public String getAppliedDate() {
		return appliedDate;
	}
	/**
	 * @param appliedDate the appliedDate to set
	 */
	public void setAppliedDate(String appliedDate) {
		this.appliedDate = appliedDate;
	}
	/**
	 * @return the issueDate
	 */
	public String getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return expirationDate;
	}
	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	/**
	 * @return the finaledDate
	 */
	public String getFinaledDate() {
		return finaledDate;
	}
	/**
	 * @param finaledDate the finaledDate to set
	 */
	public void setFinaledDate(String finaledDate) {
		this.finaledDate = finaledDate;
	}
	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}
	/**
	 * @return the updated
	 */
	public String getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	/**
	 * @return the burbankBusiness
	 */
	public String getBurbankBusiness() {
		return burbankBusiness;
	}
	/**
	 * @param burbankBusiness the burbankBusiness to set
	 */
	public void setBurbankBusiness(String burbankBusiness) {
		this.burbankBusiness = burbankBusiness;
	}
	/**
	 * @return the business_fax
	 */
	public String getBusiness_fax() {
		return business_fax;
	}
	/**
	 * @param business_fax the business_fax to set
	 */
	public void setBusiness_fax(String business_fax) {
		this.business_fax = business_fax;
	}
	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}
	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	/**
	 * @return the businessPhone
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}
	/**
	 * @param businessPhone the businessPhone to set
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}
	/**
	 * @return the businessPhoneExt
	 */
	public String getBusinessPhoneExt() {
		return businessPhoneExt;
	}
	/**
	 * @param businessPhoneExt the businessPhoneExt to set
	 */
	public void setBusinessPhoneExt(String businessPhoneExt) {
		this.businessPhoneExt = businessPhoneExt;
	}
	/**
	 * @return the businessFax
	 */
	public String getBusinessFax() {
		return businessFax;
	}
	/**
	 * @param businessFax the businessFax to set
	 */
	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}
	/**
	 * @return the businessStrNo
	 */
	public String getBusinessStrNo() {
		return businessStrNo;
	}
	/**
	 * @param businessStrNo the businessStrNo to set
	 */
	public void setBusinessStrNo(String businessStrNo) {
		this.businessStrNo = businessStrNo;
	}
	/**
	 * @return the businessStrName
	 */
	public String getBusinessStrName() {
		return businessStrName;
	}
	/**
	 * @param businessStrName the businessStrName to set
	 */
	public void setBusinessStrName(String businessStrName) {
		this.businessStrName = businessStrName;
	}
	/**
	 * @return the businessCity
	 */
	public String getBusinessCity() {
		return businessCity;
	}
	/**
	 * @param businessCity the businessCity to set
	 */
	public void setBusinessCity(String businessCity) {
		this.businessCity = businessCity;
	}
	/**
	 * @return the businessState
	 */
	public String getBusinessState() {
		return businessState;
	}
	/**
	 * @param businessState the businessState to set
	 */
	public void setBusinessState(String businessState) {
		this.businessState = businessState;
	}
	/**
	 * @return the businessZip
	 */
	public String getBusinessZip() {
		return businessZip;
	}
	/**
	 * @param businessZip the businessZip to set
	 */
	public void setBusinessZip(String businessZip) {
		this.businessZip = businessZip;
	}
	/**
	 * @return the businessCityStateZip
	 */
	public String getBusinessCityStateZip() {
		return businessCityStateZip;
	}
	/**
	 * @param businessCityStateZip the businessCityStateZip to set
	 */
	public void setBusinessCityStateZip(String businessCityStateZip) {
		this.businessCityStateZip = businessCityStateZip;
	}
	/**
	 * @return the mailStrName
	 */
	public String getMailStrName() {
		return mailStrName;
	}
	/**
	 * @param mailStrName the mailStrName to set
	 */
	public void setMailStrName(String mailStrName) {
		this.mailStrName = mailStrName;
	}
	/**
	 * @return the mailStrNo
	 */
	public String getMailStrNo() {
		return mailStrNo;
	}
	/**
	 * @param mailStrNo the mailStrNo to set
	 */
	public void setMailStrNo(String mailStrNo) {
		this.mailStrNo = mailStrNo;
	}
	/**
	 * @return the mailUnit
	 */
	public String getMailUnit() {
		return mailUnit;
	}
	/**
	 * @param mailUnit the mailUnit to set
	 */
	public void setMailUnit(String mailUnit) {
		this.mailUnit = mailUnit;
	}
	/**
	 * @return the mailCity
	 */
	public String getMailCity() {
		return mailCity;
	}
	/**
	 * @param mailCity the mailCity to set
	 */
	public void setMailCity(String mailCity) {
		this.mailCity = mailCity;
	}
	/**
	 * @return the mailState
	 */
	public String getMailState() {
		return mailState;
	}
	/**
	 * @param mailState the mailState to set
	 */
	public void setMailState(String mailState) {
		this.mailState = mailState;
	}
	/**
	 * @return the mailZip
	 */
	public String getMailZip() {
		return mailZip;
	}
	/**
	 * @param mailZip the mailZip to set
	 */
	public void setMailZip(String mailZip) {
		this.mailZip = mailZip;
	}
	/**
	 * @return the strNo
	 */
	public String getStrNo() {
		return strNo;
	}
	/**
	 * @param strNo the strNo to set
	 */
	public void setStrNo(String strNo) {
		this.strNo = strNo;
	}
	/**
	 * @return the strName
	 */
	public String getStrName() {
		return strName;
	}
	/**
	 * @param strName the strName to set
	 */
	public void setStrName(String strName) {
		this.strName = strName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the cityStateZip
	 */
	public String getCityStateZip() {
		return cityStateZip;
	}
	/**
	 * @param cityStateZip the cityStateZip to set
	 */
	public void setCityStateZip(String cityStateZip) {
		this.cityStateZip = cityStateZip;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the qtyDesc
	 */
	public String getQtyDesc() {
		return qtyDesc;
	}
	/**
	 * @param qtyDesc the qtyDesc to set
	 */
	public void setQtyDesc(String qtyDesc) {
		this.qtyDesc = qtyDesc;
	}
	/**
	 * @return the serviceFee
	 */
	public double getServiceFee() {
		return serviceFee;
	}
	/**
	 * @param serviceFee the serviceFee to set
	 */
	public void setServiceFee(double serviceFee) {
		this.serviceFee = serviceFee;
	}
	/**
	 * @return the newEmail
	 */
	public String getNewEmail() {
		return newEmail;
	}
	/**
	 * @param newEmail the newEmail to set
	 */
	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}
	/**
	 * @return the cancel
	 */
	public String getCancel() {
		return cancel;
	}
	/**
	 * @param cancel the cancel to set
	 */
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	/**
	 * @return the newPhone
	 */
	public String getNewPhone() {
		return newPhone;
	}
	/**
	 * @param newPhone the newPhone to set
	 */
	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}
	/**
	 * @return the checkBox
	 */
	public String getCheckBox() {
		return checkBox;
	}
	/**
	 * @param checkBox the checkBox to set
	 */
	public void setCheckBox(String checkBox) {
		this.checkBox = checkBox;
	}
	/**
	 * @return the selectNoOfEmp
	 */
	public String getSelectNoOfEmp() {
		return selectNoOfEmp;
	}
	/**
	 * @param selectNoOfEmp the selectNoOfEmp to set
	 */
	public void setSelectNoOfEmp(String selectNoOfEmp) {
		this.selectNoOfEmp = selectNoOfEmp;
	}
	/**
	 * @return the feeList
	 */
	public List<Fee> getFeeList() {
		return feeList;
	}
	/**
	 * @param feeList the feeList to set
	 */
	public void setFeeList(List<Fee> feeList) {
		this.feeList = feeList;
	}
	/**
	 * @return the blQtyFlag
	 */
	public String getBlQtyFlag() {
		return blQtyFlag;
	}
	/**
	 * @param blQtyFlag the blQtyFlag to set
	 */
	public void setBlQtyFlag(String blQtyFlag) {
		this.blQtyFlag = blQtyFlag;
	}
	/**
	 * @return the btQtyFlag
	 */
	public String getBtQtyFlag() {
		return btQtyFlag;
	}
	/**
	 * @param btQtyFlag the btQtyFlag to set
	 */
	public void setBtQtyFlag(String btQtyFlag) {
		this.btQtyFlag = btQtyFlag;
	}
	/**
	 * @return the qtyOther
	 */
	public String getQtyOther() {
		return qtyOther;
	}
	/**
	 * @param qtyOther the qtyOther to set
	 */
	public void setQtyOther(String qtyOther) {
		this.qtyOther = qtyOther;
	}
	/**
	 * @return the theFile
	 */
	public List<MultipartFile> getTheFile() {
		return theFile;
	}
	/**
	 * @param theFile the theFile to set
	 */
	public void setTheFile(List<MultipartFile> theFile) {
		this.theFile = theFile;
	}
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}
	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	/**
	 * @return the authCode
	 */
	public String getAuthCode() {
		return authCode;
	}
	/**
	 * @param authCode the authCode to set
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	/**
	 * @return the onlineTransactionId
	 */
	public String getOnlineTransactionId() {
		return onlineTransactionId;
	}
	/**
	 * @param onlineTransactionId the onlineTransactionId to set
	 */
	public void setOnlineTransactionId(String onlineTransactionId) {
		this.onlineTransactionId = onlineTransactionId;
	}
	/**
	 * @return the attachmentList
	 */
	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}
	/**
	 * @param attachmentList the attachmentList to set
	 */
	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}
	/**
	 * @return the noOfEmployeees
	 */
	public String getNoOfEmployeees() {
		return noOfEmployeees;
	}
	/**
	 * @param noOfEmployeees the noOfEmployeees to set
	 */
	public void setNoOfEmployeees(String noOfEmployeees) {
		this.noOfEmployeees = noOfEmployeees;
	}
	/**
	 * @return the noOfEmp
	 */
	public String getNoOfEmp() {
		return noOfEmp;
	}
	/**
	 * @param noOfEmp the noOfEmp to set
	 */
	public void setNoOfEmp(String noOfEmp) {
		this.noOfEmp = noOfEmp;
	}
	/**
	 * @return the tempBtQty
	 */
	public String getTempBtQty() {
		return tempBtQty;
	}
	/**
	 * @param tempBtQty the tempBtQty to set
	 */
	public void setTempBtQty(String tempBtQty) {
		this.tempBtQty = tempBtQty;
	}
	/**
	 * @return the displayErrorMsg
	 */
	public String getDisplayErrorMsg() {
		return displayErrorMsg;
	}
	/**
	 * @param displayErrorMsg the displayErrorMsg to set
	 */
	public void setDisplayErrorMsg(String displayErrorMsg) {
		this.displayErrorMsg = displayErrorMsg;
	}
	/**
	 * @return the displaySuccessMsg
	 */
	public String getDisplaySuccessMsg() {
		return displaySuccessMsg;
	}
	/**
	 * @param displaySuccessMsg the displaySuccessMsg to set
	 */
	public void setDisplaySuccessMsg(String displaySuccessMsg) {
		this.displaySuccessMsg = displaySuccessMsg;
	}
	/**
	 * @return the attachFlag
	 */
	public String getAttachFlag() {
		return attachFlag;
	}
	/**
	 * @param attachFlag the attachFlag to set
	 */
	public void setAttachFlag(String attachFlag) {
		this.attachFlag = attachFlag;
	}
	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}
	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	/**
	 * @return the tempFileLoc
	 */
	public String getTempFileLoc() {
		return tempFileLoc;
	}
	/**
	 * @param tempFileLoc the tempFileLoc to set
	 */
	public void setTempFileLoc(String tempFileLoc) {
		this.tempFileLoc = tempFileLoc;
	}
	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}
	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	

	/**
	 * Default constructor for Activity Bean
	 */
	public Activity() {
		super();
	}
	
	/**
	 * @param renewalCode
	 * @param businessAccNo
	 * @param permitNumber
	 * @param actType
	 * @param actTypeDesc
	 * @param tempId
	 * @param tempBusinessName
	 * @param tempEmail
	 * @param tempBusinessPhone
	 * @param tempBusinessAccNo
	 * @param tempUserDetailsCheckbox
	 * @param tempBlQtyFlag
	 * @param tempBtQtyFlag
	 * @param startDate
	 * @param appliedDate
	 * @param issueDate
	 * @param expirationDate
	 * @param finaledDate
	 * @param created
	 * @param updated
	 * @param burbankBusiness
	 * @param business_fax
	 * @param businessName
	 * @param businessPhone
	 * @param businessPhoneExt
	 * @param businessFax
	 * @param businessStrNo
	 * @param businessStrName
	 * @param businessCity
	 * @param businessState
	 * @param businessZip
	 * @param businessCityStateZip
	 * @param mailStrName
	 * @param mailStrNo
	 * @param mailUnit
	 * @param mailCity
	 * @param mailState
	 * @param mailZip
	 * @param strNo
	 * @param strName
	 * @param address
	 * @param cityStateZip
	 * @param unit
	 * @param city
	 * @param state
	 * @param zip
	 * @param name
	 * @param email
	 * @param description
	 * @param qtyDesc
	 * @param totalFee
	 * @param serviceFee
	 * @param newEmail
	 * @param cancel
	 * @param newPhone
	 * @param checkBox
	 * @param selectNoOfEmp
	 * @param feeList
	 * @param blQtyFlag
	 * @param btQtyFlag
	 * @param qtyOther
	 * @param theFile
	 * @param responseCode
	 * @param authCode
	 * @param attachmentList
	 * @param noOfEmployeees
	 * @param noOfEmp
	 * @param tempBtQty
	 * @param displayErrorMsg
	 * @param displaySuccessMsg
	 * @param attachFlag
	 * @param screenName
	 * @param tempFileLoc
	 * @param viewName
	 */
	public Activity(String renewalCode, String businessAccNo, String permitNumber, String actType, String actTypeDesc,
			String tempId, String tempBusinessName, String tempEmail, String tempBusinessPhone,
			String tempBusinessAccNo, String tempUserDetailsCheckbox, String tempBlQtyFlag, String tempBtQtyFlag,
			String startDate, String appliedDate, String issueDate, String expirationDate, String finaledDate,
			String created, String updated, String burbankBusiness, String business_fax, String businessName,
			String businessPhone, String businessPhoneExt, String businessFax, String businessStrNo,
			String businessStrName, String businessCity, String businessState, String businessZip,
			String businessCityStateZip, String mailStrName, String mailStrNo, String mailUnit, String mailCity,
			String mailState, String mailZip, String strNo, String strName, String address, String cityStateZip,
			String unit,String city, String state, String zip, String name, String email, String description, String qtyDesc,
			String totalFee, double serviceFee, String newEmail, String cancel, String newPhone, String checkBox,
			String selectNoOfEmp, List<Fee> feeList, String blQtyFlag, String btQtyFlag, String qtyOther,
			List<MultipartFile> theFile, String responseCode, String authCode, String onlineTransactionId, List<Attachment> attachmentList,
			String noOfEmployeees, String noOfEmp, String tempBtQty, String displayErrorMsg, String displaySuccessMsg,
			String attachFlag, String screenName, String tempFileLoc, String viewName) {
		super();
		this.renewalCode = renewalCode;
		this.businessAccNo = businessAccNo;
		this.permitNumber = permitNumber;
		this.actType = actType;
		this.actTypeDesc = actTypeDesc;
		this.tempId = tempId;
		this.tempBusinessName = tempBusinessName;
		this.tempEmail = tempEmail;
		this.tempBusinessPhone = tempBusinessPhone;
		this.tempBusinessAccNo = tempBusinessAccNo;
		this.tempUserDetailsCheckbox = tempUserDetailsCheckbox;
		this.tempBlQtyFlag = tempBlQtyFlag;
		this.tempBtQtyFlag = tempBtQtyFlag;
		this.startDate = startDate;
		this.appliedDate = appliedDate;
		this.issueDate = issueDate;
		this.expirationDate = expirationDate;
		this.finaledDate = finaledDate;
		this.created = created;
		this.updated = updated;
		this.burbankBusiness = burbankBusiness;
		this.business_fax = business_fax;
		this.businessName = businessName;
		this.businessPhone = businessPhone;
		this.businessPhoneExt = businessPhoneExt;
		this.businessFax = businessFax;
		this.businessStrNo = businessStrNo;
		this.businessStrName = businessStrName;
		this.businessCity = businessCity;
		this.businessState = businessState;
		this.businessZip = businessZip;
		this.businessCityStateZip = businessCityStateZip;
		this.mailStrName = mailStrName;
		this.mailStrNo = mailStrNo;
		this.mailUnit = mailUnit;
		this.mailCity = mailCity;
		this.mailState = mailState;
		this.mailZip = mailZip;
		this.strNo = strNo;
		this.strName = strName;
		this.address = address;
		this.cityStateZip = cityStateZip;
		this.unit = unit;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.name = name;
		this.email = email;
		this.description = description;
		this.qtyDesc = qtyDesc;
		this.totalFee = totalFee;
		this.serviceFee = serviceFee;
		this.newEmail = newEmail;
		this.cancel = cancel;
		this.newPhone = newPhone;
		this.checkBox = checkBox;
		this.selectNoOfEmp = selectNoOfEmp;
		this.feeList = feeList;
		this.blQtyFlag = blQtyFlag;
		this.btQtyFlag = btQtyFlag;
		this.qtyOther = qtyOther;
		this.theFile = theFile;
		this.responseCode = responseCode;
		this.authCode = authCode;
		this.onlineTransactionId = onlineTransactionId;
		this.attachmentList = attachmentList;
		this.noOfEmployeees = noOfEmployeees;
		this.noOfEmp = noOfEmp;
		this.tempBtQty = tempBtQty;
		this.displayErrorMsg = displayErrorMsg;
		this.displaySuccessMsg = displaySuccessMsg;
		this.attachFlag = attachFlag;
		this.screenName = screenName;
		this.tempFileLoc = tempFileLoc;
		this.viewName = viewName;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public String getPlaceholderDesc() {
		return placeholderDesc;
	}
	public void setPlaceholderDesc(String placeholderDesc) {
		this.placeholderDesc = placeholderDesc;
	}
	public String getLinePattern() {
		return linePattern;
	}
	public void setLinePattern(String linePattern) {
		this.linePattern = linePattern;
	}
	public String getLine1Desc() {
		return line1Desc;
	}
	public void setLine1Desc(String line1Desc) {
		this.line1Desc = line1Desc;
	}
	public String getLine2Desc() {
		return line2Desc;
	}
	public void setLine2Desc(String line2Desc) {
		this.line2Desc = line2Desc;
	}
	public String getLine3Desc() {
		return line3Desc;
	}
	public void setLine3Desc(String line3Desc) {
		this.line3Desc = line3Desc;
	}
	public String getOnlineBLBTApplication() {
		return onlineBLBTApplication;
	}
	public void setOnlineBLBTApplication(String onlineBLBTApplication) {
		this.onlineBLBTApplication = onlineBLBTApplication;
	}
	@Override
	public String toString() {
		return "Activity [renewalCodeIntValue=" + renewalCodeIntValue + ", renewalCode=" + renewalCode
				+ ", businessAccNo=" + businessAccNo + ", permitNumber=" + permitNumber + ", actType=" + actType
				+ ", actTypeDesc=" + actTypeDesc + ", tempId=" + tempId + ", tempBusinessName=" + tempBusinessName
				+ ", tempEmail=" + tempEmail + ", tempBusinessPhone=" + tempBusinessPhone + ", tempBusinessAccNo="
				+ tempBusinessAccNo + ", tempUserDetailsCheckbox=" + tempUserDetailsCheckbox + ", tempBlQtyFlag="
				+ tempBlQtyFlag + ", tempBtQtyFlag=" + tempBtQtyFlag + ", startDate=" + startDate + ", appliedDate="
				+ appliedDate + ", issueDate=" + issueDate + ", expirationDate=" + expirationDate + ", finaledDate="
				+ finaledDate + ", created=" + created + ", updated=" + updated + ", burbankBusiness=" + burbankBusiness
				+ ", business_fax=" + business_fax + ", businessName=" + businessName + ", businessPhone="
				+ businessPhone + ", businessPhoneExt=" + businessPhoneExt + ", businessFax=" + businessFax
				+ ", businessStrNo=" + businessStrNo + ", businessStrName=" + businessStrName + ", businessCity="
				+ businessCity + ", businessState=" + businessState + ", businessZip=" + businessZip
				+ ", businessCityStateZip=" + businessCityStateZip + ", mailStrName=" + mailStrName + ", mailStrNo="
				+ mailStrNo + ", mailUnit=" + mailUnit + ", mailCity=" + mailCity + ", mailState=" + mailState
				+ ", mailZip=" + mailZip + ", strNo=" + strNo + ", strName=" + strName + ", address=" + address
				+ ", cityStateZip=" + cityStateZip + ", unit=" + unit + ", city=" + city + ", state=" + state + ", zip="
				+ zip + ", name=" + name + ", email=" + email + ", description=" + description + ", qtyDesc=" + qtyDesc
				+ ", totalFee=" + totalFee + ", serviceFee=" + serviceFee + ", newEmail=" + newEmail + ", cancel="
				+ cancel + ", newPhone=" + newPhone + ", checkBox=" + checkBox + ", selectNoOfEmp=" + selectNoOfEmp
				+ ", feeList=" + feeList + ", blQtyFlag=" + blQtyFlag + ", btQtyFlag=" + btQtyFlag + ", qtyOther="
				+ qtyOther + ", theFile=" + theFile + ", responseCode=" + responseCode + ", authCode=" + authCode
				+ ", onlineTransactionId=" + onlineTransactionId + ", attachmentList=" + attachmentList
				+ ", noOfEmployeees=" + noOfEmployeees + ", noOfEmp=" + noOfEmp + ", tempBtQty=" + tempBtQty
				+ ", linePattern=" + linePattern + ", placeholderDesc=" + placeholderDesc + ", line1Desc=" + line1Desc
				+ ", line2Desc=" + line2Desc + ", line3Desc=" + line3Desc + ", displayErrorMsg=" + displayErrorMsg
				+ ", displaySuccessMsg=" + displaySuccessMsg + ", attachFlag=" + attachFlag + ", screenName="
				+ screenName + ", tempFileLoc=" + tempFileLoc + ", viewName=" + viewName + ", onlineBLBTApplication="
				+ onlineBLBTApplication + "]";
	}
}