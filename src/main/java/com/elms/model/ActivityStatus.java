package com.elms.model;

public class ActivityStatus {

	private int status;
	private String code;
	private String description;

	/**
	 * Constructor for ActivityStatus
	 */
	public ActivityStatus() {

	}

	/**
	 * Constructor for ActivityStatus
	 */

	public ActivityStatus(String code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * Constructor for ActivityStatus
	 */
	public ActivityStatus(int status, String code, String description) {
		this.status = status;
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a int
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a String
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
