package com.elms.model;

/**
 * @author Manjuprasad and Gayathri
 * 
 *         Lookup object for business license Quantity type
 */
public class QuantityType {

	public int id;
	public String description;

	/**
	 * 
	 */
	public QuantityType() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public QuantityType(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}
