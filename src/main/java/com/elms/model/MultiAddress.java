/**
 * 
 */
package com.elms.model;

import java.io.Serializable;

/**
 * @author Arjun
 *
 */
public class MultiAddress implements Serializable {

	
	protected String id;
	protected int actId;
	protected String name;
	protected String title;
	protected String type;
	protected String streetNumber;
	protected String streetName1;
	protected String streetName2;
	protected String attn;
	protected String unit;
	protected String city;
	protected String state;
	protected String zip;
	protected String zip4;
	protected String addressType;
	
	public MultiAddress() {
		super();
	}
	
	public MultiAddress(String id, int actId, String type, String streetNumber, String streetName1,
			String streetName2, String attn, String unit, String city, String state, String zip, String zip4,
			String addressType) {
		super();
		this.id = id;
		this.actId = actId;
		this.type = type;
		this.streetNumber = streetNumber;
		this.streetName1 = streetName1;
		this.streetName2 = streetName2;
		this.attn = attn;
		this.unit = unit;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.zip4 = zip4;
		this.addressType = addressType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getActId() {
		return actId;
	}
	public void setActId(int actId) {
		this.actId = actId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetName1() {
		return streetName1;
	}
	public void setStreetName1(String streetName1) {
		this.streetName1 = streetName1;
	}
	public String getStreetName2() {
		return streetName2;
	}
	public void setStreetName2(String streetName2) {
		this.streetName2 = streetName2;
	}
	public String getAttn() {
		return attn;
	}
	public void setAttn(String attn) {
		this.attn = attn;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getZip4() {
		return zip4;
	}
	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "MultiAddress [id=" + id + ", actId=" + actId + ", name=" + name + ", title=" + title + ", type=" + type
				+ ", streetNumber=" + streetNumber + ", streetName1=" + streetName1 + ", streetName2=" + streetName2
				+ ", attn=" + attn + ", unit=" + unit + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", zip4=" + zip4 + ", addressType=" + addressType + "]";
	}

}
