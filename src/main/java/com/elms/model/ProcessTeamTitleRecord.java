package com.elms.model;

public class ProcessTeamTitleRecord {

	protected String titleValue;
	protected String titleLabel;

	/**
	 * Member variable declaration
	 */

	public ProcessTeamTitleRecord() {
		titleValue = "";
		titleLabel = "";
	}

	public ProcessTeamTitleRecord(String titleValue, String titleLabel) {
		this.titleValue = titleValue;
		this.titleLabel = titleLabel;
	}

	/**
	 * Gets the titleLabel
	 * 
	 * @return Returns a String
	 */
	public String getTitleLabel() {
		return titleLabel;
	}

	/**
	 * Sets the titleLabel
	 * 
	 * @param titleLabel
	 *            The titleLabel to set
	 */
	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	/**
	 * Gets the titleValue
	 * 
	 * @return Returns a String
	 */
	public String getTitleValue() {
		return titleValue;
	}

	/**
	 * Sets the titleValue
	 * 
	 * @param titleValue
	 *            The titleValue to set
	 */
	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}

}