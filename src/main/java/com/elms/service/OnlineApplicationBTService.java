package com.elms.service;

import java.util.List;
import java.util.Map;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;

/**
 * @author Siva Kumari
 *
 */

public interface OnlineApplicationBTService {

	Map getCodesForClassCode(String clCode) throws Exception;
	
	boolean isClassCodeExists(String clCode) throws Exception;
	
	public int saveBusinessTaxActivity(BusinessTaxActivity businessTaxActivity, int lsoId) throws Exception;

	public BusinessTaxActivity getBusinessTaxActivity(int activityId, int lsoId) throws Exception;

	public BusinessTaxActivity getBusinessTaxActivity(BusinessTaxActivityForm businessTaxActivityForm) throws Exception;

	public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId);
	/**
	 * This method is to save BT Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity saveBTFeeDetails(Activity activity) throws BasicExceptionHandler;
	/** 
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity getPreviewPageDetails(Activity activity) throws BasicExceptionHandler;
	/** 
	 * This method is to fetch Fee details from temp table and Fee table
	 * for fee calculations
	 * 
	 * @return String actType, String tempId
	 * @Parameters List<Fee>
	 */
	public List<Fee> getBTFees(String actType, String tempId, String screenName) throws BasicExceptionHandler;

}
