package com.elms.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Attachment;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.MultiAddress;

public interface BusinessLicenseRenewalService {

	/**
	 * This method is to save the Business Account Number and 
	 * Renewal Code (activity Id) in the temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	
	Activity saveBLRenewalDetails(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to update user details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity saveUserDetails(Activity activity) throws BasicExceptionHandler;

	/**
	 * This method is to save BL quantity details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity saveQtyDetails(Activity activity) throws BasicExceptionHandler;

	/**
	 * This method is to save BL attachments details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity saveAttachmentDetails(Activity activity,String attachmentTypeId) throws BasicExceptionHandler;

	/**
	 * This method is to save BL fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity saveFeeDetails(Activity activity) throws BasicExceptionHandler;

	/**
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity getPreviewPageDetails(Activity activity) throws BasicExceptionHandler;

	/**
	 * This method is to save BL attachments details to attachments table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity saveReUploadAttachments(Activity activity,String attachmentTypeId) throws BasicExceptionHandler;
	
	/**
	 * This method is for checking whether the particular business type file is uploaded or not
	 * 
	 * @throws IOException
	 * @return response object, renewalCode
	 * @Parameters
	 */
	void checkFileUploadedOrNot(HttpServletResponse response, int renewalCode) throws IOException;

	/**
	 * To get the attachment list after reset upload details in BL
	 * @param renewalCode
	 * @param tempId
	 * @return
	 */
	List<Attachment> getAttachmentList(int renewalCode, String tempId);
		
}