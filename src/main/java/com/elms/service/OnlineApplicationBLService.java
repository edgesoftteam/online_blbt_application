package com.elms.service;

import java.util.List;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.MultiAddress;
/**
 * @author Siva Kumari
 *
 */

public interface OnlineApplicationBLService {

	boolean isClassCodeExists(String clCode) throws Exception;

	List getBlActivityTypes(int moduleNameBusinessLicense) throws Exception;	
	
	int saveBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity) throws Exception;

	BusinessLicenseActivity getBusinessLicenseActivity(int activityId, int lsoId) throws Exception;	

	void updateMultiAddress(MultiAddress[] multiAddessList, int activityId);

	BusinessLicenseActivity getBusinessLicenseActivity(BusinessLicenseActivityForm businessLicenseActivityForm) throws Exception;
	/**
	 * This method is to save BL Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity saveBLFeeDetails(Activity activity) throws BasicExceptionHandler;
	/**
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	Activity getBLPreviewPageDetails(Activity activity) throws BasicExceptionHandler;

}
