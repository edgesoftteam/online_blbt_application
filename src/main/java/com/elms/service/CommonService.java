package com.elms.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.ActivityStatus;
import com.elms.model.ActivitySubType;
import com.elms.model.ActivityType;
import com.elms.model.ApplicationType;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.model.ExemptionType;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.OwnershipType;
import com.elms.model.ProcessTeamForm;
import com.elms.model.ProcessTeamRecord;
import com.elms.model.QuantityType;
import com.elms.model.Street;
import com.elms.model.User;

public interface CommonService {
	
	/**
	 * This method is to check entered values Business Account Number and 
	 * Renewal Code (activity Id) are valid combination. 
	 * If the combination exists in BL_ACTIVITY / BT_ACTIVITY tables 
	 * which will return the type of renewal user trying to renew.
	 * 
	 * @throws BasicExceptionHandler
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public String checkRenewalType(String renewalCode,String businessActNo)  throws BasicExceptionHandler;

	/**
	 * This method is to check if the Fee for BL/BT is payed or the Renewal application 
	 * is submitted based on the renewal code or activity Id.
	 * @param String
	 * @throws BasicExceptionHandler
	 * @return String 
	 */
	public String getBlOrBtActivityRenewalOnlineFlag(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to fetch Fee list for BL/BT after payment of the Renewal application 
	 * based on the renewal code or activity Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	public List<Fee> getFeesFromTempActFees(String actId,String tempId) throws BasicExceptionHandler;
	
	/**
	 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment of the Renewal application 
	 * based on the fee Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	public String getFeesAccountForFeeId(String feeId) throws BasicExceptionHandler;

	/**
	 * This method is to check if the payment is fully done or partially done for BL/BT Renewal application 
	 * is submitted based on the renewal code or activity Id. If there is a balance due which is greater than zero
	 * system will allow the user to login else it will show a message that fee is completely paid.
	 * @param String
	 * @throws BasicExceptionHandler
	 * @return String 
	 */
	public double getFullOrPartialPaymentFlag(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to download attachments in the same page as a part of BL renewal
	 * @throws BasicExceptionHandler
	 * @param req
	 * @param resp
	 */
	public void downloadAttachment(HttpServletRequest req, HttpServletResponse resp) throws BasicExceptionHandler;;

	/**
	 * This method is to restrict the user login based on lkup_act_type online flag
	 * @throws BasicExceptionHandler
	 * @param activityForm
	 * @return
	 */
	public String getActTypeOnlineFlag(Activity activityForm) throws BasicExceptionHandler;
	
	/**
	 * This method is to update the TEMP_ACTIVITY_DETAILS Table for every next button click.
	 * @param activity
	 * @throws BasicExceptionHandler
	 * @return 
	 * 
	 */
	public void updateUserDetails(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to fetch the LKUP_SYSTEM Table in the form of single Map object 
	 * having key- value pairs from List of Map objects
	 * @param 
	 * @throws BasicExceptionHandler
	 * @return Map<String,String>
	 * 
	 */
	public Map<String, String> getLkupSystemDataMap() throws BasicExceptionHandler;
	
	/**
	 * This method is to remove duplicate entries of same combination of Business Acc no & Renewal Code from 
	 * TEMP_ACTIVITY_DETAILS Table. And also it insert a new record with all other details that user enters. 
	 * having key- value pairs from List of Map objects
	 * @param Activity Object
	 * @throws BasicExceptionHandler
	 * @return Activity Object 
	 */
	public Activity saveActivity(Activity activity) throws BasicExceptionHandler;

	/**
	 * For reset bl upload attachments
	 * @param tempId
	 */
	public void resetBlUploadAttachments(String tempId);

/**
	 * This method is to fetch the Email Templates based on email type
	 * @param emailDetails
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	public EmailTemplateAdminForm getEmailData(EmailDetails emailDetails) throws BasicExceptionHandler;

    public List getBTBLStreetArrayList();

	public List getBlActivityTypes(int moduleNameBusinessTax) throws Exception;

	public List getApplicationTypes() throws Exception;

	public List getBLActivityStatuses(int moduleNameBusinessTax) throws Exception;

	public List getOwnershipTypes() throws Exception;

	public List getBtQuantityTypes() throws Exception;

	public List getQuantityTypes() throws Exception;

	public QuantityType getBLQuantityTypesBasedOnActType(String actType) throws Exception;

	public List getBTQuantityTypesBasedOnActType(String actType) throws Exception;

	public List getExemptionTypes() throws Exception;
	
	List getActivitySubTypes(String activityType) throws Exception;

	Map getCodes(String activityType) throws Exception;

	public Street getStreet(String streetName) throws Exception;

	public int checkAddress(String addressStreetNumber, String addressStreetFraction, int streetId,
			String addressUnitNumber, String addressZip, BusinessLicenseActivity businessLicenseActivity) throws Exception;

	public int getAddressIdForPsaId(String lsoId) throws Exception;

	public ActivityStatus getActivityStatus(int statusId);

	public ApplicationType getApplicationType(int appTypeId);

	public ActivityType getActivityType(String actType);

	public OwnershipType getOwnershipType(int ownershipType);

	public QuantityType getQuantityType(int quantityId);

	public ExemptionType getExemptionType(int exemptionType);

	public boolean copyRequiredCondition(String typeId, int activityId, User user);

	public ActivitySubType getActivitySubType(String activitySubType) throws Exception;

	public int checkAddress(String businessAddressStreetNumber, String businessAddressStreetFraction, int streetId,
			String businessAddressUnitNumber, String businessAddressZip, String businessAddressZip4,
			BusinessTaxActivity businessTaxActivity);

	public ProcessTeamForm populateTeam(ProcessTeamForm processTeamForm) throws Exception;

	public int getProjectNameId(String psaType, int activityId) throws Exception;

	public User getUser(String userUnassigned) throws Exception;

	public void saveProcessTeam(String psaId, String psaType, int userId, ProcessTeamRecord tmRec);

	public int getDepartmentId(int userId) throws Exception;

	public void insertIntoBTapproval(int activityId, int deptId, int userId) throws Exception;

	public String getApnForLsoId(String lsoId) throws Exception;

	public int getInspectorForApn(String apnNoStr) throws Exception;

	public String getInspectorUser(int inspetorId);

	public QuantityType getBTQuantityType(int qtyId);
	
	String getActivityAddressForId(int activityId) throws Exception;
	
	MultiAddress[] getMultiAddress(int i, String string) throws Exception;

	Map getCodesForClassCode(String clCode) throws Exception;
	
	/**
	 * This method is to get the APPLICATION_ONLINE flag based on renewal code and business account number
	 * @throws BasicExceptionHandler
	 * @param activityForm
	 * @return
	 */
	public String getBlBtOnlineApplicationFlag(Activity activityForm) throws BasicExceptionHandler;

	public int generateRandomDigits(int n) throws Exception;

	public void updateEmailVerified(String emailAddress) throws Exception ;

	public User saveUserIfnotExistBasedonEmailAddr(String emailAddr) throws Exception;		
}
