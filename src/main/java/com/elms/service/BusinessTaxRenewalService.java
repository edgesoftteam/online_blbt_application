/**
 * 
 */
package com.elms.service;

import java.util.List;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;

/**
 * @author Gayathri Turlapati
 *
 */
public interface BusinessTaxRenewalService {
	
	/** 
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity getBTActivityInfo(String businessAccNo, String renewalCode) throws BasicExceptionHandler;
	
	/**
	 * This method is to fetch the Temp  Activity Table data 
	 *  
	 * @return String tempId
	 * @Parameters Activity Object
	 */
	public Activity getTempActivityDetails(String tempId)  throws BasicExceptionHandler;
	
	/**
	 * This method is to save the Business Account Number and 
	 * Renewal Code (activity Id) in the temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	
	public Activity saveBTRenewalDetails(Activity activity) throws BasicExceptionHandler;

	/**
	 * This method is to update user details to temp table.
	 * @return 
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity saveUserDetails(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to save BT quantity details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */	
	public Activity saveQtyDetails(Activity activity) throws BasicExceptionHandler;
	
	/**
	 * This method is to save BT Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity saveFeeDetails(Activity activity) throws BasicExceptionHandler;
	
	/** 
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity getPreviewPageDetails(Activity activity) throws BasicExceptionHandler;
	
	/** 
	 * This method is to fetch Fee details from temp table and Fee table
	 * for fee calculations
	 * 
	 * @return String actType, String tempId
	 * @Parameters List<Fee>
	 */
	public List<Fee> getFees(String actType, String tempId, String screenName) throws BasicExceptionHandler;

	/** 
	 * This method is to fetch Bt_Qty_Flag details from temp table and Fee table
	 * for fee calculations
	 * 
	 * @return String actType, String tempId
	 * @Parameters List<Fee>
	 */
	public Activity getBtQtyFlag(String renewalCode) throws BasicExceptionHandler;

	/**
	 * This method is to call for BT Fee details 
	 * on click of back button 
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	public Activity btFeeDetailsOnBack(Activity activity) throws BasicExceptionHandler;

	
}
