package com.elms.serviceImp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Fee;
import com.elms.repository.BusinessTaxRenewalRepository;
import com.elms.service.BusinessTaxRenewalService;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;


@Service
public class BusinessTaxRenewalServiceImpl implements BusinessTaxRenewalService {

	private static final Logger logger = Logger.getLogger(BusinessTaxRenewalServiceImpl.class);

	@Autowired
	BusinessTaxRenewalRepository  businessTaxRenewalRepository;
	
	@Autowired
	CommonService commonService;
	
	/**
	 * This method is to save the Business Account Number and 
	 * Renewal Code (activity Id) in the temp table.
	 * 
	 * @return Activity Object
	 * @throws BasicExceptionHandler 
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveBTRenewalDetails(Activity btActivityForm) throws BasicExceptionHandler {
		logger.debug("inside save of saveBTRenewalDetails in serviceimpl :: "+btActivityForm.getTempId());
		Activity activityDetails = new Activity();
		
		logger.debug("btActivityForm :: "+btActivityForm.toString());		
		activityDetails = businessTaxRenewalRepository.getBTActivityInfo(btActivityForm.getBusinessAccNo(), btActivityForm.getRenewalCode());
		
		if(StringUtils.s2i(activityDetails.getRenewalCode()) > 0) {
			commonService.saveActivity(btActivityForm);			
		}		
		activityDetails = businessTaxRenewalRepository.getBTActivityInfo(btActivityForm.getBusinessAccNo(),btActivityForm.getRenewalCode());
		activityDetails.setTempId(activityDetails.getTempId());
		return activityDetails;		
	}
	
	/**
	 * This method is to update user details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveUserDetails(Activity activity) throws BasicExceptionHandler {
		Activity activityDetails = new Activity();
		commonService.updateUserDetails(activity);	
		activityDetails=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		if(activityDetails.getLinePattern().equalsIgnoreCase("1") && (activityDetails.getTempBtQty() == null || activityDetails.getTempBtQty() == "")) {
			activityDetails.setTempBtQty(activityDetails.getQtyOther());
		}
		Activity act=businessTaxRenewalRepository.getBtQtyFlag(activity.getRenewalCode());
		activityDetails.setBtQtyFlag(act.getBtQtyFlag());
		
		return activityDetails;
	}
	
	/**
	 * This method is to save BT Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveFeeDetails(Activity activity) {

		double totalFee=0.0;
		double finalTotalFee = 0.0;
		List<Fee> feeList=businessTaxRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		feeList=businessTaxRenewalRepository.calculateFees(feeList, activity.getScreenName());
		
		for(int i=0;i<feeList.size();i++) {
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
			totalFee = totalFee + feeList.get(i).getFeeTotal();
			logger.debug("feelist :: "+feeList.get(i).toString());
			logger.debug("feelist object Total Fee :: "+feeList.get(i).getTotalFee());
			logger.debug("totalFee..."+totalFee);
		}
		
		feeList = businessTaxRenewalRepository.getBtLateFee(feeList, totalFee);
		
		for(int i=0;i<feeList.size();i++) {			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
				logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +feeList.get(i).getTotalFee());
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeDesc("");
				
				totalFee = totalFee - feeList.get(i).getFeeTotal();
				logger.debug("totalFee after removing Service Fee :: " +totalFee);
			}
		}		
		
		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}
		
		logger.debug("totalFee after service fee..."+finalTotalFee);

		activity=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		activity.setFeeList(feeList);
		activity.setTotalFee(StringUtils.roundOffDouble(finalTotalFee));
		logger.debug("inside feeDetails "+activity.toString());
		return activity;
	}

	/**
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity getPreviewPageDetails(Activity activity) {
		double totalFee=0.0;
		double finalTotalFee=0.0;

		Activity activityDetails=businessTaxRenewalRepository.getTempActivityDetails(activity.getTempId());
        logger.debug("activity.getTotalFee() : " +activityDetails.toString());
       
		activityDetails=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=businessTaxRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		feeList=businessTaxRenewalRepository.calculateFees(feeList, activity.getScreenName());
		
		for(int i=0;i<feeList.size();i++) {
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
			totalFee = totalFee+feeList.get(i).getFeeTotal();
			logger.debug("feelist :: "+feeList.get(i).toString());
		}
		
		feeList = businessTaxRenewalRepository.getBtLateFee(feeList, totalFee);
		
		for(int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
				totalFee = totalFee+feeList.get(i).getFeeTotal();
				logger.debug("feelist :: "+feeList.get(i).toString());
			}
		}
		feeList = businessTaxRenewalRepository.getBtServiceFee(feeList, totalFee, activity.getScreenName());

		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}
		
		//to remove some elements in the fee list which has feetotalstr is '0.0'
		Iterator<Fee> itr = feeList.iterator();

		while (itr.hasNext()) {
		    Fee fee = itr.next();		  
		    if(fee.getFeeTotalStr() == null || fee.getFeeTotalStr() == "" || fee.getFeeTotalStr().equalsIgnoreCase("0.0") || fee.getFeeTotalStr().equalsIgnoreCase("0.00")) {
		       itr.remove();
		    }
		}
		for (Fee fee : feeList) {
			//fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
		//	finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist ^^^^^^^^^^^&&&&&&&&&&&&&&:: "+fee.getFeeDesc());	
		}
		
		activityDetails.setFeeList(feeList);
		activityDetails.setTotalFee(StringUtils.roundOffDouble(finalTotalFee));
		logger.debug("Total Fee After roundoff :: " +activityDetails.getTotalFee());
		
		businessTaxRenewalRepository.saveBtFeeDetails(activityDetails);		
		return activityDetails;
	}

	
	/**
	 * This method is to call for BT Fee details 
	 * on click of back button 
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity btFeeDetailsOnBack(Activity activity) {

		double totalFee = 0.0;
		double finalTotalFee = 0.0;
		List<Fee> feeList=businessTaxRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		feeList=businessTaxRenewalRepository.calculateFees(feeList, activity.getScreenName());
		
		for(int i=0;i<feeList.size();i++) {
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
			totalFee = totalFee + feeList.get(i).getFeeTotal();
			logger.debug("feelist :: "+feeList.get(i).toString());
			logger.debug("feelist object Total Fee :: "+feeList.get(i).getTotalFee());
			logger.debug("totalFee..."+totalFee);
		}
		
		feeList = businessTaxRenewalRepository.getBtLateFee(feeList, totalFee);
		
		for(int i=0;i<feeList.size();i++) {			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
				logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +feeList.get(i).getTotalFee());
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeDesc("");
				
				totalFee = totalFee - feeList.get(i).getFeeTotal();
				logger.debug("totalFee after removing Service Fee :: " +totalFee);
			}
		}
		
		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}		
		activity=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		activity.setFeeList(feeList);
		activity.setTotalFee(StringUtils.roundOffDouble(finalTotalFee));

		logger.debug("inside feeDetails "+activity.toString());
		return activity;
	}
	
	@Override
	public Activity getBTActivityInfo(String businessAccNo, String renewalCode) {
		logger.debug("inside getActivityInfo in Service Implementation :: "+businessAccNo + " :: renewalCode :: " +renewalCode);
		Activity activityDetails = new Activity();
		
		activityDetails = businessTaxRenewalRepository.getBTActivityInfo(businessAccNo,renewalCode);
		return activityDetails;
	}

	@Override
	public Activity getTempActivityDetails(String tempId) throws BasicExceptionHandler {
		logger.debug("inside getTempActivityDetails - tempId :: "+tempId);
		Activity activityDetails = new Activity();
		
		activityDetails = businessTaxRenewalRepository.getTempActivityDetails(tempId);
		return activityDetails;
	}

	@Override
	public List<Fee> getFees(String actType, String tempId, String screenName) throws BasicExceptionHandler {
		logger.debug("inside getTempActivityDetails - tempId :: "+tempId);
		List<Fee> feeList = new ArrayList<Fee>();
		
		feeList = businessTaxRenewalRepository.getFees(actType, tempId);
		feeList=businessTaxRenewalRepository.calculateFees(feeList, screenName);
		return feeList;
	}

	@Override
	public Activity saveQtyDetails(Activity activity) throws BasicExceptionHandler {
		logger.debug("inside saveQtyDetails :: "+activity.toString());
		Activity activityDetails = new Activity();
		businessTaxRenewalRepository.updateBtQtyValue(activity);
		activityDetails=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());

		logger.debug("activityDetails :: " +activityDetails.toString());
				
		return activityDetails;
	}

	@Override
	public Activity getBtQtyFlag(String renewalCode) throws BasicExceptionHandler {
		logger.debug("inside getBtQtyFlag - renewalCode :: "+renewalCode);
		Activity activityDetails = new Activity();
		
		activityDetails = businessTaxRenewalRepository.getBtQtyFlag(renewalCode);
		return activityDetails;
	}
	
}
