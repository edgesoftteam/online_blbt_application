package com.elms.serviceImp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.BusinessTaxActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.repository.BusinessTaxRenewalRepository;
import com.elms.repository.OnlineApplicationBTRepository;
import com.elms.service.OnlineApplicationBTService;
import com.elms.util.StringUtils;
/**
 * @author Siva Kumari
 *
 */


@Service
public class OnlineApplicationBTServiceImpl implements OnlineApplicationBTService {

	private static final Logger logger = Logger.getLogger(OnlineApplicationBTServiceImpl.class);

	@Autowired
	OnlineApplicationBTRepository onlineApplicationBTRepository;
	@Autowired
	BusinessTaxRenewalRepository businessTaxRenewalRepository;
	
	@Override
	public Map getCodesForClassCode(String clCode) throws Exception {		
			return onlineApplicationBTRepository.getCodesForClassCode(clCode);		
	}

	@Override
	public boolean isClassCodeExists(String clCode) throws Exception {
		return onlineApplicationBTRepository.isClassCodeExists(clCode);	
	}
	@Override
	public int saveBusinessTaxActivity(BusinessTaxActivity businessTaxActivity, int lsoId) throws Exception {		
		return onlineApplicationBTRepository.saveBusinessTaxActivity(businessTaxActivity,lsoId);
	}

	@Override
	public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId) {
		onlineApplicationBTRepository.updateMultiAddress(multiAddessList,activityId);
	}

	@Override
	public BusinessTaxActivity getBusinessTaxActivity(int activityId, int lsoId) throws Exception {
		return onlineApplicationBTRepository.getBusinessTaxActivity(activityId,lsoId);
	}

	@Override
	public BusinessTaxActivity getBusinessTaxActivity(BusinessTaxActivityForm businessTaxActivityForm) throws Exception {		
		return onlineApplicationBTRepository.getBusinessTaxActivity(businessTaxActivityForm);
	}

	/**
	 * This method is to save BT Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveBTFeeDetails(Activity activity) {

		double totalFee=0.0;
		double finalTotalFee = 0.0;
		List<Fee> feeList=onlineApplicationBTRepository.getFees(activity.getActType(),activity.getTempId());
		feeList=onlineApplicationBTRepository.calculateFees(feeList, activity.getScreenName());
		
		
		for(int i=0;i<feeList.size();i++) {			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BT_ADMINISTRATION_FEE_DESC)) {
				logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +feeList.get(i).getTotalFee());
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeDesc("");		
			}
		}		
		
		for(int i=0;i<feeList.size();i++) {
			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
			totalFee = totalFee + feeList.get(i).getFeeTotal();
			logger.debug("feelist :: "+feeList.get(i).toString());
			logger.debug("feelist object Total Fee :: "+feeList.get(i).getTotalFee());
			logger.debug("totalFee..."+totalFee);
		}
		
		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}
		
		logger.debug("totalFee after service fee..."+finalTotalFee);
		logger.debug("inside feeDetails "+activity.toString());
		activity=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		activity.setFeeList(feeList);
		activity.setTotalFee(StringUtils.roundOffDouble(finalTotalFee));
		logger.debug("inside feeDetails "+activity.toString());
		return activity;
	}


	/**
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity getPreviewPageDetails(Activity activity) {
		double totalFee=0.0;
		double finalTotalFee=0.0;

//		Activity activityDetails=businessTaxRenewalRepository.getTempActivityDetails(activity.getTempId());
//        logger.debug("activity.getTotalFee() : " +activityDetails.toString());
       
		Activity activityDetails=businessTaxRenewalRepository.getBTActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=onlineApplicationBTRepository.getFees(activity.getActType(),activity.getTempId());
		feeList=onlineApplicationBTRepository.calculateFees(feeList, activity.getScreenName());
		
//		for(int i=0;i<feeList.size();i++) {
//			feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal())));
//			totalFee = totalFee+feeList.get(i).getFeeTotal();
//			logger.debug("feelist :: "+feeList.get(i).toString());
//		}
		feeList = businessTaxRenewalRepository.getBtServiceFee(feeList, totalFee, activity.getScreenName());

		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			finalTotalFee =  finalTotalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}
		
		//to remove some elements in the fee list which has feetotalstr is '0.0'
		Iterator<Fee> itr = feeList.iterator();

		while (itr.hasNext()) {
		    Fee fee = itr.next();		  
		    if(fee.getFeeTotalStr() == null || fee.getFeeTotalStr() == "" || fee.getFeeTotalStr().equalsIgnoreCase("0.0") || fee.getFeeTotalStr().equalsIgnoreCase("0.00")) {
		       itr.remove();
		    }
		}
//		for (Fee fee : feeList) {
			//fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
		//	finalTotalFee =  finalTotalFee + fee.getFeeTotal();
//			logger.debug("feelist ^^^^^^^^^^^&&&&&&&&&&&&&&:: "+fee.getFeeDesc());	
//		}
		
		activityDetails.setFeeList(feeList);
		activityDetails.setTotalFee(StringUtils.roundOffDouble(finalTotalFee));
		logger.debug("Total Fee After roundoff :: " +activityDetails.getTotalFee());
		
		businessTaxRenewalRepository.saveBtFeeDetails(activityDetails);		
		return activityDetails;
	}


	@Override
	public List<Fee> getBTFees(String actType, String tempId, String screenName) throws BasicExceptionHandler {
		logger.debug("inside getTempActivityDetails - tempId :: "+tempId);
		List<Fee> feeList = new ArrayList<Fee>();
		
		feeList = onlineApplicationBTRepository.getFees(actType, tempId);
		feeList=onlineApplicationBTRepository.calculateFees(feeList, screenName);
		return feeList;
	}

}