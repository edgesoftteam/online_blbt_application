package com.elms.serviceImp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Attachment;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.repository.CommonRepository;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.CommonService;
import com.elms.util.StringUtils;


@Service
public class BusinessLicenseRenewalServiceImpl implements BusinessLicenseRenewalService {

	private static final Logger logger = Logger.getLogger(BusinessLicenseRenewalServiceImpl.class);

	@Autowired
	BusinessLicenseRenewalRepository  businessLicenseRenewalRepository;
	
	@Autowired
	CommonService commonService;	
	
	/*
	 * This method is to save the Business Account Number and 
	 * Renewal Code (activity Id) in the temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveBLRenewalDetails(Activity blActivityForm ) throws BasicExceptionHandler {
		logger.debug("inside save of activityDetails in service"+blActivityForm.getTempId());
		Activity activityDetails = new Activity();
		
		logger.debug("blActivityForm...   "+blActivityForm.toString());
		
		activityDetails = businessLicenseRenewalRepository.getBLActivityInfo(blActivityForm.getBusinessAccNo(),blActivityForm.getRenewalCode());
		logger.debug("activityDetails...   "+activityDetails.toString());
		if(StringUtils.s2i(activityDetails.getRenewalCode()) > 0) {
			commonService.saveActivity(blActivityForm);			
		}
		
		activityDetails = businessLicenseRenewalRepository.getBLActivityInfo(blActivityForm.getBusinessAccNo(),blActivityForm.getRenewalCode());
		activityDetails.setTempId(activityDetails.getTempId());
		return activityDetails;
	
	}


	/*
	 * This method is to update user details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveUserDetails(Activity activity) throws BasicExceptionHandler {

		Activity activityDetails = new Activity();
		commonService.updateUserDetails(activity);	
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		
		if(activityDetails.getLinePattern().equalsIgnoreCase("1") && (activityDetails.getNoOfEmp() == null || activityDetails.getNoOfEmp() == "")) {
			activityDetails.setNoOfEmp(activityDetails.getQtyOther());
		}
        
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
    	
		Activity act=businessLicenseRenewalRepository.getQtyFlag(activity.getRenewalCode());
		activityDetails.setBlQtyFlag(act.getBlQtyFlag());
		
		activityDetails.setAttachmentList(attachmentList);
        logger.debug("attachmentList List size:: " +attachmentList.size());
        
		return activityDetails;
	}


	/*
	 * This method is to save BL quantity details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveQtyDetails(Activity activity) {

		Activity activityDetails = new Activity();
		activityDetails=businessLicenseRenewalRepository.updateEmp(activity);

		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
		activityDetails.setAttachmentList(attachmentList);
		logger.debug("attachmentList List size:: " +attachmentList.size());
				
		return activityDetails;
	}


	/*
	 * This method is to save BL attachments details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveAttachmentDetails(Activity activity,String attachmentTypeId) {
		logger.debug("business acc no = "+activity.getBusinessAccNo());
		logger.debug("renewal code = "+activity.getRenewalCode());
		Activity activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		logger.debug("inside attachUploads 286...");

        // obtains the upload file part in this multipart request
        List<MultipartFile> filesList=null;
        long size = 0;
		try {
			filesList=activity.getTheFile();
			logger.debug("size.... "+filesList.size());
			List<String> fileNames = new ArrayList<String>();
			if(null != filesList && filesList.size() > 0) {
				for (MultipartFile multipartFile : filesList) {
					if(multipartFile != null && multipartFile.getOriginalFilename() !=null) {
						String fileName = multipartFile.getOriginalFilename();
						size=multipartFile.getSize();
						fileNames.add(fileName);	
					}
				}
			}

			logger.debug("size... "+size);
			if(size/1024/1024 > 50) {
				activityDetails.setDisplayErrorMsg("You have exceeded the maximum allowed size for an attachment");
			}else {
				activityDetails.setDisplaySuccessMsg("You have successfully uploaded the file");
				
			}
	        if (filesList != null && size/1024/1024 <=50) {
		        businessLicenseRenewalRepository.saveStreamToFile(activity,attachmentTypeId);
	        }
	        List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
			activity.setAttachmentList(attachmentList);
			logger.debug("attachmentList List size:: " +attachmentList.size());

			activityDetails.setAttachmentList(attachmentList);
		} catch(MultipartException e1) {
			e1.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}

		return activityDetails;
	}


	/*
	 * This method is to save BL fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveFeeDetails(Activity activity) throws BasicExceptionHandler {

		List<Fee> feeList=businessLicenseRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		activity.setFeeList(feeList);
		logger.debug("activity... "+activity.toString());
		Activity activityDetails=new Activity();

		logger.debug("inside feeDetails "+activity.toString());
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		Map<String, String> lkupSystemData =   commonService.getLkupSystemDataMap();
        String lateFeeFromDate = "";
        String lateFeeToDate = "";
        logger.debug("lkupSystemData.size() :: " +lkupSystemData.size());
       
            if(lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
                lateFeeFromDate = lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE);
            }                         
            if(lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
                lateFeeToDate = lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE);
            }
		
		double totalFee = 0;
		double lateFeeFactor = 0;
		double licenseFeeAnnualTotalFactor = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");  
		 String strDate = formatter.format(new Date());  
         
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					lateFeeFactor = feeList.get(i).getFeeFactor();
					logger.debug("lateFeeFactor ::  " +lateFeeFactor);				
				}
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
		}
		
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					totalFee = totalFee + (lateFeeFactor * licenseFeeAnnualTotalFactor);// When it is Late Fee, get Factor instead of feeFactor	 
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor)));
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor));
					logger.debug("totalFee... " +totalFee);					
				}else {
					feeList.get(i).setFeeTotal(StringUtils.s2d(""));
					feeList.get(i).setFeeTotalStr("");
					feeList.get(i).setFeeDesc("");
				}
			}
			logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeDesc("");
			}
			if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC) && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				totalFee = totalFee + feeList.get(i).getFeeTotal();// When it is other than Late Fee, get FeeFactor instead of Factor					
				logger.debug("Line 324  Fee details.... " +feeList.get(i).getFeeTotal() + " :: Total Fee :: " +totalFee);
			}
		}	
		activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activity.setTotalFee(StringUtils.roundOffDouble(totalFee));

		activityDetails.setFeeList(feeList);
		return activityDetails;
	}


	/*
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity getPreviewPageDetails(Activity activity) throws BasicExceptionHandler {

		Activity activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(activity.getTempId());
        logger.debug("activity.getTotalFee() : " +activityDetails.toString());
       
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=businessLicenseRenewalRepository.getFees(activity.getActType(),activity.getTempId());

		Map<String, String> lkupSystemData =   commonService.getLkupSystemDataMap();
        String lateFeeFromDate = "";
        String lateFeeToDate = "";
        logger.debug("lkupSystemData.size() :: " +lkupSystemData.size());
       
            if(lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
                lateFeeFromDate = lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE);
            }                         
            if(lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
                lateFeeToDate = lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE);
            }
		
		double totalFee = 0;
		double feeFactor = 0;
		
		double lateFeeFactor = 0;
		double administrationFactor = 0;
		double licenseFeeAnnualTotalFactor = 0;
		double disabilityFeeTotalFactor = 0;
		double lateFeeTotalFactor = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
		 String strDate = formatter.format(new Date());  
		     
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				administrationFactor = feeList.get(i).getFactor();
				logger.debug("administrationFactor ::  " +administrationFactor);	
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());
				if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {	
				   lateFeeFactor = feeList.get(i).getFeeFactor();	
				   logger.debug("lateFeeFactor ::  " +lateFeeFactor);				
				}
			}			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.CA_STATE_DISABILITY_FEE)) {
				 disabilityFeeTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
		}
		
		if(lateFeeFactor>0 && licenseFeeAnnualTotalFactor>0) {
			lateFeeTotalFactor = (lateFeeFactor * licenseFeeAnnualTotalFactor);
			logger.debug("lateFeeTotalFactor ::  " +lateFeeTotalFactor);
		}
		
		for (int i=0;i<feeList.size();i++) {						
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.CA_STATE_DISABILITY_FEE)) {
				totalFee = totalFee + disabilityFeeTotalFactor;// When it is other than Late Fee & Administration Fee, add the totals directly
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal()));//Assigning the Fee total to Fee total Str for showing it in the UI for maintaining the digits after the period (XX.XX pattern)
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				totalFee = totalFee + licenseFeeAnnualTotalFactor;// When it is other than Late Fee & Administration Fee, add the totals directly
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal()));//Assigning the Fee total to Fee total Str for showing it in the UI for maintaining the digits after the period (XX.XX pattern)
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					totalFee = totalFee + lateFeeTotalFactor;// When it is Late Fee, get Factor instead of feeFactor	
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(lateFeeTotalFactor)));
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeTotalFactor));
				}else {
					feeList.get(i).setFeeTotal(StringUtils.s2d(""));
					feeList.get(i).setFeeTotalStr("");
					feeList.get(i).setFeeDesc("");
				}
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				feeFactor = (licenseFeeAnnualTotalFactor + disabilityFeeTotalFactor + lateFeeTotalFactor)*administrationFactor;				
				totalFee = totalFee + feeFactor;
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeFactor))); // To show the calculated value as fee factor on preview screen
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeFactor));
			}
			logger.debug("FeeDesc :: " +feeList.get(i).getFeeDesc() + ":: FeeFactor :: " +feeList.get(i).getFeeFactor() + " :: Total Fee :: " +totalFee);
			activityDetails.setQtyOther(feeList.get(i).getNoOfQuantity());
		}				
		activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activityDetails.setFeeList(feeList);
		businessLicenseRenewalRepository.saveBLFeeDetails(activityDetails);
		return activityDetails;
	}

	/*
	 * This method is to save BL attachments details to attachments table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveReUploadAttachments(Activity activity,String attachmentTypeId) {
		
		logger.debug("inside attachUploads 286..."+activity.getRenewalCode());

    // obtains the upload file part in this multipart request
    List<MultipartFile> filesList=null;
    long size = 0;
	try {
		filesList=activity.getTheFile();
		logger.debug("size.... "+filesList.size());
		List<String> fileNames = new ArrayList<String>();
		if(null != filesList && filesList.size() > 0) {
			for (MultipartFile multipartFile : filesList) {
				if(multipartFile != null && multipartFile.getOriginalFilename() !=null) {
					String fileName = multipartFile.getOriginalFilename();
					size=multipartFile.getSize();
					fileNames.add(fileName);	
				}
			}
		}

		if(size/1024/1024 > 50) {
			activity.setDisplayErrorMsg("You have exceeded the maximum allowed size for an attachment");
		}else {
			activity.setDisplaySuccessMsg("You have successfully uploaded the file");
		}
        if (filesList != null && size/1024/1024 <=50) {
	        businessLicenseRenewalRepository.saveStreamToAttachment(activity,attachmentTypeId);
        }
        List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentListForReUpload(activity.getRenewalCode());
		activity.setAttachmentList(attachmentList);
		logger.debug("attachmentList List size:: " +attachmentList.size());

	} catch(MultipartException e1) {
		e1.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	}

		return activity;
	}


	/**
	 * This method is for checking whether the particular business type file is uploaded or not
	 * 
	 * @throws IOException
	 * @return response object, renewalCode
	 * @Parameters
	 */
	@Override
	public void checkFileUploadedOrNot(HttpServletResponse response, int renewalCode) throws IOException {
		JSONObject jsonObj = businessLicenseRenewalRepository.checkFileUploadedOrNot(renewalCode, "0");	
		response.setStatus(200);
		response.setContentType("application/json");
		response.getWriter().write(jsonObj.toString());
	}

	/**
	 * To get the attachment list after reset upload details in BL
	 * @param renewalCode
	 * @param tempId
	 * @return
	 */
	@Override
	public List<Attachment> getAttachmentList(int renewalCode, String tempId) {
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(renewalCode,tempId);
		return attachmentList;
	}
	
	
}
