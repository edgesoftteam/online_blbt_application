package com.elms.serviceImp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessLicenseActivityForm;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.repository.OnlineApplicationBLRepository;
import com.elms.service.CommonService;
import com.elms.service.OnlineApplicationBLService;
import com.elms.util.StringUtils;

/**
 * @author Siva Kumari
 *
 */

@Service
public class OnlineApplicationBLServiceImpl implements OnlineApplicationBLService {

	private static final Logger logger = Logger.getLogger(OnlineApplicationBLServiceImpl.class);

	@Autowired
	OnlineApplicationBLRepository onlineApplicationBLRepository;
	@Autowired
	BusinessLicenseActivity businessLicenseActivity;	
	@Autowired
	CommonService commonService;	
	@Autowired
	BusinessLicenseRenewalRepository businessLicenseRenewalRepository;

	@Override
	public boolean isClassCodeExists(String clCode) throws Exception {
		return onlineApplicationBLRepository.isClassCodeExists(clCode);	
	}

	@Override
	public int saveBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity) throws Exception {		
		return onlineApplicationBLRepository.saveBusinessLicenseActivity(businessLicenseActivity);
	}

	@Override
	public List getBlActivityTypes(int moduleId) throws Exception {	
		return  onlineApplicationBLRepository.getBlActivityTypes(moduleId);		
	}

	@Override
	public BusinessLicenseActivity getBusinessLicenseActivity(int activityId, int lsoId) throws Exception {		
		return onlineApplicationBLRepository.getBusinessLicenseActivity(activityId, lsoId);
	}

	@Override
	public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId) {
		 onlineApplicationBLRepository.updateMultiAddress(multiAddessList, activityId);		
	}
	
	
	/**
	 * Gets the BusinessLicenseActivity
	 * 
	 * @return Returns a BusinessLicenseActivity
	 */
	public BusinessLicenseActivity getBusinessLicenseActivity(BusinessLicenseActivityForm businessLicenseActivityForm) throws Exception {

		logger.debug("In getBusinessLicenseActivity " + businessLicenseActivityForm);
		//CommonService commonService = new CommonServiceImpl();
		//businessLicenseActivity = new BusinessLicenseActivity();
		businessLicenseActivity.setBusinessName(businessLicenseActivityForm.getBusinessName());
		businessLicenseActivity.setApplicationType(commonService.getApplicationType(StringUtils.s2i(businessLicenseActivityForm.getApplicationType())));
		businessLicenseActivity.setActivityStatus(commonService.getActivityStatus(StringUtils.s2i(businessLicenseActivityForm.getActivityStatus())));
		businessLicenseActivity.setOwnershipType(commonService.getOwnershipType(StringUtils.s2i(businessLicenseActivityForm.getOwnershipType())));
		businessLicenseActivity.setActivityType(commonService.getActivityType(businessLicenseActivityForm.getActivityType()));
		businessLicenseActivity.setActivitySubType(commonService.getActivitySubType(businessLicenseActivityForm.getActivitySubType()));
		businessLicenseActivity.setQuantity(commonService.getQuantityType(StringUtils.s2i(businessLicenseActivityForm.getQuantity())));
		businessLicenseActivity.setTypeOfExemptions(commonService.getExemptionType(StringUtils.s2i(businessLicenseActivityForm.getTypeOfExemptions())));
		logger.debug("businessLicenseActivityForm.getAddressStreetName() ... "+businessLicenseActivityForm.getAddressStreetName());
		if(businessLicenseActivityForm.getAddressStreetName() != null && businessLicenseActivityForm.getAddressStreetName() != "") {
		businessLicenseActivity.setStreet(commonService.getStreet(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressStreetName())));
		}
		businessLicenseActivity.setSubProjectId(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getSubProjectId())));
		businessLicenseActivity.setBusinessLocation(businessLicenseActivityForm.isBusinessLocation());
		businessLicenseActivity.setAddressStreetNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressStreetNumber()));
		businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressStreetFraction()));
		businessLicenseActivity.setAddressUnitNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressUnitNumber()));
		businessLicenseActivity.setAddressCity(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressCity()));
		businessLicenseActivity.setAddressState(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressState()));
		businessLicenseActivity.setAddressZip(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressZip()));
		businessLicenseActivity.setAddressZip4(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getAddressZip4()));
		businessLicenseActivity.setOutOfTownStreetNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetNumber()));
		businessLicenseActivity.setOutOfTownStreetName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetName()));
		businessLicenseActivity.setOutOfTownUnitNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownUnitNumber()));
		businessLicenseActivity.setOutOfTownCity(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownCity()));
		businessLicenseActivity.setOutOfTownState(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownState()));
		businessLicenseActivity.setOutOfTownZip(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownZip()));
		businessLicenseActivity.setOutOfTownZip4(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownZip4()));
		businessLicenseActivity.setActivityNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getActivityNumber()));
		businessLicenseActivity.setBusinessName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getBusinessName()));
		businessLicenseActivity.setBusinessAccountNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getBusinessAccountNumber()));
		businessLicenseActivity.setCorporateName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getCorporateName()));
		businessLicenseActivity.setBusinessPhone(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getBusinessPhone()));
		businessLicenseActivity.setBusinessExtension(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getBusinessExtension()));
		businessLicenseActivity.setBusinessFax(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getBusinessFax()));
		businessLicenseActivity.setMuncipalCode(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMuncipalCode()));
		businessLicenseActivity.setSicCode(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getSicCode()));
		businessLicenseActivity.setDescOfBusiness(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getDescOfBusiness()));
		businessLicenseActivity.setClassCode(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getClassCode()));
		businessLicenseActivity.setHomeOccupation(businessLicenseActivityForm.isHomeOccupation());
		businessLicenseActivity.setDecalCode(businessLicenseActivityForm.isDecalCode());
		businessLicenseActivity.setFederalIdNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getFederalIdNumber()));
		businessLicenseActivity.setEmailAddress(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getEmailAddress()));
		businessLicenseActivity.setSocialSecurityNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getSocialSecurityNumber()));
		businessLicenseActivity.setMailStreetNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailStreetNumber()));
		businessLicenseActivity.setMailStreetName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailStreetName()));
		businessLicenseActivity.setMailUnitNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailUnitNumber()));
		businessLicenseActivity.setMailCity(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailCity()));
		businessLicenseActivity.setMailState(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailState()));
		businessLicenseActivity.setMailZip(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailZip()));
		businessLicenseActivity.setMailZip4(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMailZip4()));
		businessLicenseActivity.setPrevStreetNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevStreetNumber()));
		businessLicenseActivity.setPrevStreetName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevStreetName()));
		businessLicenseActivity.setPrevUnitNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevUnitNumber()));
		businessLicenseActivity.setPrevCity(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevCity()));
		businessLicenseActivity.setPrevState(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevState()));
		businessLicenseActivity.setPrevZip(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevZip()));
		businessLicenseActivity.setPrevZip4(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getPrevZip4()));
		businessLicenseActivity.setSquareFootage(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getSquareFootage()));
		businessLicenseActivity.setQuantityNum(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getQuantityNum()));
		businessLicenseActivity.setDisplayContent(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getDisplayContent()));
		businessLicenseActivity.setCreationDate(StringUtils.str2cal(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getCreationDate())));
		businessLicenseActivity.setRenewalDate(StringUtils.str2cal(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getRenewalDate())));
		businessLicenseActivity.setInsuranceExpDate(StringUtils.str2cal(businessLicenseActivityForm.getInsuranceExpDate()));
		businessLicenseActivity.setBondExpDate(StringUtils.str2cal(businessLicenseActivityForm.getBondExpDate()));
		businessLicenseActivity.setDeptOfJusticeExpDate(StringUtils.str2cal(businessLicenseActivityForm.getDeptOfJusticeExpDate()));
		businessLicenseActivity.setApplicationDate(StringUtils.str2cal(businessLicenseActivityForm.getApplicationDate()));
		businessLicenseActivity.setIssueDate(StringUtils.str2cal(businessLicenseActivityForm.getIssueDate()));
		businessLicenseActivity.setStartingDate(StringUtils.str2cal(businessLicenseActivityForm.getStartingDate()));
		businessLicenseActivity.setOutOfBusinessDate(StringUtils.str2cal(businessLicenseActivityForm.getOutOfBusinessDate()));
		businessLicenseActivity.setFederalFirearmsLiscExpDate(StringUtils.str2cal(businessLicenseActivityForm.getFederalFirearmsLiscExpDate()));
		businessLicenseActivity.setMultiAddress(businessLicenseActivityForm.getMultiAddress());
		businessLicenseActivity.setLsoId(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getLsoId()));

		logger.debug("In getBusinessLicenseActivity " + businessLicenseActivity.toString());

		return businessLicenseActivity;
	}
	/**
	 * This method is to save BL Fee details to temp table.
	 * 
	 * @return Activity Object
	 * @throws BasicExceptionHandler 
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveBLFeeDetails(Activity activity) throws BasicExceptionHandler {

		logger.debug("Entered into saveBLFeeDetails... activity... "+activity.toString());
	    List<Fee> feeList=onlineApplicationBLRepository.getFees(activity.getActType(),activity.getTempId());
		
		double totalFee = 0;
		
		for (int i=0;i<feeList.size();i++) {
			logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeDesc("");
			}
			if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC) && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				totalFee = totalFee + feeList.get(i).getFeeTotal();// When it is other than Late Fee, get FeeFactor instead of Factor					
				logger.debug("Total fee in saveBLFeeDetails.... " +feeList.get(i).getFeeTotal() + " :: Total Fee :: " +totalFee);
			}
		}	
		activity.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activity.setFeeList(feeList);
		return activity;
}


	/*
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity getBLPreviewPageDetails(Activity activity) throws BasicExceptionHandler {

//		Activity activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(activity.getTempId());
//        logger.debug("activity.getTotalFee() : " +activityDetails.toString());
       
		Activity activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=onlineApplicationBLRepository.getFees(activity.getActType(),activity.getTempId());

		
		double totalFee = 0;
		double feeFactor = 0;
		double administrationFactor = 0;
		double licenseFeeAnnualTotalFactor = 0;
		double disabilityFeeTotalFactor = 0;

		     
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				administrationFactor = feeList.get(i).getFactor();
					logger.debug("administrationFactor ::  " +administrationFactor);	
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.CA_STATE_DISABILITY_FEE)) {
				 disabilityFeeTotalFactor = feeList.get(i).getFeeTotal();
					logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
		}

		for (Fee fee : feeList) {
			fee.setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(fee.getFeeTotal())));
			totalFee =  totalFee + fee.getFeeTotal();
			logger.debug("feelist :: "+fee.toString());	
		}

		for (int i=0;i<feeList.size();i++) {						
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				feeFactor = (licenseFeeAnnualTotalFactor + disabilityFeeTotalFactor)*administrationFactor;
				logger.debug("feeFactor..."+feeFactor);
				totalFee = totalFee + feeFactor;
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeFactor))); // To show the calculated value as fee factor on preview screen
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeFactor));
				logger.debug("totalFee... "+totalFee);
			}
			logger.debug("FeeDesc :: " +feeList.get(i).getFeeDesc() + ":: FeeFactor :: " +feeList.get(i).getFeeFactor() + " :: Total Fee :: " +totalFee);
			activityDetails.setQtyOther(feeList.get(i).getNoOfQuantity());
		}				
		activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activityDetails.setFeeList(feeList);
		businessLicenseRenewalRepository.saveBLFeeDetails(activityDetails);
		return activityDetails;
	}


}