package com.elms.serviceImp;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.ActivityStatus;
import com.elms.model.ActivitySubType;
import com.elms.model.ActivityType;
import com.elms.model.ApplicationType;
import com.elms.model.BusinessLicenseActivity;
import com.elms.model.BusinessTaxActivity;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.model.ExemptionType;
import com.elms.model.Fee;
import com.elms.model.MultiAddress;
import com.elms.model.OwnershipType;
import com.elms.model.ProcessTeamForm;
import com.elms.model.ProcessTeamRecord;
import com.elms.model.QuantityType;
import com.elms.model.Street;
import com.elms.model.User;
import com.elms.repository.CommonRepository;
import com.elms.service.CommonService;


@Service
public class CommonServiceImpl implements CommonService {

	private static final Logger logger = Logger.getLogger(CommonServiceImpl.class);

	@Autowired
	CommonRepository commonRepository;

	
	/**
	 * This method is to check entered values Business Account Number and 
	 * Renewal Code (activity Id) are valid combination. 
	 * If the combination exists in BL_ACTIVITY / BT_ACTIVITY tables 
	 * then it will return the type of renewal user trying to renew.
	 * 
	 * @return renewalCode String Object
	 * @Parameters String
	 */
	@Override
	public String checkRenewalType(String renewalCode,String businessActNo) throws BasicExceptionHandler{		
		String permitNumber = commonRepository.checkRenewalType(renewalCode,businessActNo);		
		logger.debug("return value permitNumber :: " +permitNumber);
		return permitNumber;
	}
	
	/**
	 * This method is to check if the Fee for BL/BT is payed or the Renewal application 
	 * is submitted based on the renewal code or activity Id.
	 * @param String
	 * @throws Exception
	 * @return String 
	 */
	@Override
	public String getBlOrBtActivityRenewalOnlineFlag(Activity activity) throws BasicExceptionHandler{
		String renewalOnline = commonRepository.getBlOrBtActivityRenewalOnlineFlag(activity);		
		logger.debug("return value renewalOnline :: " +renewalOnline);
		return renewalOnline;
	}	

	/**
	 * This method is to fetch Fee list for BL/BT after payment of the Renewal application 
	 * based on the renewal code or activity Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	@Override
	public List<Fee> getFeesFromTempActFees(String actId, String tempId) throws BasicExceptionHandler{
		List<Fee> feeList = new ArrayList<Fee>();
		feeList = commonRepository.getFeesFromTempActFees(actId, tempId);		
		logger.debug("return value feeList :: " +feeList.size());
		return feeList;
	}
	
	/**
	 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment of the Renewal application 
	 * based on the fee Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	@Override	
	public String getFeesAccountForFeeId(String feeId) throws BasicExceptionHandler{
		String feeAccount = "";
		feeAccount = commonRepository.getFeesAccountForFeeId(feeId);		
		logger.debug("return value feeAccount :: " +feeAccount);
		return feeAccount;
	}

	/**
	 * This method is to check if the payment is fully done or partially done for BL/BT Renewal application 
	 * is submitted based on the renewal code or activity Id.
	 * @param String
	 * @throws Exception
	 * @return String 
	 */

	@Override
	public double getFullOrPartialPaymentFlag(Activity activity) throws BasicExceptionHandler {
		double partialPayFlag = commonRepository.getFullOrPartialPaymentFlag(activity);		
		logger.debug("return value partialPayFlag :: " +partialPayFlag);
		return partialPayFlag;
	}

	/**
	 * This method is to download attachments in the same page as a part of BL renewal
	 * @param req
	 * @param resp
	 */
	@Override
	public void downloadAttachment(HttpServletRequest request, HttpServletResponse response) throws BasicExceptionHandler{
		
		String fileName=request.getParameter("fileName"); 
		String filePath =request.getParameter("filePath");
		
		response.setContentType("text/html");		
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+fileName + "\".pdf");		
		
		InputStream in = null;		
		
		try {
			  URL url = new URL(filePath);
			  in = url.openStream();			  
			  byte[] buffer = new byte[10240];

			  try (
			      OutputStream output = response.getOutputStream();
			  ) {
			      for (int length = 0; (length = in.read(buffer)) > 0;) {
			          output.write(buffer, 0, length);
			      }
			  }
			 
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(in != null) {
					in.close();
				}				
			}catch(Exception e) {
				
			}
		}	
		
	}

	/**
	 * This method is to restrict the user login based on lkup_act_type online flag
	 * @param activityForm
	 * @return
	 */
	@Override
	public String getActTypeOnlineFlag(Activity activityForm) throws BasicExceptionHandler{
		String actTypeOnlineFlag = commonRepository.getActTypeOnlineFlag(activityForm);		
		logger.debug("return value actTypeOnlineFlag :: " +activityForm);
		return actTypeOnlineFlag;
	}
	
	/**
	 * This method is to update the TEMP_ACTIVITY_DETAILS Table for every next button click.
	 * @param activity
	 * @throws BasicExceptionHandler
	 * @return 
	 * 
	 */
	@Override
	public void updateUserDetails(Activity activity) throws BasicExceptionHandler {
		commonRepository.updateUserDetails(activity);		
	}
	
	/**
	 * This method is to fetch the LKUP_SYSTEM Table in the form of single Map object 
	 * having key- value pairs from List of Map objects
	 * @param 
	 * @throws Exception
	 * @return Map<String,String>
	 * 
	 */
	public Map<String, String> getLkupSystemDataMap() throws BasicExceptionHandler{
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>();
		lkupSystemDataMap = commonRepository.getLkupSystemDataMap();
		return lkupSystemDataMap;        
	}
	
	
	/**
	 * This method is to remove duplicate entries of same combination of Business Acc no & Renewal Code from 
	 * TEMP_ACTIVITY_DETAILS Table. And also it insert a new record with all other details that user enters. 
	 * having key- value pairs from List of Map objects
	 * @param Activity Object
	 * @throws BasicExceptionHandler
	 * @return Activity Object 
	 */
	@Override
	public Activity saveActivity(Activity activity) throws BasicExceptionHandler {
		activity = commonRepository.saveActivity(activity);
		return activity;
	}

	/**
	 * To reset upload attachments
	 */
	@Override
	public void resetBlUploadAttachments(String tempId) {
		commonRepository.resetBlUploadAttachments(tempId);
		
	}

	/**
	 * This method is to fetch the Email Templates based on email type
	 * @param emailDetails
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	@Override
	public EmailTemplateAdminForm getEmailData(EmailDetails emailDetails) throws BasicExceptionHandler{
		EmailTemplateAdminForm emailTemplateAdminForm = commonRepository.getEmailData(emailDetails);
		return emailTemplateAdminForm;
	}

	@Override
	public List getBTBLStreetArrayList() {		
		return commonRepository.getBTBLStreetArrayList();
	}

	@Override
	public List getBlActivityTypes(int moduleId) throws Exception {		
		return commonRepository.getBlActivityTypes(moduleId);
	}

	@Override
	public List getApplicationTypes() throws Exception {		
		return commonRepository.getApplicationTypes();
	}

	@Override
	public List getBLActivityStatuses(int moduleId) throws Exception {		
		return commonRepository.getBLActivityStatuses(moduleId);
	}

	@Override
	public List getOwnershipTypes() throws Exception {
		return commonRepository.getOwnershipTypes();
	}

	@Override
	public List getBtQuantityTypes() throws Exception {
		return commonRepository.getBtQuantityTypes();
	}

	@Override
	public List getQuantityTypes() throws Exception {
		return commonRepository.getQuantityTypes();
	}

	@Override
	public QuantityType getBLQuantityTypesBasedOnActType(String actType) throws Exception {
		return commonRepository.getBLQuantityTypesBasedOnActType(actType);
	}

	@Override
	public List getBTQuantityTypesBasedOnActType(String actType) throws Exception {
		return commonRepository.getBTQuantityTypesBasedOnActType(actType);
	}

	@Override
	public List getExemptionTypes() throws Exception {
		return commonRepository.getExemptionTypes();
	}
	
	@Override
	public List getActivitySubTypes(String activityType) throws Exception {			
		return  commonRepository.getActivitySubTypes(activityType);		
	}

	@Override
	public Map getCodes(String activityType) throws Exception {
		return commonRepository.getCodes(activityType);		 
	}

	@Override
	public Street getStreet(String streetName) throws Exception {		
		return commonRepository.getStreet(streetName);
	}

	@Override
	public int checkAddress(String streetNumber, String streetFraction, int streetId,
			String unit, String zip, BusinessLicenseActivity businessLicenseActivity) throws Exception {		
		return commonRepository.checkAddress(streetNumber, streetFraction, streetId, unit, zip, businessLicenseActivity);
	}

	@Override
	public int getAddressIdForPsaId(String lsoId) throws Exception {
		return commonRepository.getAddressIdForPsaId(lsoId);
	}

	@Override
	public ActivityStatus getActivityStatus(int statusId) {
		ActivityStatus activityStatus = null;
		try {
			activityStatus = commonRepository.getActivityStatus(statusId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return activityStatus;
	}

	@Override
	public ApplicationType getApplicationType(int appTypeId) {
		ApplicationType applicationType = null;
		try {
			applicationType = commonRepository.getApplicationType(appTypeId);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return applicationType;
	}

	@Override
	public ActivityType getActivityType(String actType) {
		ActivityType activityType = null;
		try {
			activityType = commonRepository.getActivityType(actType);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return activityType;
	}

	@Override
	public OwnershipType getOwnershipType(int ownershipType) {
		OwnershipType ownShipType = null;
		try {
			ownShipType = commonRepository.getOwnershipType(ownershipType);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return ownShipType;
	}

	@Override
	public QuantityType getQuantityType(int quantityId) {
		QuantityType quantityType = null;
		try {
			quantityType = commonRepository.getQuantityType(quantityId);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return quantityType;
	}

	@Override
	public ExemptionType getExemptionType(int exemptionType) {
		ExemptionType exempType = null;
		try {
			exempType = commonRepository.getExemptionType(exemptionType);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return exempType;
	}

	@Override
	public boolean copyRequiredCondition(String typeId, int activityId, User user) {		
		return commonRepository.copyRequiredCondition(typeId, activityId, user);
	}

	@Override
	public ActivitySubType getActivitySubType(String activitySubType) throws Exception {
		return commonRepository.getActivitySubType(activitySubType);
	}

	@Override
	public int checkAddress(String businessAddressStreetNumber, String businessAddressStreetFraction, int streetId,
			String businessAddressUnitNumber, String businessAddressZip, String businessAddressZip4,
			BusinessTaxActivity businessTaxActivity) {
		return commonRepository.checkAddress(businessAddressStreetNumber,businessAddressStreetFraction,streetId,businessAddressUnitNumber,businessAddressZip,
				businessAddressZip4,businessTaxActivity); 
	}

	@Override
	public ProcessTeamForm populateTeam(ProcessTeamForm processTeamForm) throws Exception {		
		return commonRepository.populateTeam(processTeamForm);
	}

	@Override
	public int getProjectNameId(String psaType, int activityId) throws Exception {
		return commonRepository.getProjectNameId(psaType,activityId);
	}

	@Override
	public User getUser(String userUnassigned) throws Exception {
		return commonRepository.getUser(userUnassigned);
	}

	@Override
	public void saveProcessTeam(String psaId, String psaType, int userId, ProcessTeamRecord tmRec) {
		commonRepository.saveProcessTeam(psaId, psaType, userId, tmRec);		
	}

	@Override
	public int getDepartmentId(int userId) throws Exception {		
		return commonRepository.getDepartmentId(userId);
	}

	@Override
	public void insertIntoBTapproval(int activityId, int deptId, int userId) throws Exception {
		commonRepository.insertIntoBTapproval(activityId,deptId,userId);
		
	}

	@Override
	public String getApnForLsoId(String lsoId) throws Exception {		
		return commonRepository.getApnForLsoId(lsoId);
	}

	@Override
	public int getInspectorForApn(String apnNoStr) throws Exception {
		return commonRepository.getInspectorForApn(apnNoStr);
	}

	@Override
	public String getInspectorUser(int inspetorId) {
		return commonRepository.getInspectorUser(inspetorId);
	}

	@Override
	public QuantityType getBTQuantityType(int qtyId) {
		QuantityType quantityType= null;
		try {
			quantityType = commonRepository.getBTQuantityType(qtyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quantityType;
	}
	
	@Override
	public String getActivityAddressForId(int activityId) throws Exception {
		return commonRepository.getActivityAddressForId(activityId);
	}
	
	@Override
	public MultiAddress[] getMultiAddress(int id, String value) throws Exception {		
		return commonRepository.getMultiAddress(id, value);		
	}

	@Override
	public Map getCodesForClassCode(String clCode) throws Exception {		
			return commonRepository.getCodesForClassCode(clCode);		
	}

	@Override
	public String getBlBtOnlineApplicationFlag(Activity activityForm) throws BasicExceptionHandler {
		String onlineFlag = commonRepository.getBlBtOnlineApplicationFlag(activityForm);		
		logger.debug("onlineFlag :: " +onlineFlag);
		return onlineFlag;
	}

	@Override
	public int generateRandomDigits(int n) throws Exception {		
			return commonRepository.generateRandomDigits(n);		
	}

	@Override
	public void updateEmailVerified(String emailAddress) throws Exception {
		commonRepository.updateEmailVerified(emailAddress);
	}

	@Override
	public User saveUserIfnotExistBasedonEmailAddr(String emailAddr) throws Exception {
		return commonRepository.saveUserIfnotExistBasedonEmailAddr(emailAddr);
	}

}