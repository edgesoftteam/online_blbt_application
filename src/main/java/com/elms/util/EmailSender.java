package com.elms.util;

import java.net.URI;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.common.Constants;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.repository.CommonRepository;

@Service
public class EmailSender {
	
	static Logger logger = Logger.getLogger(EmailSender.class); 
	
	@Autowired
	CommonRepository commonRepository;
	@Autowired
	EmailTemplateAdminForm emailTemplateAdminForm;
		
	public void sendEmail(EmailDetails emailDetails){
		try {			
				Map<String, String> lkupSystemDataMap = emailDetails.getLkupSystemDataMap();  
				
				String serviceURL = lkupSystemDataMap.get(Constants.EMAIL_SERVER_URL);
				String userName = lkupSystemDataMap.get(Constants.EMAIL_AUTHENTICATION_USERNAME);
				String password = lkupSystemDataMap.get(Constants.EMAIL_AUTHENTICATION_PASSWORD); 
				String fromAddress = lkupSystemDataMap.get(Constants.EMAIL_FROM_ADDRESS);
				String emailForStaff = lkupSystemDataMap.get(Constants.EMAIL_FOR_STAFF);
				
				logger.error("serviceURL :: " +serviceURL+ " :: userName :: " +userName+ " :: password :: " +password+ " :: fromAddress :: " +fromAddress + " :: emailForStaff ::" +emailForStaff);
						
				microsoft.exchange.webservices.data.ExchangeService service = new microsoft.exchange.webservices.data.ExchangeService();
				microsoft.exchange.webservices.data.ExchangeCredentials credentials = new microsoft.exchange.webservices.data.WebCredentials(userName, password);
				service.setCredentials(credentials);
					
			    service.setUrl(new URI(serviceURL)); 
			    microsoft.exchange.webservices.data.EmailMessage msg;				
				msg = new microsoft.exchange.webservices.data.EmailMessage(service);
				
				emailTemplateAdminForm = emailDetails.getEmailTemplateAdminForm();
				
			    msg.setSubject(emailTemplateAdminForm.getEmailSubject()); 
			    msg.setBody(microsoft.exchange.webservices.data.MessageBody.getMessageBodyFromText(emailTemplateAdminForm.getEmailMessage()));
			    msg.getToRecipients().add(emailDetails.getEmailId());
			    msg.sendAndSaveCopy();				
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}