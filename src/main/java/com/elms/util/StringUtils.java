package com.elms.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import com.elms.common.Constants;

public class StringUtils {

    static Logger logger = Logger.getLogger(StringUtils.class.getName());
	// values from lkup_system table
	public static String KEY_VALUE = "UNDEFINED";


	 // values from lkup_system table
	public static String DATABASE_TYPE = "sqlserver";
	

    public StringUtils() {

        //nothing doing

//		try {
//			DATABASE_TYPE = LookupAgent.getKeyValue("DATABASE");
//		} catch (AgentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }
    
    public static int Str2Int(String s) {
    	if(s != null && s.length() > 0) {
    		return Integer.parseInt(s);
    	}
    	else {
    		return 0;
    	}
    }

    public static Calendar str2cal(String s) {

        Calendar calendar = null;
        if ((s != null) && (!s.equals(""))) {

            try {

                SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
                Date date = inputFormat.parse(s);
                calendar = getCalendar(date);
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return calendar;
    }

    public static Calendar dbDate2cal(String s) {

        Calendar calendar = null;
        if (s != null) {

            try {

                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = inputFormat.parse(s);
                calendar = getCalendar(date);
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return calendar;
    }

    public static String getUniqueFileName() {

        String strTimeStamp = null;
        Calendar cal = Calendar.getInstance();
        if (cal != null) {

            try {

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat formatter2 = new SimpleDateFormat("hhmmss");
                strTimeStamp = formatter1.format(cal.getTime());
                strTimeStamp = strTimeStamp + "_" + formatter2.format(cal.getTime());
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strTimeStamp;
    }

    public static String getUniqueFileNameforBatchPrint() {

        String strTimeStamp = null;
        Calendar cal = Calendar.getInstance();
        if (cal != null) {

            try {

            	java.util.Random r = new java.util.Random();
            	SimpleDateFormat formatter = new SimpleDateFormat(""+cal.getTimeInMillis());
            	
                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat formatter2 = new SimpleDateFormat("hhmmss");
                strTimeStamp = formatter1.format(cal.getTime());
               // strTimeStamp = strTimeStamp + "_" + formatter2.format(cal.getTime());
                strTimeStamp = strTimeStamp+"_"+formatter.format(cal.getTimeInMillis())+r.nextInt(); 
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strTimeStamp;
    }

    
    public static String cal2Ts(Calendar c) {

        String strTimeStamp = null;
        Calendar cal = Calendar.getInstance();
        if (c != null) {

            try {

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatter2 = new SimpleDateFormat("hh.mm.ss");
                strTimeStamp = formatter1.format(c.getTime());
                strTimeStamp = strTimeStamp + "-" + formatter2.format(cal.getTime());
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strTimeStamp;
    }

    public static String cal2datetime(Calendar c) {

        String strTimeStamp = null;
        Calendar cal = Calendar.getInstance();
        if (c != null) {

            try {

                SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("hh:mm:ss");
                strTimeStamp = formatter1.format(c.getTime());
                strTimeStamp = strTimeStamp + " " + formatter2.format(cal.getTime());
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strTimeStamp;
    }

    public static String datetime2dbTs(String s) {

        if (s != null) {

            try {

                s = s.substring(6, 10) + "-" + s.substring(0, 2) + "-" + s.substring(3, 5) + " " + s.substring(11, 19);
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return s;
    }

    public static String cal2str(Calendar c) {

        String strCalendar = "";
        if (c != null) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                strCalendar = formatter.format(c.getTime());
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strCalendar;
    }

    public static java.sql.Date cal2SQLDate(Calendar c) {

        java.sql.Date sqlDate = null;

        if (c != null) {

            try {
                sqlDate = new java.sql.Date(c.getTime().getTime());
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return sqlDate;
    }

    public static String date2str(Date d) {

        String strDate = null;
        if (d != null) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                strDate = formatter.format(d);
            }
            catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strDate;
    }

    public static String toSqlDate(String s) {

    	String val = null;
        if (s == null) {
            return s;
        }
        else if (s.equals("")) {
            return null;
        }
        else {
        	logger.debug("DATABASE_TYPE :: "+DATABASE_TYPE);
			if(DATABASE_TYPE.equalsIgnoreCase("oracle")) { 
				val= "to_date(" + checkString(s) + ",'MM/DD/YYYY')";
			}else if(DATABASE_TYPE.equalsIgnoreCase("sqlserver")){ 
				val= "CONVERT(char(10)," + checkString(s) + ",101)";
			}
        }
		return val;
    }

    public static int s2i(String s) {

        try {

            StringBuffer b = new StringBuffer();
            for (int j = 0; j < s.length(); j++) {

                char c = s.charAt(j);
                if ((c >= '0') && (c <= '9')) {

                    b.append(c);
                }

                if (c == '.') {

                    break;
                }
            }

            Integer i = new Integer(b.toString());

            return i.intValue();
        }
        catch (Exception _ex) {

            return -1;
        }
    }

    public static double s2d(String s) {
        try {

            StringBuffer b = new StringBuffer();
            for (int j = 0; j < s.length(); j++) {

                char c = s.charAt(j);
                if (((c >= '0') && (c <= '9')) || (c == '.') || (c == '-')) {

                    b.append(c);
                }
            }

            double d = (new Double(b.toString())).doubleValue();

            return d;
        }
        catch (Exception _ex) {

            return 0.0D;
        }
    }

    public static java.math.BigDecimal s2bd(String s) {

        try {

            StringBuffer b = new StringBuffer();
            for (int j = 0; j < s.length(); j++) {

                char c = s.charAt(j);
                if (((c >= '0') && (c <= '9')) || (c == '.') || (c == '-')) {

                    b.append(c);
                }
            }

            java.math.BigDecimal bd = (new java.math.BigDecimal(b.toString()));

            return bd;
        }
        catch (Exception _ex) {

            return new java.math.BigDecimal("0.00");
        }
    }

    public static String i2s(int val) {

        try {

            Integer i = new Integer(val);

            return i.toString();
        }
        catch (Exception _ex) {

            return "";
        }
    }
    
    //long to string converter
    public static String l2s(long val) {

        try {

            Long i = new Long(val);

            return i.toString();
        }
        catch (Exception _ex) {

            return "";
        }
    }

    //double to string converter
    public static String dbl2str(double val) {

        try {

            Double i = new Double(val);

            return i.toString();
        }
        catch (Exception _ex) {

            return "";
        }
    }

    public static String b2s(boolean b) {

        return (!b) ? "N" : "Y";
    }

    public static String yn(String b) {

        String b1 = b;
        if (b1 == null) {

            b1 = "";
        }

        if (b1.equalsIgnoreCase("Yes")) {

            b1 = "Y";
        }

        return (b1.equalsIgnoreCase("Y")) ? "Yes" : "No";
    }

    public static String OnOfftoYN(String b) {

        String b1 = b;
        if (b1 == null) {

            b1 = "N";
        }

        if (b1.equalsIgnoreCase("on")) {

            b1 = "Y";
        }
        else {

            b1 = "N";
        }

        return b1;
    }

    public static String YNtoOnOff(String b) {

        String b1 = b;
        if (b1 == null) {

            b1 = "";
        }

        if (b1.equalsIgnoreCase("Y")) {

            b1 = "on";
        }
        else {

            b1 = "off";
        }

        return b1; //(b1.equalsIgnoreCase("Y")) ? "on" : "off";
    }

    public static boolean s2b(String s) {

        boolean result = false;
        if (s != null) {

            if (s.equalsIgnoreCase("Y")) {

                result = true;
            }
        }

        return result;
    }

    //converts any string to proper case
    public static String properCase(String s) {

        StringBuffer b = new StringBuffer(s.toLowerCase());
        boolean icap = false;
        for (int i = 0; i < b.length(); i++) {

            char c = b.charAt(i);
            if (!icap && Character.isLetter(c)) {

                b.setCharAt(i, Character.toUpperCase(c));
                icap = true;
            }

            if (!Character.isLetter(c)) {

                icap = false;
            }
        }

        return b.toString();
    }

    public static String checkString(String aValue) {

        if (aValue == null) {

            return "null";
        }
        else {

            if (aValue.equals("")) {

                return "null";
            }

            // preparing string for database insert
            String tempStr = "";
            char quot = '\'';
            if ((aValue != null) && (aValue.length() > 0)) {

                for (int i = 0; i < aValue.length(); i++) {

                    char c = aValue.charAt(i);
                    if (c != quot) {

                        tempStr = tempStr + aValue.charAt(i);
                    }
                    else {

                        tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
                    }
                }

                aValue = tempStr;
            }

            //else {
            aValue = "'" + aValue + "'";

            return aValue;

            //}
        }
    }

    public static String checkString(List aValue) {

        if (aValue == null) {

            return "null";
        }
        else {

            if (aValue.size() <= 0) {

                return "null";
            }
            
            StringBuffer sb = new StringBuffer(); 
            for(int i=0 ; i< aValue.size();i++ ){
            	sb.append(checkString(aValue.get(i).toString()));
            	
            	if(i != aValue.size()-1)
            		sb.append(",");
            }

            return sb.toString();

        }
    }

    public static String handleSingleQuot(String aValue) {

        if (aValue == null) {

            return null;
        }
        else {

            if (aValue.equals("")) {

                return "";
            }

            // preparing string for database insert
            String tempStr = "";
            char quot = '\'';
            if ((aValue != null) && (aValue.length() > 0)) {

                for (int i = 0; i < aValue.length(); i++) {

                    char c = aValue.charAt(i);
                    if (c != quot) {

                        tempStr = tempStr + aValue.charAt(i);
                    }
                    else {

                        tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
                    }
                }

                aValue = tempStr;
            }

            return aValue;

        }
    }
    
    public static String decodesingleQuotes(String aValue){
    	if(aValue == null || aValue.equals("")){
    		return "";
    	}else {
  
            // decode string from database
            
            aValue = aValue.replaceAll("''", "\'");

            return aValue;

        }
    }

    public static String checkString(Calendar aValue) {

        return (aValue != null) ? ("'" + cal2str(aValue) + "'") : "null";
    }

    public static String checkInteger(String aValue) {

        return ((aValue == null) || aValue.equals("")) ? "null" : aValue;
    }

    public static String checkNumber(String aValue) {

        if ((aValue == null) || aValue.trim().equals("")) {

            return "null";
        }
        else {
            return aValue.trim();
        }
    }

    public static String nullReplaceWithEmpty(String aValue) {

        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {

            return "";
        }
        else {

            return aValue.trim();
        }
    }
    
    public static String zeroReplaceWithEmpty(String aValue) {

        if ((aValue.equals("0"))) {

            return "";
        }
        else {

            return aValue.trim();
        }
    }

    public static String nullReplaceWithBlank(String aValue) {

        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {

            return " ";
        }
        else {

            return aValue.trim();
        }
    }
    public static String nullReplaceWithN(String aValue) {

        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {

            return "N";
        }
        else {

            return aValue.trim();
        }
    }
    
    public static String nullReplaceWithZero(String aValue){
        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {
            return "0";
        }
        else {
            return aValue.trim();
        }
    }
    public static String zeroReplaceWithEmpty(int aValue) {

        if ((aValue == 0) || (aValue < 0)) {

            return "";
        }
        else {

            return i2s(aValue);
        }
    }
    
    public static String EmptyReplaceWithZero(String aValue) {

        if ((aValue == "") || (aValue == null) ) {

            return "00";
        }
        else {

            return aValue;
        }
    }

    public static String checkNumber(int aValue) {

        if (aValue < 1) {

            return "null";
        }
        else {

            return i2s(aValue);
        }
    }
    
    public static String checkNumber(long aValue) {

        if (aValue < 0) {

            return "null";
        }
        else {

            return l2s(aValue);
        }
    }
    
    public static String checkNumber(float aValue) {

        if (aValue < 1) {

            return "null";
        }
        else {

            return f2s(aValue);
        }
    }

    /**
     * Function to format a String to make it suitable for SQL query
     * Allows Letters,Digits and Whitespaces
     *
     * @return A String of the form string%
     */
    public static String formatQueryForString(String aString) {

        String query = "";
        if (aString.equals("") || (aString.length() == 0)) {

            aString = "%";
        }

        for (int i = 0; i < aString.length(); i++) {

            char quot = '\'';
            if (Character.isLetterOrDigit(aString.charAt(i)) || Character.isWhitespace(aString.charAt(i))) {
                query = query + aString.charAt(i);
            }
            else if ((aString.charAt(i) == quot)) {
                query = query + quot + quot;
            }
            else if ((aString.charAt(i) == '%') || (aString.charAt(i) == '*')) {
                query = query + '%';
            }
            else
                query = query + aString.charAt(i);
        }

        return query;
    }

    /**
     * Function to format a Number to make it suitable for SQL query
     * Allows Digits only
     *
     * @return A number of the form number%
     */
    public static String formatQueryForNumber(String aString) {

        String query = "";
        if (aString.equals("") || (aString.length() == 0)) {

            aString = "%";
        }

        for (int i = 0; i < aString.length(); i++) {

            if (Character.isDigit(aString.charAt(i))) {

                query = query + aString.charAt(i);
            }
            else {

                query = query + '%';

                break;
            }
        }

        return query;
    }

    public static String f2s(float floatValue) {

        String flt = "";
        try {

            DecimalFormat df = new DecimalFormat();
            df.setGroupingUsed(false);
            flt = df.format(floatValue);
        }
        catch (Exception e) {

            logger.error(e.getMessage());
        }

        return flt;
    }

    public static String f2$(float floatValue) {

        String flt = "$0.00";
        try {

            NumberFormat nf = NumberFormat.getCurrencyInstance();
            flt = nf.format(floatValue);
        }
        catch (Exception e) {

            logger.error(e.getMessage());
        }

        return flt;
    }

    public static String d2s(double doubleValue) {

        String flt = "";
        int i = 0;
        try {

            DecimalFormat df = new DecimalFormat();
            df.setGroupingUsed(false);
            df.applyPattern("##,###,##0.00");
            flt = df.format(doubleValue);
            if (flt.endsWith(".00")) {

                flt = flt.substring(0, flt.length() - 3);
            }
        }
        catch (Exception e) {

            logger.error(e.getMessage());
        }

        return flt;
    }

    public static String dbl2prct(double doubleValue) {

        String flt = "";
        try {

            DecimalFormat df = new DecimalFormat();
            df.setGroupingUsed(false);
            df.applyPattern("#,##0.00%");
            flt = df.format(doubleValue);
        }
        catch (Exception e) {

            logger.error(e.getMessage());
        }

        return flt;
    }

    public static String dbl2$(double doubleValue) {

        String flt = "$0.00";
        try {

            NumberFormat nf = NumberFormat.getCurrencyInstance();
            flt = nf.format(doubleValue);
        }
        catch (Exception e) {

            logger.error(e.getMessage());
        }

        return flt;
    }

    public static String str2$(String value) {

        if ((value == null) || value.equals("") || value.equals("0")) {

            return "$0.00";
        }
        else {

            try {

                int intValue = Integer.parseInt(value);
                NumberFormat nf = NumberFormat.getCurrencyInstance();
                String flt = nf.format(intValue);

                return flt;
            }
            catch (Exception e) {

                logger.error(e.getMessage());

                return "$0.00";
            }
        }
    }

    public static String i2nbr(int i) {

        try {

            NumberFormat nf = NumberFormat.getNumberInstance();

            return nf.format(i);
        }
        catch (Exception e) {

            logger.warn(e.getMessage());

            return "0";
        }
    }

    public static double $2dbl(String dollorvalue) {
logger.debug("dollorvalue ..."+dollorvalue);
        String tempStr = "";
        if (dollorvalue == null) {

            dollorvalue = "";
        }

        try {

            for (int i = 0; i < dollorvalue.length(); i++) {

                if ((dollorvalue.charAt(i) != ',') && (dollorvalue.charAt(i) != '$') && (dollorvalue.charAt(i) != ')')) {

                    if (dollorvalue.charAt(i) == '(') {

                        tempStr += "-";
                    }
                    else {

                        tempStr += dollorvalue.charAt(i);
                    }
                }
            }
            boolean temp = tempStr.startsWith("$");
            logger.debug("The string value is .." + temp);
            if(temp) {
            	tempStr = tempStr.replace("$", "");
                logger.debug("The string value is .." + tempStr);
            }
            logger.debug("The string value is .." + tempStr);
        }
        catch (Exception e) {

            logger.warn("Error on " + dollorvalue + ":" + tempStr + ":" + e.getMessage()+e);
            tempStr = "0";
        }

        if (tempStr.equals("")) {

            return 0.00;
        }
        else {
System.out.println(tempStr+" ********"+(double)StringUtils.s2d(tempStr));
            return (double) StringUtils.s2d(tempStr);
        }
    }

    
    public static String $2Str(String dollorvalue) {
logger.debug("dollorvalue ..."+dollorvalue);
        String tempStr = "";
        if (dollorvalue == null) {

            dollorvalue = "";
        }

        try {

            for (int i = 0; i < dollorvalue.length(); i++) {

                if ((dollorvalue.charAt(i) != ',') && (dollorvalue.charAt(i) != '$') && (dollorvalue.charAt(i) != ')')) {

                    if (dollorvalue.charAt(i) == '(') {

                        tempStr += "-";
                    }
                    else {

                        tempStr += dollorvalue.charAt(i);
                    }
                }
            }
            boolean temp = tempStr.startsWith("$");
            logger.debug("The string value is .." + temp);
            if(temp) {
            	tempStr = tempStr.replace("$", "");
                logger.debug("The string value is .." + tempStr);
            }
            logger.debug("The string value is .." + tempStr);
        }
        catch (Exception e) {

            logger.warn("Error on " + dollorvalue + ":" + tempStr + ":" + e.getMessage()+e);
            tempStr = "0";
        }

        if (tempStr.equals("")) {

            return "0.00";
        }
        else {
System.out.println(tempStr+" ********"+Integer.parseInt(tempStr));
            return tempStr;
        }
    }

    
    /**
     * Function to format a String to make it suitable for SQL query
     * Allows Letters,Digits and Whitespaces
     * Replace * with % in the String
     * @return A String
     */
    public static String replaceStarWithPcnt(String aString) {

        if (aString.equals("") || (aString.length() == 0)) {

            aString = "%";
        }

        return aString.replace('*', '%');
    }

    public static String n2b(String b) {

        return (b == null) ? "" : b;
    }

    public static String replaceQuot(String s) {

        String rtnStr = "";
        char quot = '\'';
        if ((s != null) && (s.length() > 0)) {

            for (int i = 0; i < s.length(); i++) {

                char c = s.charAt(i);
                if (c != quot) {

                    rtnStr = rtnStr + s.charAt(i);
                }
            }
        }

        return rtnStr;
    }

    public static String encodeQuot(String s) {

        String rtnStr = "";
        char quot = '\'';
        if ((s != null) && (s.length() > 0)) {

            for (int i = 0; i < s.length(); i++) {

                char c = s.charAt(i);
                if (c == quot) {

                    rtnStr = rtnStr + "\\" + s.charAt(i);
                }
                else {

                    rtnStr = rtnStr + s.charAt(i);
                }
            }
        }

        return rtnStr;
    }

    public static String replace(String source, String from, String to) {

        StringBuffer result = new StringBuffer();
        int i = source.indexOf(from);
        if (i >= 0) {

            result.append(source.substring(0, i));
            result.append(to);
            i += from.length();
        }
        else {

            i = 0;
        }

        result.append(source.substring(i));

        return new String(result);
    }

    /* Handling double Quotes replace it with empty spaece  */
    public static String replaceDoubleQuot(String s) {
        if (s == null) return "";
        char doubleQuot = '\"';
        String find = "\"";
        String replace = "\\";

        int index = -1;
        while ((index = s.indexOf(find)) > -1) {

            s = s.substring(0, index) + replace + s.substring(index + find.length(), s.length());
        }

        return s;
    }

    /* Handling double Quotes replace it with empty spaece  */
    public static String removeDoubleQuot(String s) {
        if (s == null) return "";
        char doubleQuot = '\"';
        String find = "\"";
        String replace = " ";

        int index = -1;
        while ((index = s.indexOf(find)) > -1) {

            s = s.substring(0, index) + replace + s.substring(index + find.length(), s.length());
        }

        return s;
    }

    public static String fixedLengthString(String str, int length) {

        String tempStr = "";
        if (str == null) {

            str = "";
        }
        else if ((str.length() > 0) || (str.length() < length)) {

            length = length - str.length();
        }

        for (int i = 0; i < length; i++) {

            tempStr = tempStr + "0";
        }

        tempStr = tempStr + str;

        return tempStr;
    }

    public static List arrayToList(Object[] objects) {

        List objList = new ArrayList();
        if (objects != null) {
            for (int i = 0; i < objects.length; i++) {
                objList.add(objects[i]);
            }
        }

        return objList;
    }

    public static String stringArrayToCommaSeperatedString(String[] stringArray) {
        logger.info("stringArrayToCommaSeperatedString(" + stringArray + ")");
        if (stringArray == null) {
            return "";
        }
        else {
            String commanSeperatedString = "";
            for (int i = 0; i < stringArray.length; i++) {
                commanSeperatedString += stringArray[i] + ",";
            }
            return commanSeperatedString.substring(0, commanSeperatedString.length() - 1);
        }
    }

    /* Handling phone format  */
    public static String phoneFormat(String phoneIn) {

        String phoneOut = "";
        if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {
            logger.debug("value of phoneIn is " + phoneIn);
            try {
                if (phoneIn.charAt(3) == '-') {
                    logger.debug("previous format is having hyphens");
                    phoneOut = phoneIn  ;
                    logger.debug("Phone out"+ phoneOut);
                   } else {
                    logger.debug("previous format is having no hyphens");
                    if ((phoneIn != null) && (phoneIn.length() > 0)) {
                        for (int i = 0; i < phoneIn.length(); i++) {
                            phoneOut = phoneOut + phoneIn.charAt(i);
                            if ((i == 2) || (i == 5)) {
                                phoneOut = phoneOut + "-";
                            }
                        }
                    }
                }
            }
            catch (RuntimeException e) {
                logger.debug("Error in phone formatting");
                phoneOut = phoneIn;
            }
        }

        if (phoneOut.equalsIgnoreCase("")) {
            phoneOut = null;
        }

        return phoneOut;
    }
    
    
    /* Handling phone format  */
    public static String formatPhoneNumber(String phoneIn) {
    	String tempPhoneIn = "";
        String phoneOut = "";
        
        if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {
//            logger.debug("value of phoneIn is " + phoneIn);
            try {
            	tempPhoneIn = stripPhoneNumber(phoneIn);
            		if(tempPhoneIn == null || tempPhoneIn.equalsIgnoreCase("") || tempPhoneIn.length()<10){
            			return phoneIn;
            		}
            		
                    if ((tempPhoneIn != null) && (tempPhoneIn.length() > 0)) {
                        for (int i = 0; i < tempPhoneIn.length(); i++) {
                            phoneOut = phoneOut + tempPhoneIn.charAt(i);
                            if ((i == 2) || (i == 5)) {
                                phoneOut = phoneOut + "-";
                            }
                        }
                    }
                
            }
            catch (RuntimeException e) {
            	//logger.error("error::" ,e);
                logger.error("Error in phone formatting");
                phoneOut = phoneIn;
            }
        }else{
        	return phoneIn;
        }

        if (phoneOut.equalsIgnoreCase("")) {
            phoneOut = null;
        }

        return phoneOut;
    }
    
    /* Handling phone format  */
    public static String stripPhoneNumber(String phoneIn) {
        String phoneOut = "";
        if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {
        	phoneOut = phoneIn.replaceAll("\\(", "");
        	phoneOut = phoneOut.replaceAll("\\)", "");
        	phoneOut = phoneOut.replaceAll("\\-", "");
        	phoneOut = phoneOut.replaceAll(" ", "");
        }
        return phoneOut;
    }
    
    

    /* Handling phone format  */
    public static String stripPhone(String phoneIn) {
        logger.debug("stripPhone(" + phoneIn + ")");
        String phoneOut = "";
        if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {

            logger.debug("value of phoneIn is " + phoneIn);
            if ((phoneIn != null) && (phoneIn.length() > 0)) {

                for (int i = 0; i < phoneIn.length(); i++) {

                    logger.debug("i value" + i);
                    logger.debug("charat i  " + phoneIn.charAt(i));
                    if (!((i == 3) || (i == 7))) {

                        phoneOut = phoneOut + phoneIn.charAt(i);
                    }
                }
            }



            //phoneOut = phoneIn;
        }

        if (phoneOut.equalsIgnoreCase("")) {

            phoneOut = null;
        }
        logger.debug("The string value after replace is .." + phoneOut);
        return phoneOut;
    }

    public static int divideCeiling(double dividend, double divisor) {
        logger.info("divideCeiling(" + dividend + ", " + divisor + ")");
        int value = new Double((dividend >= 0) ? (((dividend + divisor) - 1) / divisor) : (dividend / divisor)).intValue();
        logger.debug("value returned is " + value);
        return value;
    }

    public static int divideBase(double dividend, double divisor) {

        return new Double((dividend >= 0) ? (dividend / divisor) : ((dividend - divisor + 1) / divisor)).intValue();
    }

    public static String yymmdd2date(String yymmdd) {

        try {

            int yy = s2i(yymmdd.substring(0, 2));
            if (yy < 20) {

                yy = 2000 + yy;
            }
            else {

                yy = 1900 + yy;
            }

            return yymmdd.substring(2, 4) + "/" + yymmdd.substring(4, 6) + "/" + yy;
        }
        catch (Exception e) {

            return "";
        }
    }

    public static String mmddyyyy2yyyymmdd(String date) {

        return date.substring(6) + "/" + date.substring(0, 5);
    }

    public static String lpad(String s, int len) {

        return lpad(s, len, " ");
    }

    public static String lpad(String s, int len, String padder) {

        StringBuffer buf = new StringBuffer();
        int slen = 0;
        if (s != null) slen = s.length();
        if (len > slen) {
            //int diff = len - s.length();
            int diff = len - slen;
            for (int i = 0; i < diff; i++) {
                buf.append(padder);
            }
        }
        if (s != null) buf.append(s);

        return buf.toString();
    }

    public static String rpad(String s, int len) {

        return rpad(s, len, " ");
    }

    public static String rpad(String s, int len, String padder) {
        StringBuffer buf = new StringBuffer();
        String result = "";
        int slen = 0;
        if (s != null) {
            buf.append(s);
            slen = s.length();
        }
        //int diff = len - s.length();
        int diff = len - slen;
        for (int i = 0; i < diff; i++) {
            buf.append(padder);
        }
        if(diff < 0){
        	result = buf.substring(0,len);
        	return result;
        }else{
        	return buf.toString();	
        }
    }

    /**
     * This method changes date formate YYYY-MM-DD to DD/MM/YYYY
     * java.lang.String str.
     *
     * @param dateStr
     * @return String
     * @throws Exception if the entered date is not of the format MM-DD-YYYY
     */
    public static String changeDateFormate(String dateStr) throws Exception {

        if ((dateStr == null) || (dateStr.trim().length() <= 0)) {
            return null;
        }

        StringTokenizer st = new StringTokenizer(dateStr, "-");

        String year = st.nextToken();
        String day = st.nextToken();
        if (day.length() == 1) {

            day = "0" + day;
        }

        String month = st.nextToken();
        if (month.length() == 1) {

            month = "0" + month;
        }
        return day + "/" + month + "/" + year;
    }

    /**
     * This method changes date formate YYYY-MM-DD to DD/MM/YYYY
     * java.lang.String str.
     *
     * @param dateStr
     * @return String
     * @throws Exception if the entered date is not of the format MM-DD-YYYY
     */
    public static String changeDateToSimpleDate(String dateStr) throws Exception {

        logger.debug("isDateString()");
        if ((dateStr == null) || (dateStr.trim().length() <= 0)) {

            return null;
        }

        StringTokenizer st = new StringTokenizer(dateStr, "/");

        String day = st.nextToken();
        if (day.length() == 1) {

            day = "0" + day;
        }

        String month = st.nextToken();
        if (month.length() == 1) {

            month = "0" + month;
        }

        String year = st.nextToken();
        logger.debug("Date before Return = " + year + "-" + month + "-" + day);

        return day + "/" + month + "/" + year;
    }

    /**
     * <p>Replaces all occurances of a String within another String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *)        = null
     * Operator.replace("", *, *)          = ""
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", null, null) = "aba"
     * Operator.replace("aba", "a", null)  = "aba"
     * Operator.replace("aba", "a", "")    = "aba"
     * Operator.replace("aba", "a", "z")   = "zbz"
     * </pre>
     *
     * @see #replace(String text, String repl, String with, int max)
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replaceString(String text, String repl, String with) {
        return replaceString(text, repl, with, -1);
    }

    /**
     * <p>Replaces a String with another String inside a larger String,
     * for the first <code>max</code> values of the search String.</p>
     *
     * <p>A <code>null</code> reference passed to this method is a no-op.</p>
     *
     * <pre>
     * Operator.replace(null, *, *, *)         = null
     * Operator.replace("", *, *, *)           = ""
     * Operator.replace("abaa", null, null, 1) = "abaa"
     * Operator.replace("abaa", null, null, 1) = "abaa"
     * Operator.replace("abaa", "a", null, 1)  = "abaa"
     * Operator.replace("abaa", "a", "", 1)    = "abaa"
     * Operator.replace("abaa", "a", "z", 0)   = "abaa"
     * Operator.replace("abaa", "a", "z", 1)   = "zbaa"
     * Operator.replace("abaa", "a", "z", 2)   = "zbza"
     * Operator.replace("abaa", "a", "z", -1)  = "zbzz"
     * </pre>
     *
     * @param text  text to search and replace in, may be null
     * @param repl  the String to search for, may be null
     * @param with  the String to replace with, may be null
     * @param max  maximum number of values to replace, or <code>-1</code> if no maximum
     * @return the text with any replacements processed,
     *  <code>null</code> if null String input
     */
    public static String replaceString(String text, String repl, String with, int max) {
        if (text == null || repl == null || with == null || repl.length() == 0 || max == 0) {
            return text;
        }

        StringBuffer buf = new StringBuffer(text.length());
        int start = 0, end = 0;
        while ((end = text.indexOf(repl, start)) != -1) {
            buf.append(text.substring(start, end)).append(with);
            start = end + repl.length();
            if (--max == 0) {
                break;
            }
        }
        buf.append(text.substring(start));
        return buf.toString();
    }

    public static List stringToList(String s, String delim) {

        List l = new ArrayList();
        StringTokenizer st = new StringTokenizer(s, delim);
        while (st.hasMoreElements()) {
            l.add((String) st.nextElement());
        }
        return l;


    }
    
    public static String listToString(List list){
    	String result = "";
    	for(int i=0;i<list.size();i++){
    		result+=list.get(i)+" ";
    	}
    	return result;
    }
    
    /**
     * Utility function that truncates millisecond portion of a timestamp formatted string 
	 * @param timestamp
	 * @return
	 */
	public static String truncateMilliseconds(String timestamp) {
    	return timestamp.substring(0,timestamp.lastIndexOf("."));
    }
	
	public static java.sql.Date stringToSQLDate(String strDate){
		java.sql.Date date = null;
		if(strDate == null || strDate.trim().equalsIgnoreCase("")){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Date jdate = sdf.parse(strDate);
			date= new java.sql.Date(jdate.getTime());
		} catch (ParseException e) {
			logger.error("Not a valid date : strDate",e);
		} 
		return date;
	}
	
	public static java.sql.Date stringToSQLDateWithFormat(String strDate){
		java.sql.Date date = null;
		if(strDate == null || strDate.trim().equalsIgnoreCase("")){
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			Date jdate = sdf.parse(strDate);
			date= new java.sql.Date(jdate.getTime());
		} catch (ParseException e) {
			logger.error("Not a valid date : strDate",e);
		} 
		return date;
	}
	
	public static String sqlDateToString(java.sql.Date date){
		if(date == null){
		return null;	
		}
		Date jDate = new Date(date.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dateValue = sdf.format(jDate);
		return dateValue;
		
	}
	
	public static String[] split(String str, char separatorChar) {
		// Performance tuned for 2.0 (JDK1.4)

		if (str == null) {
			return null;
		}
		int len = str.length();
		if (len == 0) {
			return new String[0];
		}
		List list = new ArrayList();
		int i = 0, start = 0;
		boolean match = false;
		while (i < len) {
			if (str.charAt(i) == separatorChar) {
				if (match) {
					list.add(str.substring(start, i).trim());
					match = false;
				}
				start = ++i;
				continue;
			}
			match = true;
			i++;
		}
		if (match) {
			list.add(str.substring(start, i).trim());
		}
		return (String[]) list.toArray(new String[list.size()]);
	}

	 public static String date2yyyymmdd(String s) {
	    	String reformattedStr = null;

	        if (s != null) {
	            try {
	                SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
	                SimpleDateFormat myFormat = new SimpleDateFormat("yyyyMMdd");
	                reformattedStr = myFormat.format(fromUser.parse(s));
	               
	            } catch (Exception e) {
	                logger.error(e.getMessage());
	            }
	        }

	        return reformattedStr;
	    }

	 
	    public static Calendar getCalendar(java.util.Date aDate) throws Exception {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(aDate);
	        return cal;
	    }

	    public static Date getDate(java.util.Calendar aCalendar) throws Exception {
	        Date date = aCalendar.getTime();
	        return date;
	    }
	    
	    public static float roundOff(int roundOffType, float f){
			BigDecimal bdF = new BigDecimal(""+f);
			if(roundOffType == 1){
				bdF = bdF.setScale(2,BigDecimal.ROUND_CEILING);
			}else if(roundOffType == -1){
				bdF = bdF.setScale(2,BigDecimal.ROUND_FLOOR);
			}else{
				bdF = bdF.setScale(2,BigDecimal.ROUND_HALF_UP);	
			}
	    	return bdF.floatValue();
	    }
	    
	    public static List<String> removeDuplicateFromStingList(List srcList){
	    	if(srcList == null){
	    		return null;
	    	}
	    	Set<String> toRetain = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	    	toRetain.addAll(srcList);
	    	Set<String> set = new LinkedHashSet<String>(srcList);
	    	set.retainAll(new LinkedHashSet<String>(toRetain));
	    	List<String> uniqueStrList = new ArrayList<String>(set);	    	
	    	return uniqueStrList;
	    }
		public static String toOracleTimestamp(String s) {

	        if (s == null) {
	            return s;
	        }
	        else if (s.equals("")) {
	            return null;
	        }
	        else {
	            return "to_timestamp(" + checkString(s) + ",'MM/DD/YYYY HH24:MI:SS')";
	        }
	    }
		
		public static String GMTToPST(String date){
			String result = date;
			try {
			 SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:SS");
			 			 
			 sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			 Date theResult1 = sdf.parse(result);
			 
			 sdf.setTimeZone(TimeZone.getTimeZone("PST"));
			 
//			 logger.debug("Time in PST is " + sdf.format(theResult1));
			 result = sdf.format(theResult1);
			 
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return result;
		}
	public static String date2CERSUploadDate(String s) {
		    	String reformattedStr = null;

		        if (s != null) {
		            try {
		                SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
		                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
		                reformattedStr = myFormat.format(fromUser.parse(s));
		               
		            } catch (Exception e) {
		                logger.error(e.getMessage());
		            }
		        }

		        return reformattedStr;
		    }

			public static String guidToUUID(String guid){
				String uuid = guid.substring(0, 8) + "-" + guid.substring(8, 12) + "-" + guid.substring(12, 16) + "-" + guid.substring(16, 20) + "-" + guid.substring(20, 32);
				return uuid;
			}
			public static String uuidToGUID(String uuid){
				String guid = uuid.substring(0, 8) + uuid.substring(9, 13) + uuid.substring(14, 18) + uuid.substring(19, 23) + uuid.substring(24, 36);
				return guid;
			}

	public static String yyyymmdd2date(String yymmdd) {
		try {
			int yy = s2i(yymmdd.substring(0, 4));

			return yymmdd.substring(5, 7) + "/" + yymmdd.substring(8, 10) + "/" + yy;
		} catch (Exception e) {
			return "";
		}
	}

	 public static String dateslashes2datehypen(String s) {
	    	String reformattedStr = null;
	        if (s != null && s.contains("/")) {
	            try {
	            	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
	        		SimpleDateFormat format2 = new SimpleDateFormat("MM-dd-yyyy");
	        		java.util.Date date = format1.parse("12/10/2012");
//	        		System.out.println(format2.format(date));

	        		reformattedStr = format2.format(format1.parse(s));
	               
	            } catch (Exception e) {
	                logger.error(e.getMessage());
	            }
	        }else {
	        	reformattedStr=s;
	        }

	        return reformattedStr;
	    }
	 public static String roundOffDouble(double d){
         DecimalFormat df = new DecimalFormat("0.00");         
         return df.format(d);
     }
	
	 public static int getMonth() {		 
		 Calendar cal2 = Calendar.getInstance();
		 int currMonth= 0;
		 currMonth= (cal2.get(cal2.MONTH) + 1) ;
		return currMonth;
	 }
	 
	 public static int getDayOfMonth() {		 
		 Calendar cal2 = Calendar.getInstance();
		 int currMonth= 0;
		 currMonth= (cal2.get(cal2.DAY_OF_MONTH) + 1) ;
		return currMonth;
	 }
	 
	 public static boolean dateCompare(int date, int month, int lateFeeMonth){	
		 boolean compareFlag = false;
		 if(month> lateFeeMonth) {
				 compareFlag = true;
		 }else {
			 compareFlag = false;
		 }
		 return compareFlag;
	 }
	 
	 public static String appendDotZeros(String value) {
		 Object obj = value;  
		 if(s2i(value) % 1 == 0 && !value.contains(".")){
			 value = value + ".00";
		    }else{
		    	System.out.println("integer");
		    }
		 return value;		 
	 }
public static Date toDateFormatDDMMMYYYY(String strDate) {
	 Date currentDate = null;
	try {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
	    sdf.setLenient(false);
	    System.out.println("Date Format with ddMMMyyyy : "+strDate);  
	   
	    currentDate = sdf.parse(strDate);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	return currentDate;
	
}

public static String toOracleDate(String s) {
	if (s == null) {
		return s;
	} else if (s.equals("")) {
		return null;
	} else {
		return "to_date(" + checkString(s) + ",'MM/DD/YYYY')";
	}
}

/*public static String toOracleDateTime(String s) {
	if (s == null) {
		return s;
	} else if (s.equals("")) {
		return null;
	} else {
		return "to_date('" +s+ " 23:59:59','MM/DD/YYYY hh24:mi:ss')";
	}
}*/

/* Putting Hyphen in APN number */
public static String apnWithHyphens(String apn) {
	String apnOut = "";

	if ((apn != null) && (!apn.trim().equalsIgnoreCase(""))) {

		try {
			if ((apn != null) && (apn.length() > 0)) {
				for (int i = 0; i < apn.length(); i++) {
					apnOut = apnOut + apn.charAt(i);

					if ((i == 3) || (i == 6)) {
						apnOut = apnOut + "-";
					}
				}
			}
		} catch (RuntimeException e) {
			logger.error("Error in apn formatting");
			apnOut = apn;
		}
	}

	if (apnOut.equalsIgnoreCase("")) {
		apnOut = null;
	}

	return apnOut;
}

	/* Removing Hyphen in APN number */
	public static String apnWithoutHyphens(String apn) {

		if ((apn != null) && (!apn.trim().equalsIgnoreCase(""))) {

			try {
				apn = apn.replaceAll("-", "");

			} catch (RuntimeException e) {
				logger.error("Error in apn formatting");
			}
		}
		if (apn.equalsIgnoreCase("")) {
			apn = null;
		}

		return apn;
	}


}
