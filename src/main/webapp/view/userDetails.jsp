<%@page import="com.elms.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>      
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
String scrnNameForBack = Constants.USER_DETAILS_SCREEN;

%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/sweetalert.min.js"></script> 
<script src="js/sweetalert.js"></script>
<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">

<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>

<script src="js/jquery-1.12.4.min.js"></script>
<style>
a {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: #f1f1f1;
  color: black;
}

.next {
  background-color: #4CAF50;
  color: white;
}

.round {
  border-radius: 50%;
}

.box{
	color: #fff;
	padding: 20px;
	margin-top: 20px;
}
.red{ background-color: rgba(100, 100, 100, 0.5); }
label{ margin-right: 15px; }
</style>

<script type="text/javascript">

$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function validateEmailAddress(email) {
	var email = document.forms[0].email.value;
	var flag = true;
	var splitted = email.match("^(.+)@(.+)$");
	if (splitted == null)
		flag = false;
	if (flag == true && splitted[1] != null) {
		var regexp_user = /^\"?[\w-_\.]*\"?$/;
		if (splitted[1].match(regexp_user) == null)
			flag = false;
	}
	if (flag == true && splitted[2] != null) {
		var regexp_domain = /^[\w-\.]*\.[A-Za-z]{2,4}$/;
		if (splitted[2].match(regexp_domain) == null) {
			var regexp_ip = /^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
			if (splitted[2].match(regexp_ip) == null)
				flag = false;
		}// if
	}
	if (email != '' && flag == false) {
		document.forms[0].elements['email'].value = '';
		swal("Invalid email address.");
		document.forms[0].elements['email'].focus();
  	  return false;
	}
}

function DisplayPhoneHyphen(str,evt){
	var str1;
	evt = (evt) ? evt : window.event;	
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if ((document.forms[0].elements['newPhone'].value.length == 3 ) || (document.forms[0].elements['newPhone'].value.length == 7 )){
			
			document.forms[0].elements['newPhone'].value = document.forms[0].elements['newPhone'].value+'-';
			str1 = document.forms[0].elements[str].value;			
 		}
		if (document.forms[0].elements[str].value.length > 11 ) {
			return  false;
		}
	}
}

function ZipCodeValidation(str,evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if (document.forms[0].elements[str].value.length > 4 )  return false;
	}
}

function numberOnly(str, evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ( charCode<48 || charCode>57 ){
		return false;
	}	
}

function validateForm(){
	if(document.forms[0].email.value=="" || document.forms[0].email.value==" "){
      swal("Please enter Business email");
      document.forms[0].email.focus();
   	  return false;
   	}else if(document.forms[0].strNo.value!="" || document.forms[0].address.value!="" || document.forms[0].city.value!="" || document.forms[0].state.value!="" || document.forms[0].zip.value!=""){
   		var strName=document.forms[0].address.value;
   		var str=false;

   		if(strName!=null && (strName.substring(0,2)=='PO' || strName.substring(0,3) =='P.O' || strName.substring(0,3) =='P O')){
   			str=true;
   		}
   		if(document.forms[0].strNo.value=="" && (str==false)){
    	  swal("Please enter mailing street number");
    	  document.forms[0].strNo.focus(); 
    	  return false;
    	}else if(document.forms[0].address.value==""){
    	  swal("Please enter mailing street Name");
    	  document.forms[0].address.focus();
    	  return false;
    	}else if(document.forms[0].city.value==""){
	      swal("Please enter mailing city");
	      document.forms[0].city.focus();
	   	  return false;
    	}else if(document.forms[0].state.value==""){
	      swal("Please enter mailing state");
	      document.forms[0].state.focus();
    	  return false;
    	}else if(document.forms[0].zip.value==""){
	      swal("Please enter mailing zip");
	      document.forms[0].zip.focus();
    	  return false;
    	}else if(isNaN(document.forms[0].zip.value)){
	      swal("Please enter valid zip code");
	      document.forms[0].zip.focus();	      
    	  return false;
    	}else{  
        	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
	   		document.forms[0].submit();
	   		return true;
    	}
  	}

	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";	
    document.forms[0].submit();
    return true;
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
     return true;
}

function IsAlphaNumeric(e) {
		var k;
	    document.all ? k = e.keyCode : k = e.which;
	    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
 }
</script>
<head>
	<title>City of Burbank Business License  / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<script src="vendor/bootstrap/js/logout-timer.js"></script>
<script src="js/popper/popper-1.12.9.min.js" ></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 	
<div>
	<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
		<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
<form:form name="userDetailsForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" class="form-horizontal">
	<div class="wrap-input100 validate-input">
	<table width="100%">
  		<tbody>
    		<tr>
    			<td width="8%">&nbsp;</td>
      			<td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
      			<td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
      			<logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if>
			       	<logic:if test = "${activityDetails.onlineBLBTApplication == 'Y'}">
     				Application Portal
     				</logic:if>
     				<logic:if test = "${activityDetails.onlineBLBTApplication == 'N'}">
     				Renewal Portal
     				</logic:if>
     				
     				</strong></td>
      			<td width="33%">${activityDetails.permitNumber}</td>
    		</tr>
    	</tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong> ${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Address : </strong></td>
		      	
		      	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
		      		<td width="30%"><strong>Business Mailing Address : </strong></td>
		      	</logic:if>
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Type : </strong> ${activityDetails.description}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"> ${activityDetails.businessStrName}</td>
		       <logic:choose>
		       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.unit)}"><td width="30%"> ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}</td></logic:when>
		       	<logic:otherwise><td width="30%"> ${activityDetails.mailStrNo}&nbsp;${activityDetails.mailStrName}&nbsp;${activityDetails.mailUnit}</td></logic:otherwise>
		       </logic:choose>
		        
		    </tr>
		    <tr>
		    
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Email : </strong>
			        <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
		        
		      	</td>
		   		<td width="10%">&nbsp;</td>
		      	<td width="30%">  ${activityDetails.businessCityStateZip}</td>
		      	 <logic:choose>
		       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}"><td width="30%"> ${activityDetails.cityStateZip}</td></logic:when>
		       	<logic:otherwise><td width="30%"> ${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip} </td></logic:otherwise>
		       </logic:choose>
		      
		    </tr>
 		 </tbody>
	</table>
	</div>
	<table align="center" width="100%" height="40%">
	
	<div class="p-3 mb-2 bg-gradient-primary text-dark"  style="font-size:15px;font-weight: 900;" align="left">
	Please indicate any changes to your account below. (Changes to business location, business name or ownership require a new application. Please call our offices at (818) 238-5280).
	</div>
			<tr>
			<td align="center">
   				<table  class="red box" style="margin-left:5%; border-collapse: collapse; border: 1px solid black;" align ="center" height="200px">
					<tr>
						<td width="50%"><div align="right">Business Email Address&nbsp;&nbsp;<font color="red">*</font>&nbsp;:&nbsp;</div></td>					
						<td width="50%">					 
							<div class="validate-input" data-validate = "Enter Email Address">		
							   	<logic:choose>
							   	<logic:when test = "${activityDetails.onlineBLBTApplication == 'Y'}">
							   	<input type="hidden" value="${activityDetails.onlineBLBTApplication}" name="onlineBLBTApplication">
							       <input type="text" name="email" class="form-control" 
								       <logic:choose>
									       	<logic:when test="${not empty activityDetails.tempEmail}"> value="${activityDetails.tempEmail}"</logic:when>
									       	<logic:otherwise>value=" ${activityDetails.email}"	</logic:otherwise>
									    </logic:choose>  aria-describedby="emailHelp" placeholder="Email" onblur="validateEmailAddress(this)" readonly>
						        
							    </logic:when>
							    <logic:otherwise>
							       <input type="text" name="email" class="form-control" 
									<logic:choose>
									       	<logic:when test="${not empty activityDetails.tempEmail}">value="${activityDetails.tempEmail}" </logic:when>
									 	<logic:otherwise>value="${ activityDetails.email}"</logic:otherwise>
									 </logic:choose> aria-describedby="emailHelp" placeholder="Email" onblur="validateEmailAddress(this)">
							    </logic:otherwise>				    	
	   					    	</logic:choose>
							</div>
						</td >
					</tr>
					<tr>
						<td width="50%"><div align="right">Business Phone Number&nbsp;&nbsp;&nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div class="validate-input" data-validate="Enter Phone Number">
								<input type="text" id="newPhone" name="newPhone" 
								<logic:choose>
								       	<logic:when test="${not empty activityDetails.tempBusinessPhone}">value="${activityDetails.tempBusinessPhone}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.businessPhone}"</logic:otherwise>
								 </logic:choose> class="form-control" placeholder="Phone Number"  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" onkeypress="return DisplayPhoneHyphen('newPhone',event)">				
							</div>					
						</td>
					</tr><tr><td></td></tr>
					<tr>					
						<td width="50%"><div align="right">Mailing Street Number&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div align="center">
								<input type="text" name="strNo" placeholder="Street Number" 
								<logic:choose>
								       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.strNo}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailStrNo}"</logic:otherwise>
								 </logic:choose> class="form-control"  maxlength="6" onkeypress="return event.charCode >= 47 && event.charCode <= 57">
							</div>					
						</td>
					</tr>	
					<tr>	
						<td width="50%"><div align="right">Mailing Street Name&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div align="center">
								<input type="text" name="address" placeholder="Street Name" 
								<logic:choose>
								       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.strName)}">value="${activityDetails.strName}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailStrName}"</logic:otherwise>
								 </logic:choose> maxlength="25" class="form-control">
							</div>					
						</td>
					</tr>	
					<tr>
						<td width="50%"><div align="right">Mailing Unit/Suite Number&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div align="center">
								<input type="text" name="unit"  placeholder="Unit" <logic:choose>
								    <logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.unit}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailUnit}"</logic:otherwise>
								 </logic:choose>  class="form-control" maxlength="5" onkeypress="return IsAlphaNumeric(event);">	
							</div>					
						</td>
					</tr>
					<tr>
						<td width="50%"><div align="right">Mailing City&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div align="center">
								<input type="text" name="city"  placeholder="City" <logic:choose>
								       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.city}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailCity}"</logic:otherwise>
								 </logic:choose>  maxlength="25" class="form-control">	
							</div>					
						</td>
					</tr>
					<tr>
						<td width="50%"><div align="right">Mailing State&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="20%">					
							<div align="center">
								<input type="text" name="state" placeholder="State" 
								<logic:choose>
								       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.state}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailState}"</logic:otherwise>
								 </logic:choose>  maxlength="2" class="form-control">	
							</div>					
						</td>
					</tr>
					<tr>
						<td width="50%"><div align="right">Mailing Zip&nbsp;&nbsp; &nbsp;:&nbsp;</div></td>
						<td width="50%">					
							<div align="center">
								<input type="text" name="zip" placeholder="Zip" <logic:choose>
								       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.zip}" </logic:when>
								 	<logic:otherwise>value="${activityDetails.mailZip}"</logic:otherwise>
								 </logic:choose>  maxlength="5" class="form-control"  onkeypress="return ZipCodeValidation('zip', event)">
							</div>					
						</td>
					</tr>
					<tr><td colspan="2"> &nbsp; </td></tr>	
					<tr><td colspan="2" style="color:red; font-size:12px;font-family: Arial, sans-serif;">* Business email is required field</td></tr>				
				</table>
			</td>
				</tr>
			</table>							
			<div class="container-login100-form-btn m-t-32">
  				<button type="button" class="btn btn-primary" onclick="return validateForm();">Next</button>&nbsp;
  				<button type="button" class="btn btn-primary" onclick="return cancel();" >Cancel</button>  
			</div>	
				<input type="hidden" name="renewalCode" value=" ${activityDetails.renewalCode}">
				<input type="hidden" name="permitNumber" value=" ${activityDetails.permitNumber}">
				<input type="hidden" name="businessAccNo" value=" ${activityDetails.businessAccNo}">
				<input type="hidden" name="tempId" value=" ${activityDetails.tempId}">
				<input type="hidden" name="businessName" value=" ${activityDetails.businessName}">
				<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
								
          </form:form>
		</div>
	  </div>
	</div>

</body>
</html>