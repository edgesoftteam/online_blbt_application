<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List,java.util.HashMap,java.util.Map" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>     
  
<%
String scrnNameForBack = Constants.ATTACHMENT_DETAILS_SCREEN;

%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<style>
input[type='file'] {
  color: transparent;    / Hides your "No File Selected" /
  direction: rtl;        / Sets the Control to Right-To-Left /
}

.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}

/* Example stylistic flourishes */

.fileContainer {
    background: #2352A1;
    color: white;
    border-radius: .5em;
    float: left;
    padding: .5em;
}

.fileContainer [type=file] {
    cursor: pointer;
}

/* table, th, td {
  border: 1px solid black;
} */
</style>
<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script type="text/javascript">
$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function AddAttachment(this1){
		var attachmentTypeId=this1.value;

		document.forms[0].action = "${pageContext.request.contextPath}/reUploadAttachments?id="+attachmentTypeId;
		document.forms[0].submit();
	}
function back(){	
	var renewalCode = document.getElementById("renewalCode").value;	
	var businessAccNo = document.getElementById("businessAccNo").value;	
	document.forms[0].action="${pageContext.request.contextPath}/attachReUpload";
	document.forms[0].submit();
}
function enableAndDisableUpload(typeId,buttonId){
	var file="fileInput("+typeId+")";
	if(document.getElementById(file).value==''){
		var id="attachmentTypeId("+typeId+")";
		document.getElementById(id).disabled = false;
	}
	
  }
/* function back(){
	alert("Calling function");
	var renewalCode = document.getElementById("renewalCode").value;
	alert("renewalCode = "+renewalCode);
	var businessAccNo = document.getElementById("businessAccNo").value;
	alert("businessAccNo = "+businessAccNo);
	 document.forms[0].action="${pageContext.request.contextPath}/attachReUpload?renewalCode="+renewalCode+"&businessAccNo="+businessAccNo+";
	 document.forms[0].submit();
} */

function disableFileUpload(id){	
	$('input:file').filter(function(){
        return this.files.length==0 
	}).prop('disabled', true);	
	
	var idVal="attachmentTypeId("+id+")";
	document.getElementById(idVal).disabled = false;	
	
	var idVal1='fileContainer'+id;	
	document.getElementById(idVal1).style.backgroundColor = "green";
} 

$( document ).ready(function() {
	//alert("Calling function");
	//var renewalCode = document.getElementById("renewalCode").value;
	var renewalCode = $('input[name=renewalCode]').val();
	//alert("renewalCode = "+renewalCode);
	
		$.ajax('${pageContext.request.contextPath}/checkFileUploadedOrNot?renewalCode='+renewalCode, 
		{
			dataType: 'json', // type of response data
			timeout: 500,     // timeout milliseconds
			success: function (data,status,xhr) {   // success callback function
				//alert(data.uploadedTypeIds);
				//alert("type ids = "+data.typeIds);
				var attachmentsJson = JSON.stringify(data.typeIds).replace("[","").replace("]","");
				//alert("json length = "+myJSON.length);				
					var allIds = attachmentsJson.split(",");					
					for(var i = 0; i < allIds.length; i++) {					   
					   //var id="attachmentTypeId("+uploadedIds[i]+")";					   
					   var id="attachmentTypeId("+allIds[i]+")";					   
					  // document.getElementById(id).disabled = true;
					   document.getElementById(id).disabled = true;
					}
				
				
				var myJSON = JSON.stringify(data.uploadedTypeIds).replace("[","").replace("]","");
				//alert("json length = "+myJSON.length);				
					var uploadedIds = myJSON.split(",");					
					for(var i = 0; i < uploadedIds.length; i++) {					   
					   //var id="attachmentTypeId("+uploadedIds[i]+")";					   
					   var id1="fileInput("+uploadedIds[i]+")";					   
					  // document.getElementById(id).disabled = true;
					   document.getElementById(id1).disabled = true;
					}
				
								
			},
			/* error: function (jqXhr, textStatus, errorMessage) { // error callback 				
				alert('Error: ' + errorMessage);
			} */
		});
	 
});
  
  
</script>

<head>
	<title>City of Burbank Business License  / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<script src="js/popper/popper-1.12.9.min.js" ></script>
<!-- <script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script> -->
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 -->
 
<div>
<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
	<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
				
<form:form name="attachReUploadForm" cssClass="login100-form validate-form p-b-3 p-t-5" modelAttribute="activityForm"  enctype="multipart/form-data" method="post" >

	<div class="wrap-input100 validate-input">
	<table width="100%">
	  <tbody>
   		 <tr>
	   		 <td width="8%">&nbsp;</td>
	      	 <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
	      	 <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
	      	 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if> Renewal Portal</strong></td>	       	 
	       	 <td width="33%">${activityDetails.permitNumber}</td>
    	</tr>
  	  </tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong>${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Renewal Code : </strong>${activityDetails.renewalCode}</td>
		    </tr>
 		 </tbody>
	</table>
	</div>	
		
	<input type="hidden" name="renewalCode" id="renewalCode" value="${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" id="businessAccNo" value="${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value="${activityDetails.tempId}">
    <input type="hidden" name="actType" value="${activityDetails.actType}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
    
	<div align="left" style="align:left" class="validate-form p-b-33 p-t-5" width="300px">
	<table style="margin-left:110px;">
	<tr>
		<td>&nbsp; </td>
		
	<!-- <div class="p-3 mb-2 bg-gradient-primary text-dark"  style="font-size:15px;font-weight: 900;" align="left">
		The documents listed below are required for your business type renewal. For your convenience downloadable links are provided, please download, fill out and upload to this page.
	</div> -->
<%-- ${not empty activityDetails.displayErrorMsg} --%>
<%-- ${not empty activityDetails.displaySuccessMsg} --%>
<!-- 		<td colspan="2"> -->
<%-- 	      <logic:if test=${not empty activityDetails.displayErrorMsg}> --%>
<!-- 				<div > -->
<!-- 				  <div class="form-group"> -->
<!-- 				   <label for="exampleInputName2"> -->
<!-- 				 <font color="red"> <${activityDetails.displayErrorMsg}</font>      -->
<!-- 				   </label> -->
<!-- 					   </div> -->
<!-- 						</div> -->
<%-- 				</logic:if> --%>
				
<%-- 	      <logic:if test=${not empty activityDetails.displaySuccessMsg}> --%>
<!-- 				<div > -->
<!-- 				   <div class="form-group"> -->
<!-- 				     <label for="exampleInputName2"> -->
<!-- 				       <font color="green"> <${activityDetails.displaySuccessMsg}</font>      -->
<!-- 				     </label> -->
<!-- 			       </div> -->
<!-- 				</div> -->
<%-- 		</logic:if> --%>
<!-- 	     </td> -->
	     <td colspan="2">&nbsp; </td>
	</tr>
	
		<%-- <logic:forEach var="attachment" items="${attachmentList}">
		<logic:if test="${empty attachment.documentURL}">
			<tr>		
				<!-- <td style = "margin-left:5px;font-family: Arial, sans-serif;font-weight: bold;" align="center" >Document Type :</td> -->
				<td style = "text-indent: 1em;font-family: Arial, sans-serif;" align="left">${attachment.attachmentDesc}</td>
				<td align="left" style="font-family: Arial, sans-serif;" ><a href="${pageContext.request.contextPath}/fopen?fileName=${attachment.fileName}&filePath=${activityDetails.tempFileLoc}" class="text-primary"><u>${attachment.fileName}</u></a></td>
				 <logic:if test="${empty attachment.fileName}"> 
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;" align="left">
					  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onclick="enableAndDisableUpload(${attachment.attachmentTypeId});" value="${attachment.attachmentTypeId}" />
					</td>
					<td  align="left" style="font-family: Arial, sans-serif;" width="10%"> 
					   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="btn btn-primary" disabled>Upload</button>
					</td>
				</logic:if>
			</tr>
        </logic:if>
	 	<logic:if test="${not empty attachment.documentURL}"> 
			<tr>
					<!-- <td style = "margin-left:5px;background-color:#A9A9A9;font-family: Arial, sans-serif;font-weight: bold;" align="center"  >Document Type :</td> --> 
					<td style = "text-indent: 1em;;font-family: Arial, sans-serif;" align="left" colspan="2">${attachment.attachmentDesc} </td>
					<td align="left" style="font-family: Arial, sans-serif;"><a href="${pageContext.request.contextPath}/fopen?fileName=${attachment.fileName}&filePath=${activityDetails.tempFileLoc}" class="text-primary"><u>${attachment.fileName}</u></a></td>
					<logic:if test="${empty attachment.fileName}"> 
						<td style = "text-indent: 1em;font-family: Arial, sans-serif;"   align="left">
						  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onclick="enableAndDisableUpload(${attachment.attachmentTypeId});" value="${attachment.attachmentTypeId}" /> 
							<label class="fileContainer" style="margin-left: 35px;">
		    					Choose file
							  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onclick="enableAndDisableUpload(${attachment.attachmentTypeId});" value="${attachment.attachmentTypeId}" /> 
							</label>
						</td>
						<td align="left" style="font-family: Arial, sans-serif;" width="10%"> 
						   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="btn btn-primary" disabled>Upload</button>
						</td>
					</logic:if>
				</tr>
            </logic:if>
	          <tr><td  colspan="5">&nbsp;</td></tr>
		</logic:forEach --%>
		<logic:forEach var="attachment" items="${attachmentList}">
			<logic:if test="${(attachment.downloadOrUploadType == 'U') || (attachment.downloadOrUploadType == 'B')}">
		    <logic:if test="${empty attachment.documentURL}">
				<tr>					
					<td style = "text-indent: 1em;;font-family: Arial, sans-serif;font-weight: bold;width:40%" align="left">${attachment.attachmentDesc} </td> 					
					<logic:if test="${empty attachment.fileName}"> 
					<td style = "font-family: Arial, sans-serif;width:30%"   align="right">
						<label class="fileContainer"  id="fileContainer${attachment.attachmentTypeId}" style="margin-left: 70px;">
	    					Choose file
						  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onchange="disableFileUpload('${attachment.attachmentTypeId}');" value="${attachment.attachmentTypeId}" /> 
						</label>
					</td>					
					<td align="left" style="font-family: Arial, sans-serif;width:40%"> 
					   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}"><img src="images/upload.png" alt="Upload" height="18%" width="30%" class="img-responsive" title="Upload ${attachment.attachmentDesc}"/>&nbsp;Upload</button>
					</td> 
					</logic:if>
				</tr>
            </logic:if> 
		 	<logic:if test="${not empty attachment.documentURL}"> 
				<tr>					
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;font-weight: bold;width:40%"  align="left">					   
					     ${attachment.attachmentDesc}				   
					</td> 	
					<logic:if test="${empty attachment.fileName}"> 				
					<td style = "font-family: Arial, sans-serif;width:30%"   align="right">
						<label class="fileContainer" id="fileContainer${attachment.attachmentTypeId}" style="margin-left: 70px;">
	    					Choose file
						  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onchange="disableFileUpload('${attachment.attachmentTypeId}');" value="${attachment.attachmentTypeId}" /> 
						</label>
					</td>
					<td align="left" style="font-family: Arial, sans-serif;width:40%"> 
					   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}"><img src="images/upload.png" alt="Upload" height="18%" width="30%" class="img-responsive"  title="Upload ${attachment.attachmentDesc}"/>&nbsp;Upload</button>
					</td> 
					</logic:if>
				</tr>
            </logic:if>
           <tr> 
           	 <td colspan="2" style = "text-indent: 1em;font-family: Arial, sans-serif;font-weight: bold;background-color:#E4E3E3;" align="left"><strong id="task"> ${attachment.fileName} </strong></td> 
           </tr>
			<tr><td  colspan="4">&nbsp;</td></tr>
			</logic:if>
		</logic:forEach>
		</table>
	
		<div class="container-login100-form-btn m-t-32">	
		  <!-- <button type="button" class="btn btn-primary" onclick="back();">Back</button>&nbsp; -->
		  <button type="button" class="btn btn-primary" onclick="back();">Back</button>
 		</div>
</div>
				</form:form>
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>