<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>   
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>     

<%
String scrnNameForBack = Constants.QUANTITY_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>

<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<head>
	<title>City of Burbank Business License  / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!-- 	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"> -->
<!-- 	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
<!-- 	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"> -->
<!-- 	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
<!-- 	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css"> -->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="js/sweetalert.min.js"></script> 
	<script src="js/sweetalert.js"></script>
	<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">

</head>

<style>
.form-control-inline {
    min-width: 0;
    width: 50px;
    height:30px;
    display: inline;
}

:-ms-input-placeholder {    
	margin: auto !important; / IE11 needs the !important flag /
    box-sizing: border-box !important;
    border-style: solid !important;
    padding:4px !important;
}
</style>

<script src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('#purpose').on('change', function() {
          if ( this.value == 'Yes'){
            $("#business").show();
          }else{
            $("#business").hide();
          }
        });
        window.setInterval("LogoutTimer(0)", 1000);
    });    
    
    function enableQtyDetails(){
  	  if (document.getElementById("purpose").value=='Yes'){
  		  document.forms[0]["qtyCheckbox"].checked=true;
          $("#business").show();
        } else{  
        	document.forms[0]["qtyCheckbox"].checked=true;
        	qtyCheckbox
          $("#business").hide();
        }
    }

    function back(){    	 
    	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
   		document.forms[0].submit();
    }

    function cancel(){
    	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
         document.forms[0].submit();
    }
    
    function showCheckbox(optionValue){
   	 if(optionValue=="Yes" || document.forms[0]["purpose"].value=="Yes"){
   		document.forms[0]["qtyCheckbox"].checked=true;
   	 }else{
   		document.forms[0].quantity.value="";
   	 }
   }
   
   function clearQty(){
   	 if(document.forms[0]["qtyCheckbox"].checked==false){
   		document.forms[0].quantity.value="";
   	 }else{
   		 return true;
   	 }
   } 
   

   function validateForm(){
     	var pattern = ${activityDetails.linePattern};
//      	swal(pattern );
     	if(pattern != 1){
     	 	if(document.forms[0].purpose.value=="Yes" && document.forms[0]["qtyCheckbox"].checked==true){
     		   	if(document.forms[0].quantity.value==""){
     		      swal("Please enter "+document.forms[0].qtyDesc.value);
     		      document.forms[0].quantity.focus();
     	    	  return false;
     	    	}
     		   	if(isNaN(document.forms[0].quantity.value)){
     		      swal("Please enter valid quantity number");
     		      document.forms[0].quantity.focus();
     	    	  return false;
     		    }    	
             	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
     	   		document.forms[0].submit();
     	   		return true;
     		}else{
             	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt"; 		
     	   		document.forms[0].submit();
     	   		return true;
     		}    		
     	}else{
     		if(document.forms[0].quantity.value==""){
   		      swal("Please enter "+document.forms[0].qtyDesc.value);
   		      document.forms[0].quantity.focus();
   	    	  return false;
   	    	}
     		if(isNaN(document.forms[0].quantity.value)){
   		      swal("Please enter valid quantity number");
 		      document.forms[0].quantity.focus();
 	    	  return false;
 		    }  
     		document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt"; 		
 	   		document.forms[0].submit();
 	   		return true;
     	}

     }
</script>
<body onload="enableQtyDetails()">

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>
     
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<script src="js/popper/popper-1.12.9.min.js" ></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>	
<div>
	<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
		<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:35px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>

<form:form name="qtyDetailsForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="qtyForm" method="post">

	<div class="wrap-input100 validate-input">
	<table width="100%">
	  <tbody>
   		 <tr>
	   		 <td width="8%">&nbsp;</td>
	      	 <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
	      	 <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
	      	 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if> 
			    <logic:if test = "${activityDetails.onlineBLBTApplication == 'Y'}">
     				Application Portal
     				</logic:if>
     				<logic:if test = "${activityDetails.onlineBLBTApplication == 'N'}">
     				Renewal Portal
     				</logic:if>
     				</strong></td>
	       	 <td width="33%"> ${activityDetails.permitNumber}</td>
    	</tr>
  	  </tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong>${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Address : </strong></td>
		      	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo)}">
		      			<td width="30%"><strong>Business Mailing Address : </strong></td>
		      	</logic:if>
		      	
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Type : </strong>${activityDetails.description}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%">${activityDetails.businessStrName}</td>
		       			<logic:choose>
							<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.unit)}">
									<td width="30%">${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}</td>
							</logic:when>
							<logic:otherwise>
								<td width="30%">${activityDetails.mailStrNo}&nbsp;${activityDetails.mailStrName}&nbsp;${activityDetails.mailUnit}</td>
							</logic:otherwise>
						</logic:choose>						
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Email : </strong>
			      		<logic:choose>
					        <logic:when test="${not empty activityDetails.tempEmail}">
					       		${activityDetails.tempEmail}
					     	</logic:when>
					     	<logic:otherwise>
					     		${activityDetails.email}
					       </logic:otherwise>
				       </logic:choose>
	
		      	</td>
		   		<td width="10%">&nbsp;</td>
		      	<td width="30%">${activityDetails.businessCityStateZip} </td>
		      	<logic:choose>
					<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">
					
						<td width="30%">${activityDetails.cityStateZip}</td>
					</logic:when>
				<logic:otherwise>					
		      		<td width="30%">${activityDetails.mailCity}${activityDetails.mailState}${activityDetails.mailZip} </td>
				</logic:otherwise>
				</logic:choose>
			
		    </tr>
 		 </tbody>
	</table>
	</div>
	<div align="left" style="align:left" class="validate-form p-b-33 p-t-5" width="300px">
		<table>
		<tr>
		<td width="20%">&nbsp; </td>
		<td>
		<div align="left" style="align:left" class="validate-form p-b-33 p-t-5" width="300px">
	 		<div class="form-group row">
	 		
				 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
				 
	        	    <table>
					 <logic:if test="${not empty activityDetails.qtyDesc && activityDetails.qtyDesc != 'None'}">
				        	 <logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern ==3 }">
							        <tr>
							        	<td>
									        <div class="col-xs-3">
								          		<label for="ex2">${activityDetails.line1Desc} &nbsp; ${activityDetails.qtyOther}</label>
									        </div>					        	
							        	</td>
							        </tr>
					        </logic:if>
					        <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 || activityDetails.linePattern ==2 )}">
							        <tr>
								        <td>
									        <div class="col-xs-3">${activityDetails.line2Desc}
									       
											<logic:if test="${not empty activityDetails.line2Desc}">
												<logic:choose>
													<logic:when test="${not empty activityDetails.tempBlQtyFlag && (activityDetails.tempBlQtyFlag == 'Y' || activityDetails.tempBlQtyFlag == 'Yes')}">
														<select name="tempBlQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
															<option value="No">No</option>
															<option value="Yes" selected="selected">Yes</option>				
														</select>
													</logic:when>
													<logic:otherwise>
														<select name="tempBlQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
															<option value="No">No</option>
															<option value="Yes">Yes</option>				
														</select>
													</logic:otherwise>
												</logic:choose>	
											</logic:if>	
											</div>								
										</td>
									</tr>
									<tr>
										<td>
											<div style="display:none;" id="business">
										   		<input type="checkbox" align="left" id="qtyCheckbox"  onclick="return clearQty()"> Number of ${activityDetails.qtyDesc}<font color="red">*</font>
									    	    <input type="text" maxlength="11" size="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="noOfEmp" value="${activityDetails.noOfEmp}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
											</div>  
										</td>
									</tr>
							</logic:if>
					 </logic:if>
					 <logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern ==1 }">					
						<tr>
							<td>
						   		 ${activityDetails.line3Desc}&nbsp;<font color="red">*</font>
					    	    <input type="text" maxlength="11" size="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="noOfEmp" value="${activityDetails.noOfEmp}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					    	    or a box.
							</td>
						</tr>
					 </logic:if> 					 
					</table>
				 </logic:if>
				 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">				        
			        <table>
					   <logic:if test="${not empty activityDetails.qtyDesc && activityDetails.qtyDesc != 'None'}">	
					        <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3)}">
					        <tr>
					        	<td>
							        <div class="col-xs-3">
						          		<label for="ex2">${activityDetails.line1Desc} &nbsp; ${activityDetails.qtyOther}</label>
							        </div>					        	
					        	</td>
					        </tr>
					        </logic:if>
					        <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 || activityDetails.linePattern ==2 )}">
					        <tr>
						        <td>
							        <div class="col-xs-3">
						          	${activityDetails.line2Desc}
						          	<logic:if test="${not empty activityDetails.line2Desc}">
										<logic:choose>
											<logic:when test="${not empty activityDetails.tempBtQtyFlag && (activityDetails.tempBtQtyFlag == 'Y' || activityDetails.tempBtQtyFlag == 'Yes')}">
												<select name="tempBtQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
													<option value="No">No</option>
													<option value="Yes" selected="selected">Yes</option>				
												</select>
											</logic:when>
											<logic:otherwise>
												<select name="tempBtQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
													<option value="No">No</option>
													<option value="Yes">Yes</option>				
												</select>
											</logic:otherwise>
										</logic:choose>
									</logic:if>
					
						          	</div>
						        </td>
					        </tr>
					        <tr>
					        	<td>
									<div style="display:none;" id="business">
									    <input type="hidden" align="left" id="qtyCheckbox" placeholder="Check Box" onclick="return clearQty()"> ${activityDetails.line3Desc}<font color="red">*</font>
								    	<input type="text" maxlength="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="tempBtQty" value="${activityDetails.tempBtQty}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
									</div>    		
					        	</td>
					        </tr>
					        </logic:if>
					    </logic:if>
					
						<logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern == 1}">
						   <tr>
					        	<td>
								    <input type="hidden" align="left" id="qtyCheckbox" placeholder="Check Box" onclick="return clearQty()"> ${activityDetails.line3Desc}<font color="red">*</font>
							    	<input type="text" maxlength="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="tempBtQty" value="${activityDetails.tempBtQty}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">    		
					        	</td>
					        </tr>					        
						</logic:if>
			        </table>
				 </logic:if>     
      		</div>     
			</div>				
		</td>
		</tr>
		</table>
		</div>
		<div class="container-login100-form-btn m-t-32">		
		  <button type="button" class="btn btn-primary" onclick="back();">Back</button>&nbsp;
		  <button type="button" class="btn btn-primary" onclick="validateForm()">Next</button>&nbsp;	
		  <button type="button" class="btn btn-primary" onclick="cancel();">Cancel</button>
 		</div>
 				
						<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
						<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">						
						<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
						<input type="hidden" name="tempId" value="${activityDetails.tempId}">
				        <input type="hidden" name="actType" value="${activityDetails.actType}">
						<input type="hidden" name="qtyOther" value="${activityDetails.qtyOther}">
						<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
						<input type="hidden" name="placeholderDesc" value="${activityDetails.placeholderDesc}">
						<input type="hidden" name="qtyDesc" value="${activityDetails.qtyDesc}">
				</form:form>
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>

</body>
</html>