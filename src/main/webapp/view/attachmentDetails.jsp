<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<%
String scrnNameForBack = Constants.ATTACHMENT_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<style>
input[type='file'] {
  color: transparent;    / Hides your "No File Selected" /
  direction: rtl;        / Sets the Control to Right-To-Left /
}
</style>
<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script type="text/javascript">
$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

function AddAttachment(this1){
		var attachmentTypeId=this1.value;

		document.forms[0].action = "${pageContext.request.contextPath}/uploadAttachments?id="+attachmentTypeId;
		document.forms[0].submit();
	}
function next(){

	document.forms[0].action = "${pageContext.request.contextPath}/feeDetails";
	document.forms[0].submit();
	
}
function enableAndDisableUpload(typeId,buttonId){
	var file="fileInput("+typeId+")";
	if(document.getElementById(file).value==''){
		var id="attachmentTypeId("+typeId+")";
		document.getElementById(id).disabled = false;
	}
	
  }
</script>

<head>
	<title>City of Burbank Business License / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<script src="js/popper/popper-1.12.9.min.js" ></script>
<!-- <script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script> -->
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 

<div>
<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
	<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
				
<form:form name="attachmentDetailsForm" cssClass="login100-form validate-form p-b-3 p-t-5" modelAttribute="activityForm"  enctype="multipart/form-data" method="post" >

	<div class="wrap-input100 validate-input">
	<table width="100%">
	  <tbody>
   		 <tr>
	   		 <td width="8%">&nbsp;</td>
	      	 <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
	      	 <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
	      	  <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if> 
			    <logic:if test = "${activityDetails.onlineBLBTApplication == 'Y'}">
     				Application Portal
     				</logic:if>
     				<logic:if test = "${activityDetails.onlineBLBTApplication == 'N'}">
     				Renewal Portal
     				</logic:if>
     				
			    </strong></td>
	       	 <td width="33%">${activityDetails.permitNumber}</td>
    	</tr>
  	  </tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		      <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong> ${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Address : </strong></td>
		      	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo)}">
		      		<td width="30%"><strong>Business Mailing Address : </strong></td>
		      	</logic:if> 
		      	
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Type : </strong>${activityDetails.description}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%">${activityDetails.businessStrName}</td>
		       
		       <logic:choose>
					<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.unit)}">
							<td width="30%">${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}</td>
					</logic:when>
					<logic:otherwise>
						<td width="30%">${activityDetails.mailStrNo}&nbsp;${activityDetails.mailStrName}&nbsp;${activityDetails.mailUnit}</td>
					</logic:otherwise>
				</logic:choose>
		    
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Email : </strong>
			    	
			       <logic:choose>
				        <logic:when test="${not empty activityDetails.tempEmail}">
				       		${activityDetails.tempEmail}
				     	</logic:when>
				     	<logic:otherwise>
				     		${activityDetails.email}
				       </logic:otherwise>
			       </logic:choose>
		      	</td>
		   		<td width="10%">&nbsp;</td>
		      	<td width="30%"> ${activityDetails.businessCityStateZip}</td>
		      	
		      	<logic:choose>
		        <logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">
		       		<td width="30%">${activityDetails.cityStateZip}</td>
		     	</logic:when>
		     	<logic:otherwise>
		     		<td width="30%">${activityDetails.mailCity} ${activityDetails.mailState}&nbsp;${activityDetails.mailZip} </td>
		       </logic:otherwise>
		       </logic:choose>
		       
		    </tr>
 		 </tbody>
	</table>
	</div>	
		
	<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value="${activityDetails.tempId}">
    <input type="hidden" name="actType" value="${activityDetails.actType}">
    <input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
    
    
	<div align="left" style="align:left" class="validate-form p-b-33 p-t-5" width="300px">
	<table width="100%" style="margin-left:30px;">
	<tr>
		<td>&nbsp; </td>
		
	<div class="p-3 mb-2 bg-gradient-primary text-dark"  style="font-size:15px;font-weight: 900;" align="left">
		The documents listed below are required for your business type renewal. For your convenience downloadable links are provided, please download, fill out and upload to this page.
	</div>

		<td colspan="2">
             <logic:if test=" ${not empty activityDetails.displayErrorMsg}">
				<div >
				  <div class="form-group">
				   <label for="exampleInputName2">
				 <font color="red"> ${activityDetails.displayErrorMsg}</font>     
				   </label>
					   </div>
						</div>
			</logic:if>
              <logic:if test=" ${not empty activityDetails.displaySuccessMsg}">
				<div >
				   <div class="form-group">
				     <label for="exampleInputName2">
				       <font color="green"> ${activityDetails.displaySuccessMsg}</font>     
				     </label>
			       </div>
				</div>
			</logic:if>
	     </td>
	     <td>&nbsp; </td>
	</tr>
	

		<logic:forEach var="attachment" items="${attachmentList}">
						
			<logic:if test="${empty attachment.documentURL}">
				<tr>		
					<td style = "margin-left:5px;font-family: Arial, sans-serif;font-weight: bold;" align="center" >Document Type :</td>
					<td style = "text-indent: 1em;;font-family: Arial, sans-serif;" align="left">${attachment.attachmentDesc}</td>
					<td style = "text-indent: 1em;;font-family: Arial, sans-serif;" align="left">
					  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onclick="enableAndDisableUpload(${attachment.attachmentTypeId});" value="${attachment.attachmentTypeId}" />
					</td>
					<td  align="left" style="font-family: Arial, sans-serif;"> 
					   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="btn btn-primary" disabled>Upload</button>
					</td>
				</tr>
            </logic:if>
		 	<logic:if test="${not empty attachment.documentURL}"> 
				<tr>
					<td style = "margin-left:5px;background-color:#A9A9A9;font-family: Arial, sans-serif;font-weight: bold;" align="center"  >Document Type :</td> 
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;"  align="left">
					   <a href="${attachment.documentURL}" onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;" class="text-primary">
					     <h6><u>${attachment.attachmentDesc}</u></h6>
					   </a>
					</td>
					<td style = "text-indent: 1em;;font-family: Arial, sans-serif;"   align="left">
					  <input id="fileInput(${attachment.attachmentTypeId})" type="file" name="theFile[${attachment.attachmentId}]" onclick="enableAndDisableUpload(${attachment.attachmentTypeId});" value="${attachment.attachmentTypeId}" /> 
					</td>
					<td align="left" style="font-family: Arial, sans-serif;"> 
					   <button id="attachmentTypeId(${attachment.attachmentTypeId})" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="btn btn-primary" disabled>Upload</button>
					</td>
				</tr>
            </logic:if>
           <tr> 
           	 <td colspan="4" style = "text-indent: 6em;margin-left:150px;font-family: Arial, sans-serif;font-weight: bold;" align="left"><strong> ${attachment.fileName} </strong></td> 
           </tr>
			<tr><td  colspan="4">&nbsp;</td></tr>
		</logic:forEach>
		</table>
	
		<div class="container-login100-form-btn m-t-32">		
		  <button type="button" class="btn btn-primary" onclick="back();">Back</button>&nbsp;
		  <button type="button" class="btn btn-primary" onclick="next();">Next</button>&nbsp;	
		  <button type="button" class="btn btn-primary" onclick="cancel();">Cancel</button>
 		</div>
</div>
				</form:form>
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>