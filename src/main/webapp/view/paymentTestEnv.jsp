<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.Map,java.util.HashMap" %>
<%@ page import="java.util.Random" %>
<%@ page import = "javax.servlet.RequestDispatcher" %>
<%@ page import="javax.crypto.Mac" %>
<%@ page import="javax.crypto.SecretKey" %>
<%@ page import="javax.crypto.spec.SecretKeySpec" %>

<%
Map<String, String> lkupSystemDataMap = new HashMap<String,String>();

lkupSystemDataMap = (Map<String,String>) request.getSession().getAttribute("lkupSystemDataMap");

System.out.println("lkupSystemDataMap :"+lkupSystemDataMap.size());

String boaURL = lkupSystemDataMap.get(Constants.ONLINE_BOA_URL_FOR_TEST_ENV);
String x_login = lkupSystemDataMap.get(Constants.ONLINE_BOA_X_LOGIN_FOR_TEST_ENV);
String transactionKey = lkupSystemDataMap.get(Constants.ONLINE_BOA_TRANSACTIONKEY_FOR_TEST_ENV); 
String boaXFpHash = lkupSystemDataMap.get(Constants.ONLINE_BOA_X_FP_HASH_FOR_TEST_ENV);
String formId = lkupSystemDataMap.get(Constants.ONLINE_BOA_FORM_ID_FOR_TEST_ENV);
String boaButtonCode = lkupSystemDataMap.get(Constants.ONLINE_BOA_BUTTON_CODE_FOR_TEST_ENV);
String prodServerFlag = lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG);

System.out.println("boaURL :"+boaURL);
System.out.println("x_login :"+x_login);
System.out.println("transactionKey :"+transactionKey);
System.out.println("boaXFpHash :"+boaXFpHash);
System.out.println("formId :"+formId);
System.out.println("boaButtonCode :"+boaButtonCode);

System.out.println("Permit Number  :"+((Activity)request.getSession().getAttribute("activityDetails")).getPermitNumber());

//String invoiceId=StringUtils.nullReplaceWithZero((String)request.getAttribute("invoiceId"));
// String invoiceId=addForm.getInvoiceId()+"";

// System.out.println(addForm.getInvoiceId());
//System.out.println(StringUtils.nullReplaceWithZero((String)request.getAttribute("invoiceId")));
String invoiceId=((Activity)request.getSession().getAttribute("activityDetails")).getTempId();
String feeAmount=StringUtils.nullReplaceWithZero((((Activity)request.getSession().getAttribute("activityDetails")).getTotalFee())); 
System.out.println("feeAmount :: "+ feeAmount + ", invoiceId :: "+invoiceId);

System.out.println("RenewalCodeIntValue :: "+ ((Activity)request.getSession().getAttribute("activityDetails")).getRenewalCodeIntValue());

// x_login and transactionKey should be taken from Payment Page settings
//String x_login        = "HCO-EDGES-853"; // aka Payment Page ID
//String transactionKey = "aX8l6TPNe0B6xuHouXIt"; // aka Transaction Key
String x_amount       = "0.50";
//String x_amount       = "0.50";
//addForm.getFeeManager().getFinanceSummary().getPermitFeeAmt();
String x_invoice_num = invoiceId;
// Generate a random sequence number
Random generator = new Random();
int x_fp_sequence = generator.nextInt(1000);

// Generate the timestamp
// Make sure this will be in UTC
long x_fp_timestamp = System.currentTimeMillis()/1000;

// Use Java Cryptography functions to generate the x_fp_hash value
// generate secret key for HMAC-SHA1 using the transaction key
SecretKey key = new SecretKeySpec(transactionKey.getBytes(), boaXFpHash);

// Get instance of Mac object implementing HMAC-SHA1, and
// Initialize it with the above secret key
Mac mac = Mac.getInstance(boaXFpHash);
mac.init(key);

// process the input string
String inputstring = x_login + "^" + x_fp_sequence + "^" +x_fp_timestamp + "^" + x_amount + "^"+"USD";
byte[] result = mac.doFinal(inputstring.getBytes());

// convert the result from byte[] to hexadecimal format
StringBuffer strbuf = new StringBuffer(result.length * 2);
for(int i=0; i< result.length; i++)
   {
       if(((int) result[i] & 0xff) < 0x10)
           strbuf.append("0");
       strbuf.append(Long.toString((int) result[i] & 0xff, 16));
   }
String x_fp_hash = strbuf.toString();
%>
<html>
<head>
 <title>City of Burbank</title>
 <style type="text/css">
   label {
      display: block;
      margin: 5px 0px;
      color: #AAA;
   }
   input {
      display: block;
   }
   input[type=submit] {
      margin-top: 20px;
   }

 </style>  
 <script>
 function postPayment(){
	 document.forms[0].action="<%=boaURL%>";
	 document.forms[0].submit();
	 
 }
 </script>
</head>
<body onload="postPayment();">
<form action="<%=boaURL%>" id="<%=formId%>" method="post">
  
 <input type="hidden" name="x_login" value="<%= x_login %>" />
 
 <input type="hidden" name="x_fp_sequence" value="<%= x_fp_sequence %>" />
 
 <input type="hidden" name="x_fp_timestamp" value="<%= x_fp_timestamp %>" />
 <input type="hidden" name="x_currency_code" value="USD" /> 

 <input type="hidden" name="x_amount" value="<%=x_amount%>"  />
 <input type="hidden" name="x_invoice_num" value="<%=x_invoice_num%>"  />
 
 <%if(prodServerFlag!=null && prodServerFlag.equalsIgnoreCase("N")){ %> 
 	<input type="hidden" name="x_test_request" value="TRUE" />
 <%}%>
 
 <input type="hidden" name="x_relay_response" value="" />
  <!-- <input type="hidden" name="x_relay_url" value="https://csi.glendaleca.gov/CLIPP_TEST/responsePayment.do" />-->
 
 <input type="hidden" name="donation_prompt" />
 
 <input type="hidden" name="button_code" value="<%=boaButtonCode%>" />

 <input type="hidden" name="x_fp_hash" value="<%=x_fp_hash %>" size="40"/>
 <input name="x_show_form" value="PAYMENT_FORM" type="hidden" />
 
 <input type="hidden" name="x_po_num" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getPermitNumber()%>" /> 
   
 <input type="hidden" name="renewalCode" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getRenewalCodeIntValue()%>">
 <input type="hidden" name="permitNumber" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getPermitNumber()%>">
 <input type="hidden" name="businessAccNo" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getBusinessAccNo()%>">
 <input type="hidden" name="tempId" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getTempId()%>">
 <input type="hidden" name="actType" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getActType()%>">
 <input type="hidden" name="totalFee" value="<%=((Activity)request.getSession().getAttribute("activityDetails")).getTotalFee()%>">

</form>
</body>
</html>