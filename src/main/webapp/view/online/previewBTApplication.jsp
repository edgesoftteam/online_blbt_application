<%@page import="com.elms.model.BusinessTaxActivityForm"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %> 
<!DOCTYPE html>
<html>
<head>
<title>City of Burbank New Business License / Business Tax</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/formValidation.js"></script> 
<script src="js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/addBLBT.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
<%
BusinessTaxActivityForm businessTaxActivityForm = new BusinessTaxActivityForm();
businessTaxActivityForm = (BusinessTaxActivityForm)request.getAttribute("businessTaxActivityForm");
session.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
System.out.println("name in preview... "+businessTaxActivityForm.getBusinessName());
%>
<script>
 $(document).on('contextmenu', function () {
 	return false;
 });
</script>
<script language="javascript" type="text/javascript">  
function save(){
	document.forms[0].action='${pageContext.request.contextPath}/saveBusinessTaxActivity';
	document.forms[0].submit();
	return true;
}

function cancel(){
	window.open("${pageContext.request.contextPath}/","_self");
}


</script>

</head>
<body class="container-login101" style="background-image: url('images/burbankCA.jpg');align-items:initial;" >
	<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;align:center;text-align:center;width: 100%;color:white;">
		City of Burbank <br/>Community Development Department<br/>${applicationType} Application Portal
	</div>
	<div class="container" style="max-width:1150px;width:75%;">
		  <html:form name="businessTaxActivityForm" method="post" autocomplete="off" class="well form-horizontal">
				<div class="card-header"><b> ${applicationType} Application</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
					<input type="submit" value="Save"  onclick="return save();" class="btn btn-primary" style="background-color:#265a88; width:140px;height:40px;" name="submit" id="submit" />
				</div>
			 	<div class="container" style="width:100%;">
		       		<table class="table" border="2" style="border:2px solid black;padding:0px;line-height:1.5px;font-size:16px;">
		          		<tbody>
				           <tr>
				             <td>
				                <table class="table " border="1">
									<tr>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Is Business located in the City of Burbank?</label></td>
										<td width="25%" style="padding:0px;">
											<div class="preview-label">
												<logic:choose>
												<logic:when test= "${businessTaxActivityForm.businessLocation == 'false'}">No</logic:when>
												<logic:otherwise>Yes</logic:otherwise>
												</logic:choose>
											 </div>
										</td>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>E-mail Address</label></td>
										<td width="25%" style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.emailAddress}</div></td>
									</tr>
			                        <tr>
			                        	<logic:choose>
											<logic:when test= "${businessTaxActivityForm.businessLocation == 'false'}">
												<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Out of Town Address</label></td>
												<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.outOfTownStreetNumber}${businessTaxActivityForm.outOfTownStreetName} ${businessTaxActivityForm.outOfTownUnitNumber}, ${businessTaxActivityForm.outOfTownCity} ${businessTaxActivityForm.outOfTownState} ${businessTaxActivityForm.outOfTownZip}, ${businessTaxActivityForm.outOfTownZip4}</div></td>
											</logic:when>
											<logic:otherwise>
						                         <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Address</label></td>
						                         <td style="padding:0px;"><div class="preview-label" >${businessTaxActivityForm.businessAddressStreetNumber} ${businessTaxActivityForm.businessAddressStreetFraction} ${businessTaxActivityForm.businessAddressStreetNameDesc} 
						                        	${businessTaxActivityForm.businessAddressUnitNumber} ${businessTaxActivityForm.businessAddressCity} ${businessTaxActivityForm.businessAddressState} ${businessTaxActivityForm.businessAddressZip} ${businessTaxActivityForm.businessAddressZip4}</div>
						                         </td>
											</logic:otherwise>
										</logic:choose>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Business Phone, Ext, Fax</label></td>
				                        <td style="padding:0px;">
					                        <div class="preview-label">${businessTaxActivityForm.businessPhone}
					                            <logic:if test="${not empty businessTaxActivityForm.businessExtension}">, ${businessTaxActivityForm.businessExtension}</logic:if>
					                            <logic:if test="${not empty businessTaxActivityForm.businessFax}">, ${businessTaxActivityForm.businessFax}</logic:if>
				                            </div>
				                        </td>
									</tr>
									<tr>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Business Name</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.businessName}</div></td>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Activity Sub-Type</label></td>
		                            	<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.activitySubTypeDesc}</div></td>
			                        </tr>
			                        <tr>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Description of Business</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.descOfBusiness}</div></td>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Corporate Name</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.corporateName}</div></td>
			                        </tr>
			                        <tr>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Activity Type</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.activityTypeDescription}</div></td>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Quantity</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.quantity} ${businessTaxActivityForm.quantityNum}</div></td>
			                        </tr>
		                          	<tr>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Federal ID Number</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.federalIdNumber}</div></td>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Driver's License</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.driverLicense}</div></td>
		                            </tr>
		                            <tr>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Ownership Type</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.ownershipTypeDesc}</div></td>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Social Security Number</label></td>
			                            <td style="padding:0px;">
				                            <logic:choose>
				                            	<logic:when test="${not empty businessTaxActivityForm.socialSecurityNumber}">
				                            		<div class="preview-label">XXX-XX-XXXX</div>
				                            	</logic:when>
				                            	<logic:otherwise>
				                            		<div class="preview-label">${businessTaxActivityForm.socialSecurityNumber}</div>
				                            	</logic:otherwise>
						             		</logic:choose>
						             	</td>
		                         	</tr>
		                         	<tr>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Square Footage(Area Occupied)</label></td>
			                            <td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.squareFootage}</div></td>
			                            <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Decal Code</label></td>
			                            <td style="padding:0px;">
			                            	<div class="preview-label">
				                            	<logic:choose>
													<logic:when test= "${businessTaxActivityForm.decalCode == 'false'}">No</logic:when>
													<logic:otherwise>Yes</logic:otherwise>
												</logic:choose>
											</div>
										</td>
									</tr>
			                        <tr>
			                         	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Are there any other businesses occupying this premise?</label></td>
			                           	<td style="padding:0px;">
			                           		<div class="preview-label">
				                            	<logic:choose>
													<logic:when test= "${businessTaxActivityForm.otherBusinessOccupancy == 'false'}">No</logic:when>
													<logic:otherwise>Yes</logic:otherwise>
												</logic:choose>
											</div>
										</td>
										<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Home Occupation</label></td>
			                            <td style="padding:0px;">
			                            	<div class="preview-label">
			                            		<logic:choose>
													<logic:when test= "${businessTaxActivityForm.homeOccupation == 'false'}">No</logic:when>
													<logic:otherwise>Yes</logic:otherwise>
												</logic:choose>
											</div>
										</td>
									</tr>
								</table>
							 </td>
						   </tr>
						   <tr>
							 <td colspan="4">
								<logic:forEach var="multiAddr" items="${multiAddress}" varStatus="vs">	  
									<input type="hidden" name="multiAddress[${vs.index}].addressType" value="${multiAddr.addressType}" />
	               					<table class="table " border="1" style="width:100%;">
										<tbody>
											<tr>
												<td colspan="4"><div class="card-header">${multiAddr.addressType}</div></td>
											</tr>
									        <tr>
									        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Name</label></td>
									        	<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].name}</div></td>
									        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Title</label></td>
									        	<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].title}</div></td>
									        </tr>
									        <tr>
									        	<td  width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Street Number, Street Name1</label></td>
									        	<td width="25%" style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].streetNumber} ${businessTaxActivityForm.multiAddress[vs.index].streetName1}</div></td>
									        	<td  width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Unit, City, State</label></td>
												<td width="25%"  style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].unit} ${businessTaxActivityForm.multiAddress[vs.index].city} ${businessTaxActivityForm.multiAddress[vs.index].state}</div></td>
									        </tr>
									        <tr>
									        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Street Name2</label></td>
												<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].streetName2}</div></td>
												<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Zip, Zip4, Attn</label></td>
												<td style="padding:0px;"><div class="preview-label">${businessTaxActivityForm.multiAddress[vs.index].zip} ${businessTaxActivityForm.multiAddress[vs.index].zip4} ${businessTaxActivityForm.multiAddress[vs.index].attn}</div></td>
									        </tr>
										</tbody>
									</table>
								</logic:forEach>
							 </td>
						   </tr>
					</table>
		     	</div>				
				<div class="text-center">
					<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
					<input type="submit" value="Save"  onclick="return save();" class="btn btn-primary" style="background-color:#265a88; width:140px;height:40px;" name="submit" id="submit" />
				</div>
		  </html:form>
	  </div>
</body>
</html>
