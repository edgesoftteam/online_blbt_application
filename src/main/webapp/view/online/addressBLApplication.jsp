<%@page import="java.util.ArrayList"%>
<%@page import="com.elms.model.MultiAddress"%>
<%@page import="com.elms.model.BusinessLicenseActivityForm"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %> 
<!DOCTYPE html>
<html>
<head>
<title>City of Burbank New Business License  / Business Tax</title>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<script src="js/sweetalert.min.js"></script> 
<script src="js/sweetalert.js"></script>
<script src="js/formValidation.js"></script> 
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="dateTimePicker/jquery.js"></script> 
<script src="dateTimePicker/jquery.datetimepicker.full.js"></script> 
<script src="js/skel.min.js"></script>
<link rel="stylesheet" type="text/css" href="dateTimePicker/jquery.datetimepicker.css">
<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/addBLBT.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
<script src="js/8d29ef114b.js" crossorigin="anonymous"></script>

<style>
.form-horizontal .form-group {
    margin-right: -100px;
    margin-left: -100px;
}

.form-horizontal .control-label
{
padding-top:0px;
}
.input-group .form-control {
    width: 72%;
    font-size: 13px;
    height:25px;
    }
.input-group-addon{
    padding:3px 9px;
    }
.form-group{
margin-bottom:9px;
}
control-label{
    font-size: 11px;
}    
label{
    font-size: 11px;
}    
</style>
<script language="javascript" type="text/javascript">    
<%
BusinessLicenseActivityForm businessLicenseActivityForm = new BusinessLicenseActivityForm();
businessLicenseActivityForm = (BusinessLicenseActivityForm)request.getAttribute("businessLicenseActivityForm");
session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
System.out.println("businessLicenseActivityForm... "+businessLicenseActivityForm.toString());

List<MultiAddress> multiAddressList = new ArrayList<MultiAddress>();
multiAddressList = (List<MultiAddress>)request.getAttribute("multiAddress");
int size =multiAddressList.size();
System.out.println("address list... "+size);
%>
  
function preview(){
	var length=<%=size%>;
	for(var i=0;i<length;i++){
		if(document.forms[0].elements['multiAddress['+i+'].streetNumber'].value != "" || document.forms[0].elements['multiAddress['+i+'].streetName1'].value!="" 
			 || document.forms[0].elements['multiAddress['+i+'].unit'].value == !"" || document.forms[0].elements['multiAddress['+i+'].city'].value!="" 
			   || document.forms[0].elements['multiAddress['+i+'].state'].value == !"" || document.forms[0].elements['multiAddress['+i+'].zip'].value!="" ){
			
			
			if(document.forms[0].elements['multiAddress['+i+'].streetNumber'].value == ""){
				document.forms[0].elements['multiAddress['+i+'].streetNumber'].focus();
				swal("","Please enter Street Number ");
				return false;
			}else if(document.forms[0].elements['multiAddress['+i+'].streetName1'].value==""){
				document.forms[0].elements['multiAddress['+i+'].streetName1'].focus();
				swal("","Please enter Street Name 1");
				return false;
			}
			else if(document.forms[0].elements['multiAddress['+i+'].city'].value == ""){
				document.forms[0].elements['multiAddress['+i+'].city'].focus();
				swal("","Please enter City ");
				return false;
			}else if(document.forms[0].elements['multiAddress['+i+'].state'].value == ""){
				document.forms[0].elements['multiAddress['+i+'].state'].focus();
				swal("","Please enter State ");
				return false;
			}else if(document.forms[0].elements['multiAddress['+i+'].zip'].value == ""){
				document.forms[0].elements['multiAddress['+i+'].zip'].focus();
				swal("","Please enter Zipcode ");
				return false;
			}			
		}
	}
	for(var i=0;i<length;i++){
		if(document.forms[0].elements['multiAddress['+i+'].streetNumber'].value != ""){
			if(!isASCII('multiAddress['+i+'].streetNumber')){
				document.forms[0].multiAddress['+i+'].streetNumber.value="";
				document.forms[0].multiAddress['+i+'].streetNumber.focus();
				swal("","Only ASCII characters allowed for Street Number");
				return false;
			}
		}

		if(document.forms[0].elements['multiAddress['+i+'].streetName1'].value != ""){
			if(!isASCII('multiAddress['+i+'].streetName1')){
				document.forms[0].multiAddress['+i+'].streetName1.value="";
				document.forms[0].multiAddress['+i+'].streetName1.focus();
				swal("","Only ASCII characters allowed for Street Name1");
				return false;
			}
		}
		if(document.forms[0].elements['multiAddress['+i+'].streetName2'].value != ""){
			if(!isASCII('multiAddress['+i+'].streetName2')){
				document.forms[0].multiAddress['+i+'].streetName2.value="";
				document.forms[0].multiAddress['+i+'].streetName2.focus();
				swal("","Only ASCII characters allowed for Street Name2");
				return false;
			}
		}

		if(document.forms[0].elements['multiAddress['+i+'].city'].value != ""){
			if(!isASCII('multiAddress['+i+'].city')){
				document.forms[0].multiAddress['+i+'].city.value="";
				document.forms[0].multiAddress['+i+'].city.focus();
				swal("","Only ASCII characters allowed for City");
				return false;
			}
		}

		if(document.forms[0].elements['multiAddress['+i+'].state'].value != ""){
			if(!isASCII('multiAddress['+i+'].state')){
				document.forms[0].multiAddress['+i+'].state.value="";
				document.forms[0].multiAddress['+i+'].state.focus();
				swal("","Only ASCII characters allowed for State");
				return false;
			}
		}

		if(document.forms[0].elements['multiAddress['+i+'].zip'].value != ""){
			if(!isASCII('multiAddress['+i+'].zip')){
				document.forms[0].multiAddress['+i+'].zip.value="";
				document.forms[0].multiAddress['+i+'].zip.focus();
				swal("","Only ASCII characters allowed for Zip");
				return false;
			}
		}

		if(document.forms[0].elements['multiAddress['+i+'].zip4'].value != ""){
			if(!isASCII('multiAddress['+i+'].zip4')){
				document.forms[0].multiAddress['+i+'].zip4.value="";
				document.forms[0].multiAddress['+i+'].zip4.focus();
				swal("","Only ASCII characters allowed for Zip4");
				return false;
			}
		}
	}
	document.forms[0].action='${pageContext.request.contextPath}/previewBLPage';
	document.forms[0].submit();
	return true;
}

function cancel(){
	window.open("${pageContext.request.contextPath}/","_self");
}

</script>
</head>
<body class="container-login101" style="background-image: url('images/burbankCA.jpg');align-items:initial;">
	<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;align:center;text-align:center;width: 100%;color:white;">
		City of Burbank <br/>Community Development Department<br/>${applicationType} Application Portal
	</div>
	<div class="container" style="width: 65%;">	 
		 <div id="accordion">
			<html:form name="businessLicenseActivityForm" method="post" autocomplete="off" class="well form-horizontal">
				<div class="card-header" style="width:100%;"><b> ${applicationType} Application</b></div>
				<div class="container" style="width:100%;">
					<logic:forEach var="multiAddr" items="${multiAddress}" varStatus="vs">	  
						<input type="hidden" name="multiAddress[${vs.index}].addressType" value="${multiAddr.addressType}" />
						<table class="table">
							<tbody>
								<tr>
									<td colspan="2"><div class="card-header">${multiAddr.addressType}</div></td>
								</tr>
						        <tr>
						           <td colspan="1" width="50%">
							           <fieldset>
											<div class="form-group">
												<label class="col-md-5 control-label">Street Number</label>
												<div class="col-md-7 inputGroupContainer">
											    	<div class="input-group"><span class="input-group-addon"><i class="fas fa-address-book"></i></span><input type="text" size="5" class="form-control col-xs-2" placeholder="Street Number" id="multiAddress[${vs.index}].streetNumber" name="multiAddress[${vs.index}].streetNumber" onkeypress="return validateInteger()" maxlength="5"></div>
											    </div>
											</div>
											<div class="form-group">
											   <label class="col-md-5 control-label">Street Name2</label>
											   <div class="col-md-7 inputGroupContainer">
											      <div class="input-group"><span class="input-group-addon"><i class="fas fa-address-book"></i></span><input type="text" size="5" class="form-control col-xs-2" placeholder="Street Name2" id="multiAddress[${vs.index}].streetName2" name="multiAddress[${vs.index}].streetName2" maxlength="100"></div>
											   </div>
											 </div>
											<div class="form-group">
											     <label class="col-md-5 control-label">City, State</label>
											     <div class="col-md-7 inputGroupContainer">
											        <div class="input-group"><span class="input-group-addon"><i class="fas fa-city"></i></span><input type="text" style="width:36%;" size="7" class="form-control col-xs-3" id="multiAddress[${vs.index}].city" name="multiAddress[${vs.index}].city" placeholder="City" maxlength="15" onkeypress="return isASCII(this)">
											        	<input type="text" style="width:36%;" size="2"  maxlength="2" placeholder="State" class="form-control col-xs-2" id="multiAddress[${vs.index}].state" name="multiAddress[${vs.index}].state" onkeypress="return NonNumericEntry()" onkeypress="return checkLength('state');">
										        	</div>
											   </div>
											</div>
							           </fieldset>
						           </td>
						    	   <td colspan="1" width="50%">
						         	   <fieldset>
											<div class="form-group">
											   <label class="col-md-5 control-label">Street Name1</label>
											   <div class="col-md-7 inputGroupContainer">
											      <div class="input-group"><span class="input-group-addon"><i class="fas fa-address-book"></i></span><input type="text" size="5" class="form-control col-xs-2" placeholder="Street Name1" id="multiAddress[${vs.index}].streetName1" name="multiAddress[${vs.index}].streetName1" maxlength="100"></div>
											   </div>
											</div>     
											<div class="form-group">
										      <label class="col-md-5 control-label">Unit, Attn</label>
										      <div class="col-md-7 inputGroupContainer">
										         <div class="input-group">
										         	<span class="input-group-addon"><i class="fas fa-address-book"></i></span><input type="text" style="width:36%;" size="5" class="form-control col-xs-2" placeholder="Unit" id="multiAddress[${vs.index}].unit" name="multiAddress[${vs.index}].unit" maxlength="10">
										         	<input type="text" size="5" style="width:36%;" maxlength="4" class="form-control col-xs-2" id="multiAddress[${vs.index}].attn" placeholder="Attn" name="multiAddress[${vs.index}].attn" onkeypress="return validateInteger()">
										         </div>
											   </div>
											</div>
											<div class="form-group">
											   <label class="col-md-5 control-label">Zip, Zip4</label>
											   <div class="col-md-7 inputGroupContainer">
											      <div class="input-group">
												      <span class="input-group-addon"><i class="fas fa-address-book"></i></span><input type="text" style="width:36%;" size="5" maxlength="5" placeholder="Zip" class="form-control col-xs-3" id="multiAddress[${vs.index}].zip" name="multiAddress[${vs.index}].zip" onkeypress="return validateInteger()">
												      <input type="text" style="width:36%;" size="5" maxlength="4" class="form-control col-xs-2" id="multiAddress[${vs.index}].zip4" placeholder="Zip4" name="multiAddress[${vs.index}].zip4" onkeypress="return validateInteger()">
												  </div>
											   </div>
											</div>
									   </fieldset>
								   </td>
								</tr>
							</tbody>
						</table>
					</logic:forEach>
				</div>	
				<div class="text-center">
				<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
				<input type="submit" value="Preview"  onclick="return preview();" class="btn btn-primary" style="background-color:#265a88; width:140px;height:40px;" name="submit" id="submit" />
				</div> 
			</html:form>
		  </div>
	</div>   
</body>
</html>
