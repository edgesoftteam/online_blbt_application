<%@page import="com.elms.model.BusinessLicenseActivityForm"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %> 
<!DOCTYPE html>
<html>
<head>
<title>City of Burbank New Business License  / Business Tax</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/formValidation.js"></script> 
<script src="js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/addBLBT.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
<%
BusinessLicenseActivityForm businessLicenseActivityForm = new BusinessLicenseActivityForm();
businessLicenseActivityForm = (BusinessLicenseActivityForm)request.getAttribute("businessLicenseActivityForm");
session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
System.out.println("name... "+businessLicenseActivityForm.getBusinessName());
%>
<style>
body{
font-size:12px;
}

label{
font-size:12px;
}
table tbody tr td{
padding:0px;
}
</style>
<script>
 $(document).on('contextmenu', function () {
 	return false;
 });
</script>
<script language="javascript" type="text/javascript">  
function save(){
	document.forms[0].action='${pageContext.request.contextPath}/saveBusinessLicenseActivity';
	document.forms[0].submit();
	return true;
}
function cancel(){
	window.open("${pageContext.request.contextPath}/","_self");
}
</script>

</head>
<body class="container-login101" style="background-image: url('images/burbankCA.jpg');align-items:initial;" >
	<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;align:center;text-align:center;width: 100%;color:white;">
		City of Burbank <br/>Community Development Department<br/>${applicationType} Application Portal
	</div>
	<div class="container" style="max-width:1100px;width:90%;">
		<html:form name="businessLicenseActivityForm" method="post" autocomplete="off" class="well form-horizontal">
			<div class="card-header"><b> ${applicationType} Application</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
				<input type="submit" value="Save"  onclick="return save();" class="btn btn-primary" style="background-color:#265a88; width:140px;height:40px;" name="submit" id="submit" />
			</div>
			<div class="container" style="width:100%;padding:0px;">
		    	<table class="table" border="2" style="border:2px solid black;padding:0px;line-height:1.5px;font-size:16px;">
		        	<tr>       
		            	<td>
							<table class="table" border="1" >
				            	<tr>
					             	<td  width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>Is Business located in the City of Burbank?</label></td>
				             		<td width="25%" style="padding:0px;"><div class="preview-label" >
		                        		<logic:choose>
											<logic:when test= "${businessLicenseActivityForm.businessLocation == 'false'}">No</logic:when>
											<logic:otherwise>Yes</logic:otherwise>
										</logic:choose></div>
				             		</td>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label>E-mail Address</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.emailAddress}</div></td>
				             	</tr>
				              	<tr>
				            		<logic:choose>
										<logic:when test= "${businessLicenseActivityForm.businessLocation == 'false'}">
											<td width="25%" style="background-color:#C0C0C0;"><label >Out of Town Address</label></td>
											<td style="padding:0px;">	<div class="preview-label">${businessLicenseActivityForm.outOfTownStreetNumber} ${businessLicenseActivityForm.outOfTownStreetName} ${businessLicenseActivityForm.outOfTownUnitNumber}, ${businessLicenseActivityForm.outOfTownCity} ${businessLicenseActivityForm.outOfTownState} ${businessLicenseActivityForm.outOfTownZip}, ${businessLicenseActivityForm.outOfTownZip4}</div></td>
										</logic:when>
										<logic:otherwise>
					                        <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Address</label></td>
					                        <td style="padding:0px;">	<div class="preview-label" >${businessLicenseActivityForm.addressStreetNumber} ${businessLicenseActivityForm.addressStreetFraction} ${businessLicenseActivityForm.addressStreetNameDesc} ${businessLicenseActivityForm.addressUnitNumber} ${businessLicenseActivityForm.addressCity} ${businessLicenseActivityForm.addressState} ${businessLicenseActivityForm.addressZip} ${businessLicenseActivityForm.addressZip4}</div></td>
										</logic:otherwise>
									</logic:choose>
				             		<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Activity Sub-Type</label></td>
				             		<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.activitySubTypeDesc}</div></td>
				             	</tr>
				             	<tr>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Business Name</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.businessName}</div></td>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Business Phone, Ext, Fax</label></td>
					             	<td style="padding:0px;">
			                            <div class="preview-label">${businessLicenseActivityForm.businessPhone}
				                            <logic:if test="${not empty businessLicenseActivityForm.businessExtension}">, ${businessLicenseActivityForm.businessExtension}</logic:if>
				                            <logic:if test="${not empty businessLicenseActivityForm.businessFax}">, ${businessLicenseActivityForm.businessFax}</logic:if>
			                            </div>
					             	</td>
				             	</tr>
					            <tr>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Description of Business</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.descOfBusiness}</div></td>
			                        <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Corporate Name</label></td>
			                        <td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.corporateName}</div></td>
				             	</tr>
				             	<tr>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Activity Type</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.activityTypeDescription}</div></td>
				             	    <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Quantity</label></td>
		                            <td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.quantity} ${businessLicenseActivityForm.quantityNum}</div></td>
								</tr>				        
				             	<tr>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >No of Days for Renewal</label></td>
			                        <td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.renewalDt}</div></td>
			                        <td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Starting Date</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.startingDate}</div></td>    
				             	</tr>
					            <tr>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Ownership Type</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.ownershipTypeDesc}</div></td>
					             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Federal ID Number</label></td>
					             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.federalIdNumber}</div></td>
					            </tr>
				             <tr>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Insurance Expiration Date</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.insuranceExpDate}</div></td>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Social Security Number</label></td>
				             	<td style="padding:0px;">
				             		<logic:choose>
		                            	<logic:when test="${not empty businessLicenseActivityForm.socialSecurityNumber}"><div class="preview-label">XXX-XX-XXXX</div></logic:when>
		                            	<logic:otherwise><div class="preview-label"></div></logic:otherwise>
				             		</logic:choose>
				             	</td>
				             </tr>
				             <tr>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Square Footage(Area Occupied)</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.squareFootage}</div></td>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Bond Expiration Date</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.bondExpDate}</div></td>
				             </tr>
				             <tr>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Type of Exemptions</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.typeOfExemptionsDesc}</div></td>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Federal Firearms License Expiration Date</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.federalFirearmsLiscExpDate}</div></td>
				             </tr>    
				             <tr>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Home Occupation</label></td>
				             	<td style="padding:0px;"><div class="preview-label">
				             		<logic:choose>
										<logic:when test= "${businessLicenseActivityForm.homeOccupation == 'false'}">No</logic:when>
										<logic:otherwise>Yes</logic:otherwise>
									</logic:choose></div>
				             	</td>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Decal Code</label></td>
				             	<td style="padding:0px;"><div class="preview-label">
		                           <logic:choose>
										<logic:when test= "${businessLicenseActivityForm.decalCode == 'false'}">No</logic:when>
										<logic:otherwise>Yes</logic:otherwise>
									</logic:choose></div>
				             	</td>
				             </tr>
				             <tr>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"> <label >Department of Justice Expiration Date</label></td>
				             	<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.deptOfJusticeExpDate}</div></td>
				             	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"> </td>
				             	<td></td>
				             </tr>
				        </table>
					</td>
		           </tr>
		           <tr>
		           	<td colspan="4">
						<logic:forEach var="multiAddr" items="${multiAddress}" varStatus="vs">	  
							<input type="hidden" name="multiAddress[${vs.index}].addressType" value="${multiAddr.addressType}" />
							<table class="table" border="1" style="padding:0px;">
								<tr>
									<td colspan="4"><div class="card-header">${multiAddr.addressType}</div></td>
								</tr>
						        <tr>
						        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Street Number, Street Name1</label></td>
						        	<td width="25%" style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.multiAddress[vs.index].streetNumber} ${businessLicenseActivityForm.multiAddress[vs.index].streetName1}</div></td>
						        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Unit, City, State</label></td>
									<td width="25%" style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.multiAddress[vs.index].unit} ${businessLicenseActivityForm.multiAddress[vs.index].city} ${businessLicenseActivityForm.multiAddress[vs.index].state}</div></td>
						        </tr>
						        <tr>
						        	<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Street Name2</label></td>
									<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.multiAddress[vs.index].streetName2}</div></td>
									<td width="25%" style="background-color:#C0C0C0;padding:0 0.5em 0 0.5em;"><label >Zip, Zip4, Attn</label></td>
									<td style="padding:0px;"><div class="preview-label">${businessLicenseActivityForm.multiAddress[vs.index].zip} ${businessLicenseActivityForm.multiAddress[vs.index].zip4} ${businessLicenseActivityForm.multiAddress[vs.index].attn}</div></td>
						        </tr>
							</table>
						</logic:forEach>
		             </td>
		           </tr>
		         </tbody>
		      </table>
		    </div>
			<div class="text-center">
				<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
				<input type="submit" value="Save"  onclick="return save();" class="btn btn-primary" style="background-color:#265a88; width:140px;height:40px;" name="submit" id="submit" />
			</div>
		</html:form>
	</div>
</body>
</html>
