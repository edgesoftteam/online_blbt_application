<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ page import="com.elms.model.*,com.elms.util.StringUtils" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script>
/*All the scripts should be here in one place and not scattered all over the file*/
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
   {
	   swal("Error initializing XMLHttpRequest!");
   }
//End Ajax functionality for Activity Status populating values to the fields


function validateEmail(sEmail) {

	var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
	
	if(!sEmail.match(reEmail)) {
		  document.forms[0].elements['emailAddress'].value = "";
		  document.forms[0].elements['emailAddress'].focus();
		  swal("","Invalid email address");
		  return false;
	}
	return true;
}
function validate(){
	var email=document.forms[0].elements['emailAddress'].value;
	var applicationType=document.forms[0].elements['applicationType'].value;
	if(document.forms[0].elements['emailAddress'].value == "" || document.forms[0].elements['emailAddress'].value == null){
		  document.forms[0].elements['emailAddress'].focus();
		swal("","Please enter the Email Address");
		return false;
// 	}else if(document.forms[0].elements['emailAddress'].value != ""){
// 		validateEmail(document.forms[0].elements['emailAddress'].value);
	}else{
		var url="${pageContext.request.contextPath}/emailVerification?emailAddr="+email+"&applicationType="+applicationType+"&action=emailCheck";
	    xmlhttp.open('POST', url,false);
	    xmlhttp.onreadystatechange = HandleResponseSecurityCode;
	    xmlhttp.send(null);
	}
}

function HandleResponseSecurityCode(){
	// To make sure valid response is received from the server, 200 means response received is OK
	var emailAddr = document.forms[0].elements['emailAddress'].value;
	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
		var result=xmlhttp.responseText;
		var text=result.split(',');
		if(text[1] != null && text[1] != 0 && text[1] != -1){
			swal({
				  title: " ",
				  text: "Enter the code we just sent to "+emailAddr+" ",
				  type: "input",
				  maxlength:8,
				  showCancelButton: true,
				  closeOnConfirm: false,
				  animation: "slide-from-top",
				  inputPlaceholder: "Security code"
				}, function (inputValue) {
					if (inputValue === false){
					    swal.showInputError("","Enter valid security code");
					  return false;
				  }
				  if (inputValue === "") {
				    swal.showInputError("","Enter valid security code");
				    return false;
				  }
				
				  if(inputValue.length == 8){
					  if(inputValue === text[1]){
						document.forms[0].action = "${pageContext.request.contextPath}/emailVerification?action=initialOne";
						document.forms[0].submit();
					}else{
						swal.showInputError("","Please enter valid security code sent the above email address" );	
					    return false;
					}
				  }else{
					swal.showInputError("","Please enter valid security code sent to above email address" );	
				    return false;				  
				  }
				  
				});
		}else  if(text[1] == -1){
			document.forms[0].action = "${pageContext.request.contextPath}/emailVerification?action=initialOne";
			document.forms[0].submit();
		}else{ 
			swal(text[0]);			
		} 
	}else{
		swal("","There was a problem retrieving data from the server." );
	}
}
function goBack(){
	document.forms[0].action = "${pageContext.request.contextPath}/";
	document.forms[0].submit();
}
</script>

<head>
<title>City of Burbank New Business License  / Business Tax </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/email.css">
<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">
<script src="js/popper/popper-1.12.9.min.js" ></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>   
<script src="js/sweetalert.min.js"></script>  
<script src="js/sweetalert.js"></script>
</head>
<body>
<div class="limiter">
	<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
		<div class=" p-t-10 p-b-10">
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
				City of Burbank <br/>Community Development Department <br> Business License / Business Tax Application Portal
			</div>
			<div class="wrap-login100 p-t-10 p-b-10 center" style="background:white;">
				<html:form name="businessLicenseActivityForm"  cssClass="login100-form validate-form p-b-33 p-t-5" method="post" style="max-width:500px;margin:auto" >	
					<div>&nbsp;</div>
					<div class="form-group ">
						<div class="input-group">
							<i class="fa fa-envelope icon"></i> 
							<input class="form-control input-field" id="emailAddress" placeholder="Email Address" name="emailAddress" type="email" onchange="validateEmail(this.value);" required/>
							 <div class="invalid-feedback">
						        Please provide a valid email.
						    </div>
						</div>
					</div>
					<div class="form-group ">				 
						<select class="form-control" name="applicationType" id="applicationType" size="1">
					        <logic:forEach var="applicationType" items="${applicationTypes}">
								<option value="${applicationType.id}">${applicationType.description}</option>
							</logic:forEach>				        				        
					    </select>
					</div>
					<div class="container-login100-form-btn m-t-20"></br></br>
						<button type="button" class="btn btn-primary button" onclick="return goBack();">Back</button><br/><br/>&nbsp;
						<button type="button" class="btn btn-primary button" onclick="return validate();">Next</button>
					</div>				
				</html:form>			
			</div>
	    </div>
	</div>
 </div>
</body>
</html>