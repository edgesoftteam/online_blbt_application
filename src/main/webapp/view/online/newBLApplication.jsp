<%@page import="com.elms.repository.CommonRepository"%>
<%@page import="com.elms.util.StringUtils"%>
<%@page import="com.elms.model.ActivityStatus"%>
<%@page import="com.elms.model.ActivityType"%>
<%@page import="com.elms.model.Street"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>City of Burbank New Business License</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/sweetalert.js"></script>
<script src="js/actb.js"></script>
<script src="js/common.js"></script>
<script src="js/formValidation.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/kendo.all.min.js"></script>
<link rel="stylesheet" href="css/kendo.default-v2.min.css" />
<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/addBLBT.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css"	type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
<script src="js/bootstrap/bootstrap.min.js"></script>
<script language="JavaScript" src="js/select2.min.js"></script>
<link rel="stylesheet" href="css/select2.min.css" type="text/css">
<script language="JavaScript" src="js/respond.min.js"></script>
<script src="js/8d29ef114b.js" crossorigin="anonymous"></script>

<style>
.form-horizontal .form-group {
    margin-right: -80px;
    margin-left: -45px;
}
.input-group .form-control {
    width: 72%;
    font-size: 13px;
    height:25px;
    }
.input-group-addon{
    padding:3px 9px;
    }
.form-group{
margin-bottom:9px;
}
control-label{
    font-size: 11px;
}    
label{
    font-size: 11px;
}    
</style>
<script type="text/javascript">

$(document).ready(function() {
	$('.js-example-basic-single').select2();
});
$(document).on('contextmenu', function () {
 	return false;
});
</script>
<script language="javascript" type="text/javascript">  
function CreateXmlHttp(str){
	//Creating object of XMLHTTP in IE
		var url="${pageContext.request.contextPath}/addBusinessLicenseActivity?activityType="+str;		
		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7,Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		}else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		if (xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityType;
		    xmlhttp.send(null);
		  }else{
		  	swal("","Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityType(){
	// To make sure receiving response data from server is completed

	var quantity = document.getElementById("qty");
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
			var text=xmlhttp.responseText;
			var length = text.length;
			text=text.split(',');
			if(length <= 50){
				if(text[3] != null){
					quantity.style.display="block";	
					document.forms[0].quantity.value = text[3];
				}else{
					quantity.style.display="none";
					document.forms[0].quantity.value = "";
				}
			}
			if(document.forms[0].activityType.value==""){
				quantity.style.display="none";
				document.forms[0].quantity.value = "";
			}
		}else{
			swal("","There was a problem retrieving data from the server." );
		}
	}
}

function checkLocation()
{
	var ba1 = document.getElementById("adField1");
	var ba2 = document.getElementById("adField2");
	var oot1 = document.getElementById("ootField1");
	var oot2 = document.getElementById("ootField2");

  	if(document.forms[0].businessLocation.checked == true){
	   ba1.style.display="block";
		ba2.style.display="block";
		oot1.style.display="none";
		oot2.style.display="none";

		document.forms[0].outOfTownStreetNumber.value="";
		document.forms[0].outOfTownStreetName.value="";
		document.forms[0].addressStreetNumber.focus();
	}else{
	   	ba1.style.display="none";
		ba2.style.display="none";
		oot1.style.display="block";
		oot2.style.display="block";
	    document.forms[0].addressStreetNumber.value="";
		document.forms[0].addressStreetName.value="";
		document.forms[0].outOfTownStreetNumber.focus();
	}
}


var streetNameArray = new Array();

function next(){
	if(document.forms[0].businessLocation.checked == false){
		if(document.forms[0].outOfTownStreetName.value==""){
			document.forms[0].outOfTownStreetName.focus();
			swal("","Please enter value for Out Of Town Street Name");
			return false;
		}else if(document.forms[0].outOfTownCity.value==""){
			document.forms[0].outOfTownCity.focus();
			swal("","Please enter value for City ");
			return false; 
		}else if(document.forms[0].outOfTownState.value==""){
			document.forms[0].outOfTownState.focus();
			swal("","Please enter value for State");
			return false;
		}else if(document.forms[0].outOfTownZip.value==""){
			document.forms[0].outOfTownZip.focus();
			swal("","Please enter value for Zip");
			return false;
		}
	}else{
		if(document.forms[0].addressStreetNumber.value == ""){
			document.forms[0].addressStreetNumber.focus();
			swal("","Please enter Address Street Number");
			return false;
		}else if(isNaN(document.forms[0].addressStreetNumber.value)){
			document.forms[0].addressStreetNumber.focus();
			swal("","Please enter numeric value for Address Street Number");
			return false;
		}else if(document.forms[0].addressStreetName.value == ""){
			document.forms[0].addressStreetName.focus();
			swal("","Please select Address Street Name");
			return false;
		}else if(document.forms[0].addressZip.value == ""){
			document.forms[0].addressZip.focus();
			swal("","Please enter Zip Code");
			return false;
		}
	}
	if(document.forms[0].businessName.value == ""){
		document.forms[0].businessName.focus();
		swal("","Please enter Business Name");
		return false;
	}else if(document.forms[0].activityType.value == ""){
		document.forms[0].activityType.focus();
		swal("","Please select Activity Type");
		return false;
	}	
	if($('#qty').css('display') == 'block')
	{
		if(document.forms[0].quantityNum.value==""){
			document.forms[0].quantityNum.focus();
			swal("","Please enter Quantity Number");
			return false;
		}
	}
    if(document.forms[0].elements['startingDate'].value != '' && !isDate(document.forms[0].elements['startingDate'].value)){
    	document.forms[0].elements['startingDate'].focus();
		return false;
	}

    if(document.forms[0].elements['insuranceExpDate'].value != '' && !isDate(document.forms[0].elements['insuranceExpDate'].value)){
    	document.forms[0].elements['insuranceExpDate'].focus();
		return false;
	}

    if(document.forms[0].elements['deptOfJusticeExpDate'].value != '' && !isDate(document.forms[0].elements['deptOfJusticeExpDate'].value)){
    	document.forms[0].elements['deptOfJusticeExpDate'].focus();
		return false;
	}
    if(document.forms[0].elements['bondExpDate'].value != '' && !isDate(document.forms[0].elements['bondExpDate'].value)){
    	document.forms[0].elements['bondExpDate'].focus();
		return false;
	}

    if(document.forms[0].elements['federalFirearmsLiscExpDate'].value != '' && !isDate(document.forms[0].elements['federalFirearmsLiscExpDate'].value)){
    	document.forms[0].elements['federalFirearmsLiscExpDate'].focus();
		return false;
	}
    
    if(document.forms[0].addressStreetNumber.value != ""){
		if(!isASCII('addressStreetNumber')){
			document.forms[0].addressStreetNumber.value="";
			document.forms[0].addressStreetNumber.focus();
			swal("","Only ASCII characters allowed for Street Number");
			return false;
		}
	}
	if(document.forms[0].addressStreetName.value != ""){
		if(!isASCII('addressStreetName')){
			document.forms[0].addressStreetName.value="";
			document.forms[0].addressStreetName.focus();
			swal("","Only ASCII characters allowed for Street Name");
			return false;
		}
	}
	if(document.forms[0].businessName.value != ""){
		if(!isASCII('businessName')){
			document.forms[0].businessName.value="";
			document.forms[0].businessName.focus();
			swal("","Only ASCII characters allowed for Business Name");
			return false;
		}
	}
	if(document.forms[0].descOfBusiness.value != ""){
		if(!isASCII('descOfBusiness')){
			document.forms[0].descOfBusiness.focus();
			document.forms[0].descOfBusiness.value="";
			swal("","Only ASCII characters allowed for Description Of Business");
			return false;
		}
	}
	if(document.forms[0].squareFootage.value != ""){
		if(!isASCII('squareFootage')){
			document.forms[0].squareFootage.focus();
			document.forms[0].squareFootage.value="";
			swal("","Only ASCII characters allowed for Square Footage");
			return false;
		}
	}
	if(document.forms[0].addressUnitNumber.value != ""){
		if(!isASCII('addressUnitNumber')){
			document.forms[0].addressUnitNumber.focus();
			document.forms[0].addressUnitNumber.value="";
			swal("","Only ASCII characters allowed for Unit Number");
			return false;
		}
	}
	if(document.forms[0].addressZip.value != ""){
		if(!isASCII('addressZip')){
			document.forms[0].addressZip.focus();
			document.forms[0].addressZip.value="";
			swal("","Only ASCII characters allowed for Zip");
			return false;
		}
	}
	if(document.forms[0].addressZip4.value != ""){
		if(!isASCII('addressZip4')){
			document.forms[0].addressZip4.focus();
			document.forms[0].addressZip4.value="";
			swal("","Only ASCII characters allowed for Zip4");
			return false;
		}
	}
	if(document.forms[0].businessPhone.value != ""){
		if(!isASCII('businessPhone')){
			document.forms[0].businessPhone.focus();
			document.forms[0].businessPhone.value="";
			swal("","Only ASCII characters allowed for Phone number");
			return false;
		}
	}
	if(document.forms[0].businessExtension.value != ""){
		if(!isASCII('businessExtension')){
			document.forms[0].businessExtension.focus();
			document.forms[0].businessExtension.value="";
			swal("","Only ASCII characters allowed for Ext");
			return false;
		}
	}
	if(document.forms[0].businessFax.value != ""){
		if(!isASCII('businessFax')){
			document.forms[0].businessFax.focus();
			document.forms[0].businessFax.value="";
			swal("","Only ASCII characters allowed for Business Fax");
			return false;
		}
	}
	if(document.forms[0].quantityNum.value != ""){
		if(!isASCII('quantityNum')){
			document.forms[0].quantityNum.focus();
			document.forms[0].quantityNum.value="";
			swal("","Only ASCII characters allowed for Quantity Number");
			return false;
		}
	}
	if(document.forms[0].corporateName.value != ""){
		if(!isASCII('corporateName')){
			document.forms[0].corporateName.focus();
			document.forms[0].corporateName.value="";
			swal("","Only ASCII characters allowed for Corporate Name");
			return false;
		}
	}
	if(document.forms[0].federalIdNumber.value != ""){
		if(!isASCII('federalIdNumber')){
			document.forms[0].federalIdNumber.focus();
			document.forms[0].federalIdNumber.value="";
			swal("","Only ASCII characters allowed for Federal Id Number");
			return false;
		}
	}
	if(document.forms[0].socialSecurityNumber.value != ""){
		if(!isASCII('socialSecurityNumber')){
			document.forms[0].socialSecurityNumber.focus();
			document.forms[0].socialSecurityNumber.value="";
			swal("","Only ASCII characters allowed for Social Security Number");
			return false;
		}
	}
	if(document.forms[0].startingDate.value != ""){
		if(!isASCII('startingDate')){
			document.forms[0].startingDate.focus();
			document.forms[0].startingDate.value="";
			swal("","Only ASCII characters allowed for Starting Date");
			return false;
		}
	}
	if(document.forms[0].insuranceExpDate.value != ""){
		if(!isASCII('insuranceExpDate')){
			document.forms[0].insuranceExpDate.focus();
			document.forms[0].insuranceExpDate.value="";
			swal("","Only ASCII characters allowed for Insurance Expiration Date");
			return false;
		}
	}
	if(document.forms[0].deptOfJusticeExpDate.value != ""){
		if(!isASCII('deptOfJusticeExpDate')){
			document.forms[0].deptOfJusticeExpDate.focus();
			document.forms[0].deptOfJusticeExpDate.value="";
			swal("","Only ASCII characters allowed for Department Of Justice Expiration Date");
			return false;
		}
	}
	if(document.forms[0].renewalDt.value != ""){
		if(!isASCII('renewalDt')){
			document.forms[0].renewalDt.focus();
			document.forms[0].renewalDt.value="";
			swal("","Only ASCII characters allowed for No Of days for Renewal Date");
			return false;
		}
	}
	if(document.forms[0].bondExpDate.value != ""){
		if(!isASCII('bondExpDate')){
			document.forms[0].bondExpDate.focus();
			document.forms[0].bondExpDate.value="";
			swal("","Only ASCII characters allowed for Bond Expiration Date");
			return false;
		}
	}
	if(document.forms[0].federalFirearmsLiscExpDate.value != ""){
		if(!isASCII('federalFirearmsLiscExpDate')){
			document.forms[0].federalFirearmsLiscExpDate.focus();
			document.forms[0].federalFirearmsLiscExpDate.value="";
			swal("","Only ASCII characters allowed for Federal Firearms License Expiration Date");
			return false;
		}
	}
	
	 if(document.forms[0].renewalDt.value == ""){
		  	//adding the days to current date Getting Current Date
			var today = new Date();
			//Adding Number Of Days To Date
			var addDays   =new Date().setDate(today.getDate());
			//Getting the Date in UTC Format
			addDays = new Date(addDays);
			//Getting the Date in M/D/YY Format
			var now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));

	       //Getting the Current Date in MM/DD/YYYY Format

	        var currentDt =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
							"/" +(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) +
							"/" + addDays.getFullYear().toString();
	         if(currentDt < now){
				now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));
			}else{
			now = "07" + "/" + "01" + "/" + (((addDays.getFullYear() + 1 ).toString()));
			}
			document.forms[0].elements['renewalDt'].value=now;
		 }else{
			//adding the days to current date Getting Current Date
			var today = new Date();
			//Adding Number Of Days To Date
			var addDays   =new Date().setDate( today.getDate() + parseInt(document.forms[0].renewalDt.value));
			//Getting the Date in UTC Format
			addDays = new Date(addDays);
			//Getting the Date in MM/DD/YYYY Format
			var now = (addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+ "/" +
			(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) + "/"
			+ addDays.getFullYear().toString();

			//Assigning The Formated Date To Hidden Date Field
	         
			document.forms[0].elements['renewalDt'].value=now;
	}


	if (document.forms[0].elements['businessPhone'] != "") {
		strValue = validatePhoneNumber(document.forms[0].elements['businessPhone']);
		if(strValue == false){
			return strValue;	
		}
	} 
	if (document.forms[0].elements['businessFax'] != "") {
		strValue = validatePhoneNumber(document.forms[0].elements['businessFax']);
		if(strValue == false){
			return strValue;	
		}
	}
		
	//To verify the address
	var streetNo = document.forms[0].addressStreetNumber.value;
	var streetFraction = document.forms[0].addressStreetFraction.value;
	var streetId = document.forms[0].addressStreetName.value;
	var unit = document.forms[0].addressUnitNumber.value;
	var zip = document.forms[0].addressZip.value;
	if(streetNo != "" && streetId != "" && zip != ""){
		var stringUrl="${pageContext.request.contextPath}/checkAddress?streetNo="+streetNo+"&streetFraction="+streetFraction+"&streetId="+streetId+"&unit="+unit+"&zip="+zip;	
		$.ajax({
	     type: "POST",
	     url: stringUrl,
	     dataType: 'json', 
	     data: {
	    	 streetNo: streetNo
	    	 },
	         success: function(output) {
	             if(output != 0 || output != ""){
	                 document.forms[0].action='${pageContext.request.contextPath}/blAddressPage';
	             	 document.forms[0].submit();
	             	 return true;
	             }else{
	            	 swal("","Address is not valid, please check.");
	            	 return false;
	             }
	        },
	         error : function(data) {
	            swal('Problem while sign in, Please contact your City ');
	        }
	    });
	}else{
	    document.forms[0].action='${pageContext.request.contextPath}/blAddressPage';
		document.forms[0].submit();
	}
}
function cancel(){
	window.open("${pageContext.request.contextPath}/","_self");
}

</script>

</head>
<body class="container-login101"
	style="background-image: url('images/burbankCA.jpg'); align-items: initial;font-size:12px;">
	<div class="p-3 mb-2 bg-gradient-primary text-white"
		style="font-size: 30px; font-weight: 900; align: center; text-align: center; width: 100%; color: white;">
		City of Burbank <br />Community Development Department<br />${applicationType} Application Portal
	</div>
	<div class="container" style="width: 75%;">
		<div id="accordion">
			<html:form name="businessLicenseActivityForm" method="post" autocomplete="off" class="well form-horizontal">
				<font color="red">&nbsp;${activityDetails.displayErrorMsg}</font>
				<div class="card-header" style="width: 100%;">
					<b> ${applicationType} Application</b>
				</div>
				<div class="container" style="width: 100%;">
					<table class="table">
						<tbody>
							<tr>
								<td colspan="1" width="50%">
									<fieldset>
										<div class="form-group form-check">
											<div class="col-md-8 inputGroupContainer">
												<label class="col-md-11 control-label">Is Business
													located in the City of Burbank?</label><input style="width: 8%;"
													type="checkbox" name="businessLocation"
													id="businessLocation" onclick="javascript:checkLocation();"
													checked class="form-check-input col-md-1" />
												<!-- 		                              <label class="col-md-1 control-label" style="text-align:left;">Yes</label> -->
											</div>
										</div>
										<div id="adField1">
											<div class="form-group">
												<label class="col-md-5 control-label">Address<font style="color: red;">*</font></label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-address-book"></i></span>
														 <input id="addressStreetNumber" name="addressStreetNumber" style="width:37%;line-height:14%;height:14%;" maxlength="5" size="2" placeholder="Street Number"class="form-control" required="required" onkeypress="return validateInteger()" type="text">
															<select class="js-example-basic-single form-control col-xs-3" size="1" style="width:35%;line-height:15%;height:15%;" placeholder="Street Fraction" name="addressStreetFraction" id="addressStreetFraction">
															<option value="">Please Select</option>
															<option value="1/4">1/4</option>
															<option value="1/3">1/3</option>
															<option value="1/2">1/2</option>
															<option value="3/4">3/4</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-5 control-label">Street Name<font
													style="color: red;">*</font></label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-address-book"></i></span>
														<select class="js-example-basic-single form-control col-xs-3" style="width:72%;" size="1" name="addressStreetName" id="addressStreetName" required="required">
															<option value="">Please select</option>
															<logic:forEach var="street" items="${streetList}">
																<option value="${street.streetId}">${street.streetName}</option>
															</logic:forEach>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div id="ootField1" style="display: none;">
											<div class="form-group">
												<label class="col-md-5 control-label">Out of town
													Address<font style="color: red;">*</font>
												</label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i
															class="fas fa-address-book"></i></span><input
															placeholder="Out Of Town Street Number"
															class="form-control" type="text" maxlength="5" style="width:23%;" 
															name="outOfTownStreetNumber" id="outOfTownStreetNumber"
															onkeypress="return validateInteger()"
															onblur="return ValidateBlankSpace(this);"> <input
															placeholder="Out Of Town Street Name"
															class="form-control" type="text" maxlength="50"
															name="outOfTownStreetName" id="outOfTownStreetName" style="width:30%;" 
															onblur="return ValidateBlankSpace(this);">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Business Name<font
												style="color: red;">*</font></label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-user"></i></span><input
														placeholder="Business Name" class="form-control"
														type="text" maxlength="100" name="businessName"
														id="businessName"
														onblur="return ValidateBlankSpace(this);"
														required="required">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Description of
												Business</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span><input
														id="descOfBusiness" name="descOfBusiness"
														placeholder="Description of Business" class="form-control"
														maxlength="150" size="20" type="text">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Activity Type<font
												style="color: red;">*</font></label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon" style="max-width: 100%;font-size:12px;"><i
														class="fas fa-list"></i></span>
													<select
														class="js-example-basic-single form-control col-xs-3"
														size="1" name="activityType" id="activityType" style="width:72%;"
														onchange="return CreateXmlHttp(this.value);" required="required">
														<option value="">Please select</option>
														<logic:forEach var="activityType" items="${activityTypes}">
															<option value="${activityType.type}">${activityType.description}</option>
														</logic:forEach>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Starting Date</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group cssearch_date">
													<span class="input-group-addon"><i
														class="fas fa-calendar"></i></span><input
														class="form-control" placeholder="Starting Date"
														type="text" id="startingDate" size="10" maxlength="10"
														name="startingDate"
														style="color: black; font-size: 12px;height:21.5px;width:72%;"
														styleClass="textbox" onkeypress="return event.keyCode!=13" />
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-5 control-label">Ownership Type</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon" ><i class="fas fa-list"></i></span> 
													<select class="js-example-basic-single form-control col-xs-3" size="1" style="width:72%;" name="ownershipType" id="ownershipType">
														<option value="">Please select</option>
														<logic:forEach var="ownershipType" items="${ownershipTypes}">
															<option value="${ownershipType.id}">${ownershipType.description}</option>
														</logic:forEach>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Insurance Expiration Date</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-calendar"></i></span>
													<input id="insuranceExpDate" name="insuranceExpDate" placeholder="Insurance Expiration Date" class="form-control" size="10" type="text" maxlength="10" style="color: black; font-size: 12px;height:21.5px;width:72%;" styleClass="textbox" onkeypress="return event.keyCode!=13" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Department of Justice Expiration Date</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-calendar"></i></span>
													<input id="deptOfJusticeExpDate" name="deptOfJusticeExpDate" placeholder="Department of Justice Expiration Date" class="form-control" size="10" type="text" maxlength="10" style="color: black; font-size: 12px;height:21.5px; width:72%;" styleClass="textbox" onkeypress="return event.keyCode!=13" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Square
												Footage(Area Occupied)</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span><input
														id="squareFootage" name="squareFootage"
														placeholder="Square Footage" class="form-control"
														size="20" type="text" maxlength="6"
														onkeypress="return NumericEntry()">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Type of
												Exemptions</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon" style="max-width: 100%;"><i
														class="fas fa-list"></i></span> <select style="width:72%;"
														class="js-example-basic-single form-control col-xs-3" size="1"
														name="typeOfExemptions" id="typeOfExemptions">
														<option value="">Please select</option>
														<logic:forEach var="exemptionType" items="${exemtList}">
															<option value="${exemptionType.id}">${exemptionType.description}</option>
														</logic:forEach>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group form-check">
											<label class="col-md-5 control-label">Home Occupation</label><input style="width: 8%;" type="checkbox" name="homeOccupation"id="homeOccupation" class="form-check-input col-md-1" />
										</div>										
									</fieldset>
								</td>
								<td colspan="1" width="50%">
									<fieldset>
										<div class="form-group">
											<label class="col-md-5 control-label">E-mail Address<font
												style="color: red;">*</font></label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-envelope"></i></span><input 
														id="emailAddress" name="emailAddress" value="${emailAddr}"
														placeholder="Email Address" class="form-control" size="24"
														type="text" maxlength="60" readonly>
												</div>
											</div>
										</div>
										<div id="adField2">
											<div class="form-group">
												<label class="col-md-5 control-label">Unit, City, State</label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-city"></i></span>
														<input style="width:26%;" type="text" size="5" class="form-control col-xs-2" placeholder="Unit" id="addressUnitNumber" name="addressUnitNumber" maxlength="10">
															<input style="width:26%;" type="text" size="7" class="form-control col-xs-3" id="addressCity" value="Burbank" name="addressCity" readonly="readonly">
															<input style="width:20%;" type="text" size="2" maxlength="2" placeholder="State" class="form-control col-xs-2" id="addressState" value="CA" name="addressState" onkeypress="return NonNumericEntry()" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-5 control-label">Zip<font style="color: red;">*</font>, Zip4 </label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-address-book"></i></span>
														<input style="width:43%;" type="text" size="5" maxlength="5" placeholder="Zip" class="form-control col-xs-3" id="addressZip" name="addressZip" required="required" onkeypress="return validateInteger()"> 
														<input type="text" style="width:29%;" size="5" maxlength="4" class="form-control col-xs-2" id="addressZip4" placeholder="Zip4" name="addressZip4" onkeypress="return validateInteger()">
													</div>
												</div>
											</div>
										</div>
										<div id="ootField2" style="display: none;">
											<div class="form-group">
												<label class="col-md-5 control-label">Unit, City<font style="color: red;">*</font>,State<font style="color: red;">*</font></label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-city"></i></span>
															<input type="text" style="width:26%;" size="5" class="form-control col-xs-2" placeholder="Unit" id="outOfTownUnitNumber" name="outOfTownUnitNumber" maxlength="10"> 
															<input type="text" style="width:26%;" size="7" class="form-control col-xs-3" id="outOfTownCity" name="outOfTownCity" placeholder="City"> 
															<input type="text" style="width:20%;" size="2" maxlength="2" placeholder="State" class="form-control col-xs-2" id="outOfTownState" name="outOfTownState" onkeypress="return NonNumericEntry()">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-5 control-label">Zip<font style="color: red;">*</font>, Zip4
												</label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-address-book"></i></span>
															<input type="text" style="width:43%;" size="5" maxlength="5" placeholder="Zip" class="form-control col-xs-3" id="outOfTownZip" name="outOfTownZip" onkeypress="return validateInteger()">
														<input type="text" style="width:29%;" size="5" maxlength="4" class="form-control col-xs-2" id="outOfTownZip4" placeholder="Zip4" name="outOfTownZip4" onkeypress="return validateInteger()">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Business Phone, Ext</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-phone-alt"></i></span> 
													<input type="text" style="width:43%;" size="14" maxlength="12" class="form-control" name="businessPhone" id="businessPhone" placeholder="Phone" onkeypress="return DisplayPhoneHyphen('businessPhone');">
													<input type="text" style="width:29%;" size="4" maxlength="4" class="form-control" name="businessExtension" id="businessExtension" placeholder="Ext" onkeypress="return validateInteger()">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Business Fax</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-phone-alt"></i></span>
													<input id="businessFax" name="businessFax" placeholder="Business Fax" class="form-control" type="text" onkeypress="return DisplayPhoneHyphen('businessFax');" size="14" maxlength="12" onkeypress="return validateInteger()">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Activity Sub-Type</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon" style="max-width: 100%;"><i class="fas fa-list"></i></span> 
													<select class="selectpicker form-control col-xs-3" size="3" name="activitySubType" id="activitySubType">
														<option value="-1">Not Applicable</option>
														<logic:forEach var="activitySubType"
															items="${activitySubTypes}">
															<option value="${activitySubTypes.id}">${activitySubTypes.description}</option>
														</logic:forEach>
													</select>
												</div>
											</div>
										</div>
										<div id="qty" style="display: none;">
											<div class="form-group">
												<label class="col-md-5 control-label">Quantity<font style="color: red;">*</font></label>
												<div class="col-md-7 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-home"></i></span>
														<input id="quantity" style="width:30%;" name="quantity" class="form-control" type="text" size="5" readonly="readonly"> 
														<input id="quantityNum" style="width:42%;" name="quantityNum" placeholder="Quantity Number" class="form-control" type="text" maxlength="5" onkeypress="return validateInteger()">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">No of Days for Renewal</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span>
														<input
														placeholder="No of Days for Renewal" class="form-control"
														type="text" maxlength="12" name="renewalDt" id="renewalDt" onkeypress="return validateInteger()" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Corporate Name</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span><input
														id="corporateName" name="corporateName"
														placeholder="Corporate Name" maxlength="100"
														class="form-control" type="text"
														onkeypress="return isASCII('corporateName');">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Federal ID
												Number</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span><input
														id="federalIdNumber" name="federalIdNumber"
														placeholder="Federal Id Number" class="form-control"
														type="text" maxlength="10">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Social Security
												Number</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-home"></i></span><input size="14"
														id="socialSecurityNumber" name="socialSecurityNumber"
														placeholder="Social Security Number" class="form-control"
														type="password"
														onkeypress="return DisplaySSNHyphen('socialSecurityNumber');"
														maxlength="10">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Bond Expiration
												Date</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fas fa-calendar"></i></span><input
														type="text" class="form-control dropdown"
														placeholder="Bond Expiration Date" id="bondExpDate"
														size="10" maxlength="10" name="bondExpDate"
														id="bondExpDate"
														style="color: black; font-size: 12px;height:21.5px;width:72%;"
														styleClass="textbox" onkeypress="return event.keyCode!=13" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Federal Firearms License Expiration Date</label>
											<div class="col-md-7 inputGroupContainer">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-calendar"></i></span>
													<input type="text" class="form-control" id="federalFirearmsLiscExpDate" placeholder="FFL Expiration Date"size="10" maxlength="10" name="federalFirearmsLiscExpDate" style="color: black; font-size: 12px;height:21.5px;width:72%;" styleClass="textbox" onkeypress="return event.keyCode!=13" />
												</div>
											</div>
										</div>
										<div class="form-group form-check ">
											<label class="col-md-5 control-label">Decal Code</label> <input
												style="width: 8%;" type="checkbox" name="decalCode"
												id="decalCode"
												class="form-check-input col-md-1 control-label"
												style="padding-top:0px;" />
											<!-- 		                            <label class="col-md-1 control-label" style="text-align:left;">Yes</label> -->
										</div>
									</fieldset>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="text-center">
					<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="width: 140px; height: 40px;" value="Home">&nbsp; 
					<input type="button" class="btn btn-primary" onclick="return next();" style="width: 140px; height: 40px;" value="Next" />
				</div>
			</html:form>
		</div>
	</div>
<script type="text/javascript">
	$("#startingDate").kendoDatePicker({});
	$("#insuranceExpDate").kendoDatePicker({});
	$("#deptOfJusticeExpDate").kendoDatePicker({});
	$("#bondExpDate").kendoDatePicker({});
	$("#federalFirearmsLiscExpDate").kendoDatePicker({});
</script>
</body>
</html>

