<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>City of Burbank New Business Tax</title>
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<script src="js/popper/popper-1.14.3.min.js"></script>  
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<script src="js/sweetalert.min.js"></script> 
<script src="js/sweetalert.js"></script>
<script src="js/actb.js"></script> 
<script src="js/common.js"></script>  
<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/mainReg.js"></script>
<script src="js/formValidation.js"></script> 
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="dateTimePicker/jquery.js"></script> 
<script src="dateTimePicker/jquery.datetimepicker.full.js"></script> 
<script src="js/skel.min.js"></script>
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="css/sweetalert.min.css" type="text/css">
<link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/addBLBT.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap-theme.min.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" type="text/css">
<script language="javascript" type="text/javascript" src="js/kendo.all.min.js"></script>
<link rel="stylesheet" href="css/kendo.default-v2.min.css"/>
<script language="JavaScript" src="js/select2.min.js"></script>
<link rel="stylesheet" href="css/select2.min.css" type="text/css">
<script language="JavaScript" src="js/respond.min.js"></script>
<script src="js/8d29ef114b.js" crossorigin="anonymous"></script>

<style>
.form-horizontal .form-group {
    margin-right: -200px;
    margin-left: -105px;
}

.form-horizontal .control-label
{
padding-top:0px;
}
.input-group .form-control {
    width: 56%;
    font-size: 13px;
    height:25px;
    }
.input-group-addon{
    padding:3px 9px;
    }
.form-group{
margin-bottom:9px;
}
control-label{
    font-size: 11px;
}    
label{
    font-size: 11px;
}    
</style>

<script type="text/javascript">

$(document).ready(function() {
	$('.js-example-basic-single').select2();
});
$(document).on('contextmenu', function () {
 	return false;
});
</script>
<script>
  
  var xmlhttp;

  	function CreateXmlHttp(str){
  	  		var url="${pageContext.request.contextPath}/addBusinessTaxActivity?activityType="+str;

  		xmlhttp=null;
  		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
  			xmlhttp=new XMLHttpRequest();
  		}else if (window.ActiveXObject){// code for IE6, IE5
  			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		if (xmlhttp!=null){
  			xmlhttp.open("POST",url,true);
  			xmlhttp.onreadystatechange=HandleResponse_ActivityType;
  			xmlhttp.send(null);
  		}else{
  		  	swal("Your browser does not support XMLHTTP.");
  		}
  	}

  function HandleResponse_ActivityType(){

	var quantity = document.getElementById("qty");
  	// To make sure receiving response data from server is completed
  	if(xmlhttp.readyState == 4){
  		// To make sure valid response is received from the server, 200 means response received is OK
  		if(xmlhttp.status == 200){
  		    var text=xmlhttp.responseText;
		    var length = text.length;
  			text=text.split(',');
  			if(length <= 50){
	  			if(text[3] != null){
					quantity.style.display="block";	
					document.forms[0].quantity.value = text[3];
				}else{
					quantity.style.display="none";
					document.forms[0].quantity.value = "";
				}
  			}else{
				document.forms[0].activityType.value = "";
				quantity.style.display="none";
				document.forms[0].quantity.value = "";
			}
  		}else{
  			swal("There was a problem retrieving data from the server." );
  		}
  	}
  }

  function loadCheck(){
	var cur = new Date();

  	var ba1 = document.getElementById("baField1");  	
  	var ba2 = document.getElementById("baField2");  	
  	var oot1 = document.getElementById("ootField1");  	
  	var oot2 = document.getElementById("ootField2");  	
  	
  	if(document.forms[0].businessLocation.checked == true){

  		ba1.style.display="block";
  		ba2.style.display="block";
  		oot1.style.display="none";
  		oot2.style.display="none";

  		document.forms[0].outOfTownStreetNumber.value="";
  		document.forms[0].outOfTownStreetName.value="";
  		document.forms[0].businessAddressStreetNumber.focus();
  	}
  }

  function checkLocation()
  {

  	var ba1 = document.getElementById("baField1");
  	var ba2 = document.getElementById("baField2");
  	var oot1 = document.getElementById("ootField1");
  	var oot2 = document.getElementById("ootField2");

    	if(document.forms[0].businessLocation.checked == true){
  	   ba1.style.display="block";
  		ba2.style.display="block";
  		oot1.style.display="none";
  		oot2.style.display="none";

  		document.forms[0].outOfTownStreetNumber.value="";
  		document.forms[0].outOfTownStreetName.value="";
  		document.forms[0].businessAddressStreetNumber.focus();
  	}else{
  	   	 ba1.style.display="none";
  		ba2.style.display="none";
  		oot1.style.display="block";
  		oot2.style.display="block";
  	    document.forms[0].businessAddressStreetNumber.value="";
  		document.forms[0].businessAddressStreetName.value="";
  		document.forms[0].outOfTownStreetNumber.focus();
  	}
  }

  function saveAddress(){

	var strValue=true;

	  if(document.forms[0].businessLocation.checked == false){
	  		if(document.forms[0].outOfTownStreetName.value==""){
	  			document.forms[0].outOfTownStreetName.focus();
	  			swal("","Please enter value for Out Of Town Street Name");
	  			return false;
	  		}else if(document.forms[0].outOfTownCity.value==""){
	  			document.forms[0].outOfTownCity.focus();
	  			swal("","Please enter value for City ");
	  			return false; 
	  		}else if(document.forms[0].outOfTownState.value==""){
	  			document.forms[0].outOfTownState.focus();
	  			swal("","Please enter value for State");
	  			return false;
	  		}else if(document.forms[0].outOfTownZip.value==""){
	  			document.forms[0].outOfTownZip.focus();
	  			swal("","Please enter value for Out Of Town Zip");
	  			return false;
	  		}
	  	}else{
			if(document.forms[0].businessAddressStreetNumber.value == ""){
				document.forms[0].businessAddressStreetNumber.focus();
				swal("","Please enter Address Street Number");
				return false;
			}else if(isNaN(document.forms[0].businessAddressStreetNumber.value)){
				document.forms[0].businessAddressStreetNumber.focus();
				swal("","Please enter numeric value for Address Street Number");
				return false;
			}else if(document.forms[0].businessAddressStreetName.value == ""){
				document.forms[0].businessAddressStreetName.focus();
				swal("","Please select Address Street Name");
				return false;
			}else if(document.forms[0].businessAddressZip.value == ""){
				document.forms[0].businessAddressZip.focus();
				swal("","Please enter Zip Code");
				return false;
			}
	  	}
	  
		if(document.forms[0].businessName.value == ""){
			document.forms[0].businessName.focus();
			swal("","Please enter Business Name");
			return false;
		}else if(document.forms[0].activityType.value == ""){
			document.forms[0].activityType.focus();
			swal("","Please select Activity Type");
			return false;
		}
		else if(document.forms[0].ownershipType.value==""){
  			document.forms[0].ownershipType.focus();
  			swal("","Please select Ownership Type");
  			return false;
  		}

	if($('#qty').css('display') == 'block')
	{
		if(document.forms[0].quantityNum.value==""){
			document.forms[0].quantityNum.focus();
			swal("","Please enter Quantity Number");
			return false;
		}
	}
    if(document.forms[0].elements['startingDate'].value != '' && !isDate(document.forms[0].elements['startingDate'].value)){
    	document.forms[0].elements['startingDate'].focus();
		return false;
	}

  	if(document.forms[0].businessAddressStreetNumber.value != ""){
  	var value = isASCII('businessAddressStreetNumber');
  		if(!isASCII('businessAddressStreetNumber')){
  			document.forms[0].businessAddressStreetNumber.value="";
  			document.forms[0].businessAddressStreetNumber.focus();
  			swal("","Only ASCII characters allowed for Street Number");
  			return false;
  		}
  	}
  	if(document.forms[0].businessAddressStreetName.value != ""){
  		if(!isASCII('businessAddressStreetName')){
  			document.forms[0].businessAddressStreetName.value="";
  			document.forms[0].businessAddressStreetName.focus();
  			swal("","Only ASCII characters allowed for Street Name");
  			return false;
  		}
  	}if(document.forms[0].businessName.value != ""){
  		if(!isASCII('businessName')){
  			document.forms[0].businessName.value="";
  			document.forms[0].businessName.focus();
  			swal("","Only ASCII characters allowed for Business Name");
  			return false;
  		}
  	}
	if(document.forms[0].descOfBusiness.value != ""){
  		if(!isASCII('descOfBusiness')){
  			document.forms[0].descOfBusiness.focus();
  			document.forms[0].descOfBusiness.value="";
  			swal("","Only ASCII characters allowed for Description Of Business");
  			return false;
  		}
  	}
  	if(document.forms[0].squareFootage.value != ""){
  		if(!isASCII('squareFootage')){
  			document.forms[0].squareFootage.focus();
  			document.forms[0].squareFootage.value="";
  			swal("","Only ASCII characters allowed for Square Footage");
  			return false;
  		}
  	}
  	if(document.forms[0].businessAddressUnitNumber.value != ""){
  		if(!isASCII('businessAddressUnitNumber')){
  			document.forms[0].businessAddressUnitNumber.focus();
  			document.forms[0].businessAddressUnitNumber.value="";
  			swal("","Only ASCII characters allowed for Unit Number");
  			return false;
  		}
  	}
  	if(document.forms[0].businessAddressZip.value != ""){
  		if(!isASCII('businessAddressZip')){
  			document.forms[0].businessAddressZip.focus();
  			document.forms[0].businessAddressZip.value="";
  			swal("","Only ASCII characters allowed for Zip");
  			return false;
  		}
  	}
  	if(document.forms[0].businessAddressZip4.value != ""){
  		if(!isASCII('businessAddressZip4')){
  			document.forms[0].businessAddressZip4.focus();
  			document.forms[0].businessAddressZip4.value="";
  			swal("","Only ASCII characters allowed for Zip4");
  			return false;
  		}
  	}
  	if(document.forms[0].businessPhone.value != ""){
  		if(!isASCII('businessPhone')){
  			document.forms[0].businessPhone.focus();
  			document.forms[0].businessPhone.value="";
  			swal("","Only ASCII characters allowed for Phone number");
  			return false;
  		}
  	}
  	if(document.forms[0].businessExtension.value != ""){
  		if(!isASCII('businessExtension')){
  			document.forms[0].businessExtension.focus();
  			document.forms[0].businessExtension.value="";
  			swal("","Only ASCII characters allowed for Ext");
  			return false;
  		}
  	}
  	if(document.forms[0].businessFax.value != ""){
  		if(!isASCII('businessFax')){
  			document.forms[0].businessFax.focus();
  			document.forms[0].businessFax.value="";
  			swal("","Only ASCII characters allowed for Business Fax");
  			return false;
  		}
  	}
  	if(document.forms[0].quantityNum.value != ""){
  		if(!isASCII('quantityNum')){
  			document.forms[0].quantityNum.focus();
  			document.forms[0].quantityNum.value="";
  			swal("","Only ASCII characters allowed for Quantity Number");
  			return false;
  		}
  	}
  	if(document.forms[0].corporateName.value != ""){
  		if(!isASCII('corporateName')){
  			document.forms[0].corporateName.focus();
  			document.forms[0].corporateName.value="";
  			swal("","Only ASCII characters allowed for Corporate Name");
  			return false;
  		}
  	}
  	if(document.forms[0].federalIdNumber.value != ""){
  		if(!isASCII('federalIdNumber')){
  			document.forms[0].federalIdNumber.focus();
  			document.forms[0].federalIdNumber.value="";
  			swal("","Only ASCII characters allowed for Federal Id Number");
  			return false;
  		}
  	}
  	if(document.forms[0].socialSecurityNumber.value != ""){
  		if(!isASCII('socialSecurityNumber')){
  			document.forms[0].socialSecurityNumber.focus();
  			document.forms[0].socialSecurityNumber.value="";
  			swal("","Only ASCII characters allowed for Social Security Number");
  			return false;
  		}
  	}
  	if(document.forms[0].driverLicense.value != ""){
  		if(!isASCII('driverLicense')){
  			document.forms[0].driverLicense.focus();
  			document.forms[0].driverLicense.value="";
  			swal("","Only ASCII characters allowed for Driver's License");
  			return false;
  		}
  	}
	if(document.forms[0].startingDate.value != ""){
		if(!isASCII('startingDate')){
			document.forms[0].startingDate.focus();
			document.forms[0].startingDate.value="";
			swal("","Only ASCII characters allowed for Starting Date");
			return false;
		}
	}
	if (document.forms[0].elements['businessPhone'] != "") {
		strValue = validatePhoneNumber(document.forms[0].elements['businessPhone']);
		if(strValue == false){
			return strValue;	
		}
	}

	if (document.forms[0].elements['businessFax'] != "") {
		strValue = validatePhoneNumber(document.forms[0].elements['businessFax']);
		if(strValue == false){
			return strValue;	
		}
	} 
	//To verify the address
	var streetNo = document.forms[0].businessAddressStreetNumber.value;
	var streetFraction = document.forms[0].businessAddressStreetFraction.value;
	var streetId = document.forms[0].businessAddressStreetName.value;
	var unit = document.forms[0].businessAddressUnitNumber.value;
	var zip = document.forms[0].businessAddressZip.value;
	if(streetNo != "" && streetId != "" && zip != ""){
		var stringUrl="${pageContext.request.contextPath}/checkAddress?streetNo="+streetNo+"&streetFraction="+streetFraction+"&streetId="+streetId+"&unit="+unit+"&zip="+zip;	
		$.ajax({
	     type: "POST",
	     url: stringUrl,
	     dataType: 'json', 
	     data: {
	    	 streetNo: streetNo
	    	 },
	         success: function(output) {
	             if(output != 0 || output != ""){
	                 document.forms[0].action='${pageContext.request.contextPath}/btAddressPage';
	             	 document.forms[0].submit();
	             	 return true;
	             }else{
	            	 swal("","Address is not valid, please check.");
	            	 return false;
	             }
	        },
	         error : function(data) {
	            swal('Problem while sign in, Please contact your City ');
	        }
	    });		
	}else{
	  	document.forms[0].action='${pageContext.request.contextPath}/btAddressPage';
	  	document.forms[0].submit();
	}
  	return true;
  }

  function cancel(){
  	window.open("${pageContext.request.contextPath}/","_self");
  }

</script> 
</head>

<body class="container-login101" style="background-image: url('images/burbankCA.jpg');align-items:initial;font-size:11px;" onload="loadCheck();">
	<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;align:center;text-align:center;width: 100%;color:white;">
		City of Burbank <br/>Community Development Department<br/>${applicationType} Application Portal
	</div>
	
	<div class="container-login100 container" style="max-width:1240px; width:73%;">
	  <div id="accordion">
		  <html:form name="businessTaxActivityForm" method="post" autocomplete="off" class="well form-horizontal">
			<div class="card-header" style="width:100%;"><b> ${applicationType} Application</b></div>
			 <div class="container" style="max-width:1280px; width:100%;">
		       <table class="table">
		          <tbody>
		             <tr>
		                <td colspan="1" width="50%">
							<fieldset>
							<div class="form-group form-check">
								      <label class="col-md-7 control-label">Is Business located in the City of Burbank?</label>
								   <div class="col-md-4 inputGroupContainer">
								   <input type="hidden" name="applicationType" id="applicationType" value="${applicationTypeId}">
								      <input style="width:8%; text-align:right;" type="checkbox" name="businessLocation" id="businessLocation" onclick="javascript:checkLocation();" checked class="form-check-input" />
								   </div>
								</div>
								
								<div id="baField1">
			                    	<div class="form-group">
			                        	<label class="col-md-5 control-label">Address<font style="color:red;">*</font></label>
			                            <div class="col-md-7 inputGroupContainer">
										    <div class="input-group" ><span class="input-group-addon"><i class="fas fa-address-book"></i></span>
										    	<input id="businessAddressStreetNumber" name="businessAddressStreetNumber" maxlength="5" style="width: 29%;line-height:13%;height:13%;" size="2" placeholder="Street Number" class="form-control" required="required"  onkeypress="return validateInteger()" type="text">
											   <select class="js-example-basic-single form-control col-xs-3" size="1" style="width: 27%;line-height:15%;height:15%;" name="businessAddressStreetFraction" id="businessAddressStreetFraction">
													<option value="">Please select</option>
													<option value="1/4">1/4</option>		
													<option value="1/3">1/3</option>
													<option value="1/2">1/2</option>
													<option value="3/4">3/4</option>								        
												</select>
											</div>				
			                            </div>
			                         </div>
			                    	<div class="form-group">
			                            <label class="col-md-5 control-label">Street Name<font style="color:red;">*</font></label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
			                                  <span class="input-group-addon"><i class="fas fa-list"></i></span>
												<select class="js-example-basic-single form-control col-xs-3" size="1" style="width:56%;" name="businessAddressStreetName" id="businessAddressStreetName" required="required">
													 <option value="">Please select</option>
													 <logic:forEach var="street" items="${streetList}">
														<option value="${street.streetId}">${street.streetName}</option>
													 </logic:forEach>				        
												</select>
			                               </div>
			                            </div>
			                         </div>
			                    </div>
				                <div id="ootField1" style="display:none;">
			                         <div class="form-group">
			                            <label class="col-md-5 control-label">Out of town Address<font style="color:red;">*</font></label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i></span><input style="width: 28%;" placeholder="Street Number" class="form-control" type="text" maxlength="5" name="outOfTownStreetNumber" id="outOfTownStreetNumber" onblur="return ValidateBlankSpace(this);" >
			                               <input placeholder="Street Name" class="form-control" type="text" style="width: 56%;" maxlength="50" name="outOfTownStreetName" id="outOfTownStreetName" onblur="return ValidateBlankSpace(this);"></div>
			                            </div>
			                         </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Business Name<font style="color:red;">*</font></label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-user"></i></span><input placeholder="Business Name" class="form-control" type="text" maxlength="100" name="businessName" id="businessName" onblur="return ValidateBlankSpace(this);" required="required"></div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Description of Business</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input id="descOfBusiness" name="descOfBusiness" placeholder="Description of Business" class="form-control" maxlength="150" size="20" type="text"></div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Activity Type<font style="color:red;">*</font></label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group">
		                                  <span class="input-group-addon" style="max-width: 100%;"><i class="fas fa-list"></i></span>
											<select class="js-example-basic-single form-control col-xs-3" size="1" style="width:56%;" name="activityType" id="activityType" onchange="CreateXmlHttp(this.value);" required="required">
												 <option value="">Please select</option>
												 <logic:forEach var="activityType" items="${activityTypes}">
													<option value="${activityType.type}">${activityType.description}</option>
												</logic:forEach>					        
											</select>
		                               </div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Starting Date</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group cssearch_date"><span class="input-group-addon"><i class="fas fa-calendar"></i></span><input class="form-control" placeholder="Starting Date" type="text" id="startingDate" size="10" maxlength="10" name="startingDate" style="color:black;font-size:12px;width:56%;height:21.5px;" styleClass="textbox" onkeypress="return event.keyCode!=13"/></div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Ownership Type<font style="color:red;">*</font></label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group">
		                                  <span class="input-group-addon"><i class="fas fa-list"></i></span>
											<select class="js-example-basic-single form-control col-xs-3" size="1" name="ownershipType" style="width:56%;" id="ownershipType"  required="required">
												 <option value="">Please select</option>
												 <logic:forEach var="ownershipType" items="${ownershipTypes}">
													<option value="${ownershipType.id}">${ownershipType.description}</option>
												</logic:forEach>					        
											</select>
		                               </div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Square Footage(Area Occupied)</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input id="squareFootage" name="squareFootage" placeholder="Square Footage" class="form-control" size="20" type="text" maxlength="6" onkeypress="return NumericEntry();"></div>
		                            </div>
		                         </div>
		                         
					         <div class="form-group">
		                            <label class="col-md-5 control-label">Federal ID Number</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input id="federalIdNumber" name="federalIdNumber" placeholder="Federal Id Number" class="form-control" type="text" maxlength="10" onkeypress="return alphaNumericEntry()" ></div>
									</div>
		                         </div>
							<div class="form-group form-check">
								      <label class="col-md-5 control-label">Home Occupation</label>								      
								   <div class="col-md-5 inputGroupContainer">
								      <input style="width:8%; text-align:right;" type="checkbox" name="homeOccupation" id="homeOccupation" class="form-check-input" /> 
								   </div>
								</div>
								
							</fieldset>
							</td>
							
		                <td colspan="1" width="50%">
							<fieldset>
		                        <div class="form-group form-check">
							      <label class="col-md-7 control-label">Are there any other businesses occupying this premise?</label>
								   <div class="col-md-5 inputGroupContainer">
							    	  <input style="width:8%; text-align:right;" type="checkbox" name="otherBusinessOccupancy" id="otherBusinessOccupancy" class="form-check-input" /> 
							   	   </div>
								</div>
								<div class="form-group">
		                            <label class="col-md-5 control-label">E-mail Address<font style="color:red;">*</font></label>
		                            <div class="col-md-7 inputGroupContainer" >
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-envelope"></i></span><input id="emailAddress" name="emailAddress" value="${emailAddr}" placeholder="Email Address" class="form-control" size="24" type="text" style="padding:0px;" maxlength="60" readonly></div>
		                            </div>
		                         </div>
 								 <div id="baField2">
			                         <div class="form-group">
			                            <label class="col-md-5 control-label">Unit, City, State</label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
			                               		<span class="input-group-addon"><i class="fas fa-city"></i></span><input type="text" size="5" style="width: 17%;" class="form-control col-xs-2" placeholder="Unit" id="businessAddressUnitNumber" name="businessAddressUnitNumber" maxlength="10">
			                               		<input type="text" size="7" style="width: 27%;" class="form-control col-xs-3" id="businessAddressCity" value="Burbank" name="businessAddressCity" readonly="readonly">
				                                <input type="text" size="2" style="width: 12%;" maxlength="2" placeholder="State" class="form-control col-xs-2" id="businessAddressState" value="CA" name="businessAddressState" onkeypress="return NonNumericEntry()" readonly="readonly">
			                               </div>
			                            </div>
			                         </div>
			                         <div class="form-group">
			                            <label class="col-md-5 control-label">Zip<font style="color:red;">*</font>, Zip4</label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
			                               		<span class="input-group-addon"><i class="fas fa-home"></i></span><input type="text" size="5" style="width: 33%;" maxlength="5" placeholder="Zip" class="form-control col-xs-3" id="businessAddressZip" name="businessAddressZip" required="required" onkeypress="return validateInteger()">
			                               		<input type="text" size="5" maxlength="4" style="width: 23%;" class="form-control col-xs-2" id="businessAddressZip4" placeholder="Zip4" name="businessAddressZip4" onkeypress="return validateInteger()">
			                               </div>
			                            </div>
			                         </div>
		                         </div>
	                         	<div id="ootField2" style="display:none;">
		                         	<div class="form-group">
			                            <label class="col-md-5 control-label">Unit, City<font style="color: red;">*</font>, State<font style="color: red;">*</font></label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
			                               		<span class="input-group-addon"><i class="fas fa-home"></i></span><input type="text" size="5" style="width: 17%;" class="form-control col-xs-2" placeholder="Unit" id="outOfTownUnitNumber" name="outOfTownUnitNumber" maxlength="10">
			                               		<input type="text" size="7" style="width: 24%;" class="form-control col-xs-3" id="outOfTownCity" name="outOfTownCity" placeholder="City">
				                                <input type="text" size="2"  maxlength="2" placeholder="State" style="width: 15%;" class="form-control col-xs-2" id="outOfTownState" name="outOfTownState" onkeypress="return NonNumericEntry()">
			                               </div>
			                            </div>
			                         </div>
			                         <div class="form-group">
			                            <label class="col-md-5 control-label">Zip<font style="color:red;">*</font>, Zip4</label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
			                               		<span class="input-group-addon"><i class="fas fa-home"></i></span>
			                               		<input type="text" size="5" maxlength="5" style="width: 33%;" placeholder="Zip" class="form-control col-xs-3" id="outOfTownZip" name="outOfTownZip" onkeypress="return validateInteger()">
			                               		<input type="text" size="5" maxlength="4" style="width: 23%;" class="form-control col-xs-2" id="outOfTownZip4" placeholder="Zip4" name="outOfTownZip4" onkeypress="return validateInteger()">
			                               </div>
			                            </div>
		                            </div>
								</div>									
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Business Phone, Ext</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-phone-alt"></i></span>
			                               <input type="text" size="14" maxlength="12" style="width: 38%;" class="form-control" name="businessPhone" id="businessPhone" placeholder="Phone" onkeypress="return DisplayPhoneHyphen('businessPhone');" >
			                               <input type="text" size="4" maxlength="4" style="width: 18%;" class="form-control" name="businessExtension" id="businessExtension" placeholder="Ext" onkeypress="return validateInteger()">
		                               </div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Business Fax</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-phone-alt"></i></span><input id="businessFax" name="businessFax" placeholder="Business Fax" class="form-control" type="text" onkeypress="return DisplayPhoneHyphen('businessFax');" size="14" maxlength="12" onkeypress="return validateInteger()"></div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Activity Sub-Type</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group">
		                                  <span class="input-group-addon" style="max-width: 100%;"><i class="fas fa-list"></i></span>
											<select class="selectpicker form-control col-xs-3" size="3" name="activitySubType" id="activitySubType">
										        <option value="-1">Not Applicable</option> 
										        <logic:forEach var="activitySubType" items="${activitySubTypes}">
													<option value="${activitySubTypes.id}">${activitySubTypes.description}</option>
												</logic:forEach>						        
											</select>
		                               </div>
		                            </div>
		                         </div>
		                         <div id="qty" style="display:none;">
			                         <div class="form-group">
			                            <label class="col-md-5 control-label">Quantity<font style="color:red;">*</font></label>
			                            <div class="col-md-7 inputGroupContainer">
			                               <div class="input-group">
				                               <input id="quantity" name="quantity" class="form-control" type="text" size="5" readonly="readonly">
				                               <span class="input-group-addon"><i class="fas fa-home"></i></span><input id="quantityNum" name="quantityNum" placeholder="Quantity Number" class="form-control" type="text" maxlength="5" onkeypress="return validateInteger()"></div>
			                            </div>
			                         </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Corporate Name</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input id="corporateName" name="corporateName" placeholder="Corporate Name" maxlength="100" class="form-control" type="text" onkeypress="return isASCII('corporateName');"></div>
		                            </div>
		                         </div>
		                         <div class="form-group">
		                            <label class="col-md-5 control-label">Social Security Number</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input size="14" id="socialSecurityNumber" name="socialSecurityNumber" placeholder="Social Security Number" class="form-control" type="password" onkeypress="return DisplaySSNHyphen('socialSecurityNumber');" maxlength="10" ></div>
									</div>
		                         </div>
		                           <div class="form-group">
		                            <label class="col-md-5 control-label">Driver's License</label>
		                            <div class="col-md-7 inputGroupContainer">
		                               <div class="input-group"><span class="input-group-addon"><i class="fas fa-home"></i></span><input id="driverLicense" name="driverLicense" placeholder="Driver's License" class="form-control" type="text" maxlength="20" onkeypress="return NumericEntry()"></div>
		                            </div>
		                         </div> 
								 
								<div class="form-group form-check">
								      <label class="col-md-5 control-label">Decal Code</label>
								   <div class="col-md-5 inputGroupContainer">
								      <input style="width:8%; text-align:right;" type="checkbox" name="decalCode" id="decalCode" class="form-check-input" /> 
								   </div>
								</div>
							</fieldset>
							</td>
							</tr>
					</tbody>
					</table>
					</div>
				<div class="text-center">
				<input type="button" class="btn btn-primary" name="home" id="home" onclick="return cancel();" style="background-color:#265a88; width:140px;height:40px;" value="Home">&nbsp;
				<input type="button" value="Next"  onclick="return saveAddress();" class="btn btn-primary"  style="background-color:#265a88; width:140px;height:40px;" />
				</div>
	  </html:form>
	  </div>
</div>
</body>
<script type="text/javascript">
	$("#startingDate").kendoDatePicker({});
</script>
</html>