<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import="java.io.BufferedInputStream,java.io.FileInputStream,java.io.FileNotFoundException,java.io.IOException,java.io.File,java.io.InputStream,java.io.OutputStream"%>
<%@ page import="java.util.ResourceBundle"%>


<html>
<body>  

<% String ss=request.getParameter("fileName");

System.out.println("ss... "+ss);
// String fname =(String) session.getServletContext().getAttribute("ATTACHMENT_PATH")+ss;

String fname =(String) request.getParameter("filePath")+"//"+ss;


System.out.println("filePath... "+ request.getParameter("filePath"));
System.out.println("fname... "+fname);
response.setHeader("Content-Disposition", "attachement; filename=\""+ss+"\"");
String extension = "";
int i = ss.lastIndexOf('.');
if (i > 0) {
    extension = ss.substring(i+1);
}

if(extension.equalsIgnoreCase("pdf"))
{
  response.setHeader("Content-Type", "application/pdf");
}

if(extension.equalsIgnoreCase("bmp"))
{
  response.setHeader("Content-Type", "image/bmp");
}

if(extension.equalsIgnoreCase("cgm"))
{
  response.setHeader("Content-Type", "image/cgm");
}

if(extension.equalsIgnoreCase("gif"))
{
  response.setHeader("Content-Type", "image/gif");
}

if(extension.equalsIgnoreCase("ief"))
{
  response.setHeader("Content-Type", "image/ief");
}

if(extension.equalsIgnoreCase("jpeg"))
{
  response.setHeader("Content-Type", "image/jpeg");
}

if(extension.equalsIgnoreCase("tiff"))
{
  response.setHeader("Content-Type", "image/tiff");
}

if(extension.equalsIgnoreCase("png"))
{
  response.setHeader("Content-Type", "image/png");
}

if(extension.equalsIgnoreCase("json"))
{
  response.setHeader("Content-Type", "application/json");
}

if(extension.equalsIgnoreCase("zip"))
{
  response.setHeader("Content-Type", "application/zip");
}

if(extension.equalsIgnoreCase("tgz"))
{
  response.setHeader("Content-Type", "application/tgz");
}

if(extension.equalsIgnoreCase("doc"))
{
  response.setHeader("Content-Type", "application/msword");
}

if(extension.equalsIgnoreCase("docx"))
{
  response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");  
}

if(extension.equalsIgnoreCase("ppt") )
{
  response.setHeader("Content-Type", "application/vnd.ms-powerpointtd>");
}

if(extension.equalsIgnoreCase("pptx") )
{
  response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
}

if(extension.equalsIgnoreCase("txt"))
{
  response.setHeader("Content-Type", "text/plain");
}

if(extension.equalsIgnoreCase("xml"))
{
  response.setHeader("Content-Type", "application/xml");
}

if(extension.equalsIgnoreCase("xls"))
{
  response.setHeader("Content-Type", "application/vnd.ms-excel");
}

if(extension.equalsIgnoreCase("xlsx"))
{
  response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
}

if(extension.equalsIgnoreCase("rtf"))
{
  response.setHeader("Content-Type", "text/rtf");
}

if(extension.equalsIgnoreCase("html"))
{
  response.setHeader("Content-Type", "text/html");
}

if(extension.equalsIgnoreCase("mpeg"))
{
  response.setHeader("Content-Type", "audio/mpeg");
}

if(extension.equalsIgnoreCase("midi"))
{
  response.setHeader("Content-Type", "audio/midi");
}



long fileSize = new File(fname).length();
response.setHeader("Content-Length",fileSize+"");

InputStream inStream=null;
/*Getting output Stream from response*/
OutputStream outStream = response.getOutputStream();
		
		
try
{
inStream = new BufferedInputStream(new FileInputStream(fname));
/*Setting buffer size*/
byte[] buf = new byte[8 * 1024];
int bytesRead = 0;

/*Reading file in buffer stream*/

while ((bytesRead = inStream.read(buf)) != -1) {
outStream.write(buf, 0, buf.length);
}
out.clear(); // where out is a JspWriter
out = pageContext.pushBody();
}
catch (IOException e)
{
//e.printStackTrace();
}

inStream.close();

%> 

</body>
</html>