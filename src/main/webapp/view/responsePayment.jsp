<%@page import="com.elms.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.HashMap,java.util.Map" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>  
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
Activity activity= (Activity)request.getSession().getAttribute("activity");
String renewalCode = "";
String paymentReceiptUrl = "";
String prodFlag = "";

System.out.println("responsePayment.jsp :: Entered");

if(activity!=null){
	renewalCode = activity.getRenewalCode();
	if(renewalCode==null) renewalCode="";
	System.out.println("responsePayment.jsp :: in jsp 17..renewalCode :: "+renewalCode);

}

Map<String, String> lkupSystemDataMap = new HashMap<String,String>();

lkupSystemDataMap = (Map<String,String>) request.getSession().getAttribute("lkupSystemDataMap");
System.out.println("lkupSystemDataMap :"+lkupSystemDataMap.size());

prodFlag = lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG);
System.out.println("prodFlag :: " +prodFlag);

if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
	paymentReceiptUrl = lkupSystemDataMap.get(Constants.BL_PAYMENT_RECEIPT_URL);
}

if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
	if(prodFlag.equalsIgnoreCase("Y")) {
		paymentReceiptUrl = lkupSystemDataMap.get(Constants.BT_PAYMENT_RECEIPT_PROD_URL);
		System.out.println("responsePayment.jsp :: paymentReceiptUrl :: If Condition " +paymentReceiptUrl);
	}else{
		paymentReceiptUrl = lkupSystemDataMap.get(Constants.BT_PAYMENT_RECEIPT_NON_PROD_URL);
		System.out.println("responsePayment.jsp :: paymentReceiptUrl :: else Condition " +paymentReceiptUrl);
	}	
}

System.out.println("blPaymentReceiptUrl before appending act_id : "+paymentReceiptUrl);

paymentReceiptUrl = paymentReceiptUrl.concat(renewalCode);
System.out.println("blPaymentReceiptUrl after appending act_id :"+paymentReceiptUrl);

%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/backHistory";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

</script>
<head>
	<title>City of Burbank Business License / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body onload="enableBusiness()">

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 -->
<script src="js/popper/popper-1.12.9.min.js" ></script>
<!-- <script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script> -->
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<div>
	<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
		<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
				
<form:form name="previewForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" action="${pageContext.request.contextPath}/paymentPage" class="form-horizontal">
				
			<div class="wrap-input100 validate-input">
			<table width="100%">
			  <tbody>
			    <tr>
			    <td width="8%">&nbsp;</td>
			      <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
			      <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
				 <logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if>  Renewal Portal</strong></td>
			      <td width="33%">
			      </td>
			    </tr>
			  </tbody>
			</table>
		  </div>
<%if(activity.getResponseCode().equals("1")){%>				
<table align ="center" width="100%" height="200px">
	<tr >
    	<td >&nbsp;</td>
		<td align="center"></td>
		<td align="center"></td>
    </tr>
	<tr>
		<td align="center"></td><td align="center"></td><td align="center"></td>
		
	</tr>
	<tr>
		<td align ="center" colspan="3"><strong>
 				<logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if>  Renewal Transaction Is Successful</strong></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td ><strong>Payee Name:	${activity.name}</strong></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td ><strong>Transaction ID:	${activity.authCode}</strong></td>
		<td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td ><div class="col-xs-2"><strong>Total Amount:	${activity.totalFee}
		&nbsp;</strong></div></td><td align="center"></td>
	</tr>
	<tr><td width="40%"> &nbsp; </td>
	<td colspan="3"> &nbsp; </td></tr>
	<tr><td colspan="3"> &nbsp; </td></tr>
</table>
				
<div class="container-login100-form-btn m-t-32">
  <button type="button" class="btn btn-primary" onclick="cancel();" >Exit</button>&nbsp;&nbsp;
  <a class="btn btn-primary" href="<%=paymentReceiptUrl%>" target="_blank">Print Receipt</a>
</div>
<%}else{%>
<table align ="center" width="700px" height="200px">
	<tr >
    	<td width="8%">&nbsp;</td>
		<td align="center"></td>
		<td align="center"></td>
    </tr>
	<tr>
		<td align="center"></td><td align="center"></td><td align="center"></td>
	</tr>
	<tr>
		<td align="center"></td>
		<td colspan="2"><strong><font color="Red"> 
		<logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if>  Renewal Transaction Is Declined</font></strong></td>
	</tr>	
	<tr><td width="50%"><br/></td></tr>
	<tr><td colspan="2"> &nbsp; </td></tr>
</table>
<div class="container-login100-form-btn m-t-32">
  <button type="button" class="btn btn-primary" onclick="cancel();">Exit</button>
</div>

<%} %>

</form:form>	
			</div>
		</div>
	</div>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>