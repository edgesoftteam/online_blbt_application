<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<%
String scrnNameForBack = Constants.DOWNLOAD_ATTACHMENT_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
 <script src="vendor/jquery/jquery-1.8.3.js"></script>  
 <script src=https://code.jquery.com/jquery-1.11.3.min.js></script>
<!--  <script src="js/main.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 --><style>
input[type='file'] {
  color: transparent;    / Hides your "No File Selected" /
  direction: rtl;        / Sets the Control to Right-To-Left /
}

/* .tooltip-inner {
    max-width: 350px !important;    
} */

</style>
<script>
$(document).on('contextmenu', function () {
	return false;
});
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<script type="text/javascript">
$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

function AddAttachment(this1){
		var attachmentTypeId=this1.value;
		document.forms[0].action = "${pageContext.request.contextPath}/uploadAttachments?id="+attachmentTypeId;
		document.forms[0].submit();
	}
	
function next(){
	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
    document.forms[0].submit();	
}

function enableAndDisableUpload(typeId,buttonId){
	var file="fileInput("+typeId+")";
	if(document.getElementById(file).value==''){
		var id="attachmentTypeId("+typeId+")";
		document.getElementById(id).disabled = false;
	}	
}   

$( document ).ready(function() {
	//alert("Calling function");
	var comments = $('input[name=comments]').val().replace("[","").replace("]","");
	//alert("comments = "+comments);
	var bmcPopUpWindowUrl = $('input[name=bmcPopUpWindowUrl]').val();
	//alert("bmcPopUpWindowUrl = "+bmcPopUpWindowUrl);
	//var bmcDescription = $('input[name=bmcDescription]').val().replace("{","").replace("}","");		
	var bmcDescription = $('input[name=bmcDescription]').val().replace("{","").replace("}","");
	//alert("bmcDescription = "+bmcDescription);
	var bmclinkDesc = bmcDescription.split(",");
	var cmnts = '';	
	var count = 0;
	for(var i = 0; i < bmclinkDesc.length; i++) {	
		//alert("bmc = "+bmclinkDesc[i]);
		cmnts = cmnts+'&bmcDesc'+i+'='+bmclinkDesc[i].trim().replace("#","!!!");
		//cmnts = cmnts+'&bmcDesc='+bmclinkDesc[i].trim();
		
		count++;
		
	}
	//alert("count ="+count);
	cmnts = cmnts+"&count="+count;
	
	if(comments=='')
	{
		//console.log('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+cmnts+'" >Friendly description</a>');
			$('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+cmnts+'" >Friendly description</a>').fancybox({
			type: 'iframe',
		    width: '60%',
		    height: '60%',	
		    autoScale: false,
		    transitionIn: 'none',
		    transitionOut: 'none',
		    fitToView: true,
		    autoSize: false	
		}).click();
	
	}else{
			 $('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+comments+cmnts+'" >Friendly description</a>').fancybox({
	         'width': '60%',
	         'height': '60%',
	         'autoScale': false,
	         'transitionIn': 'none',
	         'transitionOut': 'none',
	         'fitToView': false,
	         'autoSize' : false,
	         'type' : 'iframe'
	      
	    }).click();

	}
});

</script>

<style type="text/css">
.fancybox-outer {
 background-color: #f2f3f4; /* or whatever */
}

.fancybox-content {
 border-color: #F0F8FF !important; /* or whatever */
}

.fancybox-skin {
 background-color: #333399 !important; /* or whatever */ 
 padding: 5px !important; 
}

</style>

<head>
	<title>City of Burbank Business License / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> 
	<link rel="stylesheet" type="text/css" href="css/util.css"> 
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
    <!-- <script type="text/javascript" src="fancyapps/source/cms.fancybox.js"></script> -->
    
</head>
<body>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 -->
 <script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>
<div>
<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
	<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
				
<form:form name="attachmentDetailsForm" cssClass="login100-form validate-form p-b-3 p-t-5" modelAttribute="activityForm"  enctype="multipart/form-data" method="post" >

	<div class="wrap-input100 validate-input">
	<table width="100%">
	  <tbody>
   		 <tr>
	   		 <td width="8%">&nbsp;</td>
	      	 <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
	      	 <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
	      	  <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if> 
				<logic:if test = "${activityDetails.onlineBLBTApplication == 'Y'}">
     				Application Portal
     				</logic:if>
     				<logic:if test = "${activityDetails.onlineBLBTApplication == 'N'}">
     				Renewal Portal
     				</logic:if>
     				</strong></td>
	       	 <td width="33%">${activityDetails.permitNumber}</td>
    	</tr>
  	  </tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		      <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong> ${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Address : </strong></td>
		      	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo)}">
		      		<td width="30%"><strong>Business Mailing Address : </strong></td>
		      	</logic:if> 
		      	
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Type : </strong>${activityDetails.description}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%">${activityDetails.businessStrName}</td>
		       
		       <logic:choose>
					<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.unit)}">
							<td width="30%">${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}</td>
					</logic:when>
					<logic:otherwise>
						<td width="30%">${activityDetails.mailStrNo}&nbsp;${activityDetails.mailStrName}&nbsp;${activityDetails.mailUnit}</td>
					</logic:otherwise>
				</logic:choose>
		    
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Email : </strong>
			    	
			       <logic:choose>
				        <logic:when test="${not empty activityDetails.tempEmail}">
				       		${activityDetails.tempEmail}
				     	</logic:when>
				     	<logic:otherwise>
				     		${activityDetails.email}
				       </logic:otherwise>
			       </logic:choose>
		      	</td>
		   		<td width="10%">&nbsp;</td>
		      	<td width="30%"> ${activityDetails.businessCityStateZip}</td>
		      	
		      	<logic:choose>
		        <logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">
		       		<td width="30%">${activityDetails.cityStateZip}</td>
		     	</logic:when>
		     	<logic:otherwise>
		     		<td width="30%">${activityDetails.mailCity} ${activityDetails.mailState}&nbsp;${activityDetails.mailZip} </td>
		       </logic:otherwise>
		       </logic:choose>
		       
		    </tr>
 		 </tbody>
	</table>
	</div>	
		
	<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value="${activityDetails.tempId}">
    <input type="hidden" name="actType" value="${activityDetails.actType}">
    <input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
    
    
	<div align="left" style="align:left" class="validate-form p-b-33 p-t-5" width="300px">
	<table style="margin-left:80px;">
	<tr>
		<td>&nbsp; </td>
		
	<%-- <div class="p-3 mb-2 bg-gradient-primary text-dark"  style="font-size:15px;font-weight: 900;" align="left">
		<span style="color:red;">*</span>${attachment.comments}<span style="color:red;">*</span>
	</div> --%>

		<td colspan="2">
             <logic:if test=" ${not empty activityDetails.displayErrorMsg}">
				<div >
				  <div class="form-group">
				   <label for="exampleInputName2">
				 <font color="red"> ${activityDetails.displayErrorMsg}</font>     
				   </label>
					   </div>
						</div>
			</logic:if>
              <logic:if test=" ${not empty activityDetails.displaySuccessMsg}">
				<div >
				   <div class="form-group">
				     <label for="exampleInputName2">
				       <font color="green"> ${activityDetails.displaySuccessMsg}</font>     
				     </label>
			       </div>
				</div>
			</logic:if>
	     </td>
	     <td>&nbsp; </td>
	</tr>
	

	    <%-- <logic:forEach var="attachment" items="${attachmentList}">
	    	<logic:if test="${not empty attachment.documentURL}"> 
				<input type="hidden" name="comments" value="${attachment.comments}">
				<input type="hidden" name="bmcPopUpWindowUrl" value="${attachment.bmcPopUpWindowUrl}">
				<input type="hidden" name="bmcDescription" value="${attachment.bmcDescription}">	
			</logic:if>			
		</logic:forEach>  --%>
		<logic:forEach var="attachment" items="${attachmentList}">	
			<logic:if test="${(attachment.downloadOrUploadType == 'D') or (attachment.downloadOrUploadType == 'B')}">       
		 	<logic:if test="${not empty attachment.documentURL}"> 
		 	
				<tr>					
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;font-weight: bold;" align="left" colspan="3">
					<logic:if test="${attachment.documentURL == 'No documents to download and upload'}"><font color="red"></logic:if>						   
					   ${attachment.attachmentDesc}
					<logic:if test="${attachment.documentURL == 'No documents to download and upload'}"></font></logic:if>	
					</td>
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;text-align:center" colspan="1">
							
						
						
						<a href="${pageContext.request.contextPath}/downloadAttachment?fileName=${attachment.attachmentDesc}&filePath=${attachment.documentURL}">
						<logic:if test="${attachment.documentURL != 'No documents to download and upload'}">				
							<img src="images/download.png" alt="download" width="15%" class="img-responsive"   title="Download ${attachment.attachmentDesc}"/>
						</logic:if>
					</a>&nbsp;Download</td>				
					<%--<logic:if test="${not empty attachment.bmcDescription}"> 
					<td style = "text-indent: 6em;font-family: Arial, sans-serif;" align="left">					   
					   ${attachment.bmcPopUpWindowUrl}
					</td>
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;font-weight: bold;" align="left">						
						<img src="images/info.png" alt="download" height="50" width="50" class="img-responsive" data-toggle="modal" data-target="#myModalBmc${attachment.attachmentTypeId}"/>		
						<img src="images/info.png" alt="BMC Description" height="25" width="25" class="img-responsive" onclick="showFancy('${attachment.bmcDescription}')"/>			
					
					</td>
					</logic:if>	
					
					<logic:if test="${not empty attachment.comments}"> 
					<td style = "text-indent: 6em;font-family: Arial, sans-serif;" align="left">					   
					   Comments
					</td>
					<td style = "text-indent: 1em;font-family: Arial, sans-serif;font-weight: bold;" align="left">						
						 <img src="images/info.png" alt="Comments" height="25" width="25" class="img-responsive" data-toggle="modal" data-target="#myModal${attachment.attachmentTypeId}"/>				
						  
					</td> 				
					</logic:if>	--%>						
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td  colspan="4">						
						<input type="hidden" name="comments" value="${attachment.commentsList}">						
						<input type="hidden" name="bmcPopUpWindowUrl" value="${attachment.bmcPopUpWindowUrl}">
						<input type="hidden" name="bmcDescription" value="${attachment.bmcLinkDescMap}">
					</td>
				</tr>
            </logic:if>
           <%-- <tr> 
           	 <td colspan="4" style = "text-indent: 6em;margin-left:150px;font-family: Arial, sans-serif;font-weight: bold;" align="left"><strong> ${attachment.fileName} </strong></td> 
           </tr> --%>
			<!-- <tr><td  colspan="4">&nbsp;</td></tr> -->
			</logic:if>
		</logic:forEach>
		</table>
	
		<div class="container-login100-form-btn m-t-32">		
		  <button type="button" class="btn btn-primary" onclick="back();">Back</button>&nbsp;
		  <button type="button" class="btn btn-primary" onclick="next();">Next</button>&nbsp;	
		  <button type="button" class="btn btn-primary" onclick="cancel();">Cancel</button>
 		</div>
</div>
				</form:form>
			</div>
		</div>
		
		
		<%-- <logic:forEach var="attachment" items="${attachmentList}"> --%>
			<!-- Modal -->
						  <%-- <div class="modal fade" id="myModal${attachment.attachmentTypeId}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    
						      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header" style="background-color:#2352A1;">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title" style="color:white;font-family: Arial, sans-serif;">Comments</h4>
						        </div>
						        <div class="modal-body" style="font-family: Arial, sans-serif;">
						          <p>${attachment.comments}</p>
						        </div>
						        <div class="modal-footer">
						          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color:#2352A1;color:white">Close</button>
						        </div>
						      </div>
						      
						    </div>
						  </div> --%>
						  
						  <%-- <div class="modal fade" id="myModalBmc${attachment.attachmentTypeId}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    
						      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header" style="background-color:#2352A1;">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title" style="color:white;font-family: Arial, sans-serif;">BMC Description</h4>
						        </div>
						        <div class="modal-body" style="font-family: Arial, sans-serif;">
						          <p>${attachment.bmcDescription}</p>
						        </div>
						        <div class="modal-footer">
						          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color:#2352A1;color:white">Close</button>
						        </div>
						      </div>
						      
						    </div>
						  </div> --%>
		
		<%-- </logic:forEach> --%>	
		
		
	</div>
	
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>