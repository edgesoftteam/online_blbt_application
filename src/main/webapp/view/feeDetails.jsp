<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>   
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils" %>
<%
String scrnNameForBack = Constants.FEE_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script type="text/javascript">

$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){    	 
	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
	document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

function validateForm(){
	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
	document.forms[0].submit();
}

</script>
<head>
	<title>City of Burbank Business License  / Business Tax Renewal</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<style>
.form-control-inline {
    min-width: 0;
    width: 80px;
    height:30px;
    display: inline;
}
</style>
<body>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="vendor/bootstrap/js/logout-timer.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 -->
<script src="js/popper/popper-1.12.9.min.js" ></script>
<!-- <script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script> -->
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script> 
<div>
	<div class="container-login100" style="background-image: url('images/burbankCA.jpg');">
<div>
			<div class="p-3 mb-2 bg-gradient-primary text-white"  style="font-size:30px;font-weight: 900;" align="center">
			City of Burbank <br/>
			Community Development Department - Building and Safety Division
			</div>
<form:form name="feeDetailsForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" >

	<div class="wrap-input100 validate-input">
		<table width="100%">
	  <tbody>
   		 <tr>
	   		 <td width="8%">&nbsp;</td>
	      	 <td width="20%" rowspan="2"><img src="images/burbanklogo.jpg" height="122" width="112" class="img-responsive" alt="Cinque Terre"></td>
	      	 <td width="53%" align="center" ><strong>City of Burbank<br>Community Development Department<br>
	      	 
	      	 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if> 
				<logic:if test = "${activityDetails.onlineBLBTApplication == 'Y'}">
     				Application Portal
     				</logic:if>
     				<logic:if test = "${activityDetails.onlineBLBTApplication == 'N'}">
     				Renewal Portal
     				</logic:if>
     				</strong></td>
	       	 <td width="33%">${activityDetails.permitNumber}</td>
    	</tr>
  	  </tbody>
	</table>
	</div>
	<div class="wrap-input100" >	
	<table width="100%">
  		<tbody >
  			<tr><td>&nbsp;</td></tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Account No : </strong> ${activityDetails.businessAccNo}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%"><strong>Business Address : </strong></td>
		      	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo)}">
		      		<td width="30%"><strong>Business Mailing Address : </strong></td>
		      	</logic:if> 
		      	
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Type : </strong>${activityDetails.description}</td>
		    	<td width="10%">&nbsp;</td>
		      	<td width="30%">${activityDetails.businessStrName}</td>
			      	<logic:choose>
						<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.unit)}">
								<td width="30%">${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}</td>
						</logic:when>
						<logic:otherwise>
							<td width="30%">${activityDetails.mailStrNo}&nbsp;${activityDetails.mailStrName}&nbsp;${activityDetails.mailUnit}</td>
						</logic:otherwise>
					</logic:choose>
		    </tr>
		    <tr>
		    	<td width="8%">&nbsp;</td>
		      	<td width="30%"><strong>Business Email : </strong>
			    	
			       <logic:choose>
				        <logic:when test="${not empty activityDetails.tempEmail}">
				       		${activityDetails.tempEmail}
				     	</logic:when>
				     	<logic:otherwise>
				     		${activityDetails.email}
				       </logic:otherwise>
			       </logic:choose>
		      	</td>
		   		<td width="10%">&nbsp;</td>
		      	<td width="30%"> ${activityDetails.businessCityStateZip}</td>
		      	
		      	<logic:choose>
		        <logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">
		       		<td width="30%">${activityDetails.cityStateZip}</td>
		     	</logic:when>
		     	<logic:otherwise>
		     		<td width="30%">${activityDetails.mailCity} ${activityDetails.mailState}&nbsp;${activityDetails.mailZip} </td>
		       </logic:otherwise>
		       </logic:choose>
		       
		    </tr>
 		 </tbody>
	</table>
	</div>
					
	<div>
		<table class="validate-form p-b-33 p-t-5" width ="90%" style = "margin-right:20px;margin-left:50px;valign:left;">
			
	 	   <tr style = "margin-left:15px;valign:right; background-color:#A9A9A9;">					
				<td width="80%" style ="text-indent: 1em;background-color:#A9A9A9;font-family: Arial, sans-serif;" colspan="2"><strong>Fee Description</strong></td>
				<td align="right" style = "margin-right:50px;font-family: Arial, sans-serif;" colspan="2"><strong>Amount&nbsp;&nbsp; </strong></td>
			</tr>
	        <logic:forEach var="fee" items="${feeList}" >
		     <tr style = "text-indent: 1em;valign:left;">			
					<logic:if test="${not empty fee.feeDesc && fee.feeTotal gt 0.0}">
						<td  width="80%" style="text-indent: 1em;font-family: Arial, sans-serif;" colspan="1">
							${fee.feeDesc}
						</td>
					</logic:if>
					<td align="right" style="text-indent: 45em;"><logic:if test="${not empty fee.feeTotal && fee.feeTotal gt 0.00}">$</logic:if></td>		
					<td  align="right" style = "margin-right:50px;font-family: Arial, sans-serif;">
						<logic:if test="${not fn:contains(fee.feeTotal, '.') && not empty fee.feeTotal && fee.feeTotal gt 0.00}">  
					         ${fee.feeTotalStr}.00
					     </logic:if>
					     <logic:if test="${fn:contains(fee.feeTotal, '.') && fee.feeTotal gt 0.00}">  
					         ${fee.feeTotalStr}
					     </logic:if>
					</td>
			    </tr>
	       </logic:forEach>	       	 	  
	      <tr style = "align:right;">
	      	   	<td style="font-family: Arial, sans-serif;" colspan="3"><hr style="border: 0.5px solid black;" /></td>
	      </tr>
	 	   <tr style = "text-indent: 1em;valign:left;">				
		 	   	<td width="80%" style="font-family: Arial, sans-serif;"><strong>Total</strong></td>
		 	    <logic:choose>
		 	    <logic:when test="${activityDetails.totalFee gt 0.00}">
			 	   	<td align="right">$</td>
			 	   	<td align="right" style="font-family: Arial, sans-serif;">
			 	   		<strong>
			 	   		<logic:choose>
			 	   		<logic:when test="${activityDetails.totalFee gt 0.00} && ${not fn:contains(activityDetails.totalFee,'.')}">
								${activityDetails.totalFee}.00
			 	   		</logic:when>
			 	   		<logic:otherwise>
<%-- 								${StringUtils.roundOffDouble(activityDetails.totalFee)} --%>
								${totalFee}
			 	   		</logic:otherwise>
			 	   		</logic:choose>
				 	   	 
					 	</strong>
			 	   	</td>			 	   	
		 	    </logic:when>
		 	   	<logic:otherwise>
		 	   		<td>$</td>
					<td align="right"><strong> 0.00</strong></td>
				</logic:otherwise>
				</logic:choose>
	 	   </tr>
	 	   <tr style = "text-indent: 1em;valign:left;">
				<td style="color:red;font-weight: bold;font-size:14px;font-family: Arial, sans-serif;" colspan="3"><i>*Credit card processing of 2% will be added upon payment</i></td>	 	   				        
	 	   </tr>
		</table>
	</div>
	
	<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value="${activityDetails.tempId}">
    <input type="hidden" name="actType" value="${activityDetails.actType}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
    
    	<logic:choose>
 	   		<logic:when test="${activityDetails.totalFee > 0.0}}">
					<input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
 	   		</logic:when>
 	   		<logic:otherwise>
					<input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
 	   		</logic:otherwise>
 	   	</logic:choose>
 	   		
	<div class="container-login100-form-btn m-t-32">
		<button type="button" class="btn btn-primary" onclick="back();" >Back</button>&nbsp;
		<button type="submit" class="btn btn-primary" onclick="validateForm();">Next</button>&nbsp;	
		<button type="button" class="btn btn-primary" onclick="cancel();" >Cancel</button>
	</div>
	</form:form>
</div>
</div>
</div>

	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>