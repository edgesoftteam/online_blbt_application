/**
 * Time set for logout message is 20 min
 */

// Logout after counter time reaches 00:00 min

var mywindow;
var timeoutMinutes = 120 // Timeout set for 5 minutes
var lTime = timeoutMinutes * 60;
var timeoutWarning = 110  // Timeout warning set for 5 minutes

function LogoutTimer(parmTime) {
	lTime = lTime - 1;
	var lMins = (lTime - (lTime % 60)) / 60;
	var lSecs = lTime % 60;

	if (parmTime != 0) {
    	lTime = parmTime;
	}
	window.status = "Auto Logout: " + lMins + " Min " + lSecs + " Sec";
	
	 if (lMins == timeoutWarning && lSecs == 00) {
		$('<div></div>').prependTo('[id$=divMaster]').attr('class', 'overlayTimeOut');
		ActivateCountDown("CountDownPanel", (timeoutWarning * 60));
		$('#divpopup').css('display', 'block');
	}
}

// On click of continue button
function restartTimeOut() {
	 $(".overlayTimeOut").remove();
	 $('#divpopup').css('display', 'none');
	  LogoutTimer(timeoutMinutes * 60);
	  clearTimeout(timer);
     _currentSeconds = timeoutMinutes * 60;
	return false;
}

var _countDowncontainer = 0;
var _currentSeconds = 0;

function ActivateCountDown(strContainerID, initialValue) {
	/* _countDowncontainer = document.getElementById(strContainerID);
	if (!_countDowncontainer) {
		alert("count down error: container does not exist: " + strContainerID + "\nmake sure html element with this ID exists");
		return;
	} */
	
	SetCountdownText(initialValue);
	timer = window.setTimeout("CountDownTick()", 1000);
}

function CountDownTick() {
	if (_currentSeconds <= 0) {
	    alert("your time has expired!");
	    cancel();
	}else{
	    SetCountdownText(_currentSeconds - 1);
	    timer = window.setTimeout("CountDownTick()", 1000);
	}
}

function SetCountdownText(seconds) {
	//store:
	_currentSeconds = seconds;
	
	//get minutes:
	var minutes = parseInt(seconds / 60);
	
	//shrink:
	seconds = (seconds % 60);
	
	//get hours:
	var hours = parseInt(minutes / 60);
	
	//shrink:
	minutes = (minutes % 60);
	
	//build text:
	var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);
	
	//apply:
	_countDowncontainer.innerHTML = strText;
}

function AddZero(num) {
	return ((num >= 0) && (num < 10)) ? "0" + num : num + "";
}

function onBodyLoadComplete() {
		try {
	 		window.open('','_self',''); 
			window.close();
		} catch (err) {}
		try {
			
		} catch (err) {
			window.self.close();
		}
 	}

function cancel(){
	document.forms[0].action = "cancelSession";
    document.forms[0].submit();
}
