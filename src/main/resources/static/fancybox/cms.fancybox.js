
	$(document).ready(
		function() {
			//initialize fancybox
			var a_lightbox = $("a.lightbox");
			var a_rel_lightbox = $("a[rel='lightbox']");
			var a_target_lightbox = $("a[target='lightbox']");

			var a_lightbox_iframe = $("a.lightbox-iframe");
			var a_rel_lightbox_iframe = $("a[rel='lightbox-iframe']");
			var a_target_lightbox_iframe_700width = $("a[target='lightbox-iframe-700width']");
			var a_target_lightbox_iframe = $("a[target='lightbox-iframe']");

			var a_lightbox_control = $("a.lightbox-control");

			a_target_lightbox.fancybox();
			a_rel_lightbox.fancybox();
			a_lightbox.fancybox();

			a_rel_lightbox_iframe.fancybox({
				width				: '75%',
				height				: '75%',
				autoScale			: false,
				transitionIn		: 'none',
				transitionOut		: 'none',
				type				: 'iframe'
			});

			a_target_lightbox_iframe.fancybox({
				width				: '75%',
				height				: '75%',
				autoScale			: false,
				transitionIn		: 'none',
				transitionOut		: 'none',
				type				: 'iframe'
			});

			a_lightbox_iframe.fancybox({
				width				: '75%',
				height				: '75%',
				autoScale			: false,
				transitionIn		: 'none',
				transitionOut		: 'none',
				type				: 'iframe'
			});

			a_lightbox_control.fancybox({
				width				: '75%',
				height				: '75%',
				autoScale			: false,
				transitionIn		: 'none',
				transitionOut		: 'none',
				type				: 'iframe',
				afterClose		: function() { window.location.reload( true ); }
			});

			a_target_lightbox_iframe_700width.fancybox({
				width				: 700,
				autoScale			: true,
				transitionIn		: 'none',
				transitionOut		: 'none',
				type				: 'iframe'
			});
		}

	);
