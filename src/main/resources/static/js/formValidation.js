function validateEmail(){
	var email = document.forms[0].elements['username'].value;
	var flag = true;
	var splitted = email.match("^(.+)@(.+)$");    
	if(splitted == null) flag = false;
    if(flag == true && splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) flag = false;
    }
    if(flag == true && splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) flag = false;
      }// if
    }
    if(email != '' && flag == false) {
    	document.forms[0].elements['username'].value = '';
    	alert("Invalid Email Address.");
    }
}
function passwordLength(password){
	var pass = password.value
	if(pass != '' && pass.length < 6) {
		alert("The password must be at least 6 characters.");
	}
}
function showMessage(message, color){
	document.getElementById("profile_status").innerHTML = message;	
	document.getElementById('profile_status').style.color = color;
}
function showactiveMessage(message){
	document.getElementById("profile_status").innerHTML = message;	
}
function showHomeMessage(message){
	alert(message);
}
function hideMessage(){
	document.getElementById('profile_status').style.display = "none";
}

function DisplayPhoneHyphen(str,evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 )){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
		if (document.forms[0].elements[str].value.length > 11 )  return  false;
	}
}
function DisplayExt(str,evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if (document.forms[0].elements[str].value.length > 3 )  return false;
	}
}


function ZipCodeValidation(str,evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if (document.forms[0].elements[str].value.length > 4 )  return false;
	}
}

function numberOnly(str, evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ( charCode<48 || charCode>57 ){
		return false;
	}	
}
function isASCII(str) {	
	var val= document.forms[0].elements[str].value;
    return /^[\x00-\x7F]*$/.test(val);
}

function NumericEntry(){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		return false;
	}
}

function NonNumericEntry(){
	if (!( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}
}

function NumericEntryWithSlash(){
	if (( (event.keyCode<48) || (event.keyCode>57) || (event.keyCode == 47) ) && (event.keyCode != 46)){
		return false;
	}
}

function DisplayPhoneHyphen(str){

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 )){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}

function DisplaySSNHyphen(str){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[str].value.length == 3){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';

 		}
		if ((document.forms[0].elements[str].value.length == 6))  document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		if (document.forms[0].elements[str].value.length > 10 )  event.returnValue = false;
	}
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			swal('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}

function validateEmailId(str){
	if(document.forms[0].emailAddress.value!=""){
			var validemail= validateEmail(document.forms[0].emailAddress.value);
			if(validemail==false){
				swal('Please enter correct E-mail Id');
				document.forms[0].emailAddress.value="";
				document.forms[0].emailAddress.focus();
				return false;
			}
		}
}

function validateInteger()
{
    if (( event.keyCode<48 || event.keyCode>57 ))
    event.returnValue = false;
}
function validatePhoneNumber(phoneNumber){
		if(phoneNumber.value != ""){		
			var reg = /\d{3}-\d{3}-\d{4}/; 				
			var OK = reg.exec(phoneNumber.value); 
			if (!OK)  {
				phoneNumber.focus();
				swal("","Invalid phone number. Phone number format should be like 123-456-7890."); 
				phoneNumber.value = "";
				return false;
			}
		}
		return true;
}


var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
		// February has 29 days in any year evenly divisible by four,
	    // EXCEPT for centurial years which are not also divisible by 400.
	    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31;
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30};
		if (i==2) {this[i] = 29};
   }
   return this;
}
function isDate(dtStr){
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
	}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy");
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month");
		return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day");
		return false;
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid year in YYYY format");
		return false;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date");
		return false;
	}


	today=new Date();
	var christmas= new Date(year,month-1,day); //Month is 0-11 in JavaScript
	var one_day=1000*60*60*24;

	//Calculate difference btw the two dates, and convert to days
	var no_of_days =Math.ceil((christmas.getTime()-today.getTime())/(one_day));
return true;
}

